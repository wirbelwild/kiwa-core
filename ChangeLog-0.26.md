# Changes in Kiwa Core v0.26.

## 0.26.3 2022-03-23

### Changed

-   The error reporting takes care of the `@` operator now. 

## 0.26.2 2022-03-23

### Fixed

-   Fixed typo that maleformed the content type.

## 0.26.1 2022-03-22

### Fixed

-   Fixed bug with `ob_gzhandler`.

## 0.26.0 2022-03-21

## Changed

-   When loading PHP files the content type will be `text/html` per default.
-   The error handling has been improved.

## Added

-   Added security headers. They can be configured using the `Kiwa\Config\Generator\SecurityHeadersGenerator` class.