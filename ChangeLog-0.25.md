# Changes in Kiwa Core v0.25.

## 0.25.1 2022-02-28

### Fixed

-   Fixed missing value in `Kiwa\Config\Page`.

## 0.25.0 2022-02-28

## Fixed 

-   Fixed that URLs containing dots caused errors.

## Changed

-   Alternate links will be sorted alphabetically now.
-   Added `Monolog\Handler\FingersCrossedHandler` to decrease the number of log files.
-   The method `Language\CurrentLanguageCode::getLanguageCode` has been deprecated, use `Language\CurrentLanguageCode::getCurrentLanguageCode` instead. Pay attention: The new method is not static and returns a `Kiwa\Language\LanguageCode` object or `null`.
-   When receiving a POST request, no cache file will be used even it exists.
-   Changed how long responses should be cached from the browser.
-   PHP >=7.4 is required.

## Added

-   Added possibility to get a specific translation from `Language\Text` by adding the language code as last parameter. 
-   Added colors to the CLI output.
-   Added `pageUID` to the javascript variables, so it can be accessed there too.
-   Added possibility to define the cache lifetime.