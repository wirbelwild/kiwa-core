# Changes in Kiwa Core v0.28

## 0.28.2 2024-02-07

### Fixed

-   Fixed wrong url creation with parameter `index`.

## 0.28.1 2024-01-22

### Fixed

-   Fixed bug with subpage routing.

## 0.28.0 2024-01-18

### Changed

-   The routing has been refactored. When running in production mode (`APP_ENV=prod`), a cache file containing all routes will be generated and stored under `/var/cache/routes.php`.