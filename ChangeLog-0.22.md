# Changes in Kiwa Core 0.22.

## 0.22.15 2021-01-23

### Fixed

-   Updated `places2be/locales` and its usage to prevent notice.

## 0.22.14 2020-12-07

### Fixed 

-   Reverted commits because of wrong tagging.

## 0.22.13 2020-12-02

### Fixed

-   Added missing method `setInstance` to class `DI`.

## 0.22.12 2020-11-10

### Fixed

-   Fixed handling of related pages that could go wrong with dynamic subpages.

## 0.22.11 2020-11-09

### Fixed 

-   Fixed a bug that caused AJAX requests for PHP files to not load when they have GET parameters.

## 0.22.10 2020-11-03

### Fixed

-   Fixed a bug that caused wrong URLs when redirecting with URL parameters.

## 0.22.9 2020-10-12

### Fixed 

-   Fixed a bug that appeared when translating text.

## 0.22.8 2020-10-12

### Fixed

-   Fixed bug in `IsChildOf` iterator.

## 0.22.7 2020-10-12

### Added 

-   Added missing support information.

## 0.22.6 2020-10-12

### Fixed 

-   Fixed a bug that caused the page title to be empty when the index wasn't defined. A solution to take the og:title per default had been wrong. 

## 0.22.5 2020-10-02

### Fixed 

-   Fixed a bug that could result in strange URLs at the index page.

## 0.22.4 2020-10-02

### Fixed 

-   Updated `bitandblack/composer-helper` to improve support for Windows environments.

## 0.22.3 2020-09-07

### Fixed

-   In some cases, uppercase letters in file names causes Kiwa to not find and handle the file correctly. This has been fixed. 
-   When a website required its URLs to have a language code, a wrong language code didn't end in a 404 response. This has been fixed.

## 0.22.2 2020-08-28 

### Fixed 

-   Caching dynamic subpages went wrong. This has been fixed now.
-   Alternate links had been wrong on dynamic subpages. This has been fixed.

## 0.22.1 2020-08-04 

### Fixed 

-   The `BaseConfigLoader` loads all config values only once now. This prevents overriding values.

## 0.22.0 2020-08-04 

### Fixed 

-   Pages may be cached correctly now. Caching is only active in production mode and when a page allows it in its config.

### Changed

-   All constants holding paths to folders have been deprecated now. They can be replaced with methods in the `Kiwa\Path` class. For example `KIWA_BUILD` is now `Path::getBuildFolder()`.
-   `.env` files will be loaded to the config. The `.env.local` has a higher priority and will overload.
-   There is no longer a wrapping `base.phtml` file needed. All files can stand by their own and may include whatever they want.
-   The new class `Kiwa\DI` is now responsible for handling dependency injections.

### Added 

-   `Kiwa\Log::debug` calls the new method `printInCLI` which allows to have messages in the `CLI` when Kiwa runs with the PHP builtin server.
-   A lot of methods have moved to their own class to make them easier to access.
-   A lot of methods have moved to their own class to make them easier to access.
