# Changes in Kiwa Core 0.23.

## 0.23.5 2021-02-22

### Fixed

-   Fixed bug with meta tags.

## 0.23.4 2021-02-22

### Fixed

-   Fixed that meta tags didn't override default tags.

## 0.23.3 2021-02-08

### Changed

-   Allowed setting multiple `og:image` tags.

## 0.23.2 2021-01-11

### Fixed

-   Fixed deprecated parameter in `places2be/locales`

## 0.23.1 2020-12-22

### Fixed

-   Fixed how response is set up, so it's possible to change it from a page.

## 0.23.0 2020-12-09

### Added 

-   Kiwa now handles the cache control.
-   Added class `CacheControlGenerator` to handle the cache control in the config.
-   The dependency injection class `DI` has the method `setInstance` to add objects to it.
-   Added support for PHP 8.

## Changed 

-   The path where `robots.txt` and `sitemap.xml` can be stored has changed. `getCrawlerFolder` is now equal to `getBuildFolder`, so they are located in the public root folder.