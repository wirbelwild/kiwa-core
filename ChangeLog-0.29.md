# Changes in Kiwa Core v0.29

## 0.29.1 2024-07-12

### Changed

-   Improve handling of links in the [LanguageChangeLinks](./src/Language/LanguageChangeLinks.php) class.

## 0.29.0 2024-05-25

### Changed

-   Further improvements in routing.