# Changes in Kiwa Core v0.27

## 0.27.12 2024-01-18

### Changed

-   Updated dependencies.

## 0.27.11 2023-11-28

### Fixed

-   Fixed handling of requests with wrong url parts.

## 0.27.10 2023-11-27

### Fixed

-   Fixed handling of static properties.

## 0.27.9 2023-09-29

### Fixed

-   Fix env var handling.

## 0.27.8 2023-09-29

### Fixed

-   Start session only when headers aren't sent yet and hide the HTML error output from the CLI.

## 0.27.7 2023-09-29

### Fixed

-   Convert ampersand in page title.

## 0.27.6 2023-05-26

### Fixed

-   Start session only if not already started.

## 0.27.5 2023-05-25

### Fixed

-   Add missing session start (another one).

## 0.27.4 2023-05-24

### Fixed

-   Add missing session start.

## 0.27.3 2023-05-23

### Fixed

-   Fixed missing Session object in Request object.

## 0.27.2 2022-10-07

### Fixed

-   Fixed wrong tag.

## 0.27.1 2022-10-07

### Changed

-   Updated `psr/log` and changed return types in deprecated classes `File` and `Language`.

## 0.27.0 2022-05-12

### Changed

-   Updated dependencies.