# Changes in Kiwa Core v1.1

## 1.1.0 2024-12-23

### Changed

-   `HtaccessFile::write()` has been deprecated. Instead, `HtaccessFile::add()` should be used. This new method uses a namespace for each setting to update it later without touching the rest of a `.htaccess` file.
