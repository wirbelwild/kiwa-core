# Changes in Kiwa Core v0.24.

## 0.24.10 2022-03-28

### Fixed

-   Fixed bug with `ob_gzhandler`.

## 0.24.9 2022-01-10

## Fixed

- Added compatibility to PHP 8.

## 0.24.8 2021-11-05

## Fixed 

- Fixed missing language code from default language when the request contained an invalid code.

## Changed 

- It is now required to use existing language codes.

## 0.24.7 2021-09-24

## Fixed

- Fixed wrong loading of test env files.

## 0.24.6 2021-09-23

### Fixed

- Fixed missing loading of `.env.test` and `.env.test.local` files.

## 0.24.5 2021-09-22

### Changed 

- `psr/log` has been added to `composer.json` to prevent installing `v3` which is not compatible at the moment.

### Fixed 

-   The `ErrorWriter` handles a possibly broken `error.php` without showing errors now. 

## 0.24.4 2021-06-08

### Fixed

-   Fixed missing dependency.

## 0.24.3 2021-06-08

### Fixed

-   Fixed that urls with dots may cause errors.
-   Fixed handling of dynamic children that could end in an error.

## 0.24.2 2021-06-07

### Fixed

-   Fixed that multiple instances of the `URLFromFile` class affected each other.

## 0.24.1 2021-06-01

### Fixed

-   Fixed a wrong .htaccess rewrite rule.

## 0.24.0 2021-03-23

### Added 

-   Added class `Config\Page`. 

## Changed 

-   The class `Page\PageList` runs with `Config\Page`s now. That allows it to handle pages as simple objects. 
-   Removed `roave/better-reflection` as it was not ready for PHP 8.