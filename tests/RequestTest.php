<?php

/**
 * Kiwa. A feather-light web framework for professional static websites.
 *
 * @author Tobias Köngeter
 * @copyright Copyright © Bit&Black
 * @link https://www.bitandblack.com
 * @link https://www.kiwa.io
 * @license MIT
 */

namespace Kiwa\Tests;

use Exception;
use Generator;
use Kiwa\Config\Generator\ConfigGenerator;
use Kiwa\Config\Generator\PageGenerator;
use Kiwa\DI;
use Kiwa\Exception\Config\MisleadingConfigException;
use Kiwa\Exception\Config\MissingValueException;
use Kiwa\Frontend\Controller;
use Kiwa\Path;
use PHPUnit\Framework\Attributes\DataProvider;
use PHPUnit\Framework\Attributes\PreserveGlobalState;
use PHPUnit\Framework\Attributes\RunInSeparateProcess;
use PHPUnit\Framework\TestCase;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class RequestTest
 *
 * @package Kiwa\Tests
 */
class RequestTest extends TestCase
{
    /**
     * Creates needed template folders.
     */
    public static function setUpBeforeClass(): void
    {
        Helper::setUpBefore();
    }

    /**
     * Removes template folders.
     */
    public static function tearDownAfterClass(): void
    {
        Helper::tearDownAfter();
    }

    #[PreserveGlobalState(false)]
    #[RunInSeparateProcess]
    public function testCanHandleByteRangeRequest(): void
    {
        $fileName = 'video.mp4';
            
        $tempAssetFile = Path::getBuildFolder() . DIRECTORY_SEPARATOR . $fileName;
        $fileContentOriginal = '0123456789';

        $file = fopen($tempAssetFile, 'wb+');

        if (false === $file) {
            self::fail();
        }

        fwrite($file, $fileContentOriginal);
        fclose($file);
        
        $_SERVER['HTTP_RANGE'] = 'bytes=0-1';
        $_SERVER['REQUEST_URI'] = '/build/' . $fileName;
        
        $controller = new Controller();

        ob_start();
        DI::getResponse()->sendContent();
        $fileContentPartial = ob_get_clean();
        
        self::assertSame(
            '01',
            $fileContentPartial
        );
        
        self::assertSame(
            206,
            DI::getResponse()->getStatusCode()
        );
        
        unset($controller);
    }

    /**
     * @throws MissingValueException
     */
    #[PreserveGlobalState(false)]
    #[RunInSeparateProcess]
    public function testReturns404OnWrongLanguageCode(): void
    {
        $_SERVER['REQUEST_URI'] = '/missing';

        $configFile = Path::getConfigFolder() . DIRECTORY_SEPARATOR . 'config.php';

        $config = new ConfigGenerator();
        $config
            ->setURLStructure('languageCode', 'name', 'subname')
            ->addPage(
                PageGenerator::create()
                ->setLanguageCode('de')
                ->setName('index')
                ->setFile('index')
            )
            ->setPageSuffix(false)
        ;

        file_put_contents(
            $configFile,
            '<?php return ' . var_export($config->getConfig(), true) . ';'
        );

        $htmlFile = Path::getHTMLFolder() . DIRECTORY_SEPARATOR . 'index.phtml';
        $htmlValue = 'Hello World!';
        
        file_put_contents(
            $htmlFile,
            $htmlValue
        );
        
        $controller = new Controller();
        $content = DI::getResponse()->getContent();
        
        self::assertNotSame(
            $htmlValue,
            $content
        );
        
        self::assertStringContainsString(
            '404',
            (string) $content
        );
        
        unset($controller);

        $_SERVER['REQUEST_URI'] = '/de';
        
        $controller = new Controller();
        $content = DI::getResponse()->getContent();

        self::assertSame(
            $htmlValue,
            (string) $content
        );

        unset($controller);
    }

    /**
     * @param string $url
     * @param string|false $suffix
     * @throws MissingValueException
     * @throws MisleadingConfigException
     */
    #[DataProvider('getURLsWithDot')]
    #[PreserveGlobalState(false)]
    #[RunInSeparateProcess]
    public function testCanHandleURLsWithADot(string $url, $suffix): void
    {
        $_SERVER['REQUEST_URI'] = $url;

        $configFile = Path::getConfigFolder() . DIRECTORY_SEPARATOR . 'config.php';

        $config = new ConfigGenerator();
        $config
            ->setURLStructure('name', 'subname')
            ->addPage(
                PageGenerator::create()
                ->setLanguageCode('de')
                ->setName('name')
                ->setFile('name')
                ->setDynamicChild('dynamic-child')
            )
            ->addPage(
                PageGenerator::create()
                ->setLanguageCode('de')
                ->setName('dynamic-child')
                ->setFile('dynamic-child')
                ->setChildOf('name')
            )
            ->setPageSuffix($suffix)
        ;

        file_put_contents(
            $configFile,
            '<?php return ' . var_export($config->getConfig(), true) . ';'
        );

        $htmlFile = Path::getHTMLFolder() . DIRECTORY_SEPARATOR . 'dynamic-child.phtml';
        $htmlValue = 'Hello World!';

        file_put_contents(
            $htmlFile,
            $htmlValue
        );

        $controller = new Controller();
        $content = DI::getResponse()->getContent();
        
        self::assertSame(
            $htmlValue,
            $content
        );
        
        self::assertSame(
            200,
            DI::getResponse()->getStatusCode()
        );
        
        unset($controller);
    }

    /**
     * @return Generator<array<(string | false)>>
     * @throws Exception
     */
    public static function getURLsWithDot(): Generator
    {
        yield [
            '/name/dynamic.+name+' . Helper::getRandomString(),
            false,
        ];
        
        yield [
            '/name/dynamic.+name+' . Helper::getRandomString() . '.html',
            'html',
        ];
    }

    /**
     * @throws MissingValueException
     * @throws MisleadingConfigException
     * @throws Exception
     */
    #[PreserveGlobalState(false)]
    #[RunInSeparateProcess]
    public function testCanHandlePagesWithIdenticalNames(): void
    {
        $configFile = Path::getConfigFolder() . DIRECTORY_SEPARATOR . 'config.php';

        $config = new ConfigGenerator();
        $config
            ->setURLStructure('languageCode', 'name', 'subname')
            ->addPage(
                PageGenerator::create()
                ->setLanguageCode('de')
                ->setName('same-name')
                ->setFile('file1')
            )
            ->addPage(
                PageGenerator::create()
                ->setLanguageCode('de')
                ->setName('blog')
                ->setFile('file2')
                ->setDynamicChild('dynamic-child')
            )
            ->addPage(
                PageGenerator::create()
                ->setLanguageCode('de')
                ->setName('dynamic-child')
                ->setFile('file3')
                ->setChildOf('blog')
            )
            ->setPageSuffix('html')
        ;

        file_put_contents(
            $configFile,
            '<?php return ' . var_export($config->getConfig(), true) . ';'
        );

        $htmlFile1 = Path::getHTMLFolder() . DIRECTORY_SEPARATOR . 'file1.phtml';
        $htmlValue1 = Helper::getRandomString();

        file_put_contents(
            $htmlFile1,
            $htmlValue1
        );

        $htmlFile2 = Path::getHTMLFolder() . DIRECTORY_SEPARATOR . 'file2.phtml';
        $htmlValue2 = Helper::getRandomString();

        file_put_contents(
            $htmlFile2,
            $htmlValue2
        );

        $htmlFile3 = Path::getHTMLFolder() . DIRECTORY_SEPARATOR . 'file3.phtml';
        $htmlValue3 = Helper::getRandomString();

        file_put_contents(
            $htmlFile3,
            $htmlValue3
        );

        $_SERVER['REQUEST_URI'] = '/de/same-name.html';

        $controller = new Controller();
        $content = DI::getResponse()->getContent();

        self::assertSame(
            $htmlValue1,
            $content
        );

        self::assertSame(
            200,
            DI::getResponse()->getStatusCode()
        );

        unset($controller);

        $_SERVER['REQUEST_URI'] = '/de/blog/same-name.html';

        $controller = new Controller();
        $content = DI::getResponse()->getContent();

        self::assertSame(
            $htmlValue3,
            $content
        );

        self::assertSame(
            200,
            DI::getResponse()->getStatusCode()
        );

        unset($controller);
    }

    /**
     * @param string $url
     * @param int $statusCodeExpected
     * @return void
     * @throws MisleadingConfigException
     * @throws MissingValueException
     */
    #[DataProvider('getReturns404WhenUrlPartIsWrongData')]
    #[PreserveGlobalState(false)]
    #[RunInSeparateProcess]
    public function testReturns404WhenUrlPartIsWrong(string $url, int $statusCodeExpected): void
    {
        $_SERVER['REQUEST_URI'] = $url;

        $configFile = Path::getConfigFolder() . DIRECTORY_SEPARATOR . 'config.php';

        $config = new ConfigGenerator();
        $config
            ->setURLStructure('name', 'subname')
            ->addPage(
                PageGenerator::create()
                    ->setLanguageCode('de')
                    ->setName('nameright')
                    ->setFile('nameright-file')
            )
            ->addPage(
                PageGenerator::create()
                    ->setLanguageCode('de')
                    ->setName('subnameright')
                    ->setFile('subnameright-file')
                    ->setChildOf('nameright')
            )
        ;

        file_put_contents(
            $configFile,
            '<?php return ' . var_export($config->getConfig(), true) . ';'
        );

        $htmlFile = Path::getHTMLFolder() . DIRECTORY_SEPARATOR . 'subnameright-file.phtml';
        $htmlValue = 'Hello World!';

        file_put_contents(
            $htmlFile,
            $htmlValue
        );

        $controller = new Controller();

        self::assertSame(
            $statusCodeExpected,
            DI::getResponse()->getStatusCode()
        );

        unset($controller);
    }

    public static function getReturns404WhenUrlPartIsWrongData(): Generator
    {
        yield [
            '/nameright/subnameright.html',
            Response::HTTP_OK,
        ];

        yield [
            '/namewrong/subnameright.html',
            Response::HTTP_NOT_FOUND,
        ];
    }

    public static function getRefusesToLoadIndexPhpFileData(): Generator
    {
        yield ['/index'];
        yield ['/index.php'];
        yield ['/index.htm'];
        yield ['/index.html'];
        yield ['/index.php?trick'];
    }

    /**
     * @throws MissingValueException
     */
    #[DataProvider('getRefusesToLoadIndexPhpFileData')]
    #[PreserveGlobalState(false)]
    #[RunInSeparateProcess]
    public function testRefusesToLoadIndexPhpFile(string $requestUri): void
    {
        $_SERVER['REQUEST_URI'] = $requestUri;

        $configFile = Path::getConfigFolder() . DIRECTORY_SEPARATOR . 'config.php';

        $config = new ConfigGenerator();
        $config
            ->setURLStructure('languageCode', 'name', 'subname')
            ->addPage(
                PageGenerator::create()
                    ->setLanguageCode('de')
                    ->setName('index')
                    ->setFile('index')
            )
        ;

        file_put_contents(
            $configFile,
            '<?php return ' . var_export($config->getConfig(), true) . ';'
        );

        $htmlFile = Path::getHTMLFolder() . DIRECTORY_SEPARATOR . 'index.phtml';
        $htmlValue = 'Hello World!';

        file_put_contents(
            $htmlFile,
            $htmlValue
        );

        $controller = new Controller();
        $content = DI::getResponse()->getContent();

        self::assertNotSame(
            $htmlValue,
            $content
        );

        self::assertStringContainsString(
            'Redirecting to /',
            (string) $content
        );

        self::assertSame(
            Response::HTTP_MOVED_PERMANENTLY,
            DI::getResponse()->getStatusCode()
        );

        unset($controller);
    }
}
