<?php

/**
 * Kiwa. A feather-light web framework for professional static websites.
 *
 * @author Tobias Köngeter
 * @copyright Copyright © Bit&Black
 * @link https://www.bitandblack.com
 * @link https://www.kiwa.io
 * @license MIT
 */

namespace Kiwa\Tests;

use Exception;
use Kiwa\Config\Generator\ConfigGenerator;
use Kiwa\Config\Generator\PageGenerator;
use Kiwa\DI;
use Kiwa\Exception\Config\MisleadingConfigException;
use Kiwa\Exception\Config\MissingValueException;
use Kiwa\Frontend\Controller;
use Kiwa\Page\PageTitle;
use Kiwa\Path;
use PHPUnit\Framework\Attributes\PreserveGlobalState;
use PHPUnit\Framework\Attributes\RunInSeparateProcess;
use PHPUnit\Framework\TestCase;

/**
 * Class PageTest
 *
 * @package Kiwa\Tests
 */
class PageTest extends TestCase
{
    /**
     * Creates needed template folders.
     */
    public static function setUpBeforeClass(): void
    {
        Helper::setUpBefore();
    }

    /**
     * Removes template folders.
     */
    public static function tearDownAfterClass(): void
    {
        Helper::tearDownAfter();
    }

    /**
     * Tests if a subpage can be loaded correctly.
     *
     * @throws MissingValueException
     * @throws MisleadingConfigException
     * @see \Kiwa\Tests\PageTest::testReturns404WhenURLIsDifferent() The opposite test.
     */
    #[PreserveGlobalState(false)]
    #[RunInSeparateProcess]
    public function testTakesChild(): void
    {
        $fileName = 'second';
        $attrFirst = '/first/second.html';
        $_SERVER['REQUEST_URI'] = $attrFirst;

        $configFile = Path::getConfigFolder() . DIRECTORY_SEPARATOR . 'config.php';

        $config = new ConfigGenerator();
        $config
            ->setURLStructure('name', 'subname')
            ->addPage(
                PageGenerator::create()
                ->setLanguageCode('de')
                ->setName('second')
                ->setFile($fileName)
                ->setChildOf('first')
                ->setPageTitle('Welcome to Kiwa')
            )
        ;

        file_put_contents(
            $configFile,
            '<?php return ' . var_export($config->getConfig(), true) . ';'
        );

        $htmlFile = Path::getHTMLFolder() . DIRECTORY_SEPARATOR . $fileName . '.phtml';
        $htmlValue = 'Hello World!';

        $file = fopen($htmlFile, 'wb+');

        if (false === $file) {
            self::fail();
        }

        fwrite($file, $htmlValue);
        fclose($file);

        $controller = new Controller();
        
        self::assertSame(
            'Welcome to Kiwa',
            (string) new PageTitle()
        );

        self::assertSame(
            $htmlValue,
            DI::getResponse()->getContent()
        );
        
        unset($controller);
    }

    /**
     * Tests if a subpage is not loaded when the request is for a page in hierarchy 1.
     *
     * @throws MissingValueException
     * @throws MisleadingConfigException
     * @see \Kiwa\Tests\PageTest::testTakesChild() The opposite test.
     */
    #[PreserveGlobalState(false)]
    #[RunInSeparateProcess]
    public function testReturns404WhenURLIsDifferent(): void
    {
        $fileName = 'second';
        $attrFirst = '/second.html';
        $_SERVER['REQUEST_URI'] = $attrFirst;

        $configFile = Path::getConfigFolder() . DIRECTORY_SEPARATOR . 'config.php';

        $config = new ConfigGenerator();
        $config
            ->setURLStructure('name', 'subname')
            ->addPage(
                PageGenerator::create()
                ->setLanguageCode('de')
                ->setName('second')
                ->setFile($fileName)
                ->setChildOf('first')
                ->setPageTitle('Welcome to Kiwa')
            )
        ;

        file_put_contents(
            $configFile,
            '<?php return ' . var_export($config->getConfig(), true) . ';'
        );

        $htmlFile = Path::getHTMLFolder() . DIRECTORY_SEPARATOR . $fileName . '.phtml';
        $htmlValue = 'Hello World!';

        $file = fopen($htmlFile, 'wb+');

        if (false === $file) {
            self::fail();
        }

        fwrite($file, $htmlValue);
        fclose($file);

        $controller = new Controller();

        $statusCode = DI::getResponse()->getStatusCode();

        self::assertSame(
            404,
            $statusCode
        );

        self::assertStringContainsString(
            '404',
            (string) DI::getResponse()->getContent()
        );
        
        unset($controller);
    }

    /**
     * Tests if a request for a subpage can be handled when it is defined as dynamic child.
     *
     * @throws MissingValueException
     * @throws MisleadingConfigException
     * @throws Exception
     */
    #[PreserveGlobalState(false)]
    #[RunInSeparateProcess]
    public function testCanHandleDynamicChild(): void
    {
        $randomPageName = Helper::getRandomString();
        $parentFileName = 'first';
        $childFileName = 'second';
        $attrFirst = '/' . $parentFileName . '/' . $randomPageName . '.html';
        $_SERVER['REQUEST_URI'] = $attrFirst;

        $configFile = Path::getConfigFolder() . DIRECTORY_SEPARATOR . 'config.php';

        $config = new ConfigGenerator();
        $config
            ->setURLStructure('name', 'subname')
            ->addPage(
                PageGenerator::create()
                ->setLanguageCode('de')
                ->setName($parentFileName)
                ->setFile($parentFileName)
                ->setDynamicChild($childFileName)
            )
            ->addPage(
                PageGenerator::create()
                ->setLanguageCode('de')
                ->setName($childFileName)
                ->setFile($childFileName)
                ->setChildOf($parentFileName)
            )
        ;

        file_put_contents(
            $configFile,
            '<?php return ' . var_export($config->getConfig(), true) . ';'
        );
        
        $htmlFile = Path::getHTMLFolder() . DIRECTORY_SEPARATOR . $childFileName . '.phtml';
        $htmlValue = 'Hello World!';

        $file = fopen($htmlFile, 'wb+');

        if (false === $file) {
            self::fail();
        }

        fwrite($file, $htmlValue);
        fclose($file);

        $controller = new Controller();

        self::assertStringContainsString(
            $htmlValue,
            (string) DI::getResponse()->getContent()
        );

        unset($controller);
    }

    /**
     * @throws MissingValueException
     * @throws MisleadingConfigException
     * @throws Exception
     */
    #[PreserveGlobalState(false)]
    #[RunInSeparateProcess]
    public function testCanHandleDynamicChildWithSamePageName(): void
    {
        $randomPageName = Helper::getRandomString();
        $parentFileName = 'first';
        $childFileName = 'second';
        $attrFirst = '/en/' . $parentFileName . '/' . $randomPageName . '.html';
        $_SERVER['REQUEST_URI'] = $attrFirst;

        $configFile = Path::getConfigFolder() . DIRECTORY_SEPARATOR . 'config.php';

        $config = new ConfigGenerator();
        $config
            ->setURLStructure('languageCode', 'name', 'subname')
            ->addPage(
                PageGenerator::create()
                ->setLanguageCode('de')
                ->setName('temp')
                ->setFile('temp')
            )
            ->addPage(
                PageGenerator::create()
                ->setLanguageCode('de')
                ->setName($parentFileName)
                ->setFile($parentFileName)
                ->setDynamicChild($childFileName)
            )
            ->addPage(
                PageGenerator::create()
                ->setLanguageCode('de')
                ->setName($childFileName)
                ->setFile($childFileName)
                ->setChildOf($parentFileName)
            )
            ->addPage(
                PageGenerator::create()
                ->setLanguageCode('en')
                ->setName($parentFileName)
                ->setFile($parentFileName)
                ->setDynamicChild($childFileName)
            )
            ->addPage(
                PageGenerator::create()
                ->setLanguageCode('en')
                ->setName($childFileName)
                ->setFile($childFileName)
                ->setChildOf($parentFileName)
            )
        ;

        file_put_contents(
            $configFile,
            '<?php return ' . var_export($config->getConfig(), true) . ';'
        );

        $htmlFile = Path::getHTMLFolder() . DIRECTORY_SEPARATOR . $childFileName . '.phtml';
        $htmlValue = 'Hello World!';

        file_put_contents(
            $htmlFile,
            $htmlValue
        );

        $controller = new Controller();

        self::assertStringContainsString(
            $htmlValue,
            (string) DI::getResponse()->getContent()
        );

        unset($controller);
    }
}
