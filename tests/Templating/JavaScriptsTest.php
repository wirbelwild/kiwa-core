<?php

/**
 * Kiwa. A feather-light web framework for professional static websites.
 *
 * @author Tobias Köngeter
 * @copyright Copyright © Bit&Black
 * @link https://www.bitandblack.com
 * @link https://www.kiwa.io
 * @license MIT
 */

namespace Kiwa\Tests\Templating;

use Kiwa\Templating\JavaScripts;
use PHPUnit\Framework\Attributes\PreserveGlobalState;
use PHPUnit\Framework\Attributes\RunInSeparateProcess;
use PHPUnit\Framework\TestCase;

/**
 * Class JavaScriptsTest.
 *
 * @package Kiwa\Tests\Templating\Assets
 */
class JavaScriptsTest extends TestCase
{
    #[PreserveGlobalState(false)]
    #[RunInSeparateProcess]
    public function testCanAddAndPositionAssets(): void
    {
        $string1 = '<script>alert("Hello world!");</script>';
        $string2 = '<script src="/notexisting.js" async defer></script>';
        
        JavaScripts::add($string1);

        $output = JavaScripts::getAssets();

        self::assertIsArray($output);
        self::assertSame(
            $string1,
            $output[0]
        );

        JavaScripts::add($string2, 0);

        $output = JavaScripts::getAssets();

        self::assertSame(
            $string1,
            $output[1]
        );

        $javascripts = new JavaScripts();

        self::assertSame(
            $string2 . PHP_EOL . $string1,
            (string) $javascripts
        );
        
        self::assertCount(
            2,
            iterator_to_array($javascripts)
        );
    }
}
