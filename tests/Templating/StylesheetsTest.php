<?php

/**
 * Kiwa. A feather-light web framework for professional static websites.
 *
 * @author Tobias Köngeter
 * @copyright Copyright © Bit&Black
 * @link https://www.bitandblack.com
 * @link https://www.kiwa.io
 * @license MIT
 */

namespace Kiwa\Tests\Templating;

use Kiwa\DI;
use Kiwa\Frontend\Controller;
use Kiwa\Path;
use Kiwa\Templating\Stylesheets;
use Kiwa\Tests\Helper;
use PHPUnit\Framework\Attributes\PreserveGlobalState;
use PHPUnit\Framework\Attributes\RunInSeparateProcess;
use PHPUnit\Framework\TestCase;

/**
 * Class StylesheetsTest.
 *
 * @package Kiwa\Tests\Templating\Assets
 */
class StylesheetsTest extends TestCase
{
    /**
     * Creates needed template folders.
     */
    public static function setUpBeforeClass(): void
    {
        Helper::setUpBefore();
    }

    /**
     * Removes template folders.
     */
    public static function tearDownAfterClass(): void
    {
        Helper::tearDownAfter();
    }
    
    public function testCanAddAndPositionAssets(): void
    {
        $string1 = '<style>body { color: red; }</style>';
        $string2 = '<link href="/notexisting.css" rel="stylesheet" media="all">';
        
        Stylesheets::add($string1);
        
        $output = Stylesheets::getAssets();
        
        self::assertIsArray($output);
        self::assertSame(
            $string1,
            $output[0]
        );

        Stylesheets::add($string2, 0);

        $output = Stylesheets::getAssets();

        self::assertSame(
            $string1,
            $output[1]
        );

        $stylesheets = new Stylesheets();

        self::assertSame(
            $string2 . PHP_EOL . $string1,
            (string) $stylesheets
        );
        
        self::assertCount(
            2,
            iterator_to_array($stylesheets)
        );
    }
    
    /**
     * This test proofs that it's possible to set a stylesheet information
     * from one page to another.
     */
    #[PreserveGlobalState(false)]
    #[RunInSeparateProcess]
    public function testCanSetStylesheetFromOneToAnotherPage(): void
    {
        $testString1 = (string) time();
        $testString2 = 'I am the header';

        $htmlFile = Path::getHTMLFolder() . DIRECTORY_SEPARATOR . 'header.phtml';
        $htmlValue = '<?php echo new \Kiwa\Templating\Stylesheets(); echo \'' . $testString2 . '\';';

        file_put_contents(
            $htmlFile,
            $htmlValue
        );

        $htmlFile = Path::getHTMLFolder() . DIRECTORY_SEPARATOR . 'index.phtml';
        $htmlValue = '<?php \Kiwa\Templating\Stylesheets::add(\'' . $testString1 . '\'); require(\'header.phtml\'); ';

        file_put_contents(
            $htmlFile,
            $htmlValue
        );

        $controller = new Controller();

        self::assertIsString(
            DI::getResponse()->getContent()
        );

        self::assertStringContainsString(
            $testString1,
            (string) DI::getResponse()->getContent()
        );

        self::assertSame(
            $testString1 . $testString2,
            DI::getResponse()->getContent()
        );

        unset($controller);
    }
}
