<?php

/**
 * Kiwa. A feather-light web framework for professional static websites.
 *
 * @author Tobias Köngeter
 * @copyright Copyright © Bit&Black
 * @link https://www.bitandblack.com
 * @link https://www.kiwa.io
 * @license MIT
 */

namespace Kiwa\Tests\Templating;

use Kiwa\Config\Generator\ConfigGenerator;
use Kiwa\Config\Generator\PageGenerator;
use Kiwa\Exception\Config\MissingValueException;
use Kiwa\Frontend\Controller;
use Kiwa\Path;
use Kiwa\Templating\MetaTags;
use Kiwa\Tests\Helper;
use PHPUnit\Framework\Attributes\PreserveGlobalState;
use PHPUnit\Framework\Attributes\RunInSeparateProcess;
use PHPUnit\Framework\TestCase;

/**
 * Class MetaTagsTest
 *
 * @package Kiwa\Tests
 */
class MetaTagsTest extends TestCase
{
    /**
     * Creates needed template folders.
     */
    public static function setUpBeforeClass(): void
    {
        Helper::setUpBefore();
    }

    /**
     * Removes template folders.
     */
    public static function tearDownAfterClass(): void
    {
        Helper::tearDownAfter();
    }

    /**
     * @throws MissingValueException
     */
    #[PreserveGlobalState(false)]
    #[RunInSeparateProcess]
    public function testCanHandleMetaTagsAtIndex(): void
    {
        $attrFirst = null;

        $_SERVER['REQUEST_URI'] = $attrFirst;

        $configFile = Path::getConfigFolder() . DIRECTORY_SEPARATOR . 'config.php';

        $config = new ConfigGenerator();
        $config
            ->setURLStructure('name', 'subname')
            ->addPage(
                PageGenerator::create()
                ->setLanguageCode('de')
                ->setName('/')
                ->setFile('index')
            )
        ;

        file_put_contents(
            $configFile,
            '<?php return ' . var_export($config->getConfig(), true) . ';'
        );
        
        $htmlFile = Path::getHTMLFolder() . DIRECTORY_SEPARATOR . 'index.phtml';
        $htmlValue = 'Hello World!';

        file_put_contents(
            $htmlFile,
            $htmlValue
        );

        $controller = new Controller();

        $metaTags = MetaTags::getMetaTags();

        self::assertNotEmpty($metaTags);

        self::assertSame(
            '<meta property="generator" content="Kiwa">',
            $metaTags[0]
        );
        
        unset($controller);
    }

    /**
     * @throws MissingValueException
     */
    #[PreserveGlobalState(false)]
    #[RunInSeparateProcess]
    public function testCanAddURLToOGImage(): void
    {
        $attrFirst = '/';

        $_SERVER['REQUEST_URI'] = $attrFirst;

        $configFile = Path::getConfigFolder() . DIRECTORY_SEPARATOR . 'config.php';

        $config = new ConfigGenerator();
        $config
            ->setURLStructure('name', 'subname')
            ->addPage(
                PageGenerator::create()
                ->setLanguageCode('de')
                ->setName('index')
                ->setFile('index')
                ->setOGImage('/build/images/testimage.jpg')
            )
            ->setMainURL('http://www.test.de')
        ;

        file_put_contents(
            $configFile,
            '<?php return ' . var_export($config->getConfig(), true) . ';'
        );

        $htmlFile = Path::getHTMLFolder() . DIRECTORY_SEPARATOR . 'index.phtml';
        $htmlValue = 'Hello World!';

        file_put_contents(
            $htmlFile,
            $htmlValue
        );

        $controller = new Controller();
        $metaTags = MetaTags::getMetaTags();

        self::assertCount(
            3,
            $metaTags
        );
        
        self::assertSame(
            '<meta property="og:image" content="http://www.test.de/build/images/testimage.jpg">',
            $metaTags[2]
        );
        
        unset($controller);
    }

    #[PreserveGlobalState(false)]
    #[RunInSeparateProcess]
    public function testCanAddMultipleTagsOfSameType(): void
    {
        MetaTags::add('og:image', 'image1');
        MetaTags::add('og:image', 'image2');

        MetaTags::add('article:tag', 'tag1');
        MetaTags::add('article:tag', 'tag2');

        MetaTags::add('og:description', 'description1');
        MetaTags::add('og:description', 'description2');

        $metaTags = (string) new MetaTags();
        
        self::assertStringContainsString(
            'image1',
            $metaTags
        );

        self::assertStringContainsString(
            'image2',
            $metaTags
        );

        self::assertStringContainsString(
            'tag1',
            $metaTags
        );

        self::assertStringContainsString(
            'tag2',
            $metaTags
        );

        self::assertStringContainsString(
            'description2',
            $metaTags
        );
        
        self::assertStringNotContainsString(
            'description1',
            $metaTags
        );
    }

    /**
     * @throws MissingValueException
     */
    #[PreserveGlobalState(false)]
    #[RunInSeparateProcess]
    public function testRemovesDefaultTagsWhenNotNeeded(): void
    {
        $attrFirst = 'test';

        $_SERVER['REQUEST_URI'] = '/' . $attrFirst;

        $configFile = Path::getConfigFolder() . DIRECTORY_SEPARATOR . 'config.php';

        $config = new ConfigGenerator();
        $config
            ->setURLStructure('name', 'subname')
            ->setDefaultOGImage('image-default.jpg')
            ->setDefaultDescription('de', 'description-default-de')
            ->setDefaultDescription('en', 'description-default-en')
            ->setDefaultOGTitle('de', 'title-default-de')
            ->setDefaultOGTitle('en', 'title-default-en')
            ->setPageSuffix(false)
            ->addPage(
                PageGenerator::create()
                ->setLanguageCode('de')
                ->setName($attrFirst)
                ->setFile($attrFirst)
                ->setOGImage('image-specific.jpg')
                ->setDescription('description-specific-de')
                ->setOGTitle('title-specific-de')
            )
        ;

        file_put_contents(
            $configFile,
            '<?php return ' . var_export($config->getConfig(), true) . ';'
        );

        $htmlFile = Path::getHTMLFolder() . DIRECTORY_SEPARATOR . 'index.phtml';
        $htmlValue = 'Hello World!';

        file_put_contents(
            $htmlFile,
            $htmlValue
        );

        $controller = new Controller();

        $metaTags = (string) new MetaTags();

        self::assertStringContainsString(
            'image-specific.jpg',
            $metaTags
        );

        self::assertStringNotContainsString(
            'image-default.jpg',
            $metaTags
        );

        self::assertStringContainsString(
            'description-specific-de',
            $metaTags
        );

        self::assertStringNotContainsString(
            'description-default-de',
            $metaTags
        );

        self::assertStringNotContainsString(
            'description-default-en',
            $metaTags
        );

        unset($controller);
    }
}
