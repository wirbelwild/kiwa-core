<?php

/**
 * Kiwa. A feather-light web framework for professional static websites.
 *
 * @author Tobias Köngeter
 * @copyright Copyright © Bit&Black
 * @link https://www.bitandblack.com
 * @link https://www.kiwa.io
 * @license MIT
 */

namespace Kiwa\Tests\Templating;

use Kiwa\Config\Generator\ConfigGenerator;
use Kiwa\Config\Generator\PageGenerator;
use Kiwa\DI;
use Kiwa\Exception\Config\MissingValueException;
use Kiwa\Frontend\Controller;
use Kiwa\Path;
use Kiwa\Tests\Helper;
use PHPUnit\Framework\Attributes\PreserveGlobalState;
use PHPUnit\Framework\Attributes\RunInSeparateProcess;
use PHPUnit\Framework\TestCase;

/**
 * Class CanonicalsTest.
 *
 * @package Kiwa\Tests\Templating
 */
class CanonicalsTest extends TestCase
{
    /**
     * Creates needed template folders.
     */
    public static function setUpBeforeClass(): void
    {
        Helper::setUpBefore();
    }

    /**
     * Removes template folders.
     */
    public static function tearDownAfterClass(): void
    {
        Helper::tearDownAfter();
    }

    /**
     * Tests if canonical links can be added.
     *
     * @throws MissingValueException
     */
    #[PreserveGlobalState(false)]
    #[RunInSeparateProcess]
    public function testCanAddCanonicals(): void
    {
        $fileName = 'test';

        $_SERVER['REQUEST_URI'] = '/' . strtoupper($fileName) . '.html';

        $tempAssetFile = Path::getHTMLFolder() . DIRECTORY_SEPARATOR . $fileName . '.phtml';

        file_put_contents($tempAssetFile, '<?php require \'header.phtml\';');

        $header = Path::getHTMLFolder() . DIRECTORY_SEPARATOR . 'header.phtml';

        file_put_contents($header, '<?php echo new \Kiwa\Templating\Canonicals();');

        $configFile = Path::getConfigFolder() . DIRECTORY_SEPARATOR . 'config.php';

        $config = new ConfigGenerator();
        $config
            ->setURLStructure('name', 'subname')
            ->addPage(
                PageGenerator::create()
                ->setLanguageCode('de')
                ->setName(strtolower($fileName))
                ->setFile(strtolower($fileName))
            )
            ->enableWWW()
            ->setPageSuffix('html')
        ;

        file_put_contents(
            $configFile,
            '<?php return ' . var_export($config->getConfig(), true) . ';'
        );

        $controller = new Controller();
        $content = (string) DI::getResponse()->getContent();

        self::assertStringContainsString(
            '<link rel="canonical" href="http://www.localhost/' . $fileName . '.html">',
            $content
        );

        unset($controller);
    }
}
