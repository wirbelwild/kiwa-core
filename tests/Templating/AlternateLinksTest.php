<?php

/**
 * Kiwa. A feather-light web framework for professional static websites.
 *
 * @author Tobias Köngeter
 * @copyright Copyright © Bit&Black
 * @link https://www.bitandblack.com
 * @link https://www.kiwa.io
 * @license MIT
 */

namespace Kiwa\Tests\Templating;

use Exception;
use Kiwa\Config\Generator\ConfigGenerator;
use Kiwa\Config\Generator\PageGenerator;
use Kiwa\Exception\Config\MisleadingConfigException;
use Kiwa\Exception\Config\MissingValueException;
use Kiwa\Frontend\Controller;
use Kiwa\Path;
use Kiwa\Templating\AlternateLinks;
use Kiwa\Tests\Helper;
use PHPUnit\Framework\Attributes\PreserveGlobalState;
use PHPUnit\Framework\Attributes\RunInSeparateProcess;
use PHPUnit\Framework\TestCase;

/**
 * Class AlternateLinksTest.
 *
 * @package Kiwa\Tests\Templating
 */
class AlternateLinksTest extends TestCase
{
    /**
     * Creates needed template folders.
     */
    public static function setUpBeforeClass(): void
    {
        Helper::setUpBefore();
    }

    /**
     * Removes template folders.
     */
    public static function tearDownAfterClass(): void
    {
        Helper::tearDownAfter();
    }

    /**
     * @throws MissingValueException
     * @throws MisleadingConfigException
     */
    #[PreserveGlobalState(false)]
    #[RunInSeparateProcess]
    public function testCanCreateHrefLangs(): void
    {
        $pages = [
            [
                'attrFirst' => 'de',
                'attrSecond' => 'subname',
                'attrThird' => 'test-page-de',
            ],
            [
                'attrFirst' => 'en',
                'attrSecond' => 'subname',
                'attrThird' => 'test-page-en',
            ],
        ];

        $_SERVER['REQUEST_URI'] = '/' . $pages[1]['attrFirst'] . '/' . $pages[1]['attrSecond'] . '/' . $pages[1]['attrThird'] . '.html';

        $configFile = Path::getConfigFolder() . DIRECTORY_SEPARATOR . 'config.php';

        $config = new ConfigGenerator();
        $config
            ->setURLStructure('languageCode', 'name', 'subname')
            ->addPage(
                PageGenerator::create()
                ->setLanguageCode($pages[0]['attrFirst'])
                ->setName($pages[0]['attrThird'])
                ->setFile($pages[0]['attrThird'])
                ->setChildOf($pages[0]['attrSecond'])
            )
            ->addPage(
                PageGenerator::create()
                ->setLanguageCode($pages[1]['attrFirst'])
                ->setName($pages[1]['attrThird'])
                ->setFile($pages[0]['attrThird'])
                ->setChildOf($pages[1]['attrSecond'])
            )
            ->enableWWW()
            ->setPageSuffix('html')
        ;

        file_put_contents(
            $configFile,
            '<?php return ' . var_export($config->getConfig(), true) . ';'
        );

        $controller = new Controller();

        $alternateLinks = (string) new AlternateLinks();

        self::assertSame(
            '<link rel="alternate" href="http://www.localhost/' . $pages[0]['attrFirst'] . '/' . $pages[0]['attrSecond'] . '/' . $pages[0]['attrThird'] . '.html" hreflang="de">' . PHP_EOL .
            '<link rel="alternate" href="http://www.localhost/' . $pages[1]['attrFirst'] . '/' . $pages[1]['attrSecond'] . '/' . $pages[1]['attrThird'] . '.html" hreflang="en">',
            $alternateLinks
        );

        unset($controller);
    }

    /**
     * @throws MissingValueException
     */
    #[PreserveGlobalState(false)]
    #[RunInSeparateProcess]
    public function testCanCreateHrefLangsAutomatically(): void
    {
        $configFile = Path::getConfigFolder() . DIRECTORY_SEPARATOR . 'config.php';

        $config = new ConfigGenerator();
        $config
            ->setURLStructure('name', 'subname')
            ->addPage(
                PageGenerator::create()
                    ->setLanguageCode('de')
                    ->setName('index')
                    ->setFile('index')
            )
            ->enableWWW()
            ->setPageSuffix('html')
        ;

        file_put_contents(
            $configFile,
            '<?php return ' . var_export($config->getConfig(), true) . ';'
        );

        $htmlFile = Path::getHTMLFolder() . DIRECTORY_SEPARATOR . 'index.phtml';
        $htmlValue = '';

        file_put_contents(
            $htmlFile,
            $htmlValue
        );

        $controller = new Controller();
        $alternateLinks = (string) new AlternateLinks();

        self::assertSame(
            '<link rel="alternate" href="http://www.localhost" hreflang="de">',
            $alternateLinks
        );

        unset($controller);
    }

    /**
     * @throws MissingValueException
     */
    #[PreserveGlobalState(false)]
    #[RunInSeparateProcess]
    public function testCanCreateHrefLangsDynamically(): void
    {
        $configFile = Path::getConfigFolder() . DIRECTORY_SEPARATOR . 'config.php';

        $config = new ConfigGenerator();
        $config
            ->setURLStructure('name', 'subname')
            ->addPage(
                PageGenerator::create()
                ->setLanguageCode('de')
                ->setName('index')
                ->setFile('index')
            )
            ->enableWWW()
            ->setPageSuffix('html')
        ;

        file_put_contents(
            $configFile,
            '<?php return ' . var_export($config->getConfig(), true) . ';'
        );

        $htmlFile = Path::getHTMLFolder() . DIRECTORY_SEPARATOR . 'index.phtml';
        $htmlValue = '<?php \Kiwa\Page\RelatedPages::addPage(\'fr-fr\', \'test1\', \'test2\');';

        file_put_contents(
            $htmlFile,
            $htmlValue
        );

        $controller = new Controller();
        $alternateLinks = (string) new AlternateLinks();

        self::assertSame(
            '<link rel="alternate" href="http://www.localhost" hreflang="de">' . PHP_EOL .
            '<link rel="alternate" href="http://www.localhost/test1/test2.html" hreflang="fr-fr">',
            $alternateLinks
        );

        unset($controller);
    }

    /**
     * @throws MissingValueException
     * @throws MisleadingConfigException
     * @throws Exception
     */
    #[PreserveGlobalState(false)]
    #[RunInSeparateProcess]
    public function testCanCreateHrefLangsForDynamicSubpages(): void
    {
        $randomString = Helper::getRandomString();

        $_SERVER['REQUEST_URI'] = '/de/parent-name/' . $randomString . '.html';

        $configFile = Path::getConfigFolder() . DIRECTORY_SEPARATOR . 'config.php';

        $config = new ConfigGenerator();
        $config
            ->setURLStructure('languageCode', 'name', 'subname')
            ->addPage(
                PageGenerator::create()
                ->setLanguageCode('de')
                ->setName('parent-name')
                ->setFile('parent-file')
                ->setDynamicChild('dynamic-name')
            )
            ->addPage(
                PageGenerator::create()
                ->setLanguageCode('de')
                ->setName('dynamic-name')
                ->setFile('dynamic-file')
                ->setChildOf('parent-name')
            )
            ->enableWWW()
            ->setPageSuffix('html')
        ;

        file_put_contents(
            $configFile,
            '<?php return ' . var_export($config->getConfig(), true) . ';'
        );

        $controller = new Controller();

        $alternateLinks = (string) new AlternateLinks();

        self::assertSame(
            '<link rel="alternate" href="http://www.localhost/de/parent-name/' . $randomString . '.html" hreflang="de">',
            $alternateLinks
        );

        unset($controller);
    }
}
