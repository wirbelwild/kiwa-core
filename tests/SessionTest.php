<?php

/**
 * Kiwa. A feather-light web framework for professional static websites.
 *
 * @author Tobias Köngeter
 * @copyright Copyright © Bit&Black
 * @link https://www.bitandblack.com
 * @link https://www.kiwa.io
 * @license MIT
 */

namespace Kiwa\Tests;

use Exception;
use Kiwa\Config\Generator\ConfigGenerator;
use Kiwa\Config\Generator\PageGenerator;
use Kiwa\DI;
use Kiwa\Exception\Config\MisleadingConfigException;
use Kiwa\Exception\Config\MissingValueException;
use Kiwa\Frontend\Controller;
use Kiwa\Path;
use PHPUnit\Framework\Attributes\PreserveGlobalState;
use PHPUnit\Framework\Attributes\RunInSeparateProcess;
use PHPUnit\Framework\TestCase;
use Symfony\Component\HttpFoundation\Response;

class SessionTest extends TestCase
{
    /**
     * Creates needed template folders.
     */
    public static function setUpBeforeClass(): void
    {
        Helper::setUpBefore();
    }

    /**
     * Removes template folders.
     */
    public static function tearDownAfterClass(): void
    {
        Helper::tearDownAfter();
    }

    /**
     * @return void
     * @throws MisleadingConfigException
     * @throws MissingValueException
     * @throws Exception
     */
    #[PreserveGlobalState(false)]
    #[RunInSeparateProcess]
    public function testCanHandleSessionFlashBags(): void
    {
        $defaultLanguageCode = 'en';

        $pages = [
            [
                'attrFirst' => 'de',
                'attrSecond' => 'test-page-name',
                'attrThird' => 'test-page-subname',
            ],
        ];

        $languageFile = Path::getLanguageFolder() . DIRECTORY_SEPARATOR . $pages[0]['attrFirst'] . '.php';
        file_put_contents($languageFile, '<?php return []; ');

        $configFile = Path::getConfigFolder() . DIRECTORY_SEPARATOR . 'config.php';

        $config = new ConfigGenerator();
        $config
            ->setURLStructure('languageCode', 'name', 'subname')
            ->addPage(
                PageGenerator::create()
                ->setLanguageCode($pages[0]['attrFirst'])
                ->setName($pages[0]['attrThird'])
                ->setFile($pages[0]['attrThird'])
                ->setChildOf($pages[0]['attrSecond'])
            )
            ->setDefaultLanguageCode($defaultLanguageCode)
            ->setPageSuffix(false)
        ;

        file_put_contents(
            $configFile,
            '<?php return ' . var_export($config->getConfig(), true) . ';'
        );

        $_SERVER['REQUEST_URI'] = '/hello';

        $controller = new Controller();

        $flashMessage = Helper::getRandomString();

        DI::getSession()->getFlashBag()->add('test', $flashMessage);

        self::assertContains(
            $flashMessage,
            DI::getSession()->getFlashBag()->get('test')
        );

        self::assertNotContains(
            $flashMessage,
            DI::getSession()->getFlashBag()->get('test')
        );

        unset($controller);
    }

    /**
     * @return void
     * @throws MissingValueException
     * @throws Exception
     */
    #[PreserveGlobalState(false)]
    #[RunInSeparateProcess]
    public function testCanHandleSessionFlashBagsFromPages(): void
    {
        $fileName = Helper::getRandomString();

        $flashMessage = Helper::getRandomString();
        $pageContent = Helper::getRandomString();

        file_put_contents(
            Path::getHTMLFolder() . DIRECTORY_SEPARATOR . $fileName . '.phtml',
            '<?php 
                \Kiwa\DI::getSession()->getFlashBag()->add(\'test\', \'' . $flashMessage . '\'); 
                echo \'' . $pageContent . '\';
            '
        );

        $configFile = Path::getConfigFolder() . DIRECTORY_SEPARATOR . 'config.php';

        $config = new ConfigGenerator();
        $config
            ->setURLStructure('name', 'subname')
            ->addPage(
                PageGenerator::create()
                ->setLanguageCode('en')
                ->setName($fileName)
                ->setFile($fileName)
            )
            ->setPageSuffix(false)
        ;

        file_put_contents(
            $configFile,
            '<?php return ' . var_export($config->getConfig(), true) . ';'
        );

        $_SERVER['REQUEST_URI'] = '/' . $fileName;

        $controller = new Controller();

        self::assertSame(
            Response::HTTP_OK,
            DI::getResponse()->getStatusCode()
        );

        self::assertContains(
            $flashMessage,
            DI::getSession()->getFlashBag()->get('test')
        );

        self::assertNotContains(
            $flashMessage,
            DI::getSession()->getFlashBag()->get('test')
        );

        self::assertSame(
            $pageContent,
            DI::getResponse()->getContent()
        );

        unset($controller);
    }

    /**
     * @return void
     * @throws MissingValueException
     * @throws Exception
     */
    #[PreserveGlobalState(false)]
    #[RunInSeparateProcess]
    public function testCanHandleSessionFlashBagsWithRedirect(): void
    {
        $fileName1 = Helper::getRandomString();
        $fileName2 = Helper::getRandomString();

        $flashMessage = Helper::getRandomString();

        $page1Content = Helper::getRandomString();
        $page2Content = Helper::getRandomString();

        file_put_contents(
            Path::getHTMLFolder() . DIRECTORY_SEPARATOR . $fileName1 . '.phtml',
            '<?php 
                \Kiwa\DI::getSession()->getFlashBag()->add(\'test\', \'' . $flashMessage . '\'); 
                $response = new \Symfony\Component\HttpFoundation\RedirectResponse(\'' . $fileName2 . '\');
                \Kiwa\DI::setInstance(\Symfony\Component\HttpFoundation\Response::class, $response);
                echo \'' . $page1Content . '\';
            '
        );

        file_put_contents(
            Path::getHTMLFolder() . DIRECTORY_SEPARATOR . $fileName2 . '.phtml',
            '<?php 
                echo \'' . $page2Content . '\';
            '
        );

        $configFile = Path::getConfigFolder() . DIRECTORY_SEPARATOR . 'config.php';

        $config = new ConfigGenerator();
        $config
            ->setURLStructure('name', 'subname')
            ->addPage(
                PageGenerator::create()
                ->setLanguageCode('en')
                ->setName($fileName1)
                ->setFile($fileName1)
            )
            ->addPage(
                PageGenerator::create()
                ->setLanguageCode('en')
                ->setName($fileName2)
                ->setFile($fileName2)
            )
            ->setPageSuffix(false)
        ;

        file_put_contents(
            $configFile,
            '<?php return ' . var_export($config->getConfig(), true) . ';'
        );

        $_SERVER['REQUEST_URI'] = '/' . $fileName1;

        $controller = new Controller();

        self::assertSame(
            Response::HTTP_FOUND,
            DI::getResponse()->getStatusCode()
        );

        self::assertStringContainsString(
            'Redirecting to ' . $fileName2,
            (string) DI::getResponse()->getContent()
        );

        unset($controller);

        $_SERVER['REQUEST_URI'] = '/' . $fileName2;

        $controller = new Controller();

        self::assertContains(
            $flashMessage,
            DI::getSession()->getFlashBag()->get('test')
        );

        self::assertNotContains(
            $flashMessage,
            DI::getSession()->getFlashBag()->get('test')
        );

        self::assertSame(
            $page2Content,
            DI::getResponse()->getContent()
        );

        unset($controller);
    }
}
