<?php

/**
 * Kiwa. A feather-light web framework for professional static websites.
 *
 * @author Tobias Köngeter
 * @copyright Copyright © Bit&Black
 * @link https://www.bitandblack.com
 * @link https://www.kiwa.io
 * @license MIT
 */

namespace Kiwa\Tests\File;

use Exception;
use Kiwa\Config\Generator\ConfigGenerator;
use Kiwa\Config\Generator\PageGenerator;
use Kiwa\DI;
use Kiwa\Exception\Config\MissingValueException;
use Kiwa\Frontend\Controller;
use Kiwa\Path;
use Kiwa\Tests\Helper;
use PHPUnit\Framework\Attributes\PreserveGlobalState;
use PHPUnit\Framework\Attributes\RunInSeparateProcess;
use PHPUnit\Framework\TestCase;

/**
 * Class BinaryFileTest.
 *
 * @package Kiwa\Tests\File
 */
class BinaryFileTest extends TestCase
{
    /**
     * Creates needed template folders.
     */
    public static function setUpBeforeClass(): void
    {
        Helper::setUpBefore();
    }

    /**
     * Removes template folders.
     */
    public static function tearDownAfterClass(): void
    {
        Helper::tearDownAfter();
    }
    
    #[PreserveGlobalState(false)]
    #[RunInSeparateProcess]
    public function testReturnsJavascriptFile1(): void
    {
        $fileName = 'test.js';
        $fileFolder = '/build';

        $_SERVER['REQUEST_URI'] = $fileFolder . '/' . $fileName;

        $tempAssetFile = Path::getBuildFolder() . DIRECTORY_SEPARATOR . $fileName;
        $currentTime = microtime();

        $file = fopen($tempAssetFile, 'wb+');

        if (false === $file) {
            self::fail();
        }

        fwrite($file, $currentTime);
        fclose($file);

        $controller = new Controller();

        ob_start();
        DI::getResponse()->sendContent();
        $fileContent = ob_get_clean();

        self::assertSame(
            $currentTime,
            $fileContent
        );

        self::assertSame(
            200,
            DI::getResponse()->getStatusCode()
        );

        self::assertStringContainsString(
            'text/javascript',
            (string) DI::getResponse()->headers->get('content-type')
        );

        unset($controller);
    }

    /**
     * @throws MissingValueException
     */
    #[PreserveGlobalState(false)]
    #[RunInSeparateProcess]
    public function testReturnsJavascriptFile2(): void
    {
        $fileName = 'test.js';
        $fileFolder = '/build';

        $_SERVER['REQUEST_URI'] = $fileFolder . '/' . $fileName;

        $tempAssetFile = Path::getBuildFolder() . DIRECTORY_SEPARATOR . $fileName;
        $currentTime = microtime();

        $file = fopen($tempAssetFile, 'wb+');

        if (false === $file) {
            self::fail();
        }

        fwrite($file, $currentTime);
        fclose($file);

        $configFile = Path::getConfigFolder() . DIRECTORY_SEPARATOR . 'config.php';

        $config = new ConfigGenerator();
        $config
            ->setURLStructure('languageCode', 'name', 'subname')
            ->addPage(
                PageGenerator::create()
                ->setLanguageCode('de')
                ->setName('index')
                ->setFile('index')
            )
        ;

        file_put_contents(
            $configFile,
            '<?php return ' . var_export($config->getConfig(), true) . ';'
        );

        $controller = new Controller();

        ob_start();
        DI::getResponse()->sendContent();
        $fileContent = ob_get_clean();

        self::assertSame(
            $currentTime,
            $fileContent
        );

        self::assertSame(
            200,
            DI::getResponse()->getStatusCode()
        );

        unset($controller);
    }

    #[PreserveGlobalState(false)]
    #[RunInSeparateProcess]
    public function testReturnsJavascriptFileFromSubDir(): void
    {
        $fileName = 'test.js';
        $fileFolder = '/build/subdir';

        $_SERVER['REQUEST_URI'] = $fileFolder . '/' . $fileName;

        $tempAssetFile = Path::getBuildFolder() . DIRECTORY_SEPARATOR . 'subdir' . DIRECTORY_SEPARATOR . $fileName;
        $currentTime = microtime();

        $file = fopen($tempAssetFile, 'wb+');

        if (false === $file) {
            self::fail();
        }

        fwrite($file, $currentTime);
        fclose($file);

        $controller = new Controller();

        ob_start();
        DI::getResponse()->sendContent();
        $fileContent = ob_get_clean();

        self::assertSame(
            $currentTime,
            $fileContent
        );

        self::assertSame(
            200,
            DI::getResponse()->getStatusCode()
        );

        unset($controller);
    }

    #[PreserveGlobalState(false)]
    #[RunInSeparateProcess]
    public function testReturnsStylesheetFile(): void
    {
        $fileName = 'test.css';
        $fileFolder = 'css';

        $_SERVER['REQUEST_URI'] = '/' . $fileFolder . '/' . $fileName;

        $tempAssetFile = Path::getPublicFolder() . DIRECTORY_SEPARATOR . $fileFolder;
        $currentTime = microtime();

        mkdir($tempAssetFile, 0777, true);
        
        file_put_contents(
            $tempAssetFile . DIRECTORY_SEPARATOR . $fileName,
            $currentTime
        );

        $controller = new Controller();

        ob_start();
        DI::getResponse()->sendContent();
        $fileContent = ob_get_clean();

        self::assertSame(
            $currentTime,
            $fileContent
        );

        self::assertStringContainsString(
            'text/css',
            (string) DI::getResponse()->headers->get('content-type')
        );

        unset($controller);
    }

    #[PreserveGlobalState(false)]
    #[RunInSeparateProcess]
    public function testReturnsImage(): void
    {
        $fileName = 'test.jpg';
        $fileFolder = '/assets/images';

        $_SERVER['REQUEST_URI'] = $fileFolder . '/' . $fileName;

        $tempAssetFile = Path::getPublicFolder() . DIRECTORY_SEPARATOR . $fileFolder;
        $currentTime = microtime();

        mkdir($tempAssetFile, 0777, true);

        file_put_contents(
            $tempAssetFile . DIRECTORY_SEPARATOR . $fileName,
            $currentTime
        );

        $controller = new Controller();

        ob_start();
        DI::getResponse()->sendContent();
        $fileContent = ob_get_clean();

        self::assertSame(
            $currentTime,
            $fileContent
        );

        self::assertStringContainsString(
            'image/jpeg',
            (string) DI::getResponse()->headers->get('content-type')
        );

        unset($controller);
    }

    #[PreserveGlobalState(false)]
    #[RunInSeparateProcess]
    public function testReturnsFile(): void
    {
        $fileName = 'test.txt';
        $fileFolder = 'my_files';

        $_SERVER['REQUEST_URI'] = '/build/' . $fileFolder . '/' . $fileName;

        $tempAssetFile = Path::getBuildFolder() . DIRECTORY_SEPARATOR . $fileFolder;
        $currentTime = microtime();

        mkdir($tempAssetFile, 0777, true);

        file_put_contents(
            $tempAssetFile . DIRECTORY_SEPARATOR . $fileName,
            $currentTime
        );

        $controller = new Controller();

        ob_start();
        DI::getResponse()->sendContent();
        $fileContent = ob_get_clean();

        self::assertSame(
            $currentTime,
            $fileContent
        );

        self::assertSame(
            200,
            DI::getResponse()->getStatusCode()
        );

        unset($controller);
    }

    #[PreserveGlobalState(false)]
    #[RunInSeparateProcess]
    public function testReturnsPHPFile(): void
    {
        $fileName = 'test.php';
        $fileFolder = 'files';

        $_SERVER['REQUEST_URI'] = '/build/' . $fileFolder . '/' . $fileName;

        $tempAssetFile = Path::getBuildFolder() . DIRECTORY_SEPARATOR . $fileFolder . DIRECTORY_SEPARATOR . $fileName;
        $currentTime = microtime();

        $file = fopen($tempAssetFile, 'wb+');

        if (false === $file) {
            self::fail();
        }

        fwrite($file, '<?php echo "' . $currentTime . '";');
        fclose($file);

        $controller = new Controller();

        ob_start();
        DI::getResponse()->sendContent();
        $fileContent = ob_get_clean();

        self::assertSame(
            $currentTime,
            $fileContent
        );

        self::assertSame(
            200,
            DI::getResponse()->getStatusCode()
        );

        unset($controller);
    }

    #[PreserveGlobalState(false)]
    #[RunInSeparateProcess]
    public function testPHPFileCanChangeHeaders(): void
    {
        $fileName = 'test.php';
        $fileFolder = 'files';

        $_SERVER['REQUEST_URI'] = '/build/' . $fileFolder . '/' . $fileName;

        $tempAssetFile = Path::getBuildFolder() . DIRECTORY_SEPARATOR . $fileFolder . DIRECTORY_SEPARATOR . $fileName;

        $file = fopen($tempAssetFile, 'wb+');

        if (false === $file) {
            self::fail();
        }

        fwrite($file, '<?php \Kiwa\DI::getInstance(\Symfony\Component\HttpFoundation\Response::class)->headers->set(\'content-type\', \'text/css\'); $color = \'#bada55\'; echo \'body { color: \'.$color.\'; }\';');
        fclose($file);

        $controller = new Controller();

        self::assertSame(
            'body { color: #bada55; }',
            (string) DI::getResponse()->getContent()
        );

        self::assertStringContainsString(
            'text/css',
            (string) DI::getResponse()->headers->get('content-type')
        );

        self::assertSame(
            200,
            DI::getResponse()->getStatusCode()
        );

        unset($controller);
    }

    #[PreserveGlobalState(false)]
    #[RunInSeparateProcess]
    public function testCanHandlePHPFileWithErrors(): void
    {
        $fileName = 'test.php';
        $fileFolder = 'files';

        $_SERVER['REQUEST_URI'] = '/build/' . $fileFolder . '/' . $fileName;

        $tempAssetFile = Path::getBuildFolder() . DIRECTORY_SEPARATOR . $fileFolder . DIRECTORY_SEPARATOR . $fileName;

        $file = fopen($tempAssetFile, 'wb+');

        if (false === $file) {
            self::fail();
        }

        fwrite($file, '<?php echo "Hello world!"; throw new \Exception("Boo");');
        fclose($file);

        $controller = new Controller();

        self::assertStringContainsString(
            'Hello world!',
            (string) DI::getResponse()->getContent()
        );

        self::assertStringContainsString(
            'Exception: Boo',
            (string) DI::getResponse()->getContent()
        );

        self::assertSame(
            200,
            DI::getResponse()->getStatusCode()
        );

        unset($controller);
    }

    #[PreserveGlobalState(false)]
    #[RunInSeparateProcess]
    public function testCanLoadFavicon(): void
    {
        $fileName = 'favicon.ico';

        $_SERVER['REQUEST_URI'] = '/' . $fileName;

        $tempAssetFile = Path::getBuildFolder() . DIRECTORY_SEPARATOR . 'images' . DIRECTORY_SEPARATOR . $fileName;
        $currentTime = microtime();

        file_put_contents(
            $tempAssetFile,
            $currentTime
        );

        $controller = new Controller();

        ob_start();
        DI::getResponse()->sendContent();
        $fileContent = ob_get_clean();

        self::assertSame(
            $currentTime,
            $fileContent
        );

        self::assertSame(
            200,
            DI::getResponse()->getStatusCode()
        );

        unset($controller);
    }

    #[PreserveGlobalState(false)]
    #[RunInSeparateProcess]
    public function testCanLoadSitemap(): void
    {
        $fileName = 'sitemap.xml';

        $_SERVER['REQUEST_URI'] = '/' . $fileName;

        $tempAssetFile = Path::getCrawlerFolder() . DIRECTORY_SEPARATOR . $fileName;
        $currentTime = microtime();

        file_put_contents(
            $tempAssetFile,
            $currentTime
        );

        $controller = new Controller();

        ob_start();
        DI::getResponse()->sendContent();
        $fileContent = ob_get_clean();

        self::assertSame(
            $currentTime,
            $fileContent
        );

        self::assertSame(
            200,
            DI::getResponse()->getStatusCode()
        );

        unset($controller);
    }

    #[PreserveGlobalState(false)]
    #[RunInSeparateProcess]
    public function testCanLoadSitemapLanguageVersion(): void
    {
        $fileName = 'sitemap-de.xml';

        $_SERVER['REQUEST_URI'] = '/' . $fileName;

        $tempAssetFile = Path::getCrawlerFolder() . DIRECTORY_SEPARATOR . $fileName;
        $currentTime = microtime();

        $file = fopen($tempAssetFile, 'wb+');

        if (false === $file) {
            self::fail();
        }

        fwrite($file, $currentTime);
        fclose($file);

        $controller = new Controller();

        ob_start();
        DI::getResponse()->sendContent();
        $fileContent = ob_get_clean();

        self::assertSame(
            $currentTime,
            $fileContent
        );

        self::assertSame(
            200,
            DI::getResponse()->getStatusCode()
        );

        unset($controller);
    }

    #[PreserveGlobalState(false)]
    #[RunInSeparateProcess]
    public function testIgnoresCaseAndHandlesUnknownMime(): void
    {
        $fileName = 'singlePage.idml';
        $fileFolder = 'files';

        $_SERVER['REQUEST_URI'] = '/build/' . $fileFolder . '/' . strtolower($fileName);

        $tempAssetFile = Path::getBuildFolder() . DIRECTORY_SEPARATOR . $fileFolder . DIRECTORY_SEPARATOR . $fileName;
        $currentTime = microtime();

        file_put_contents(
            $tempAssetFile,
            $currentTime
        );

        $controller = new Controller();

        ob_start();
        DI::getResponse()->sendContent();
        $fileContent = ob_get_clean();

        self::assertSame(
            $currentTime,
            $fileContent
        );

        self::assertSame(
            200,
            DI::getResponse()->getStatusCode()
        );

        self::assertSame(
            'application/octet-stream',
            DI::getResponse()->headers->get('content-type')
        );

        unset($controller);
    }

    /**
     * @throws Exception
     */
    #[PreserveGlobalState(false)]
    #[RunInSeparateProcess]
    public function testCanLoadFileWithDynamicParameter(): void
    {
        $randomString = Helper::getRandomString();
        
        $fileName = 'ajax.php';

        $_SERVER['REQUEST_URI'] = '/some-folder/' . $fileName . '?test=' . $randomString;

        $tempAssetFile = Path::getPublicFolder() . DIRECTORY_SEPARATOR . 'some-folder';
        $currentTime = microtime();

        @mkdir($tempAssetFile, 0777, true);
        
        file_put_contents(
            $tempAssetFile . DIRECTORY_SEPARATOR . $fileName,
            $currentTime
        );

        $controller = new Controller();

        ob_start();
        DI::getResponse()->sendContent();
        $fileContent = ob_get_clean();

        self::assertSame(
            $currentTime,
            $fileContent
        );

        self::assertSame(
            200,
            DI::getResponse()->getStatusCode()
        );

        unset($controller);
    }
}
