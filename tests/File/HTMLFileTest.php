<?php

/**
 * Kiwa. A feather-light web framework for professional static websites.
 *
 * @author Tobias Köngeter
 * @copyright Copyright © Bit&Black
 * @link https://www.bitandblack.com
 * @link https://www.kiwa.io
 * @license MIT
 */

namespace Kiwa\Tests\File;

use Exception;
use Kiwa\Config\Generator\ConfigGenerator;
use Kiwa\Config\Generator\PageGenerator;
use Kiwa\DI;
use Kiwa\Exception\Config\MisleadingConfigException;
use Kiwa\Exception\Config\MissingValueException;
use Kiwa\Frontend\Controller;
use Kiwa\Path;
use Kiwa\Response\ErrorPage;
use Kiwa\Templating\Canonicals;
use Kiwa\Tests\Helper;
use PHPUnit\Framework\Attributes\PreserveGlobalState;
use PHPUnit\Framework\Attributes\RunInSeparateProcess;
use PHPUnit\Framework\TestCase;

/**
 * Class HTMLFileTest.
 *
 * @package Kiwa\Tests\File
 */
class HTMLFileTest extends TestCase
{
    /**
     * Creates needed template folders.
     */
    public static function setUpBeforeClass(): void
    {
        Helper::setUpBefore();
    }

    /**
     * Removes template folders.
     */
    public static function tearDownAfterClass(): void
    {
        Helper::tearDownAfter();
    }

    /**
     * @throws Exception
     */
    #[PreserveGlobalState(false)]
    #[RunInSeparateProcess]
    public function testPageCanChangeResponseCode(): void
    {
        $fileName = Helper::getRandomString();
        $randomString = Helper::getRandomString();
        $statusCode = random_int(100, 600);

        $_SERVER['REQUEST_URI'] = '/' . $fileName . '.html';
        
        file_put_contents(
            Path::getHTMLFolder() . DIRECTORY_SEPARATOR . $fileName . '.phtml',
            '<?php \Kiwa\DI::getResponse()->setStatusCode(' . $statusCode . '); echo "' . $randomString . '";'
        );

        $config = new ConfigGenerator();
        $config
            ->setURLStructure('name', 'subname')
            ->addPage(
                PageGenerator::create()
                ->setLanguageCode('de')
                ->setName($fileName)
                ->setFile($fileName)
            )
            ->setPageSuffix('html')
        ;

        file_put_contents(
            Path::getConfigFolder() . DIRECTORY_SEPARATOR . 'config.php',
            '<?php return ' . var_export($config->getConfig(), true) . ';'
        );
        
        $controller = new Controller();

        self::assertSame(
            $randomString,
            (string) DI::getResponse()->getContent()
        );

        self::assertSame(
            $statusCode,
            DI::getResponse()->getStatusCode()
        );

        unset($controller);
    }

    /**
     * @throws Exception
     */
    #[PreserveGlobalState(false)]
    #[RunInSeparateProcess]
    public function testReturns404AtExistingErrorPage(): void
    {
        $randomString = Helper::getRandomString();

        $_SERVER['REQUEST_URI'] = '/notexisting.html';

        file_put_contents(
            Path::getHTMLFolder() . DIRECTORY_SEPARATOR . '404.phtml',
            $randomString
        );
        
        $controller = new Controller();

        self::assertSame(
            $randomString,
            (string) DI::getResponse()->getContent()
        );

        self::assertSame(
            404,
            DI::getResponse()->getStatusCode()
        );

        unset($controller);
    }
    
    #[PreserveGlobalState(false)]
    #[RunInSeparateProcess]
    public function testReturnsErrorPage1(): void
    {
        $_SERVER['REQUEST_URI'] = '/notexisting.html';

        $controller = new Controller();

        self::assertSame(
            (string) new ErrorPage(404),
            (string) DI::getResponse()->getContent()
        );

        self::assertSame(
            404,
            DI::getResponse()->getStatusCode()
        );

        unset($controller);
    }

    #[PreserveGlobalState(false)]
    #[RunInSeparateProcess]
    public function testReturnsErrorPage2(): void
    {
        $_SERVER['REQUEST_URI'] = '/notexisting/really/crazy/url.html';

        $controller = new Controller();

        self::assertSame(
            (string) new ErrorPage(404),
            (string) DI::getResponse()->getContent()
        );

        self::assertSame(
            404,
            DI::getResponse()->getStatusCode()
        );

        unset($controller);
    }

    #[PreserveGlobalState(false)]
    #[RunInSeparateProcess]
    public function testReturnsIndexPage(): void
    {
        $_SERVER['REQUEST_URI'] = '/';

        $tempAssetFile = Path::getHTMLFolder() . DIRECTORY_SEPARATOR . 'index.phtml';
        $currentTime = microtime();

        file_put_contents(
            $tempAssetFile,
            $currentTime
        );

        $controller = new Controller();

        self::assertSame(
            $currentTime,
            (string) DI::getResponse()->getContent()
        );

        unset($controller);
    }

    /**
     * This test proves that a request for an index page with a language version redirects to a subpage
     * with status 301 and null output.
     *
     * @throws MissingValueException
     */
    #[PreserveGlobalState(false)]
    #[RunInSeparateProcess]
    public function testReturnsIndexPageWithLanguageRedirect(): void
    {
        $_SERVER['REQUEST_URI'] = '/';
        
        $configFile = Path::getConfigFolder() . DIRECTORY_SEPARATOR . 'config.php';

        $config = new ConfigGenerator();
        $config
            ->setURLStructure('languageCode', 'name', 'subname')
            ->addPage(
                PageGenerator::create()
                ->setLanguageCode('de')
                ->setName('index')
                ->setFile('index')
            )
        ;

        file_put_contents(
            $configFile,
            '<?php return ' . var_export($config->getConfig(), true) . ';'
        );

        $controller = new Controller();

        self::assertStringContainsString(
            'Redirecting',
            (string) DI::getResponse()->getContent()
        );

        unset($controller);
    }

    /**
     * @throws MissingValueException
     */
    #[PreserveGlobalState(false)]
    #[RunInSeparateProcess]
    public function testReturnsPageFileWithSuffix(): void
    {
        $fileName = 'test';

        $_SERVER['REQUEST_URI'] = '/' . $fileName . '.html';

        $tempAssetFile = Path::getHTMLFolder() . DIRECTORY_SEPARATOR . $fileName . '.phtml';
        $currentTime = microtime();

        $file1 = fopen($tempAssetFile, 'wb+');

        if (false === $file1) {
            self::assertIsResource($file1);
            return;
        }

        fwrite($file1, $currentTime);
        fclose($file1);

        $configFile = Path::getConfigFolder() . DIRECTORY_SEPARATOR . 'config.php';

        $config = new ConfigGenerator();
        $config
            ->setURLStructure('name', 'subname')
            ->addPage(
                PageGenerator::create()
                ->setLanguageCode('de')
                ->setName($fileName)
                ->setFile($fileName)
            )
            ->setPageSuffix('html')
        ;

        file_put_contents(
            $configFile,
            '<?php return ' . var_export($config->getConfig(), true) . ';'
        );

        $controller = new Controller();

        self::assertSame(
            $currentTime,
            (string) DI::getResponse()->getContent()
        );
        
        unset($controller);
    }

    /**
     * @throws MissingValueException
     */
    #[PreserveGlobalState(false)]
    #[RunInSeparateProcess]
    public function testReturnsPageFileWithoutSuffix(): void
    {
        $fileName = 'test';

        $_SERVER['REQUEST_URI'] = '/' . $fileName;

        $tempAssetFile = Path::getHTMLFolder() . DIRECTORY_SEPARATOR . $fileName . '.phtml';
        $currentTime = microtime();

        $file1 = fopen($tempAssetFile, 'wb+');

        if (false === $file1) {
            self::assertIsResource($file1);
            return;
        }

        fwrite($file1, $currentTime);
        fclose($file1);

        $configFile = Path::getConfigFolder() . DIRECTORY_SEPARATOR . 'config.php';

        $config = new ConfigGenerator();
        $config
            ->setURLStructure('name', 'subname')
            ->addPage(
                PageGenerator::create()
                ->setLanguageCode('de')
                ->setName($fileName)
                ->setFile($fileName)
            )
            ->setPageSuffix(false)
        ;

        file_put_contents(
            $configFile,
            '<?php return ' . var_export($config->getConfig(), true) . ';'
        );


        $controller = new Controller();

        self::assertSame(
            $currentTime,
            (string) DI::getResponse()->getContent()
        );
        
        unset($controller);
    }

    /**
     * @throws MissingValueException
     */
    #[PreserveGlobalState(false)]
    #[RunInSeparateProcess]
    public function testRedirectsToPageWhenURLHasWrongSuffix1(): void
    {
        $fileName = 'test';
        $suffix = 'html';

        $_SERVER['REQUEST_URI'] = '/' . $fileName . '.htm';

        $tempAssetFile = Path::getHTMLFolder() . DIRECTORY_SEPARATOR . $fileName . '.phtml';
        $currentTime = microtime();

        $file1 = fopen($tempAssetFile, 'wb+');

        if (false === $file1) {
            self::assertIsResource($file1);
            return;
        }

        fwrite($file1, $currentTime);
        fclose($file1);

        $configFile = Path::getConfigFolder() . DIRECTORY_SEPARATOR . 'config.php';

        $config = new ConfigGenerator();
        $config
            ->setURLStructure('name', 'subname')
            ->addPage(
                PageGenerator::create()
                ->setLanguageCode('de')
                ->setName($fileName)
                ->setFile($fileName)
            )
            ->setPageSuffix($suffix)
        ;

        file_put_contents(
            $configFile,
            '<?php return ' . var_export($config->getConfig(), true) . ';'
        );
        
        $controller = new Controller();

        self::assertStringContainsString(
            '<meta http-equiv="refresh" content="0;url=\'/' . $fileName . '.' . $suffix . '\'" />',
            (string) DI::getResponse()->getContent()
        );

        self::assertSame(
            301,
            DI::getResponse()->getStatusCode()
        );
        
        unset($controller);
    }

    /**
     * @throws MissingValueException
     */
    #[PreserveGlobalState(false)]
    #[RunInSeparateProcess]
    public function testRedirectsToPageWhenURLHasWrongSuffix2(): void
    {
        $fileName = 'test';
        $suffix = 'htm';

        $_SERVER['REQUEST_URI'] = '/' . $fileName;

        $tempAssetFile = Path::getHTMLFolder() . DIRECTORY_SEPARATOR . $fileName . '.phtml';
        $currentTime = microtime();

        $file1 = fopen($tempAssetFile, 'wb+');

        if (false === $file1) {
            self::assertIsResource($file1);
            return;
        }

        fwrite($file1, $currentTime);
        fclose($file1);

        $configFile = Path::getConfigFolder() . DIRECTORY_SEPARATOR . 'config.php';

        $config = new ConfigGenerator();
        $config
            ->setURLStructure('name', 'subname')
            ->addPage(
                PageGenerator::create()
                ->setLanguageCode('de')
                ->setName($fileName)
                ->setFile($fileName)
            )
            ->setPageSuffix($suffix)
        ;

        file_put_contents(
            $configFile,
            '<?php return ' . var_export($config->getConfig(), true) . ';'
        );

        $controller = new Controller();

        self::assertStringContainsString(
            '<meta http-equiv="refresh" content="0;url=\'/' . $fileName . '.' . $suffix . '\'" />',
            (string) DI::getResponse()->getContent()
        );

        self::assertSame(
            301,
            DI::getResponse()->getStatusCode()
        );
        
        unset($controller);
    }

    /**
     * @throws MissingValueException
     */
    #[PreserveGlobalState(false)]
    #[RunInSeparateProcess]
    public function testRedirectsToPageWhenURLHasWrongSuffix3(): void
    {
        $fileName = 'test';
        $suffix = false;

        $_SERVER['REQUEST_URI'] = '/' . $fileName . '.html';

        $tempAssetFile = Path::getHTMLFolder() . DIRECTORY_SEPARATOR . $fileName . '.phtml';
        $currentTime = microtime();

        $file1 = fopen($tempAssetFile, 'wb+');

        if (false === $file1) {
            self::assertIsResource($file1);
            return;
        }

        fwrite($file1, $currentTime);
        fclose($file1);

        $configFile = Path::getConfigFolder() . DIRECTORY_SEPARATOR . 'config.php';

        $config = new ConfigGenerator();
        $config
            ->setURLStructure('name', 'subname')
            ->addPage(
                PageGenerator::create()
                ->setLanguageCode('de')
                ->setName($fileName)
                ->setFile($fileName)
            )
            ->setPageSuffix($suffix)
        ;

        file_put_contents(
            $configFile,
            '<?php return ' . var_export($config->getConfig(), true) . ';'
        );
        
        $controller = new Controller();

        self::assertStringContainsString(
            '<meta http-equiv="refresh" content="0;url=\'/' . $fileName . '\'" />',
            (string) DI::getResponse()->getContent()
        );

        self::assertSame(
            301,
            DI::getResponse()->getStatusCode()
        );
        
        unset($controller);
    }

    /**
     * Tests the redirect from a subpage.
     *
     * @throws MissingValueException
     * @throws MisleadingConfigException
     */
    #[PreserveGlobalState(false)]
    #[RunInSeparateProcess]
    public function testRedirectsToPageWhenURLHasWrongSuffix4(): void
    {
        $fileName = 'test';
        $suffix = false;

        $_SERVER['REQUEST_URI'] = 'first/' . $fileName . '.html';

        $tempAssetFile = Path::getHTMLFolder() . DIRECTORY_SEPARATOR . $fileName . '.phtml';
        $currentTime = microtime();

        $file1 = fopen($tempAssetFile, 'wb+');

        if (false === $file1) {
            self::assertIsResource($file1);
            return;
        }

        fwrite($file1, $currentTime);
        fclose($file1);

        $configFile = Path::getConfigFolder() . DIRECTORY_SEPARATOR . 'config.php';

        $config = new ConfigGenerator();
        $config
            ->setURLStructure('name', 'subname')
            ->addPage(
                PageGenerator::create()
                ->setLanguageCode('de')
                ->setName($fileName)
                ->setFile($fileName)
                ->setChildOf('first')
            )
            ->setPageSuffix($suffix)
        ;

        file_put_contents(
            $configFile,
            '<?php return ' . var_export($config->getConfig(), true) . ';'
        );

        $controller = new Controller();

        self::assertStringContainsString(
            '<meta http-equiv="refresh" content="0;url=\'/first/' . $fileName . '\'" />',
            (string) DI::getResponse()->getContent()
        );

        self::assertSame(
            301,
            DI::getResponse()->getStatusCode()
        );
        
        unset($controller);
    }

    /**
     * @throws MissingValueException
     */
    #[PreserveGlobalState(false)]
    #[RunInSeparateProcess]
    public function testIgnoresCaseAtPageRequest(): void
    {
        $fileName = 'test';

        $_SERVER['REQUEST_URI'] = '/' . strtoupper($fileName) . '.html';

        $tempAssetFile = Path::getHTMLFolder() . DIRECTORY_SEPARATOR . $fileName . '.phtml';
        $currentTime = microtime();

        $file1 = fopen($tempAssetFile, 'wb+');

        if (false === $file1) {
            self::assertIsResource($file1);
            return;
        }

        fwrite($file1, $currentTime);
        fclose($file1);

        $configFile = Path::getConfigFolder() . DIRECTORY_SEPARATOR . 'config.php';

        $config = new ConfigGenerator();
        $config
            ->setURLStructure('name', 'subname')
            ->addPage(
                PageGenerator::create()
                ->setLanguageCode('de')
                ->setName(strtolower($fileName))
                ->setFile(strtolower($fileName))
            )
            ->enableWWW()
        ;

        file_put_contents(
            $configFile,
            '<?php return ' . var_export($config->getConfig(), true) . ';'
        );
        
        $controller = new Controller();

        ob_start();
        DI::getResponse()->sendContent();
        $fileContent = ob_get_clean();

        self::assertSame(
            $currentTime,
            $fileContent
        );

        self::assertSame(
            'http://www.localhost/' . $fileName . '.html',
            Canonicals::getURLs()[0]
        );
        
        unset($controller);
    }

    /**
     * @throws MissingValueException
     */
    #[PreserveGlobalState(false)]
    #[RunInSeparateProcess]
    public function testCanCachePages(): void
    {
        $fileName = 'test';

        $_SERVER['REQUEST_URI'] = '/' . $fileName . '.html';

        $tempAssetFile = Path::getHTMLFolder() . DIRECTORY_SEPARATOR . $fileName . '.phtml';

        $file1 = fopen($tempAssetFile, 'wb+');

        if (false === $file1) {
            self::assertIsResource($file1);
            return;
        }

        fwrite($file1, '<?php echo microtime();');
        fclose($file1);

        $configFile = Path::getConfigFolder() . DIRECTORY_SEPARATOR . 'config.php';

        $config = new ConfigGenerator();
        $config
            ->setURLStructure('name', 'subname')
            ->addPage(
                PageGenerator::create()
                ->setLanguageCode('de')
                ->setName(strtolower($fileName))
                ->setFile(strtolower($fileName))
                ->enableCaching()
            )
        ;

        file_put_contents(
            $configFile,
            '<?php return ' . var_export($config->getConfig(), true) . ';'
        );

        $envFile = Path::getRootFolder() . DIRECTORY_SEPARATOR . '.env.local';
        $envContent = 'APP_ENV=prod';

        file_put_contents(
            $envFile,
            $envContent
        );

        $controller = new Controller();
        $firstResponse = (string) DI::getResponse()->getContent();

        unset($controller);
        $controller = new Controller();
        $secondResponse = (string) DI::getResponse()->getContent();

        self::assertSame(
            $firstResponse,
            $secondResponse
        );

        Helper::deleteDirectory(
            Path::getCacheFolder()
        );

        unset($controller);
        $controller = new Controller();
        $thirdResponse = (string) DI::getResponse()->getContent();

        self::assertNotSame(
            $firstResponse,
            $thirdResponse
        );
        
        unset($controller);
    }

    /**
     * @throws MissingValueException
     * @throws MisleadingConfigException
     * @throws Exception
     */
    #[PreserveGlobalState(false)]
    #[RunInSeparateProcess]
    public function testCanCacheDynamicPages(): void
    {
        $childFileName = 'test';
            
        $_SERVER['REQUEST_URI'] = '/parent/' . Helper::getRandomString() . '.html';

        $childFile = Path::getHTMLFolder() . DIRECTORY_SEPARATOR . $childFileName . '.phtml';

        file_put_contents(
            $childFile,
            '<?php echo microtime();'
        );

        $configFile = Path::getConfigFolder() . DIRECTORY_SEPARATOR . 'config.php';

        $config = new ConfigGenerator();
        $config
            ->setURLStructure('name', 'subname')
            ->addPage(
                PageGenerator::create()
                ->setLanguageCode('de')
                ->setName('parent')
                ->setFile('parent')
                ->setDynamicChild('child')
            )
            ->addPage(
                PageGenerator::create()
                ->setLanguageCode('de')
                ->setName('child')
                ->setFile(strtolower($childFileName))
                ->setChildOf('parent')
                ->enableCaching()
            )
        ;

        file_put_contents(
            $configFile,
            '<?php return ' . var_export($config->getConfig(), true) . ';'
        );

        $envFile = Path::getRootFolder() . DIRECTORY_SEPARATOR . '.env.local';
        $envContent = 'APP_ENV=prod';

        file_put_contents(
            $envFile,
            $envContent
        );

        $controller = new Controller();
        $firstResponse = (string) DI::getResponse()->getContent();

        unset($controller);
        
        $controller = new Controller();
        $secondResponse = (string) DI::getResponse()->getContent();

        self::assertSame(
            $firstResponse,
            $secondResponse
        );

        Helper::deleteDirectory(
            Path::getCacheFolder()
        );

        unset($controller);
        
        $controller = new Controller();
        $thirdResponse = (string) DI::getResponse()->getContent();

        self::assertNotSame(
            $firstResponse,
            $thirdResponse
        );

        unset($controller);

        $_SERVER['REQUEST_URI'] = '/parent/' . Helper::getRandomString() . '.html';
        $controller = new Controller();
        $fourthResponse = (string) DI::getResponse()->getContent();
        
        self::assertNotSame(
            $thirdResponse,
            $fourthResponse
        );
        
        unset($controller);
    }

    /**
     * @throws MissingValueException
     * @throws Exception
     */
    #[PreserveGlobalState(false)]
    #[RunInSeparateProcess]
    public function testCanHandlePostRequestOnCachedPages(): void
    {
        $fileName = 'test';

        $_SERVER['REQUEST_URI'] = '/' . $fileName . '.html';

        $tempAssetFile = Path::getHTMLFolder() . DIRECTORY_SEPARATOR . $fileName . '.phtml';

        file_put_contents(
            $tempAssetFile,
            '<?php echo microtime(); var_dump($_POST);'
        );
        
        $configFile = Path::getConfigFolder() . DIRECTORY_SEPARATOR . 'config.php';

        $config = new ConfigGenerator();
        $config
            ->setURLStructure('name', 'subname')
            ->addPage(
                PageGenerator::create()
                ->setLanguageCode('de')
                ->setName(strtolower($fileName))
                ->setFile(strtolower($fileName))
                ->enableCaching()
            )
        ;

        file_put_contents(
            $configFile,
            '<?php return ' . var_export($config->getConfig(), true) . ';'
        );

        $envFile = Path::getRootFolder() . DIRECTORY_SEPARATOR . '.env.local';
        $envContent = 'APP_ENV=prod';

        file_put_contents(
            $envFile,
            $envContent
        );

        $controller = new Controller();
        $firstResponse = (string) DI::getResponse()->getContent();

        unset($controller);
        $controller = new Controller();
        $secondResponse = (string) DI::getResponse()->getContent();

        self::assertSame(
            $firstResponse,
            $secondResponse
        );

        $postData = Helper::getRandomString();
        
        $_POST['someData'] = $postData;

        unset($controller);
        $controller = new Controller();
        $thirdResponse = (string) DI::getResponse()->getContent();

        self::assertNotSame(
            $firstResponse,
            $thirdResponse
        );

        self::assertNotSame(
            $secondResponse,
            $thirdResponse
        );

        self::assertStringNotContainsString(
            $postData,
            $firstResponse
        );

        self::assertStringContainsString(
            $postData,
            $thirdResponse
        );

        unset($controller);
    }


    /**
     * @throws MissingValueException
     * @throws Exception
     */
    #[PreserveGlobalState(false)]
    #[RunInSeparateProcess]
    public function testCanHandleCacheLifetime(): void
    {
        $fileName = 'test';

        $_SERVER['REQUEST_URI'] = '/' . $fileName . '.html';

        $tempAssetFile = Path::getHTMLFolder() . DIRECTORY_SEPARATOR . $fileName . '.phtml';

        file_put_contents(
            $tempAssetFile,
            '<?php echo microtime();'
        );

        $configFile = Path::getConfigFolder() . DIRECTORY_SEPARATOR . 'config.php';

        $config = new ConfigGenerator();
        $config
            ->setURLStructure('name', 'subname')
            ->addPage(
                PageGenerator::create()
                ->setLanguageCode('de')
                ->setName(strtolower($fileName))
                ->setFile(strtolower($fileName))
                ->enableCaching(2)
            )
        ;

        file_put_contents(
            $configFile,
            '<?php return ' . var_export($config->getConfig(), true) . ';'
        );

        $envFile = Path::getRootFolder() . DIRECTORY_SEPARATOR . '.env.local';
        $envContent = 'APP_ENV=prod';

        file_put_contents(
            $envFile,
            $envContent
        );

        $controller = new Controller();
        $firstResponse = DI::getResponse();
        $firstResponseContent = (string) $firstResponse->getContent();
        $maxAge = $firstResponse->getMaxAge();
        
        self::assertSame(
            2,
            $maxAge
        );
        
        unset($controller);

        $controller = new Controller();
        $secondResponse = DI::getResponse();
        $secondResponseContent = (string) $secondResponse->getContent();
        $maxAge = $firstResponse->getMaxAge();

        self::assertSame(
            2,
            $maxAge
        );
        
        self::assertSame(
            $firstResponseContent,
            $secondResponseContent
        );

        unset($controller);

        sleep(3);
        
        $controller = new Controller();
        $thirdResponse = DI::getResponse();
        $thirdResponseContent = (string) $thirdResponse->getContent();
        $maxAge = $firstResponse->getMaxAge();

        self::assertSame(
            2,
            $maxAge
        );
        
        self::assertNotSame(
            $firstResponseContent,
            $thirdResponseContent
        );
        
        unset($controller);
    }

    /**
     * @throws Exception
     */
    #[PreserveGlobalState(false)]
    #[RunInSeparateProcess]
    public function testReturnsErrorPageOnException(): void
    {
        $_SERVER['REQUEST_URI'] = '/';

        file_put_contents(
            Path::getHTMLFolder() . DIRECTORY_SEPARATOR . 'index.phtml',
            '<?php throw new RuntimeException(\'=(\');'
        );

        $controller = new Controller();

        $content = (string) DI::getResponse()->getContent();

        self::assertStringContainsString(
            (string) new ErrorPage(500),
            $content
        );

        self::assertSame(
            500,
            DI::getResponse()->getStatusCode()
        );

        unset($controller);
    }
}
