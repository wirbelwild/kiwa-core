<?php

/**
 * Kiwa. A feather-light web framework for professional static websites.
 *
 * @author Tobias Köngeter
 * @copyright Copyright © Bit&Black
 * @link https://www.bitandblack.com
 * @link https://www.kiwa.io
 * @license MIT
 */

namespace Kiwa\Tests\Config;

use Kiwa\Config\RobotsTxtContent;
use Kiwa\Config\RobotsTxtFile;
use Kiwa\Path;
use PHPUnit\Framework\TestCase;

class RobotsTxtFileTest extends TestCase
{
    private string $file;

    protected function setUp(): void
    {
        $this->file = Path::getCrawlerFolder() . DIRECTORY_SEPARATOR . 'robots.txt';
    }

    protected function tearDown(): void
    {
        unlink($this->file);
    }

    public function testCanWrite(): void
    {
        $content = [
            'sitemap' => 'Sitemap: https://www.example.org/sitemap.xml',
        ];

        $robotsTxtContent = new RobotsTxtContent($content);

        $success = RobotsTxtFile::write($robotsTxtContent);

        self::assertTrue($success);

        $contentExpected = <<<TEXT
        User-agent: *
        Allow: /
        Sitemap: https://www.example.org/sitemap.xml
        TEXT;

        self::assertSame(
            $contentExpected,
            file_get_contents($this->file)
        );
    }
}
