<?php

/**
 * Kiwa. A feather-light web framework for professional static websites.
 *
 * @author Tobias Köngeter
 * @copyright Copyright © Bit&Black
 * @link https://www.bitandblack.com
 * @link https://www.kiwa.io
 * @license MIT
 */

namespace Kiwa\Tests\Config;

use Kiwa\Config\PropertyHandler;
use Kiwa\Exception\Config\SetupNotPossibleException;
use Kiwa\Language\CurrentLanguageCode;
use Kiwa\Language\LanguageCode;
use PHPUnit\Framework\TestCase;

class PropertyHandlerTest extends TestCase
{
    /**
     * @return void
     * @throws SetupNotPossibleException
     */
    public function testSet(): void
    {
        $currentLanguageCode = new CurrentLanguageCode();

        self::assertNull(
            $currentLanguageCode->getCurrentLanguageCode()
        );
        
        $languageCode = new LanguageCode('de-de');
        
        PropertyHandler::set(
            CurrentLanguageCode::class,
            'languageCode',
            $languageCode
        );

        $currentLanguageCode = new CurrentLanguageCode();

        self::assertSame(
            $languageCode,
            $currentLanguageCode->getCurrentLanguageCode()
        );
    }
}
