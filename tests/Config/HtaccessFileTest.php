<?php

/**
 * Kiwa. A feather-light web framework for professional static websites.
 *
 * @author Tobias Köngeter
 * @copyright Copyright © Bit&Black
 * @link https://www.bitandblack.com
 * @link https://www.kiwa.io
 * @license MIT
 */

namespace Kiwa\Tests\Config;

use Kiwa\Config\HtaccessFile;
use Kiwa\Path;
use PHPUnit\Framework\TestCase;
use Random\RandomException;

class HtaccessFileTest extends TestCase
{
    private static string $htaccessFile;

    public static function setUpBeforeClass(): void
    {
        self::$htaccessFile = Path::getPublicFolder() . DIRECTORY_SEPARATOR . '.htaccess';
    }

    protected function tearDown(): void
    {
        unlink(self::$htaccessFile);
    }

    /**
     * @throws RandomException
     */
    public function testCanAddAndUpdateFile(): void
    {
        $initialContent = bin2hex(
            random_bytes(16)
        );

        file_put_contents(self::$htaccessFile, $initialContent);

        $htaccessContent = (string) file_get_contents(self::$htaccessFile);

        self::assertSame(
            $initialContent,
            $htaccessContent
        );

        $namespace = bin2hex(
            random_bytes(8)
        ) . '/' . bin2hex(
            random_bytes(8)
        );

        $namespaceContent1 = bin2hex(
            random_bytes(16)
        );

        $namespaceContent2 = bin2hex(
            random_bytes(8)
        );

        HtaccessFile::add($namespaceContent1, $namespace);

        $htaccessContent = (string) file_get_contents(self::$htaccessFile);

        self::assertSame(
            $initialContent
            . '###> ' . $namespace . ' ###' . PHP_EOL
            . $namespaceContent1 . PHP_EOL
            . '###< ' . $namespace . ' ###' . PHP_EOL,
            $htaccessContent
        );

        HtaccessFile::add($namespaceContent2, $namespace);

        $htaccessContent = (string) file_get_contents(self::$htaccessFile);

        self::assertSame(
            $initialContent
            . '###> ' . $namespace . ' ###' . PHP_EOL
            . $namespaceContent2 . PHP_EOL
            . '###< ' . $namespace . ' ###' . PHP_EOL,
            $htaccessContent
        );
    }
}
