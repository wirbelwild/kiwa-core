<?php

/**
 * Kiwa. A feather-light web framework for professional static websites.
 *
 * @author Tobias Köngeter
 * @copyright Copyright © Bit&Black
 * @link https://www.bitandblack.com
 * @link https://www.kiwa.io
 * @license MIT
 */

namespace Kiwa\Tests\Config\Generator;

use Kiwa\Config\Generator\PageGenerator;
use Kiwa\Exception\Config\MisleadingConfigException;
use Kiwa\Exception\Config\MissingValueException;
use PHPUnit\Framework\TestCase;

/**
 * Class PageGeneratorTest.
 *
 * @package Kiwa\Tests\Config\Generator
 */
class PageGeneratorTest extends TestCase
{
    /**
     * @throws MisleadingConfigException
     */
    public function testCanThrowMisleadingException(): void
    {
        $this->expectException(MisleadingConfigException::class);

        $pageGenerator = new PageGenerator();
        $pageGenerator
            ->setDynamicChild('test1')
            ->setChildOf('test2')
        ;
    }

    /**
     * @throws MissingValueException
     */
    public function testCanThrowMissingValueException(): void
    {
        $this->expectException(MissingValueException::class);

        $pageGenerator = new PageGenerator();
        $pageGenerator->getConfig();
    }
}
