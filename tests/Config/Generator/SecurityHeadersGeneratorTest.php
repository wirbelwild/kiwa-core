<?php

/**
 * Kiwa. A feather-light web framework for professional static websites.
 *
 * @author Tobias Köngeter
 * @copyright Copyright © Bit&Black
 * @link https://www.bitandblack.com
 * @link https://www.kiwa.io
 * @license MIT
 */

namespace Kiwa\Tests\Config\Generator;

use Exception;
use Kiwa\Config\Generator\ConfigGenerator;
use Kiwa\Config\Generator\PageGenerator;
use Kiwa\Config\Generator\SecurityHeadersGenerator;
use Kiwa\DI;
use Kiwa\Exception\Config\MissingValueException;
use Kiwa\Frontend\Controller;
use Kiwa\Path;
use Kiwa\Tests\Helper;
use PHPUnit\Framework\Attributes\PreserveGlobalState;
use PHPUnit\Framework\Attributes\RunInSeparateProcess;
use PHPUnit\Framework\TestCase;

class SecurityHeadersGeneratorTest extends TestCase
{
    /**
     * Removes template folders.
     */
    public static function tearDownAfterClass(): void
    {
        Helper::tearDownAfter();
    }
    
    /**
     * @throws MissingValueException
     * @throws Exception
     */
    #[PreserveGlobalState(false)]
    #[RunInSeparateProcess]
    public function testCanHandleSecurityHeaders(): void
    {
        $fileName = 'test';

        $_SERVER['REQUEST_URI'] = '/' . $fileName . '.html';

        $tempAssetFile = Path::getHTMLFolder() . DIRECTORY_SEPARATOR . $fileName . '.phtml';
        $currentTime = microtime();

        file_put_contents(
            $tempAssetFile,
            $currentTime
        );

        $configFile = Path::getConfigFolder() . DIRECTORY_SEPARATOR . 'config.php';

        $config = new ConfigGenerator();
        $config
            ->setURLStructure('name', 'subname')
            ->addPage(
                PageGenerator::create()
                ->setLanguageCode('de')
                ->setName($fileName)
                ->setFile($fileName)
            )
            ->setPageSuffix('html')
            ->enableSSL()
        ;

        file_put_contents(
            $configFile,
            '<?php return ' . var_export($config->getConfig(), true) . ';'
        );

        $controller = new Controller();
        
        self::assertSame(
            'strict-origin-when-cross-origin',
            DI::getResponse()->headers->get('referrer-policy')
        );

        self::assertSame(
            'max-age=31536000; includeSubDomains',
            DI::getResponse()->headers->get('strict-transport-security')
        );

        self::assertSame(
            'nosniff',
            DI::getResponse()->headers->get('x-content-type-options')
        );

        self::assertSame(
            'SAMEORIGIN',
            DI::getResponse()->headers->get('x-frame-options')
        );

        self::assertSame(
            'script-src \'unsafe-inline\' \'self\' https:',
            DI::getResponse()->headers->get('Content-Security-Policy')
        );
        
        unset($controller);
    }
    
    public function testSetContentSecurityPolicy(): void
    {
        $securityHeadersGenerator = SecurityHeadersGenerator::create()
            ->setContentSecurityPolicy(
                'default-src \'none\'',
                'script-src https://kiwa.io'
            )
        ;
        
        self::assertSame(
            'default-src \'none\'; script-src https://kiwa.io',
            $securityHeadersGenerator->getContentSecurityPolicy()
        );
    }
}
