<?php

/**
 * Kiwa. A feather-light web framework for professional static websites.
 *
 * @author Tobias Köngeter
 * @copyright Copyright © Bit&Black
 * @link https://www.bitandblack.com
 * @link https://www.kiwa.io
 * @license MIT
 */

namespace Kiwa\Tests\Config\Generator;

use Kiwa\Config\Generator\ConfigFile;
use Kiwa\Config\Generator\ConfigGenerator;
use Kiwa\Exception\Config\MethodNotAvailableException;
use PHPUnit\Framework\TestCase;
use ReflectionException;

/**
 * Class ConfigFileTest.
 *
 * @package Kiwa\Tests\Config\Generator
 */
class ConfigFileTest extends TestCase
{
    /**
     * @throws MethodNotAvailableException
     * @throws ReflectionException
     */
    public function testCanGenerate(): void
    {
        $configFile = new ConfigFile();

        $variable = $configFile->addVariableFromClass(ConfigGenerator::class);
        $variable->addChainedMethodCall('setMainURL', [
            'foo' => 'bar',
        ]);

        $configFile->addContentFragment($variable);
        $configFile->addReturn($variable, 'someMethodCall');
        
        $content = (string) $configFile;
        
        self::assertStringContainsString(
            'use Kiwa\Config\Generator\ConfigGenerator;',
            $content
        );

        self::assertStringContainsString(
            '$configGenerator = new ConfigGenerator();',
            $content
        );
        
        self::assertStringContainsString(
            '\'foo\' => \'bar\'',
            $content
        );
        
        self::assertStringContainsString(
            'return $configGenerator->someMethodCall();',
            $content
        );
    }
}
