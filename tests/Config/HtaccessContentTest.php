<?php

/**
 * Kiwa. A feather-light web framework for professional static websites.
 *
 * @author Tobias Köngeter
 * @copyright Copyright © Bit&Black
 * @link https://www.bitandblack.com
 * @link https://www.kiwa.io
 * @license MIT
 */

namespace Kiwa\Tests\Config;

use Kiwa\Config\HtaccessContent;
use PHPUnit\Framework\TestCase;
use ReflectionClass;

/**
 * Class HtaccessContentTest
 *
 * @package Kiwa\Tests\Config
 */
class HtaccessContentTest extends TestCase
{
    public function testCanMergeSettings(): void
    {
        $time = (string) time();
        
        $settings = [
            'useWWW' => true,
            'unknownSetting' => $time,
        ];

        $htaccessContentReflected = new ReflectionClass(HtaccessContent::class);
        $propertySettings = $htaccessContentReflected->getProperty('settings');

        $htaccessContent = new HtaccessContent($settings);

        $propertySettingsContent = $propertySettings->getValue($htaccessContent);

        self::assertIsArray(
            $propertySettingsContent
        );
        
        self::assertArrayHasKey(
            'useWWW',
            $propertySettingsContent
        );
        
        self::assertTrue(
            $propertySettingsContent['useWWW']
        );
        
        self::assertArrayHasKey(
            'unknownSetting',
            $propertySettingsContent
        );
        
        self::assertSame(
            $time,
            $propertySettingsContent['unknownSetting']
        );
    }

    public function testRedirectSettings1(): void
    {
        $settings = [
            'useWWW' => false,
            'useSSL' => false,
        ];

        $htaccessContent = new HtaccessContent($settings);
        $content = $htaccessContent->getContent();

        self::assertStringContainsString(
            'RewriteCond %{HTTPS} on',
            $content
        );

        self::assertStringContainsString(
            'RewriteRule .* http://%{HTTP_HOST}%{REQUEST_URI} [L,R=301]',
            $content
        );

        self::assertStringContainsString(
            'RewriteCond %{HTTP_HOST} ^www\.(.+)$ [NC]',
            $content
        );

        self::assertStringContainsString(
            'RewriteRule ^ http://%1%{REQUEST_URI} [L,R=301]',
            $content
        );
    }

    public function testRedirectSettings2(): void
    {
        $settings = [
            'useWWW' => true,
            'useSSL' => false,
        ];

        $htaccessContent = new HtaccessContent($settings);
        $content = $htaccessContent->getContent();

        self::assertStringContainsString(
            'RewriteCond %{HTTPS} on',
            $content
        );

        self::assertStringContainsString(
            'RewriteRule .* http://%{HTTP_HOST}%{REQUEST_URI} [L,R=301]',
            $content
        );

        self::assertStringContainsString(
            'RewriteCond %{HTTP_HOST} !^www\. [NC]',
            $content
        );

        self::assertStringContainsString(
            'RewriteRule .* http://www.%{HTTP_HOST}%{REQUEST_URI} [L,R=301]',
            $content
        );
    }

    public function testRedirectSettings3(): void
    {
        $settings = [
            'useWWW' => true,
            'useSSL' => true,
        ];

        $htaccessContent = new HtaccessContent($settings);
        $content = $htaccessContent->getContent();

        self::assertStringContainsString(
            'RewriteCond %{HTTPS} off',
            $content
        );

        self::assertStringContainsString(
            'RewriteRule .* https://%{HTTP_HOST}%{REQUEST_URI} [L,R=301]',
            $content
        );

        self::assertStringContainsString(
            'RewriteCond %{HTTP_HOST} !^www\. [NC]',
            $content
        );

        self::assertStringContainsString(
            'RewriteRule .* https://www.%{HTTP_HOST}%{REQUEST_URI} [L,R=301]',
            $content
        );
    }

    public function testRedirectSettings4(): void
    {
        $settings = [
            'useWWW' => false,
            'useSSL' => true,
        ];

        $htaccessContent = new HtaccessContent($settings);
        $content = $htaccessContent->getContent();

        self::assertStringContainsString(
            'RewriteCond %{HTTPS} off',
            $content
        );

        self::assertStringContainsString(
            'RewriteRule .* https://%{HTTP_HOST}%{REQUEST_URI} [L,R=301]',
            $content
        );

        self::assertStringContainsString(
            'RewriteCond %{HTTP_HOST} ^www\.(.+)$ [NC]',
            $content
        );

        self::assertStringContainsString(
            'RewriteRule ^ https://%1%{REQUEST_URI} [L,R=301]',
            $content
        );
    }
}
