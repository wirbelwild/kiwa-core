<?php

/**
 * Kiwa. A feather-light web framework for professional static websites.
 *
 * @author Tobias Köngeter
 * @copyright Copyright © Bit&Black
 * @link https://www.bitandblack.com
 * @link https://www.kiwa.io
 * @license MIT
 */

namespace Kiwa\Tests\Config;

use Kiwa\Config\RobotsTxtContent;
use PHPUnit\Framework\TestCase;
use ReflectionClass;

/**
 * Class RobotsTxtContentTest
 *
 * @package Kiwa\Tests\Config
 */
class RobotsTxtContentTest extends TestCase
{
    public function testCanMergeSettings(): void
    {
        $time = (string) time();
        
        $settings = [
            'allowAll' => false,
            'unknownSetting' => $time,
        ];
        
        $htaccessContentReflected = new ReflectionClass(RobotsTxtContent::class);
        $propertySettings = $htaccessContentReflected->getProperty('settings');
        $propertySettings->setAccessible(true);

        $propertySettingsContent = $propertySettings->getValue(
            new RobotsTxtContent($settings)
        );

        self::assertIsArray(
            $propertySettingsContent
        );

        self::assertArrayHasKey(
            'allowAll',
            $propertySettingsContent
        );

        self::assertFalse(
            $propertySettingsContent['allowAll']
        );

        self::assertArrayHasKey(
            'unknownSetting',
            $propertySettingsContent
        );

        self::assertSame(
            $time,
            $propertySettingsContent['unknownSetting']
        );
    }
}
