<?php

/**
 * Kiwa. A feather-light web framework for professional static websites.
 *
 * @author Tobias Köngeter
 * @copyright Copyright © Bit&Black
 * @link https://www.bitandblack.com
 * @link https://www.kiwa.io
 * @license MIT
 */

namespace Kiwa\Tests\Language;

use Kiwa\Config\Generator\ConfigGenerator;
use Kiwa\Config\Generator\PageGenerator;
use Kiwa\DI;
use Kiwa\Exception\Config\MisleadingConfigException;
use Kiwa\Exception\Config\MissingValueException;
use Kiwa\Frontend\Controller;
use Kiwa\Language\CurrentLanguageCode;
use Kiwa\Language\MetaLanguageCode;
use Kiwa\Path;
use Kiwa\Tests\Helper;
use PHPUnit\Framework\Attributes\PreserveGlobalState;
use PHPUnit\Framework\Attributes\RunInSeparateProcess;
use PHPUnit\Framework\TestCase;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class CurrentLanguageCodeTest.
 *
 * @package Kiwa\Tests\Language
 */
class CurrentLanguageCodeTest extends TestCase
{
    /**
     * Creates needed template folders.
     */
    public static function setUpBeforeClass(): void
    {
        Helper::setUpBefore();
    }

    /**
     * Removes template folders.
     */
    public static function tearDownAfterClass(): void
    {
        Helper::tearDownAfter();
    }

    /**
     * @throws MissingValueException
     * @throws MisleadingConfigException
     */
    #[PreserveGlobalState(false)]
    #[RunInSeparateProcess]
    public function testCanHandleLanguage(): void
    {
        $pages = [
            [
                'attrFirst' => 'de',
                'attrSecond' => 'subname',
                'attrThird' => 'test-page-de',
            ],
            [
                'attrFirst' => 'en',
                'attrSecond' => 'subname',
                'attrThird' => 'test-page-en',
            ],
        ];
        
        $languageFile = Path::getLanguageFolder() . DIRECTORY_SEPARATOR . $pages[0]['attrFirst'] . '.php';
        file_put_contents($languageFile, '<?php return []; ');

        $configFile = Path::getConfigFolder() . DIRECTORY_SEPARATOR . 'config.php';

        $config = new ConfigGenerator();
        $config
            ->setURLStructure('languageCode', 'name', 'subname')
            ->addPage(
                PageGenerator::create()
                ->setLanguageCode($pages[0]['attrFirst'])
                ->setName($pages[0]['attrThird'])
                ->setFile($pages[0]['attrThird'])
                ->setChildOf($pages[0]['attrSecond'])
            )
            ->addPage(
                PageGenerator::create()
                ->setLanguageCode($pages[1]['attrFirst'])
                ->setName($pages[1]['attrThird'])
                ->setFile($pages[0]['attrThird'])
                ->setChildOf($pages[1]['attrSecond'])
            )
            ->setPageSuffix(false)
        ;

        file_put_contents(
            $configFile,
            '<?php return ' . var_export($config->getConfig(), true) . ';'
        );

        $_SERVER['REQUEST_URI'] = '/' . $pages[0]['attrFirst'] . '/' . $pages[0]['attrSecond'] . '/' . $pages[0]['attrThird'];

        $controller = new Controller();

        $currentLanguageCode = (string) new CurrentLanguageCode();
        $metaLanguageCode = MetaLanguageCode::get();

        self::assertSame(
            $pages[0]['attrFirst'],
            $currentLanguageCode
        );

        self::assertSame(
            $pages[0]['attrFirst'],
            $metaLanguageCode
        );

        self::assertSame(
            $pages[0]['attrFirst'],
            DI::getSession()->get('kiwaLanguageCode')
        );

        unset($controller);
    }
    
    /**
     * @throws MissingValueException
     * @throws MisleadingConfigException
     */
    #[PreserveGlobalState(false)]
    #[RunInSeparateProcess]
    public function testCanHandleWrongLanguageCode(): void
    {
        $defaultLanguageCode = 'en';
        
        $pages = [
            [
                'attrFirst' => 'de',
                'attrSecond' => 'test-page-name',
                'attrThird' => 'test-page-subname',
            ],
        ];

        $languageFile = Path::getLanguageFolder() . DIRECTORY_SEPARATOR . $pages[0]['attrFirst'] . '.php';
        file_put_contents($languageFile, '<?php return []; ');

        $configFile = Path::getConfigFolder() . DIRECTORY_SEPARATOR . 'config.php';

        $config = new ConfigGenerator();
        $config
            ->setURLStructure('languageCode', 'name', 'subname')
            ->addPage(
                PageGenerator::create()
                ->setLanguageCode($pages[0]['attrFirst'])
                ->setName($pages[0]['attrThird'])
                ->setFile($pages[0]['attrThird'])
                ->setChildOf($pages[0]['attrSecond'])
            )
            ->setDefaultLanguageCode($defaultLanguageCode)
            ->setPageSuffix(false)
        ;

        file_put_contents(
            $configFile,
            '<?php return ' . var_export($config->getConfig(), true) . ';'
        );

        $_SERVER['REQUEST_URI'] = '/hello';

        $controller = new Controller();

        $currentLanguageCode = (string) new CurrentLanguageCode();
        $metaLanguageCode = MetaLanguageCode::get();

        self::assertSame(
            $defaultLanguageCode,
            $currentLanguageCode
        );

        self::assertSame(
            $defaultLanguageCode,
            $metaLanguageCode
        );

        self::assertSame(
            $defaultLanguageCode,
            DI::getSession()->get('kiwaLanguageCode')
        );

        self::assertSame(
            Response::HTTP_NOT_FOUND,
            DI::getResponse()->getStatusCode()
        );
        
        unset($controller);
    }
}
