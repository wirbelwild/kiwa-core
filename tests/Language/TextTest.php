<?php

/**
 * Kiwa. A feather-light web framework for professional static websites.
 *
 * @author Tobias Köngeter
 * @copyright Copyright © Bit&Black
 * @link https://www.bitandblack.com
 * @link https://www.kiwa.io
 * @license MIT
 */

namespace Kiwa\Tests\Language;

use Kiwa\Frontend\Controller;
use Kiwa\Language\CurrentLanguageCode;
use Kiwa\Language\Text;
use Kiwa\Path;
use Kiwa\Tests\Helper;
use PHPUnit\Framework\Attributes\PreserveGlobalState;
use PHPUnit\Framework\Attributes\RunInSeparateProcess;
use PHPUnit\Framework\TestCase;

/**
 * Class TextTest.
 *
 * @package Kiwa\Tests\Language
 */
class TextTest extends TestCase
{
    /**
     * Creates needed template folders.
     */
    public static function setUpBeforeClass(): void
    {
        Helper::setUpBefore();
    }

    /**
     * Removes template folders.
     */
    public static function tearDownAfterClass(): void
    {
        Helper::tearDownAfter();
    }

    #[PreserveGlobalState(false)]
    #[RunInSeparateProcess]
    public function testCanTranslate(): void
    {
        $sentenceEn = 'This is a {{variable}} translation';
        $sentenceDe = 'Dies ist eine {{variable}} Übersetzung';

        $languageValueEn = [
            $sentenceEn => $sentenceEn,
        ];

        $languageValueDe = [
            $sentenceEn => $sentenceDe,
        ];

        $languageFileEn = Path::getLanguageFolder() . DIRECTORY_SEPARATOR . 'en-test.php';

        file_put_contents(
            $languageFileEn,
            '<?php return ' . var_export($languageValueEn, true) . ';'
        );

        $languageFileDe = Path::getLanguageFolder() . DIRECTORY_SEPARATOR . 'de-test.php';
        
        file_put_contents(
            $languageFileDe,
            '<?php return ' . var_export($languageValueDe, true) . ';'
        );

        $controller = new \Kiwa\Connect\Controller();

        CurrentLanguageCode::setLanguageCode('de-test');

        self::assertSame(
            $sentenceDe,
            (string) new Text($sentenceEn)
        );

        unset($controller);
    }

    #[PreserveGlobalState(false)]
    #[RunInSeparateProcess]
    public function testCanHandleVariables(): void
    {
        $time = time();
        
        ob_start();
        $controller = new Controller();
        $htmlOutput = ob_get_clean();

        CurrentLanguageCode::setLanguageCode('de-test');
        
        self::assertSame(
            'This is a ' . $time . ' translation',
            (string) new Text('This is a {{variable}} translation', [
                'variable' => $time,
            ])
        );
        
        unset($controller, $htmlOutput);
    }

    #[PreserveGlobalState(false)]
    #[RunInSeparateProcess]
    public function testCanHandleLanguageDomains(): void
    {
        $root = 'This is a translation';
        $sentenceDomain1 = 'This is the domain1 translation';
        $sentenceDomain2 = 'This is the domain2 translation';

        $domain1 = [
            $root => $sentenceDomain1,
        ];

        $domain2 = [
            $root => $sentenceDomain2,
        ];

        $domain1File = Path::getLanguageFolder() . DIRECTORY_SEPARATOR . 'domain1.en.php';

        file_put_contents(
            $domain1File,
            '<?php return ' . var_export($domain1, true) . ';'
        );

        $domain2File = Path::getLanguageFolder() . DIRECTORY_SEPARATOR . 'domain2.en.php';

        file_put_contents(
            $domain2File,
            '<?php return ' . var_export($domain2, true) . ';'
        );

        ob_start();
        $controller = new Controller();
        $htmlOutput = ob_get_clean();

        CurrentLanguageCode::setLanguageCode('en');

        self::assertSame(
            $sentenceDomain1,
            (string) new Text($root, [], 'domain1')
        );

        self::assertSame(
            $sentenceDomain2,
            (string) new Text($root, [], 'domain2')
        );

        unset($controller, $htmlOutput);
    }
}
