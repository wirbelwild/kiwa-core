<?php

/**
 * Kiwa. A feather-light web framework for professional static websites.
 *
 * @author Tobias Köngeter
 * @copyright Copyright © Bit&Black
 * @link https://www.bitandblack.com
 * @link https://www.kiwa.io
 * @license MIT
 */

namespace Kiwa\Tests;

use Exception;
use Kiwa\Config\Generator\CacheControlGenerator;
use Kiwa\Config\Generator\ConfigGenerator;
use Kiwa\Config\Generator\PageGenerator;
use Kiwa\DI;
use Kiwa\Exception\Config\MissingValueException;
use Kiwa\Frontend\Controller;
use Kiwa\Path;
use PHPUnit\Framework\Attributes\PreserveGlobalState;
use PHPUnit\Framework\Attributes\RunInSeparateProcess;
use PHPUnit\Framework\TestCase;

/**
 * Class ResponseTest
 *
 * @package Kiwa\Tests
 */
class ResponseTest extends TestCase
{
    /**
     * Creates needed template folders.
     */
    public static function setUpBeforeClass(): void
    {
        Helper::setUpBefore();
    }

    /**
     * Removes template folders.
     */
    public static function tearDownAfterClass(): void
    {
        Helper::tearDownAfter();
    }

    #[PreserveGlobalState(false)]
    #[RunInSeparateProcess]
    public function testCanSetCookie(): void
    {
        $cookieName = 'TestCookie';
        $cookieValue = 'TestValue';

        $htmlFile = Path::getHTMLFolder() . DIRECTORY_SEPARATOR . 'index.phtml';
        $htmlValue = '<?php 

use Symfony\Component\HttpFoundation\Cookie;

$cookie = new Cookie(
    \'' . $cookieName . '\',
    \'' . $cookieValue . '\'
);

\Kiwa\DI::getInstance(\Symfony\Component\HttpFoundation\Response::class)->headers->setCookie($cookie); 
';

        $file = fopen($htmlFile, 'wb+');

        if (false === $file) {
            self::fail();
        }

        fwrite($file, $htmlValue);
        fclose($file);

        $controller = new Controller();

        $cookies = DI::getResponse()->headers->getCookies();

        self::assertIsArray($cookies);

        $cookie = $cookies[0];

        self::assertSame(
            $cookieName,
            $cookie->getName()
        );

        self::assertSame(
            $cookieValue,
            $cookie->getValue()
        );

        unset($controller);
    }

    /**
     * @throws MissingValueException
     * @throws Exception
     */
    #[PreserveGlobalState(false)]
    #[RunInSeparateProcess]
    public function testCanSetHandleCacheControl(): void
    {
        $maxAgeImmutableFiles = Helper::getRandomInt();
        $maxAgeAssetFiles = Helper::getRandomInt();
        $maxAgeUnknownFiles = Helper::getRandomInt();
        
        $fileName = 'test';

        $_SERVER['REQUEST_URI'] = '/' . $fileName . '.html';

        $tempAssetFile = Path::getHTMLFolder() . DIRECTORY_SEPARATOR . $fileName . '.phtml';
        $currentTime = microtime();

        file_put_contents(
            $tempAssetFile,
            $currentTime
        );

        $configFile = Path::getConfigFolder() . DIRECTORY_SEPARATOR . 'config.php';

        $config = new ConfigGenerator();
        $config
            ->setURLStructure('name', 'subname')
            ->addPage(
                PageGenerator::create()
                ->setLanguageCode('de')
                ->setName($fileName)
                ->setFile($fileName)
            )
            ->setPageSuffix('html')
            ->setCacheControl(
                CacheControlGenerator::create()
                ->setMaxAgeImmutableFiles($maxAgeImmutableFiles)
                ->setMaxAgeAssetFiles($maxAgeAssetFiles)
                ->setMaxAgeUnknownFiles($maxAgeUnknownFiles)
            )
        ;

        file_put_contents(
            $configFile,
            '<?php return ' . var_export($config->getConfig(), true) . ';'
        );

        $controller = new Controller();

        self::assertSame(
            $currentTime,
            (string) DI::getResponse()->getContent()
        );

        self::assertNull(
            DI::getResponse()->getMaxAge()
        );

        $lastModifiedTime = '';

        if (null !== $lastModified = DI::getResponse()->getLastModified()) {
            $lastModifiedTime = $lastModified->format('U');
        }

        self::assertStringContainsString(
            $lastModifiedTime,
            $currentTime
        );

        unset($controller);

        file_put_contents(
            Path::getBuildFolder() . DIRECTORY_SEPARATOR . 'manifest.json',
            json_encode([
                'build/template-primary.js' => '/build/template-primary.6484504c.js',
                'build/files/whitepaper.pdf' => '/build/files/whitepaper.pdf',
            ])
        );

        $fileName = 'template-primary.6484504c.js';

        $_SERVER['REQUEST_URI'] = '/build/javascripts/' . $fileName;

        $tempAssetFile = Path::getBuildFolder() . DIRECTORY_SEPARATOR . 'javascripts' . DIRECTORY_SEPARATOR . $fileName;
        $currentTime = microtime();

        file_put_contents(
            $tempAssetFile,
            $currentTime
        );

        $controller = new Controller();

        self::assertTrue(
            DI::getResponse()->isImmutable()
        );

        self::assertSame(
            $maxAgeImmutableFiles,
            DI::getResponse()->getMaxAge()
        );

        $lastModifiedTime = '';

        if (null !== $lastModified = DI::getResponse()->getLastModified()) {
            $lastModifiedTime = $lastModified->format('U');
        }

        self::assertStringContainsString(
            $lastModifiedTime,
            $currentTime
        );

        unset($controller);

        $fileName = 'whitepaper.pdf';

        $_SERVER['REQUEST_URI'] = '/build/files/' . $fileName;

        $tempAssetFile = Path::getBuildFolder() . DIRECTORY_SEPARATOR . 'files' . DIRECTORY_SEPARATOR . $fileName;
        $currentTime = microtime();

        file_put_contents(
            $tempAssetFile,
            $currentTime
        );

        $controller = new Controller();

        self::assertFalse(
            DI::getResponse()->isImmutable()
        );

        self::assertSame(
            $maxAgeAssetFiles,
            DI::getResponse()->getMaxAge()
        );

        $lastModifiedTime = '';

        if (null !== $lastModified = DI::getResponse()->getLastModified()) {
            $lastModifiedTime = $lastModified->format('U');
        }

        self::assertStringContainsString(
            $lastModifiedTime,
            $currentTime
        );

        unset($controller);
    }

    /**
     * @throws MissingValueException
     * @throws Exception
     */
    #[PreserveGlobalState(false)]
    #[RunInSeparateProcess]
    public function testPageCanAddCustomCacheControl(): void
    {
        $maxAge = Helper::getRandomInt();

        $fileName = 'test';

        $_SERVER['REQUEST_URI'] = '/' . $fileName . '.html';

        $pageFile = Path::getHTMLFolder() . DIRECTORY_SEPARATOR . $fileName . '.phtml';
        $currentTime = microtime();

        file_put_contents(
            $pageFile,
            '<?php \Kiwa\DI::getResponse()->setPrivate()->setMaxAge(' . $maxAge . '); ?>' . $currentTime
        );

        $configFile = Path::getConfigFolder() . DIRECTORY_SEPARATOR . 'config.php';

        $config = new ConfigGenerator();
        $config
            ->setURLStructure('name', 'subname')
            ->addPage(
                PageGenerator::create()
                ->setLanguageCode('de')
                ->setName($fileName)
                ->setFile($fileName)
            )
            ->setPageSuffix('html')
        ;

        file_put_contents(
            $configFile,
            '<?php return ' . var_export($config->getConfig(), true) . ';'
        );

        $controller = new Controller();

        self::assertSame(
            $currentTime,
            (string) DI::getResponse()->getContent()
        );

        self::assertSame(
            $maxAge,
            DI::getResponse()->getMaxAge()
        );

        self::assertFalse(
            DI::getResponse()->isCacheable()
        );
        
        unset($controller);
    }
}
