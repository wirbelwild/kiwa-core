<?php

/**
 * Kiwa. A feather-light web framework for professional static websites.
 *
 * @author Tobias Köngeter
 * @copyright Copyright © Bit&Black
 * @link https://www.bitandblack.com
 * @link https://www.kiwa.io
 * @license MIT
 */

namespace Kiwa\Tests;

use Kiwa\Config\Generator\ConfigGenerator;
use Kiwa\Config\Generator\PageGenerator;
use Kiwa\DI;
use Kiwa\Exception\Config\MisleadingConfigException;
use Kiwa\Exception\Config\MissingValueException;
use Kiwa\Frontend\Controller;
use Kiwa\Language\CurrentLanguageCode;
use Kiwa\Path;
use PHPUnit\Framework\Attributes\PreserveGlobalState;
use PHPUnit\Framework\Attributes\RunInSeparateProcess;
use PHPUnit\Framework\TestCase;

/**
 * Class URLTest.
 *
 * @package Kiwa\Tests
 */
class URLTest extends TestCase
{
    /**
     * Creates needed template folders.
     */
    public static function setUpBeforeClass(): void
    {
        Helper::setUpBefore();
    }

    /**
     * Removes template folders.
     */
    public static function tearDownAfterClass(): void
    {
        Helper::tearDownAfter();
    }

    /**
     * @throws MissingValueException
     * @throws MisleadingConfigException
     */
    #[PreserveGlobalState(false)]
    #[RunInSeparateProcess]
    public function testCanDifferPagesWithSameNames(): void
    {
        $pages = [
            [
                'attrFirst' => 'en',
                'attrSecond' => 'subname',
                'attrThird' => 'test-page',
            ],
            [
                'attrFirst' => 'de',
                'attrSecond' => 'subname',
                'attrThird' => 'test-page',
            ],
            [
                'attrFirst' => 'at',
                'attrSecond' => 'subname',
                'attrThird' => 'test-page',
            ],
        ];
        
        $_SERVER['REQUEST_URI'] = '/' . $pages[1]['attrFirst'] . '/' . $pages[1]['attrSecond'] . '/' . $pages[1]['attrThird'] . '.html';
        
        $configFile = Path::getConfigFolder() . DIRECTORY_SEPARATOR . 'config.php';

        $config = new ConfigGenerator();
        $config
            ->setURLStructure('languageCode', 'name', 'subname')
            ->addPage(
                PageGenerator::create()
                ->setLanguageCode($pages[0]['attrFirst'])
                ->setName($pages[0]['attrThird'])
                ->setFile($pages[0]['attrThird'])
                ->setChildOf($pages[0]['attrSecond'])
            )
            ->addPage(
                PageGenerator::create()
                ->setLanguageCode($pages[1]['attrFirst'])
                ->setName($pages[1]['attrThird'])
                ->setFile($pages[1]['attrThird'])
                ->setChildOf($pages[1]['attrSecond'])
            )
        ;

        file_put_contents(
            $configFile,
            '<?php return ' . var_export($config->getConfig(), true) . ';'
        );

        $languageFile = Path::getLanguageFolder() . DIRECTORY_SEPARATOR . $pages[1]['attrFirst'] . '.php';
        $hasCreatedTempLanguageFile = false;
        
        if (!file_exists($languageFile)) {
            file_put_contents($languageFile, '<?php return []; ');
            $hasCreatedTempLanguageFile = true;
        }
        
        $controller = new Controller();

        $currentLanguageCode = (string) new CurrentLanguageCode();

        self::assertSame(
            'de',
            $currentLanguageCode
        );

        
        if ($hasCreatedTempLanguageFile) {
            unlink($languageFile);
        }
        
        unset($controller);
    }

    /**
     * @throws MissingValueException
     */
    #[PreserveGlobalState(false)]
    #[RunInSeparateProcess]
    public function testCanHandleURLWithParameter1(): void
    {
        $fileName = 'test';

        $_SERVER['REQUEST_URI'] = '/' . $fileName . '?value=test';

        $tempAssetFile = Path::getHTMLFolder() . DIRECTORY_SEPARATOR . $fileName . '.phtml';

        $file1 = fopen($tempAssetFile, 'wb+');

        if (false === $file1) {
            self::assertIsResource($file1);
            return;
        }
        
        fwrite($file1, 'Success');
        fclose($file1);

        $configFile = Path::getConfigFolder() . DIRECTORY_SEPARATOR . 'config.php';

        $config = new ConfigGenerator();
        $config
            ->setURLStructure('name', 'subname')
            ->addPage(
                PageGenerator::create()
                ->setLanguageCode('de')
                ->setName($fileName)
                ->setFile($fileName)
            )
            ->setPageSuffix(false)
        ;

        file_put_contents(
            $configFile,
            '<?php return ' . var_export($config->getConfig(), true) . ';'
        );

        $controller = new Controller();
        $response = DI::getResponse()->getContent();

        self::assertSame(
            'Success',
            $response
        );

        unset($controller);
    }

    /**
     * @throws MissingValueException
     */
    #[PreserveGlobalState(false)]
    #[RunInSeparateProcess]
    public function testCanHandleURLWithParameter2(): void
    {
        $fileName = 'test';

        $_SERVER['REQUEST_URI'] = '/' . $fileName . '.html?value=test';

        $tempAssetFile = Path::getHTMLFolder() . DIRECTORY_SEPARATOR . $fileName . '.phtml';

        $file1 = fopen($tempAssetFile, 'wb+');

        if (false === $file1) {
            self::assertIsResource($file1);
            return;
        }
        
        fwrite($file1, 'Success');
        fclose($file1);

        $configFile = Path::getConfigFolder() . DIRECTORY_SEPARATOR . 'config.php';

        $config = new ConfigGenerator();
        $config
            ->setURLStructure('name', 'subname')
            ->addPage(
                PageGenerator::create()
                ->setLanguageCode('de')
                ->setName($fileName)
                ->setFile($fileName)
            )
            ->setPageSuffix('html')
        ;

        file_put_contents(
            $configFile,
            '<?php return ' . var_export($config->getConfig(), true) . ';'
        );

        $controller = new Controller();
        $response = DI::getResponse()->getContent();

        self::assertSame(
            'Success',
            $response
        );

        unset($controller);
    }

    /**
     * @throws MissingValueException
     * @throws MisleadingConfigException
     */
    #[PreserveGlobalState(false)]
    #[RunInSeparateProcess]
    public function testCanHandleURLWithParameter3(): void
    {
        $fileName = 'test';

        $_SERVER['REQUEST_URI'] = '/page1/' . $fileName . '?value=test';

        $tempAssetFile = Path::getHTMLFolder() . DIRECTORY_SEPARATOR . $fileName . '.phtml';

        $file1 = fopen($tempAssetFile, 'wb+');

        if (false === $file1) {
            self::assertIsResource($file1);
            return;
        }
        
        fwrite($file1, 'Success');
        fclose($file1);

        $configFile = Path::getConfigFolder() . DIRECTORY_SEPARATOR . 'config.php';

        $config = new ConfigGenerator();
        $config
            ->setURLStructure('name', 'subname')
            ->addPage(
                PageGenerator::create()
                ->setLanguageCode('de')
                ->setName($fileName)
                ->setFile($fileName)
                ->setChildOf('page1')
            )
            ->setPageSuffix(false)
        ;

        file_put_contents(
            $configFile,
            '<?php return ' . var_export($config->getConfig(), true) . ';'
        );

        $controller = new Controller();
        $response = DI::getResponse()->getContent();

        self::assertSame(
            'Success',
            $response
        );

        unset($controller);
    }

    /**
     * @throws MissingValueException
     * @throws MisleadingConfigException
     */
    #[PreserveGlobalState(false)]
    #[RunInSeparateProcess]
    public function testCanHandleURLWithParameter4(): void
    {
        $fileName = 'test';

        $_SERVER['REQUEST_URI'] = '/page1/' . $fileName . '.html?value=test';

        $tempAssetFile = Path::getHTMLFolder() . DIRECTORY_SEPARATOR . $fileName . '.phtml';

        $file1 = fopen($tempAssetFile, 'wb+');

        if (false === $file1) {
            self::assertIsResource($file1);
            return;
        }
        
        fwrite($file1, 'Success');
        fclose($file1);

        $configFile = Path::getConfigFolder() . DIRECTORY_SEPARATOR . 'config.php';

        $config = new ConfigGenerator();
        $config
            ->setURLStructure('name', 'subname')
            ->addPage(
                PageGenerator::create()
                ->setLanguageCode('de')
                ->setName($fileName)
                ->setFile($fileName)
                ->setChildOf('page1')
            )
            ->setPageSuffix('html')
        ;

        file_put_contents(
            $configFile,
            '<?php return ' . var_export($config->getConfig(), true) . ';'
        );

        $controller = new Controller();
        $response = DI::getResponse()->getContent();

        self::assertSame(
            'Success',
            $response
        );

        unset($controller);
    }
}
