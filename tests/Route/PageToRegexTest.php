<?php

/**
 * Kiwa. A feather-light web framework for professional static websites.
 *
 * @author Tobias Köngeter
 * @copyright Copyright © Bit&Black
 * @link https://www.bitandblack.com
 * @link https://www.kiwa.io
 * @license MIT
 */

namespace Kiwa\Tests\Route;

use Generator;
use Kiwa\Config\Page;
use Kiwa\Route\PageToRegex;
use PHPUnit\Framework\Attributes\DataProvider;
use PHPUnit\Framework\TestCase;

class PageToRegexTest extends TestCase
{
    /**
     * @return void
     */
    #[DataProvider('getGetRegexData')]
    public function testGetRegex(Page $page, string $regexExpected): void
    {
        $regex = PageToRegex::getRegex($page);

        self::assertSame(
            $regexExpected,
            $regex
        );
    }

    public static function getGetRegexData(): Generator
    {
        yield [
            new Page([
                'languageCode' => 'A',
                'name' => 'B',
            ]),
            '/a/b',
        ];

        yield [
            new Page([
                'languageCode' => 'A',
                'name' => 'B',
                'childOf' => 'C',
            ]),
            '/a/c/b',
        ];

        yield [
            new Page([
                'languageCode' => 'A',
                'name' => 'B',
                'hasDynamicChild' => 'C',
            ]),
            '/a/b',
        ];
    }
}
