<?php

/**
 * Kiwa. A feather-light web framework for professional static websites.
 *
 * @author Tobias Köngeter
 * @copyright Copyright © Bit&Black
 * @link https://www.bitandblack.com
 * @link https://www.kiwa.io
 * @license MIT
 */

namespace Kiwa\Tests\Route;

use ArrayObject;
use Kiwa\Config\Page;
use Kiwa\Page\PageList;
use Kiwa\Route\PageListWithRegex;
use Kiwa\URL\URLBuilder;
use PHPUnit\Framework\TestCase;
use ReflectionClass;

class PageListWithRegexTest extends TestCase
{
    public function testGetPageList(): void
    {
        $reflectionClass = new ReflectionClass(URLBuilder::class);
        $reflectionClass->setStaticPropertyValue('urlStructure', [
            0 => 'languageCode',
            1 => 'name',
            2 => 'subname',
        ]);

        $pageList = new PageList();
        $reflectionClass = new ReflectionClass($pageList);

        $pages = new ArrayObject([
            new Page([
                'languageCode' => 'de',
                'name' => 'index',
            ]),
            new Page([
                'languageCode' => 'de',
                'name' => 'blog',
                'hasDynamicChild' => 'blog-children',
            ]),
            new Page([
                'languageCode' => 'de',
                'name' => 'blog-children',
                'childOf' => 'blog',
            ]),
            new Page([
                'languageCode' => 'de',
                'name' => 'imprint',
            ]),
            new Page([
                'languageCode' => 'de',
                'name' => 'privacy-policy',
                'childOf' => 'imprint',
            ]),
        ]);

        $reflectionClass->setStaticPropertyValue('pages', $pages);

        self::assertCount(
            5,
            $pageList
        );

        $pageListWithRegex = new PageListWithRegex($pageList);
        $pageListRegexed = $pageListWithRegex->getPageList();

        $routes = array_keys($pageListRegexed);

        self::assertCount(
            5,
            $routes
        );

        self::assertSame(
            [
                '/de',
                '/de/blog',
                '/de/blog/{slug}',
                '/de/imprint',
                '/de/imprint/privacy-policy',
            ],
            $routes
        );
    }
}
