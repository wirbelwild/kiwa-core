<?php

/**
 * Kiwa. A feather-light web framework for professional static websites.
 *
 * @author Tobias Köngeter
 * @copyright Copyright © Bit&Black
 * @link https://www.bitandblack.com
 * @link https://www.kiwa.io
 * @license MIT
 */

namespace Kiwa\Tests;

use Kiwa\Config\Generator\ConfigGenerator;
use Kiwa\Config\Generator\PageGenerator;
use Kiwa\DI;
use Kiwa\Exception\Config\MissingValueException;
use Kiwa\Frontend\Controller;
use Kiwa\Path;
use PHPUnit\Framework\Attributes\PreserveGlobalState;
use PHPUnit\Framework\Attributes\RunInSeparateProcess;
use PHPUnit\Framework\TestCase;
use Symfony\Component\Yaml\Yaml;

/**
 * Class ConfigTest
 */
class ConfigTest extends TestCase
{
    /**
     * Creates needed template folders.
     */
    public static function setUpBeforeClass(): void
    {
        Helper::setUpBefore();
    }

    /**
     * Removes template folders.
     */
    public static function tearDownAfterClass(): void
    {
        Helper::tearDownAfter();
    }

    #[PreserveGlobalState(false)]
    #[RunInSeparateProcess]
    public function testCanReadEnvFile(): void
    {
        $_SERVER['REQUEST_URI'] = '/';
        $url1 = 'http://www.test1.de';
        $url2 = 'http://www.test2.de';

        $envFile = Path::getRootFolder() . DIRECTORY_SEPARATOR . '.env.local';
        $envContent = 'mainURL=' . $url1 . PHP_EOL .
            'useWWW=wwwValueEnv' . PHP_EOL .
            'useSSL=sslValueEnv' . PHP_EOL
        ;

        $env = fopen($envFile, 'wb+');

        if (false === $env) {
            self::assertIsResource($env);
            return;
        }

        fwrite($env, $envContent);
        fclose($env);

        $configFile = Path::getConfigFolder() . DIRECTORY_SEPARATOR . 'config.yaml';
        $configValue = [
            'mainURL' => $url2,
            'htaccess' => [
                'useWWW' => 'wwwValueConfig',
                'useSSL' => 'sslValueConfig',
            ],
        ];

        file_put_contents(
            $configFile,
            Yaml::dump($configValue, 5)
        );

        $controller = new Controller();

        self::assertSame(
            $url1,
            DI::getConfig()->getValue('mainURL')
        );

        /** @var array<string, string|bool> $htaccess */
        $htaccess = DI::getConfig()->getValue('htaccess');
        
        self::assertSame(
            'wwwValueEnv',
            $htaccess['useWWW']
        );

        self::assertSame(
            'sslValueEnv',
            $htaccess['useSSL']
        );

        unset($controller);
    }

    #[PreserveGlobalState(false)]
    #[RunInSeparateProcess]
    public function testCanHandleEnvFiles(): void
    {
        $_SERVER['REQUEST_URI'] = '/';
        $url1 = 'http://www.test1.de';
        $url2 = 'http://www.test2.de';
        $url3 = 'http://www.test3.de';

        $envFile1 = Path::getRootFolder() . DIRECTORY_SEPARATOR . '.env';
        $envContent1 = 'mainURL=' . $url1 . PHP_EOL .
            'useWWW=wwwValueEnv1' . PHP_EOL .
            'useSSL=sslValueEnv1' . PHP_EOL
        ;

        $env = fopen($envFile1, 'wb+');

        if (false === $env) {
            self::assertIsResource($env);
            return;
        }

        fwrite($env, $envContent1);
        fclose($env);

        $envFile2 = Path::getRootFolder() . DIRECTORY_SEPARATOR . '.env.local';
        $envContent2 = 'mainURL=' . $url2 . PHP_EOL .
            'useWWW=wwwValueEnv2' . PHP_EOL
        ;

        $env = fopen($envFile2, 'wb+');

        if (false === $env) {
            self::assertIsResource($env);
            return;
        }

        fwrite($env, $envContent2);
        fclose($env);

        $configFile = Path::getConfigFolder() . DIRECTORY_SEPARATOR . 'config.yaml';
        $configValue = [
            'mainURL' => $url3,
            'htaccess' => [
                'useWWW' => 'wwwValueConfig',
                'useSSL' => 'sslValueConfig',
            ],
        ];

        file_put_contents(
            $configFile,
            Yaml::dump($configValue, 5)
        );

        $controller = new Controller();

        self::assertSame(
            $url2,
            DI::getConfig()->getValue('mainURL')
        );

        /** @var array<string, string|bool> $htaccess */
        $htaccess = DI::getConfig()->getValue('htaccess');
        
        self::assertSame(
            'wwwValueEnv2',
            $htaccess['useWWW']
        );

        self::assertSame(
            'sslValueEnv1',
            $htaccess['useSSL']
        );

        unset($controller);
    }

    /**
     * @throws MissingValueException
     */
    #[PreserveGlobalState(false)]
    #[RunInSeparateProcess]
    public function testCanRedirectWithURLParameter(): void
    {
        $attrFirst = 'contact';

        $_SERVER['REQUEST_URI'] = '/' . $attrFirst;

        $configFile = Path::getConfigFolder() . DIRECTORY_SEPARATOR . 'config.php';

        $config = new ConfigGenerator();
        $config
            ->setURLStructure('name', 'subname')
            ->setPageSuffix('html')
            ->addPage(
                PageGenerator::create()
                ->setLanguageCode('en')
                ->setName($attrFirst)
                ->setFile($attrFirst)
            )
        ;

        file_put_contents(
            $configFile,
            '<?php return ' . var_export($config->getConfig(), true) . ';'
        );

        $htmlFile = Path::getHTMLFolder() . DIRECTORY_SEPARATOR . $attrFirst . '.phtml';
        $htmlValue = 'Hello World!';

        file_put_contents($htmlFile, $htmlValue);
        
        $controller = new Controller();
        
        self::assertStringContainsString(
            '<meta http-equiv="refresh" content="0;url=\'/' . $attrFirst . '.html\'" />',
            (string) DI::getResponse()->getContent()
        );

        self::assertSame(
            301,
            DI::getResponse()->getStatusCode()
        );

        unset($controller);
        
        $attrFirst = 'contact';

        $_SERVER['REQUEST_URI'] = '/' . $attrFirst . '?foo=bar&baz';

        $controller = new Controller();
        
        self::assertStringContainsString(
            '<meta http-equiv="refresh" content="0;url=\'/' . $attrFirst . '.html?foo=bar&amp;baz\'" />',
            (string) DI::getResponse()->getContent()
        );

        self::assertSame(
            301,
            DI::getResponse()->getStatusCode()
        );
        
        unset($controller);
    }

    #[PreserveGlobalState(false)]
    #[RunInSeparateProcess]
    public function testCanHandleTestEnvFiles1(): void
    {
        $envFile = Path::getRootFolder() . DIRECTORY_SEPARATOR . '.env';
        $envContent = 'APP_ENV=prod' . PHP_EOL . 'VALUE_1=true';
        file_put_contents($envFile, $envContent);

        $envFile = Path::getRootFolder() . DIRECTORY_SEPARATOR . '.env.local';
        $envContent = 'APP_ENV=dev' . PHP_EOL . 'VALUE_2=true';
        file_put_contents($envFile, $envContent);

        $envFile = Path::getRootFolder() . DIRECTORY_SEPARATOR . '.env.test';
        $envContent = 'APP_ENV=test1' . PHP_EOL . 'VALUE_3=true';
        file_put_contents($envFile, $envContent);

        $envFile = Path::getRootFolder() . DIRECTORY_SEPARATOR . '.env.test.local';
        $envContent = 'APP_ENV=test2' . PHP_EOL . 'VALUE_4=true';
        file_put_contents($envFile, $envContent);

        $controller = new Controller();
        unset($controller);

        $config = DI::getConfig()->getAllValues();

        self::assertSame(
            'test2',
            $config['APP_ENV']
        );

        self::assertArrayHasKey(
            'VALUE_1',
            $config
        );

        self::assertArrayHasKey(
            'VALUE_2',
            $config
        );

        self::assertArrayHasKey(
            'VALUE_3',
            $config
        );

        self::assertArrayHasKey(
            'VALUE_4',
            $config
        );
    }

    #[PreserveGlobalState(false)]
    #[RunInSeparateProcess]
    public function testCanHandleTestEnvFiles2(): void
    {
        $envFile = Path::getRootFolder() . DIRECTORY_SEPARATOR . '.env';
        $envContent = 'APP_ENV=prod' . PHP_EOL . 'VALUE_1=true';
        file_put_contents($envFile, $envContent);

        $envFile = Path::getRootFolder() . DIRECTORY_SEPARATOR . '.env.local';
        $envContent = 'APP_ENV=dev' . PHP_EOL . 'VALUE_2=true';
        file_put_contents($envFile, $envContent);

        $envFile = Path::getRootFolder() . DIRECTORY_SEPARATOR . '.env.test';
        $envContent = 'APP_ENV=test1' . PHP_EOL . 'VALUE_3=true';
        file_put_contents($envFile, $envContent);

        $envFile = Path::getRootFolder() . DIRECTORY_SEPARATOR . '.env.test.local';
        $envContent = 'APP_ENV=test2' . PHP_EOL . 'VALUE_4=true';
        file_put_contents($envFile, $envContent);

        $_ENV['TEST_MODE'] = 'disabled';
        
        $controller = new Controller();
        unset($controller);

        $config = DI::getConfig()->getAllValues();

        self::assertSame(
            'dev',
            $config['APP_ENV']
        );

        self::assertArrayHasKey(
            'VALUE_1',
            $config
        );

        self::assertArrayHasKey(
            'VALUE_2',
            $config
        );

        self::assertArrayNotHasKey(
            'VALUE_3',
            $config
        );

        self::assertArrayNotHasKey(
            'VALUE_4',
            $config
        );
    }
}
