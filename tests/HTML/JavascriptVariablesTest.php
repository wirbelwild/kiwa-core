<?php

/**
 * Kiwa. A feather-light web framework for professional static websites.
 *
 * @author Tobias Köngeter
 * @copyright Copyright © Bit&Black
 * @link https://www.bitandblack.com
 * @link https://www.kiwa.io
 * @license MIT
 */

namespace Kiwa\Tests\HTML;

use Kiwa\Connect\Controller;
use Kiwa\HTML\JavascriptVariables;
use Kiwa\Tests\Helper;
use PHPUnit\Framework\Attributes\PreserveGlobalState;
use PHPUnit\Framework\Attributes\RunInSeparateProcess;
use PHPUnit\Framework\TestCase;

/**
 * Class JavascriptVariablesTest.
 *
 * @package Kiwa\Tests\HTML
 */
class JavascriptVariablesTest extends TestCase
{
    /**
     * Creates needed template folders.
     */
    public static function setUpBeforeClass(): void
    {
        Helper::setUpBefore();
    }

    /**
     * Removes template folders.
     */
    public static function tearDownAfterClass(): void
    {
        Helper::tearDownAfter();
    }

    #[PreserveGlobalState(false)]
    #[RunInSeparateProcess]
    public function testCanSetUp(): void
    {
        $controller = new Controller();
        $javascriptVariables = new JavascriptVariables();

        self::assertStringContainsString(
            'window.environment',
            (string) $javascriptVariables
        );

        self::assertStringContainsString(
            'pageUID: "page-index"',
            (string) $javascriptVariables
        );
        
        unset($controller);
    }
}
