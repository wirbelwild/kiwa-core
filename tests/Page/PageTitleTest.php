<?php

/**
 * Kiwa. A feather-light web framework for professional static websites.
 *
 * @author Tobias Köngeter
 * @copyright Copyright © Bit&Black
 * @link https://www.bitandblack.com
 * @link https://www.kiwa.io
 * @license MIT
 */

namespace Kiwa\Tests\Page;

use Generator;
use Kiwa\Config\Generator\ConfigGenerator;
use Kiwa\Config\Generator\PageGenerator;
use Kiwa\Exception\Config\MissingValueException;
use Kiwa\Frontend\Controller;
use Kiwa\Page\PageTitle;
use Kiwa\Path;
use Kiwa\Tests\Helper;
use PHPUnit\Framework\Attributes\DataProvider;
use PHPUnit\Framework\Attributes\PreserveGlobalState;
use PHPUnit\Framework\Attributes\RunInSeparateProcess;
use PHPUnit\Framework\TestCase;

/**
 * Class PageTitleTest.
 *
 * @package Kiwa\Tests\Page
 */
class PageTitleTest extends TestCase
{
    /**
     * Creates needed template folders.
     */
    public static function setUpBeforeClass(): void
    {
        Helper::setUpBefore();
    }

    /**
     * Removes template folders.
     */
    public static function tearDownAfterClass(): void
    {
        Helper::tearDownAfter();
    }

    /**
     * @throws MissingValueException
     */
    #[PreserveGlobalState(false)]
    #[RunInSeparateProcess]
    public function testCanHandlePageTitle(): void
    {
        $attrFirst = null;

        $_SERVER['REQUEST_URI'] = $attrFirst;

        $configFile = Path::getConfigFolder() . DIRECTORY_SEPARATOR . 'config.php';

        $config = new ConfigGenerator();
        $config
            ->setURLStructure('name', 'subname')
            ->addPage(
                PageGenerator::create()
                ->setLanguageCode('de')
                ->setName('/')
                ->setFile('index')
                ->setPageTitle('Welcome to Kiwa')
            )
        ;

        file_put_contents(
            $configFile,
            '<?php return ' . var_export($config->getConfig(), true) . ';'
        );

        $htmlFile = Path::getHTMLFolder() . DIRECTORY_SEPARATOR . 'index.phtml';
        $htmlValue = 'Hello World!';

        file_put_contents($htmlFile, $htmlValue);

        $controller = new Controller();
        
        self::assertSame(
            'Welcome to Kiwa',
            (string) new PageTitle()
        );
        
        unset($controller);
    }

    #[PreserveGlobalState(false)]
    #[RunInSeparateProcess]
    public function testCanAddDefaultTitleToIndex(): void
    {
        $attrFirst = null;

        $_SERVER['REQUEST_URI'] = $attrFirst;

        $configFile = Path::getConfigFolder() . DIRECTORY_SEPARATOR . 'config.php';

        $config = new ConfigGenerator();
        $config
            ->setMainURL('http://www.example.com')
            ->setDefaultOGTitle('en', 'title-default-en')
            ->setDefaultOGTitle('de', 'title-default-de')
        ;

        file_put_contents(
            $configFile,
            '<?php return ' . var_export($config->getConfig(), true) . ';'
        );

        $htmlFile = Path::getHTMLFolder() . DIRECTORY_SEPARATOR . 'index.phtml';
        $htmlValue = 'Hello World!';

        file_put_contents(
            $htmlFile,
            $htmlValue
        );

        $controller = new Controller();

        $pageTitle = (string) new PageTitle();

        self::assertSame(
            'title-default-en',
            $pageTitle
        );

        unset($controller);
    }

    #[DataProvider('getCanHandleSpecialCharsData')]
    #[PreserveGlobalState(false)]
    #[RunInSeparateProcess]
    public function testCanHandleSpecialChars(string $input, string $outputExpected): void
    {
        PageTitle::setTitle($input);

        $pageTitle = new PageTitle();

        self::assertSame(
            $outputExpected,
            $pageTitle->getTitle()
        );
    }

    public static function getCanHandleSpecialCharsData(): Generator
    {
        yield [
            'Print & Digital Convention',
            'Print &amp; Digital Convention',
        ];

        yield [
            'Print &amp; Digital Convention',
            'Print &amp; Digital Convention',
        ];

        yield [
            '&äéìÖüß–',
            '&amp;äéìÖüß–',
        ];

        yield [
            '&amp;&auml;&szlig;&mdash;',
            '&amp;äß—',
        ];
    }
}
