<?php

/**
 * Kiwa. A feather-light web framework for professional static websites.
 *
 * @author Tobias Köngeter
 * @copyright Copyright © Bit&Black
 * @link https://www.bitandblack.com
 * @link https://www.kiwa.io
 * @license MIT
 */

namespace Kiwa\Tests\Page;

use Kiwa\Config\Generator\ConfigGenerator;
use Kiwa\Config\Generator\PageGenerator;
use Kiwa\Exception\Config\MissingValueException;
use Kiwa\Frontend\Controller;
use Kiwa\Page\PageList;
use Kiwa\Path;
use Kiwa\Tests\Helper;
use PHPUnit\Framework\Attributes\PreserveGlobalState;
use PHPUnit\Framework\Attributes\RunInSeparateProcess;
use PHPUnit\Framework\TestCase;

/**
 * Class PageListTest
 *
 * @package Kiwa\Tests\Page
 */
class PageListTest extends TestCase
{
    /**
     * Creates needed template folders.
     */
    public static function setUpBeforeClass(): void
    {
        Helper::setUpBefore();
    }

    /**
     * Removes template folders.
     */
    public static function tearDownAfterClass(): void
    {
        Helper::tearDownAfter();
    }

    /**
     * @throws MissingValueException
     */
    #[PreserveGlobalState(false)]
    #[RunInSeparateProcess]
    public function testCanHandlePages(): void
    {
        $configFile = Path::getConfigFolder() . DIRECTORY_SEPARATOR . 'config.php';

        $config = new ConfigGenerator();
        $config
            ->setURLStructure('name', 'subname')
            ->addPage(
                PageGenerator::create()
                ->setLanguageCode('de')
                ->setName('/')
                ->setFile('index')
            )
        ;

        file_put_contents(
            $configFile,
            '<?php return ' . var_export($config->getConfig(), true) . ';'
        );
        
        $controller = new Controller();
        
        $pageList = new PageList();
        $pages = $pageList
            ->hasLanguageCode('de')
        ;

        self::assertCount(
            1,
            $pages
        );

        unset($controller);
    }
}
