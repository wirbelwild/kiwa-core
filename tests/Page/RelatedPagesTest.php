<?php

/**
 * Kiwa. A feather-light web framework for professional static websites.
 *
 * @author Tobias Köngeter
 * @copyright Copyright © Bit&Black
 * @link https://www.bitandblack.com
 * @link https://www.kiwa.io
 * @license MIT
 */

namespace Kiwa\Tests\Page;

use Exception;
use Kiwa\Config\Generator\ConfigGenerator;
use Kiwa\Config\Generator\PageGenerator;
use Kiwa\Exception\Config\MisleadingConfigException;
use Kiwa\Exception\Config\MissingValueException;
use Kiwa\Frontend\Controller;
use Kiwa\Page\RelatedPages;
use Kiwa\Path;
use Kiwa\Tests\Helper;
use Kiwa\URL\URLBuilder;
use PHPUnit\Framework\Attributes\PreserveGlobalState;
use PHPUnit\Framework\Attributes\RunInSeparateProcess;
use PHPUnit\Framework\TestCase;

/**
 * Class RelatedPagesTest.
 *
 * @package Kiwa\Tests\Page
 */
class RelatedPagesTest extends TestCase
{
    /**
     * Creates needed template folders.
     */
    public static function setUpBeforeClass(): void
    {
        Helper::setUpBefore();
    }

    /**
     * Removes template folders.
     */
    public static function tearDownAfterClass(): void
    {
        Helper::tearDownAfter();
    }

    /**
     * @throws MisleadingConfigException
     * @throws MissingValueException
     */
    #[PreserveGlobalState(false)]
    #[RunInSeparateProcess]
    public function testCanHandleDynamicChildren1(): void
    {
        $this->createDynChildConfig();
        
        $_SERVER['REQUEST_URI'] = '/de/test-de-2.html';
        
        $controller = new Controller();

        /**
         * @var array<int, array{
         *     languageCode: string|null,
         *     name: string|null,
         *     subname: string|null,
         * }> $pages
         */
        $pages = RelatedPages::getRelatedPages();
        
        self::assertCount(
            2,
            $pages
        );

        $urls = array_map(
            static fn (array $page) => (string) new URLBuilder([
                'languageCode' => $page['languageCode'] ?? null,
                'name' => $page['name'] ?? null,
                'subname' => $page['subname'] ?? null,
            ]),
            $pages
        );
        
        self::assertSame(
            [
                'de' => '/de/test-de-2.html',
                'en' => '/en/test-en-2.html',
            ],
            $urls
        );
        
        unset($controller);
    }

    /**
     * @throws MisleadingConfigException
     * @throws MissingValueException
     */
    #[PreserveGlobalState(false)]
    #[RunInSeparateProcess]
    public function testCanHandleDynamicChildren2(): void
    {
        $this->createDynChildConfig();

        $_SERVER['REQUEST_URI'] = '/de/test-de-1.html';

        $controller = new Controller();

        /**
         * @var array<int, array{
         *     languageCode: string|null,
         *     name: string|null,
         *     subname: string|null,
         * }> $pages
         */
        $pages = RelatedPages::getRelatedPages();
        
        self::assertCount(
            2,
            $pages
        );

        $urls = array_map(
            static fn (array $page) => (string) new URLBuilder([
                'languageCode' => $page['languageCode'] ?? null,
                'name' => $page['name'] ?? null,
                'subname' => $page['subname'] ?? null,
            ]),
            $pages
        );

        self::assertSame(
            [
                'de' => '/de/test-de-1.html',
                'en' => '/en/test-en-1.html',
            ],
            $urls
        );
        
        unset($controller);
    }

    /**
     * @throws MisleadingConfigException
     * @throws MissingValueException
     * @throws Exception
     */
    #[PreserveGlobalState(false)]
    #[RunInSeparateProcess]
    public function testCanHandleDynamicChildren3(): void
    {
        $this->createDynChildConfig();

        $randomString = Helper::getRandomString();

        $_SERVER['REQUEST_URI'] = '/de/test-de-1/' . $randomString . '.html';

        $controller = new Controller();

        /**
         * @var array<int, array{
         *     languageCode: string|null,
         *     name: string|null,
         *     subname: string|null,
         * }> $pages
         */
        $pages = RelatedPages::getRelatedPages();
        
        self::assertCount(
            1,
            $pages
        );

        $urls = array_map(
            static fn (array $page) => (string) new URLBuilder([
                'languageCode' => $page['languageCode'] ?? null,
                'name' => $page['name'] ?? null,
                'subname' => $page['subname'] ?? null,
            ]),
            $pages
        );

        self::assertSame(
            [
                'de' => '/de/test-de-1/' . $randomString . '.html',
            ],
            $urls
        );
        
        unset($controller);
    }

    /**
     * @throws MisleadingConfigException
     * @throws MissingValueException
     */
    #[PreserveGlobalState(false)]
    #[RunInSeparateProcess]
    private function createDynChildConfig(): void
    {
        $configFile = Path::getConfigFolder() . DIRECTORY_SEPARATOR . 'config.php';

        $config = new ConfigGenerator();
        $config
            ->setURLStructure('languageCode', 'name', 'subname')
            ->addPage(
                PageGenerator::create()
                ->setLanguageCode('de')
                ->setName('test-de-1')
                ->setFile('file1')
                ->setDynamicChild('test-child-de-1')
            )
            ->addPage(
                PageGenerator::create()
                ->setLanguageCode('en')
                ->setName('test-en-1')
                ->setFile('file1')
                ->setDynamicChild('file3')
            )
            ->addPage(
                PageGenerator::create()
                ->setLanguageCode('de')
                ->setName('test-de-2')
                ->setFile('file2')
                ->setDynamicChild('file4')
            )
            ->addPage(
                PageGenerator::create()
                ->setLanguageCode('en')
                ->setName('test-en-2')
                ->setFile('file2')
                ->setDynamicChild('file4')
            )
            ->addPage(
                PageGenerator::create()
                ->setLanguageCode('de')
                ->setName('test-child-de-1')
                ->setFile('file3')
                ->setChildOf('test-de-1')
            )
            ->addPage(
                PageGenerator::create()
                ->setLanguageCode('en')
                ->setName('test-child-en-1')
                ->setFile('file3')
                ->setChildOf('test-en-1')
            )
            ->addPage(
                PageGenerator::create()
                ->setLanguageCode('de')
                ->setName('test-child-de-2')
                ->setFile('file4')
                ->setChildOf('test-de-2')
            )
            ->addPage(
                PageGenerator::create()
                ->setLanguageCode('en')
                ->setName('test-child-en-2')
                ->setFile('file4')
                ->setChildOf('test-en-2')
            )
        ;

        file_put_contents(
            $configFile,
            '<?php return ' . var_export($config->getConfig(), true) . ';'
        );
    }

    /**
     * @throws MissingValueException
     */
    #[PreserveGlobalState(false)]
    #[RunInSeparateProcess]
    public function testCanHandleVersionsWithoutURLParameter(): void
    {
        $configFile = Path::getConfigFolder() . DIRECTORY_SEPARATOR . 'config.php';

        $config = new ConfigGenerator();
        $config
            ->setURLStructure('name', 'subname')
            ->addPage(
                PageGenerator::create()
                ->setLanguageCode('de')
                ->setName('test-de-1')
                ->setFile('file1')
            )
            ->addPage(
                PageGenerator::create()
                ->setLanguageCode('en')
                ->setName('test-en-1')
                ->setFile('file1')
            )
            ->addPage(
                PageGenerator::create()
                ->setLanguageCode('en')
                ->setName('test-en-2')
                ->setFile('file2')
            )
        ;

        file_put_contents(
            $configFile,
            '<?php return ' . var_export($config->getConfig(), true) . ';'
        );

        $_SERVER['REQUEST_URI'] = '/test-de-1.html';

        $controller = new Controller();

        /**
         * @var array<string, array{
         *     languageCode: string|null,
         *     name: string|null,
         *     subname: string|null,
         * }> $relatedPages
         */
        $relatedPages = RelatedPages::getRelatedPages();
        
        self::assertCount(
            2,
            $relatedPages
        );

        self::assertSame(
            'test-de-1',
            $relatedPages['de']['name'] ?? null
        );

        self::assertSame(
            'test-en-1',
            $relatedPages['en']['name'] ?? null
        );

        unset($controller);
    }
}
