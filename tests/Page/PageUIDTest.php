<?php

/**
 * Kiwa. A feather-light web framework for professional static websites.
 *
 * @author Tobias Köngeter
 * @copyright Copyright © Bit&Black
 * @link https://www.bitandblack.com
 * @link https://www.kiwa.io
 * @license MIT
 */

namespace Kiwa\Tests\Page;

use Iterator;
use Kiwa\Config\Generator\ConfigGenerator;
use Kiwa\Config\Generator\PageGenerator;
use Kiwa\Exception\Config\MisleadingConfigException;
use Kiwa\Exception\Config\MissingValueException;
use Kiwa\Frontend\Controller;
use Kiwa\Page\PageUID;
use Kiwa\Path;
use Kiwa\Tests\Helper;
use PHPUnit\Framework\Attributes\DataProvider;
use PHPUnit\Framework\Attributes\PreserveGlobalState;
use PHPUnit\Framework\Attributes\RunInSeparateProcess;
use PHPUnit\Framework\TestCase;

/**
 * Class PageUIDTest.
 *
 * @package Kiwa\Tests
 */
class PageUIDTest extends TestCase
{
    /**
     * Creates needed template folders.
     *
     * @throws MissingValueException
     * @throws MisleadingConfigException
     */
    public static function setUpBeforeClass(): void
    {
        Helper::setUpBefore();

        $configFile = Path::getConfigFolder() . DIRECTORY_SEPARATOR . 'config.php';

        $config = new ConfigGenerator();
        $config
            ->setURLStructure('name', 'subname')
            ->addPage(
                PageGenerator::create()
                ->setLanguageCode('en')
                ->setName('page1en')
                ->setFile('page1file')
            )
            ->addPage(
                PageGenerator::create()
                ->setLanguageCode('en')
                ->setName('page1ensubpage2en')
                ->setFile('page2file')
                ->setChildOf('page1')
            )
            ->addPage(
                PageGenerator::create()
                ->setLanguageCode('de')
                ->setName('page1de')
                ->setFile('page1file')
            )
            ->addPage(
                PageGenerator::create()
                ->setLanguageCode('de')
                ->setName('page1desubpage2de')
                ->setFile('page2file')
                ->setChildOf('page1')
            )
            ->addPage(
                PageGenerator::create()
                ->setLanguageCode('de')
                ->setName('page3de')
                ->setFile('page3file')
                ->setDynamicChild('page4file')
            )
        ;

        file_put_contents(
            $configFile,
            '<?php return ' . var_export($config->getConfig(), true) . ';'
        );

        $htmlFile1 = Path::getHTMLFolder() . DIRECTORY_SEPARATOR . 'page1.phtml';
        $htmlValue = 'Hello World!';

        $file = fopen($htmlFile1, 'wb+');

        if (false === $file) {
            self::fail();
        }

        fwrite($file, $htmlValue);
        fclose($file);

        $htmlFile2 = Path::getHTMLFolder() . DIRECTORY_SEPARATOR . 'page2.phtml';
        $htmlValue = 'Hello World!';

        $file = fopen($htmlFile2, 'wb+');

        if (false === $file) {
            self::fail();
        }

        fwrite($file, $htmlValue);
        fclose($file);
    }

    /**
     * Removes template folders.
     */
    public static function tearDownAfterClass(): void
    {
        Helper::tearDownAfter();
    }

    /**
     * @param string $uri
     * @param string $pageName
     */
    #[DataProvider('provide')]
    #[PreserveGlobalState(false)]
    #[RunInSeparateProcess]
    public function testCanHandlePageUID(string $uri, string $pageName): void
    {
        $_SERVER['REQUEST_URI'] = $uri;

        $controller = new Controller();
        
        self::assertSame(
            $pageName,
            (string) new PageUID()
        );

        unset($controller);
    }

    /**
     * @return Iterator<int, array<int, string>>
     */
    public static function provide(): Iterator
    {
        yield ['/page1en.html', 'page-page1file'];
        yield ['/page1de.html', 'page-page1file'];
        yield ['/page1ensubpage2en.html', 'page-page2file'];
        yield ['/page1desubpage2de.html', 'page-page2file'];
        yield ['/page3de/dynamicpage.html', 'page-page4file'];
    }
}
