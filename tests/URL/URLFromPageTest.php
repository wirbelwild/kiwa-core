<?php

/**
 * Kiwa. A feather-light web framework for professional static websites.
 *
 * @author Tobias Köngeter
 * @copyright Copyright © Bit&Black
 * @link https://www.bitandblack.com
 * @link https://www.kiwa.io
 * @license MIT
 */

namespace Kiwa\Tests\URL;

use Generator;
use Kiwa\Config\Page;
use Kiwa\URL\URLBuilder;
use Kiwa\URL\URLFromPage;
use PHPUnit\Framework\Attributes\DataProvider;
use PHPUnit\Framework\TestCase;
use ReflectionClass;

class URLFromPageTest extends TestCase
{
    /**
     * @return void
     */
    #[DataProvider('getGetURLData')]
    public function testGetURL(Page $page, string $urlExpected): void
    {
        $reflectionClass = new ReflectionClass(URLBuilder::class);
        $reflectionClass->setStaticPropertyValue('urlStructure', [
            'languageCode',
            'name',
            'subname',
        ]);

        $urlFromPage = new URLFromPage($page);

        self::assertSame(
            $urlExpected,
            $urlFromPage->getURL()
        );
    }

    public static function getGetURLData(): Generator
    {
        yield [
            new Page([
                'languageCode' => 'de',
                'name' => 'index',
            ]),
            '/de',
        ];

        yield [
            new Page([
                'languageCode' => 'de',
                'name' => 'blog',
                'hasDynamicChild' => 'blog-children',
            ]),
            '/de/blog',
        ];

        yield [
            new Page([
                'languageCode' => 'de',
                'name' => 'blog-children',
                'childOf' => 'blog',
            ]),
            '/de/blog/blog-children',
        ];
    }
}
