<?php

/**
 * Kiwa. A feather-light web framework for professional static websites.
 *
 * @author Tobias Köngeter
 * @copyright Copyright © Bit&Black
 * @link https://www.bitandblack.com
 * @link https://www.kiwa.io
 * @license MIT
 */

namespace Kiwa\Tests\URL;

use Generator;
use Kiwa\Config\Generator\ConfigGenerator;
use Kiwa\Config\Generator\PageGenerator;
use Kiwa\DI;
use Kiwa\Exception\Config\MissingValueException;
use Kiwa\Frontend\Controller;
use Kiwa\Path;
use Kiwa\Tests\Helper;
use Kiwa\URL\URLBuilder;
use PHPUnit\Framework\Attributes\DataProvider;
use PHPUnit\Framework\Attributes\PreserveGlobalState;
use PHPUnit\Framework\Attributes\RunInSeparateProcess;
use PHPUnit\Framework\TestCase;

/**
 * Class URLBuilderTest
 *
 * @package Kiwa\Tests\Config
 */
class URLBuilderTest extends TestCase
{
    /**
     * Creates needed template folders.
     */
    public static function setUpBeforeClass(): void
    {
        Helper::setUpBefore();
    }

    /**
     * Removes template folders.
     */
    public static function tearDownAfterClass(): void
    {
        Helper::tearDownAfter();
    }

    #[PreserveGlobalState(false)]
    #[RunInSeparateProcess]
    public function testCanSetup(): void
    {
        $attrFirst = 'de';
        $attrSecond = 'contact';
        $attrThird = 'imprint.html';

        $_SERVER['REQUEST_URI'] = '/' . $attrFirst . '/' . $attrSecond . '/' . $attrThird;

        $configFile = Path::getConfigFolder() . DIRECTORY_SEPARATOR . 'config.php';

        $config = new ConfigGenerator();
        $config
            ->setURLStructure('languageCode', 'name', 'subname')
            ->setPageSuffix('html')
        ;

        file_put_contents(
            $configFile,
            '<?php return ' . var_export($config->getConfig(), true) . ';'
        );

        $controller = new Controller();
        
        self::assertSame(
            '/de/about-us/contact.html',
            (string) new URLBuilder([
                'languageCode' => 'de',
                'name' => 'about-us',
                'subname' => 'contact',
            ])
        );

        self::assertSame(
            '/first/second/third.html',
            (string) new URLBuilder(['first', 'second', 'third'])
        );

        self::assertSame(
            '/file.js',
            (string) new URLBuilder(['file.js'])
        );

        self::assertSame(
            '/file.js',
            (string) new URLBuilder(['/file.js'])
        );

        unset($controller);
    }

    #[PreserveGlobalState(false)]
    #[RunInSeparateProcess]
    public function testGeneratesURLForJavascript(): void
    {
        $fileName = 'test.js';
        $fileFolder = 'javascripts';

        $_SERVER['REQUEST_URI'] = '/' . $fileFolder . '/' . $fileName;

        $tempAssetFile = Path::getBuildFolder() . DIRECTORY_SEPARATOR . $fileFolder . DIRECTORY_SEPARATOR . $fileName;
        $currentTime = microtime();

        $file = fopen($tempAssetFile, 'wb+');

        if (false === $file) {
            self::fail();
        }

        fwrite($file, $currentTime);
        fclose($file);

        $controller = new Controller();

        self::assertSame(
            '/' . $fileFolder . '/' . $fileName,
            (string) new URLBuilder([
                'name' => $fileFolder,
                'subname' => $fileName,
            ], true)
        );

        self::assertSame(
            'http://www.localhost/' . $fileFolder . '/' . $fileName,
            (string) new URLBuilder([
                'name' => $fileFolder,
                'subname' => $fileName,
            ], false)
        );

        unset($controller);
    }

    #[PreserveGlobalState(false)]
    #[RunInSeparateProcess]
    public function testGeneratesURLForFile(): void
    {
        $fileName = 'test.txt';
        $fileFolder = 'files';

        $_SERVER['REQUEST_URI'] = '/' . $fileFolder . '/' . $fileName;

        $tempAssetFile = Path::getBuildFolder() . DIRECTORY_SEPARATOR . $fileFolder . DIRECTORY_SEPARATOR . $fileName;
        $currentTime = microtime();

        $file = fopen($tempAssetFile, 'wb+');

        if (false === $file) {
            self::fail();
        }

        fwrite($file, $currentTime);
        fclose($file);

        $controller = new Controller();

        self::assertSame(
            '/' . $fileFolder . '/' . $fileName,
            (string) new URLBuilder([
                'name' => $fileFolder,
                'subname' => $fileName,
            ], true)
        );

        self::assertSame(
            'http://www.localhost/' . $fileFolder . '/' . $fileName,
            (string) new URLBuilder([
                'name' => $fileFolder,
                'subname' => $fileName,
            ], false)
        );

        unset($controller);
    }

    #[PreserveGlobalState(false)]
    #[RunInSeparateProcess]
    public function testGeneratesURLForPageLevel1(): void
    {
        $attrFirst = 'contact';

        $_SERVER['REQUEST_URI'] = '/' . $attrFirst . '.html';

        $controller = new Controller();

        self::assertSame(
            '/' . $attrFirst . '.html',
            (string) new URLBuilder([
                'name' => $attrFirst,
            ], true)
        );

        self::assertSame(
            'http://www.localhost/' . $attrFirst . '.html',
            (string) new URLBuilder([
                'name' => $attrFirst,
            ], false)
        );

        unset($controller);
    }

    #[PreserveGlobalState(false)]
    #[RunInSeparateProcess]
    public function testGeneratesURLForPageLevel2(): void
    {
        $attrFirst = 'contact';
        $attrSecond = 'imprint';

        $_SERVER['REQUEST_URI'] = '/' . $attrFirst . '/' . $attrSecond . '.html';

        $controller = new Controller();

        self::assertSame(
            '/' . $attrFirst . '/' . $attrSecond . '.html',
            (string) new URLBuilder([
                'name' => $attrFirst,
                'subname' => $attrSecond,
            ], true)
        );

        self::assertSame(
            'http://www.localhost/' . $attrFirst . '/' . $attrSecond . '.html',
            (string) new URLBuilder([
                'name' => $attrFirst,
                'subname' => $attrSecond,
            ], false)
        );

        unset($controller);
    }

    /**
     * @throws MissingValueException
     */
    #[PreserveGlobalState(false)]
    #[RunInSeparateProcess]
    public function testGeneratesURLForPageLevel3(): void
    {
        $attrFirst = 'de';
        $attrSecond = 'contact';
        $attrThird = 'imprint.html';

        $_SERVER['REQUEST_URI'] = '/' . $attrFirst . '/' . $attrSecond . '/' . $attrThird;

        $configFile = Path::getConfigFolder() . DIRECTORY_SEPARATOR . 'config.php';

        $config = new ConfigGenerator();
        $config
            ->setURLStructure('languageCode', 'name', 'subname')
            ->addPage(
                PageGenerator::create()
                ->setLanguageCode('de')
                ->setName('/')
                ->setFile('index')
            )
            ->enableWWW()
        ;

        file_put_contents(
            $configFile,
            '<?php return ' . var_export($config->getConfig(), true) . ';'
        );
        
        $controller = new Controller();

        self::assertSame(
            '/' . $attrFirst . '/' . $attrSecond . '/' . $attrThird,
            (string) new URLBuilder([
                'languageCode' => $attrFirst,
                'name' => $attrSecond,
                'subname' => $attrThird,
            ], true)
        );

        self::assertSame(
            'http://www.localhost/' . $attrFirst . '/' . $attrSecond . '/' . $attrThird,
            (string) new URLBuilder([
                'languageCode' => $attrFirst,
                'name' => $attrSecond,
                'subname' => $attrThird,
            ], false)
        );

        unset($controller);
    }

    #[PreserveGlobalState(false)]
    #[RunInSeparateProcess]
    public function testAppendsExtensionWhenMissing(): void
    {
        $attrFirst = 'contact';

        $_SERVER['REQUEST_URI'] = '/' . $attrFirst;

        $controller = new Controller();

        self::assertStringContainsString(
            '<meta http-equiv="refresh" content="0;url=\'/' . $attrFirst . '.html\'" />',
            (string) DI::getResponse()->getContent()
        );

        self::assertSame(
            301,
            DI::getResponse()->getStatusCode()
        );

        self::assertSame(
            '/' . $attrFirst . '.html',
            (string) new URLBuilder([
                'name' => $attrFirst,
            ], true)
        );

        self::assertSame(
            'http://www.localhost/' . $attrFirst . '.html',
            (string) new URLBuilder([
                'name' => $attrFirst,
            ], false)
        );

        unset($controller);
    }
    
    #[PreserveGlobalState(false)]
    #[RunInSeparateProcess]
    public function testGeneratesURLForIndexPage(): void
    {
        $attrFirst = null;

        $_SERVER['REQUEST_URI'] = $attrFirst;

        $controller = new Controller();

        $url = (string) new URLBuilder([
            'name' => $attrFirst,
        ], false);
        
        self::assertSame(
            'http://www.localhost',
            $url
        );

        unset($controller);
    }

    public static function getGeneratesURLForIndexHavingDashData(): Generator
    {
        yield [
            true,
            '/',
            null,
        ];

        yield [
            true,
            '/',
            'de',
        ];

        yield [
            false,
            'http://www.localhost',
            null,
        ];

        yield [
            false,
            'http://www.localhost',
            'de',
        ];
    }

    #[DataProvider('getGeneratesURLForIndexHavingDashData')]
    #[PreserveGlobalState(false)]
    #[RunInSeparateProcess]
    public function testGeneratesURLForIndexHavingDash(bool $pathRelative, string $urlExpected, ?string $languageCode): void
    {
        $controller = new Controller();

        $url = (string) new URLBuilder([
            'languageCode' => $languageCode,
            'name' => '/',
        ], $pathRelative);

        self::assertSame(
            $urlExpected,
            $url
        );

        unset($controller);
    }

    /**
     * @throws MissingValueException
     */
    #[PreserveGlobalState(false)]
    #[RunInSeparateProcess]
    public function testCanHandleIndexWithoutLanguage(): void
    {
        $attrFirst = null;

        $_SERVER['REQUEST_URI'] = $attrFirst;

        $configFile = Path::getConfigFolder() . DIRECTORY_SEPARATOR . 'config.php';

        $config = new ConfigGenerator();
        $config
            ->setURLStructure('name', 'subname')
            ->addPage(
                PageGenerator::create()
                ->setLanguageCode('de')
                ->setName('/')
                ->setFile('index')
            )
            ->enableWWW()
            ->setPageSuffix('html')
            ->setMainURL('http://www.test.de')
        ;

        file_put_contents(
            $configFile,
            '<?php return ' . var_export($config->getConfig(), true) . ';'
        );

        $controller = new Controller();

        self::assertSame(
            '/',
            (string) new URLBuilder([
                'languageCode' => null,
                'name' => null,
                'subname' => null,
            ], true)
        );

        self::assertSame(
            'http://www.test.de',
            (string) new URLBuilder([
                'languageCode' => null,
                'name' => null,
                'subname' => null,
            ], false)
        );

        unset($controller);
    }

    /**
     * @throws MissingValueException
     */
    #[PreserveGlobalState(false)]
    #[RunInSeparateProcess]
    public function testCanHandleIndexWithLanguage(): void
    {
        $attrFirst = null;

        $_SERVER['REQUEST_URI'] = $attrFirst;

        $configFile = Path::getConfigFolder() . DIRECTORY_SEPARATOR . 'config.php';

        $config = new ConfigGenerator();
        $config
            ->setURLStructure('languageCode', 'name', 'subname')
            ->addPage(
                PageGenerator::create()
                ->setLanguageCode('de')
                ->setName('/')
                ->setFile('index')
            )
            ->enableWWW()
            ->setPageSuffix('html')
            ->setMainURL('http://www.test.de')
        ;

        file_put_contents(
            $configFile,
            '<?php return ' . var_export($config->getConfig(), true) . ';'
        );

        $controller = new Controller();

        self::assertStringContainsString(
            'Redirecting',
            (string) DI::getResponse()->getContent()
        );

        self::assertSame(
            '/',
            (string) new URLBuilder([
                'languageCode' => null,
                'name' => null,
                'subname' => null,
            ], true)
        );

        self::assertSame(
            'http://www.test.de',
            (string) new URLBuilder([
                'languageCode' => null,
                'name' => null,
                'subname' => null,
            ], false)
        );

        self::assertSame(
            '/',
            (string) new URLBuilder([
                'languageCode' => null,
                'name' => 'index',
                'subname' => null,
            ], true)
        );

        self::assertSame(
            'http://www.test.de',
            (string) new URLBuilder([
                'languageCode' => null,
                'name' => 'index',
                'subname' => null,
            ], false)
        );
        unset($controller);
    }
}
