<?php

/**
 * Kiwa. A feather-light web framework for professional static websites.
 *
 * @author Tobias Köngeter
 * @copyright Copyright © Bit&Black
 * @link https://www.bitandblack.com
 * @link https://www.kiwa.io
 * @license MIT
 */

namespace Kiwa\Tests\URL;

use Kiwa\Config\Generator\ConfigGenerator;
use Kiwa\Config\Generator\PageGenerator;
use Kiwa\Exception\Config\MisleadingConfigException;
use Kiwa\Exception\Config\MissingValueException;
use Kiwa\Frontend\Controller;
use Kiwa\Path;
use Kiwa\Tests\Helper;
use Kiwa\URL\URLFromFile;
use PHPUnit\Framework\Attributes\PreserveGlobalState;
use PHPUnit\Framework\Attributes\RunInSeparateProcess;
use PHPUnit\Framework\TestCase;

/**
 * Class URLFromFileTest.
 *
 * @package Kiwa\Tests\URL
 */
class URLFromFileTest extends TestCase
{
    /**
     * Creates needed template folders.
     */
    public static function setUpBeforeClass(): void
    {
        Helper::setUpBefore();
    }

    /**
     * Removes template folders.
     */
    public static function tearDownAfterClass(): void
    {
        Helper::tearDownAfter();
    }

    /**
     * @throws MissingValueException
     * @throws MisleadingConfigException
     */
    #[PreserveGlobalState(false)]
    #[RunInSeparateProcess]
    public function testCanCreateURLsFromFilesWithLanguageCode(): void
    {
        $attrFirst = 'de';
        $attrSecond = 'page1-name';
        $attrThird = 'page2-subname';

        $_SERVER['REQUEST_URI'] = '/' . $attrFirst . '/' . $attrSecond . '/' . $attrThird . '.html';

        $configFile = Path::getConfigFolder() . DIRECTORY_SEPARATOR . 'config.php';

        $config = new ConfigGenerator();
        $config
            ->setURLStructure('languageCode', 'name', 'subname')
            ->addPage(
                PageGenerator::create()
                ->setLanguageCode($attrFirst)
                ->setName($attrThird)
                ->setFile($attrThird)
                ->setChildOf($attrSecond)
            )
            ->addPage(
                PageGenerator::create()
                ->setLanguageCode($attrFirst)
                ->setName($attrSecond)
                ->setFile($attrSecond)
            )
            ->enableWWW()
        ;

        file_put_contents(
            $configFile,
            '<?php return ' . var_export($config->getConfig(), true) . ';'
        );

        $controller = new Controller();

        self::assertSame(
            '/' . $attrFirst . '/' . $attrSecond . '.html',
            (string) new URLFromFile($attrSecond, true)
        );

        self::assertSame(
            '/' . $attrFirst . '/' . $attrSecond . '/' . $attrThird . '.html',
            (string) new URLFromFile($attrThird, true)
        );

        self::assertSame(
            'http://www.localhost/' . $attrFirst . '/' . $attrSecond . '/' . $attrThird . '.html',
            (string) new URLFromFile($attrThird, false)
        );

        self::assertSame(
            '/',
            (string) new URLFromFile('index', true)
        );

        self::assertSame(
            'http://www.localhost',
            (string) new URLFromFile('index', false)
        );

        unset($controller);
    }

    /**
     * @throws MissingValueException
     * @throws MisleadingConfigException
     */
    #[PreserveGlobalState(false)]
    #[RunInSeparateProcess]
    public function testCanCreateURLsFromFilesWithoutLanguageCode(): void
    {
        $attrFirst = 'page1-name';
        $attrSecond = 'page2-subname';

        $_SERVER['REQUEST_URI'] = '/' . $attrFirst . '/' . $attrSecond . '.html';

        $configFile = Path::getConfigFolder() . DIRECTORY_SEPARATOR . 'config.php';

        $config = new ConfigGenerator();
        $config
            ->setURLStructure('name', 'subname')
            ->addPage(
                PageGenerator::create()
                ->setLanguageCode($attrFirst)
                ->setName($attrSecond)
                ->setFile($attrSecond)
                ->setChildOf($attrFirst)
            )
            ->addPage(
                PageGenerator::create()
                ->setLanguageCode($attrFirst)
                ->setName($attrFirst)
                ->setFile($attrFirst)
            )
            ->enableWWW()
        ;

        file_put_contents(
            $configFile,
            '<?php return ' . var_export($config->getConfig(), true) . ';'
        );

        $controller = new Controller();

        self::assertSame(
            '/' . $attrFirst . '.html',
            (string) new URLFromFile($attrFirst, true)
        );

        self::assertSame(
            '/' . $attrFirst . '/' . $attrSecond . '.html',
            (string) new URLFromFile($attrSecond, true)
        );

        self::assertSame(
            'http://www.localhost/' . $attrFirst . '/' . $attrSecond . '.html',
            (string) new URLFromFile($attrSecond, false)
        );

        self::assertSame(
            '/',
            (string) new URLFromFile('index', true)
        );

        self::assertSame(
            'http://www.localhost',
            (string) new URLFromFile('index', false)
        );

        unset($controller);
    }

    /**
     * @throws MissingValueException
     * @throws MisleadingConfigException
     */
    #[PreserveGlobalState(false)]
    #[RunInSeparateProcess]
    public function testCanCreateAbsoluteURL(): void
    {
        $attrFirst = 'de';
        $attrSecond = 'subname';
        $attrThird = 'test-page';

        $_SERVER['REQUEST_URI'] = '/' . $attrFirst . '/' . $attrSecond . '/' . $attrThird . '.html';

        $configFile = Path::getConfigFolder() . DIRECTORY_SEPARATOR . 'config.php';

        $config = new ConfigGenerator();
        $config
            ->setURLStructure('languageCode', 'name', 'subname')
            ->addPage(
                PageGenerator::create()
                ->setLanguageCode($attrFirst)
                ->setName($attrThird)
                ->setFile($attrThird)
                ->setChildOf($attrSecond)
            )
            ->enableWWW()
        ;

        file_put_contents(
            $configFile,
            '<?php return ' . var_export($config->getConfig(), true) . ';'
        );

        $controller = new Controller();

        $absoluteURL = (string) new URLFromFile('test-page', false);

        self::assertSame(
            'http://www.localhost/' . $attrFirst . '/' . $attrSecond . '/' . $attrThird . '.html',
            $absoluteURL
        );

        unset($controller);
    }

    /**
     * @throws MissingValueException
     * @throws MisleadingConfigException
     */
    #[PreserveGlobalState(false)]
    #[RunInSeparateProcess]
    public function testCanCreateAbsoluteURLOnSubdomain(): void
    {
        $attrFirst = 'de';
        $attrSecond = 'subname';
        $attrThird = 'test-page';

        $_SERVER['REQUEST_URI'] = '/' . $attrFirst . '/' . $attrSecond . '/' . $attrThird . '.html';

        $configFile = Path::getConfigFolder() . DIRECTORY_SEPARATOR . 'config.php';

        $config = new ConfigGenerator();
        $config
            ->setURLStructure('languageCode', 'name', 'subname')
            ->addPage(
                PageGenerator::create()
                ->setLanguageCode($attrFirst)
                ->setName($attrThird)
                ->setFile($attrThird)
                ->setChildOf($attrSecond)
            )
            ->enableWWW()
        ;

        file_put_contents(
            $configFile,
            '<?php return ' . var_export($config->getConfig(), true) . ';'
        );

        $controller = new Controller();

        $absoluteURL = (string) new URLFromFile('test-page', false);

        self::assertSame(
            'http://www.localhost/' . $attrFirst . '/' . $attrSecond . '/' . $attrThird . '.html',
            $absoluteURL
        );

        unset($controller);
    }

    /**
     * @throws MissingValueException
     * @throws MisleadingConfigException
     */
    #[PreserveGlobalState(false)]
    #[RunInSeparateProcess]
    public function testCanCreateMultipleInstances(): void
    {
        $attrFirst = 'page';
        $attrSecond = 'subpage';

        $_SERVER['REQUEST_URI'] = '/' . $attrFirst . '/' . $attrSecond . '.html';

        $configFile = Path::getConfigFolder() . DIRECTORY_SEPARATOR . 'config.php';

        $config = new ConfigGenerator();
        $config
            ->setURLStructure('name', 'subname')
            ->addPage(
                PageGenerator::create()
                ->setLanguageCode('de')
                ->setName($attrFirst)
                ->setFile($attrFirst)
            )
            ->addPage(
                PageGenerator::create()
                ->setLanguageCode('de')
                ->setName($attrSecond)
                ->setFile($attrSecond)
                ->setChildOf($attrFirst)
            )
        ;

        file_put_contents(
            $configFile,
            '<?php return ' . var_export($config->getConfig(), true) . ';'
        );

        $controller = new Controller();

        $url1 = new URLFromFile('page');
        $url2 = new URLFromFile('subpage');

        self::assertSame(
            '/page/subpage.html',
            (string) $url2
        );

        self::assertSame(
            '/page.html',
            (string) $url1
        );
        
        unset($controller);
    }
}
