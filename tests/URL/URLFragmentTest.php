<?php

/**
 * Kiwa. A feather-light web framework for professional static websites.
 *
 * @author Tobias Köngeter
 * @copyright Copyright © Bit&Black
 * @link https://www.bitandblack.com
 * @link https://www.kiwa.io
 * @license MIT
 */

namespace Kiwa\Tests\URL;

use Kiwa\Config\Generator\ConfigGenerator;
use Kiwa\Config\Generator\PageGenerator;
use Kiwa\Exception\Config\MisleadingConfigException;
use Kiwa\Exception\Config\MissingValueException;
use Kiwa\Frontend\Controller;
use Kiwa\Path;
use Kiwa\Tests\Helper;
use Kiwa\URL\URLFragment;
use PHPUnit\Framework\Attributes\PreserveGlobalState;
use PHPUnit\Framework\Attributes\RunInSeparateProcess;
use PHPUnit\Framework\TestCase;

/**
 * Class URLFragmentTest.
 *
 * @package Kiwa\Tests\URL
 */
class URLFragmentTest extends TestCase
{
    /**
     * Creates needed template folders.
     */
    public static function setUpBeforeClass(): void
    {
        Helper::setUpBefore();
    }

    /**
     * Removes template folders.
     */
    public static function tearDownAfterClass(): void
    {
        Helper::tearDownAfter();
    }

    /**
     * @throws MissingValueException
     * @throws MisleadingConfigException
     */
    #[PreserveGlobalState(false)]
    #[RunInSeparateProcess]
    public function testCanHandleSpecialChars(): void
    {
        $attrFirst = '1äÄß';
        $attrSecond = '2öÖß';
        $attrThird = '3üÜß';

        $_SERVER['REQUEST_URI'] = '/' . $attrFirst . '/' . $attrSecond . '/' . $attrThird . '.html';

        $configFile = Path::getConfigFolder() . DIRECTORY_SEPARATOR . 'config.php';

        $config = new ConfigGenerator();
        $config
            ->setURLStructure('languageCode', 'name', 'subname')
            ->addPage(
                PageGenerator::create()
                ->setLanguageCode($attrFirst)
                ->setName($attrThird)
                ->setFile($attrThird)
                ->setChildOf($attrSecond)
            )
            ->enableWWW()
        ;

        file_put_contents(
            $configFile,
            '<?php return ' . var_export($config->getConfig(), true) . ';'
        );
        
        $controller = new Controller();

        self::assertSame(
            $attrFirst,
            (string) new URLFragment('languageCode')
        );

        self::assertSame(
            $attrSecond,
            (string) new URLFragment('name')
        );

        self::assertSame(
            $attrThird,
            (string) new URLFragment('subname')
        );

        unset($controller);
    }
}
