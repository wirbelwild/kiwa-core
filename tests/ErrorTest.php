<?php

/**
 * Kiwa. A feather-light web framework for professional static websites.
 *
 * @author Tobias Köngeter
 * @copyright Copyright © Bit&Black
 * @link https://www.bitandblack.com
 * @link https://www.kiwa.io
 * @license MIT
 */

namespace Kiwa\Tests;

use Kiwa\Config\Generator\ConfigGenerator;
use Kiwa\Config\Generator\PageGenerator;
use Kiwa\DI;
use Kiwa\Exception\Config\MissingValueException;
use Kiwa\Frontend\Controller;
use Kiwa\Path;
use PHPUnit\Framework\Attributes\PreserveGlobalState;
use PHPUnit\Framework\Attributes\RunInSeparateProcess;
use PHPUnit\Framework\TestCase;

/**
 * Class ErrorTest
 *
 * @package Kiwa\Tests
 */
class ErrorTest extends TestCase
{
    /**
     * Creates needed template folders.
     */
    public static function setUpBeforeClass(): void
    {
        Helper::setUpBefore();
    }

    /**
     * Removes template folders.
     */
    public static function tearDownAfterClass(): void
    {
        Helper::tearDownAfter();
    }

    /**
     * @throws MissingValueException
     */
    #[PreserveGlobalState(false)]
    #[RunInSeparateProcess]
    public function testCanHandleErrorInDevMode(): void
    {
        /**
         * The TEST_MODE variable causes errors to be printed in the CLI which is needed to run tests correctly.
         * For this test, it is changed because Kiwa needs to catch the error by itself.
         */
        $_ENV['TEST_MODE'] = 'disabled';
        
        $fileName = 'test';
        $message = 'Some error here';
        $content = 'Hello World!';

        $_SERVER['REQUEST_URI'] = '/' . $fileName . '.html';

        $tempAssetFile = Path::getHTMLFolder() . DIRECTORY_SEPARATOR . $fileName . '.phtml';

        file_put_contents(
            $tempAssetFile,
            '<?php trigger_error(\'' . $message . '\', \E_USER_ERROR); ?>' . $content
        );
        
        $configFile = Path::getConfigFolder() . DIRECTORY_SEPARATOR . 'config.php';
        
        $config = new ConfigGenerator();
        $config
            ->setURLStructure('name', 'subname')
            ->addPage(
                PageGenerator::create()
                ->setLanguageCode('de')
                ->setName($fileName)
                ->setFile($fileName)
            )
        ;

        file_put_contents(
            $configFile,
            '<?php return ' . var_export($config->getConfig(), true) . ';'
        );
        
        $envFile = Path::getRootFolder() . DIRECTORY_SEPARATOR . '.env.local';
        $envContent = 'APP_ENV=dev';

        file_put_contents($envFile, $envContent);

        $controller = new Controller();
        $responseContent = (string) DI::getResponse()->getContent();

        self::assertNotSame(
            $content,
            $responseContent
        );

        self::assertStringContainsString(
            'An error occured',
            $responseContent
        );

        unset($controller);
    }

    /**
     * @throws MissingValueException
     */
    #[PreserveGlobalState(false)]
    #[RunInSeparateProcess]
    public function testCanHandleErrorInProduction(): void
    {
        /**
         * The TEST_MODE variables causes errors to be printed in the CLI which is needed to run tests correctly.
         * For this test it is changed, because Kiwa needs to catch the error by itself.
         */
        $_ENV['TEST_MODE'] = 'disabled';

        $fileName = 'test';
        $message = 'Some error here';
        $content = 'Hello World!';

        $_SERVER['REQUEST_URI'] = '/' . $fileName . '.html';

        $tempAssetFile = Path::getHTMLFolder() . DIRECTORY_SEPARATOR . $fileName . '.phtml';

        $file1 = fopen($tempAssetFile, 'wb+');

        if (false === $file1) {
            self::fail('Could not create file "' . $tempAssetFile . '".');
        }

        fwrite($file1, '<?php trigger_error(\'' . $message . '\', \E_USER_ERROR); ?>' . $content);
        fclose($file1);

        $configFile = Path::getConfigFolder() . DIRECTORY_SEPARATOR . 'config.php';

        $config = new ConfigGenerator();
        $config
            ->setURLStructure('name', 'subname')
            ->addPage(
                PageGenerator::create()
                ->setLanguageCode('de')
                ->setName($fileName)
                ->setFile($fileName)
            )
        ;

        file_put_contents(
            $configFile,
            '<?php return ' . var_export($config->getConfig(), true) . ';'
        );
        
        $envFile = Path::getRootFolder() . DIRECTORY_SEPARATOR . '.env.local';
        $envContent = 'APP_ENV=prod';

        $env = fopen($envFile, 'wb+');

        if (false === $env) {
            self::fail('Could not create file "' . $envFile . '".');
        }

        fwrite($env, $envContent);
        fclose($env);

        $controller = new Controller();
        $responseContent = (string) DI::getResponse()->getContent();

        self::assertSame(
            $content,
            $responseContent
        );

        unset($controller);
    }
    
    /**
     * @throws MissingValueException
     */
    #[PreserveGlobalState(false)]
    #[RunInSeparateProcess]
    public function testCanMuteError(): void
    {
        /**
         * The TEST_MODE variables causes errors to be printed in the CLI which is needed to run tests correctly.
         * For this test it is changed, because Kiwa needs to catch the error by itself.
         */
        $_ENV['TEST_MODE'] = 'disabled';

        $content = time();
        
        $fileName = 'test';

        $_SERVER['REQUEST_URI'] = '/' . $fileName . '.html';

        $tempAssetFile = Path::getHTMLFolder() . DIRECTORY_SEPARATOR . $fileName . '.phtml';
        
        file_put_contents(
            $tempAssetFile,
            '<?php $foo = @file_get_contents(\'notexisting\'); echo \'' . $content . '\';'
        );

        $configFile = Path::getConfigFolder() . DIRECTORY_SEPARATOR . 'config.php';

        $config = new ConfigGenerator();
        $config
            ->setURLStructure('name', 'subname')
            ->addPage(
                PageGenerator::create()
                    ->setLanguageCode('de')
                    ->setName($fileName)
                    ->setFile($fileName)
            )
        ;

        file_put_contents(
            $configFile,
            '<?php return ' . var_export($config->getConfig(), true) . ';'
        );

        $envFile = Path::getRootFolder() . DIRECTORY_SEPARATOR . '.env.local';
        $envContent = 'APP_ENV=dev';

        file_put_contents($envFile, $envContent);

        $controller = new Controller();
        $responseContent = (string) DI::getResponse()->getContent();

        self::assertNotSame(
            $content,
            $responseContent
        );

        self::assertStringNotContainsString(
            'An error occured',
            $responseContent
        );

        unset($controller);
    }
}
