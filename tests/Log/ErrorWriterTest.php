<?php

/**
 * Kiwa. A feather-light web framework for professional static websites.
 *
 * @author Tobias Köngeter
 * @copyright Copyright © Bit&Black
 * @link https://www.bitandblack.com
 * @link https://www.kiwa.io
 * @license MIT
 */

namespace Kiwa\Tests\Log;

use Kiwa\Log\ErrorWriter;
use Kiwa\Path;
use PHPUnit\Framework\TestCase;

/**
 * Class ErrorWriterTest.
 */
class ErrorWriterTest extends TestCase
{
    public function testCanHandleBrokenFile(): void
    {
        $errorFile = Path::getLogFolder() . DIRECTORY_SEPARATOR . 'error.php';
        
        file_put_contents(
            $errorFile,
            '<?php return [0 => [\'foo\']];);'
        );

        new ErrorWriter([
            'timestamp' => time(),
            'message' => 'baz',
            'file' => 'some-file.phtml',
            'line' => 1,
            'type' => E_USER_ERROR,
            'site' => 'some-site.html',
        ]);

        $errorFileContent = include $errorFile;

        self::assertIsArray($errorFileContent);
        self::assertIsArray($errorFileContent[0]);

        self::assertSame(
            'baz',
            $errorFileContent[0]['message']
        );
    }
}
