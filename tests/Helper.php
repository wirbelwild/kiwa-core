<?php

/**
 * Kiwa. A feather-light web framework for professional static websites.
 *
 * @author Tobias Köngeter
 * @copyright Copyright © Bit&Black
 * @link https://www.bitandblack.com
 * @link https://www.kiwa.io
 * @license MIT
 */

namespace Kiwa\Tests;

use Exception;
use Kiwa\Path;

/**
 * Class Helper
 *
 * @package Kiwa\Tests
 */
class Helper
{
    /**
     * @var array<int, string>
     */
    private static array $neededFolders = [];

    /**
     * @return void
     */
    public static function setUpBefore(): void
    {
        self::$neededFolders = [
            Path::getBuildFolder(),
            Path::getBuildFolder() . DIRECTORY_SEPARATOR . 'images',
            Path::getBuildFolder() . DIRECTORY_SEPARATOR . 'files',
            Path::getBuildFolder() . DIRECTORY_SEPARATOR . 'subdir',
            Path::getBuildFolder() . DIRECTORY_SEPARATOR . 'javascripts',
            Path::getBuildFolder() . DIRECTORY_SEPARATOR . 'stylesheets',
            Path::getConfigFolder(),
            Path::getHTMLFolder(),
            Path::getLanguageFolder(),
            Path::getTempFolder(),
            Path::getPublicFolder(),
        ];

        foreach (self::$neededFolders as $folder) {
            if (!file_exists($folder)) {
                mkdir($folder, 0777, true);
            }
        }
    }

    /**
     * @return void
     */
    public static function tearDownAfter(): void
    {
        foreach (self::$neededFolders as $folder) {
            if (file_exists($folder)) {
                self::deleteDirectory($folder);
            }
        }

        self::deleteDirectory(Path::getRootFolder() . DIRECTORY_SEPARATOR . '.env');
        self::deleteDirectory(Path::getRootFolder() . DIRECTORY_SEPARATOR . '.env.local');
        self::deleteDirectory(Path::getRootFolder() . DIRECTORY_SEPARATOR . '.env.test');
        self::deleteDirectory(Path::getRootFolder() . DIRECTORY_SEPARATOR . '.env.test.local');
    }
    
    /**
     * @param string $dir
     * @return bool
     */
    public static function deleteDirectory(string $dir): bool
    {
        if (!file_exists($dir)) {
            return true;
        }

        if (!is_dir($dir)) {
            return unlink($dir);
        }

        $items = scandir($dir);

        if (false === $items) {
            $items = [];
        }

        foreach ($items as $item) {
            if ('.' === $item || '..' === $item) {
                continue;
            }

            if (!self::deleteDirectory($dir . DIRECTORY_SEPARATOR . $item)) {
                return false;
            }
        }

        return rmdir($dir);
    }

    /**
     * @return string
     * @throws Exception
     */
    public static function getRandomString(): string
    {
        $string = random_bytes(8);
        return bin2hex($string);
    }

    /**
     * @param int $min
     * @param int $max
     * @return int
     * @throws Exception
     */
    public static function getRandomInt(int $min = 0, int $max = PHP_INT_MAX): int
    {
        return random_int($min, $max);
    }
}
