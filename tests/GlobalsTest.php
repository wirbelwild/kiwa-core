<?php

/**
 * Kiwa. A feather-light web framework for professional static websites.
 *
 * @author Tobias Köngeter
 * @copyright Copyright © Bit&Black
 * @link https://www.bitandblack.com
 * @link https://www.kiwa.io
 * @license MIT
 */

namespace Kiwa\Tests;

use Kiwa\Globals;
use PHPUnit\Framework\TestCase;

/**
 * Class GlobalsTest.
 *
 * @package Kiwa\Tests
 */
class GlobalsTest extends TestCase
{
    public function testCanHandleVersion(): void
    {
        self::assertStringContainsString(
            '@',
            Globals::getKiwaVersion()
        );
    }
}
