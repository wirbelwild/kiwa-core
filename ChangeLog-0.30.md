# Changes in Kiwa Core v0.30

## 0.30.2 2024-07-26

### Fixed

-   Fixed url generation for index pages.

## 0.30.1 2024-07-12

### Changed

-   [URLFromPage](./src/URL/URLFromPage.php) objects are now stringable.

## 0.30.0 2024-07-12

### Added

-   Added the [URLFromPage](./src/URL/URLFromPage.php) class, to convert a [Page](./src/Config/Page.php) into a URL.