# Changes in Kiwa Core v1.0

## 1.0.0 2024-12-12

### Changed

-   PHP >= 8.2 is now required.
-   All previously deprecated classes and methods have been removed.