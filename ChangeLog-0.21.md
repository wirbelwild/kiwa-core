# Changes in Kiwa Core.

## 0.21.10 2021-01-23

### Fixed

-   Updated `places2be/locales` and its usage to prevent notice.

## 0.21.9 2020-10-02

### Fixed

*   Updated `bitandblack/composer-helper` to improve support for Windows environments.

## 0.21.8 2020-07-31

### Fixed 

*   Dynamic subpages can be loaded correctly now.

## 0.21.7 2020-07-15

### Fixed 

*   Fixed wrong redirect from a subpage when the page suffix was wrong.

## 0.21.6 2020-07-15

### Fixed 

*   Fixed method names in `Kiwa\Config\Generator\ConfigGenerator`.

## 0.21.5 2020-07-15

### Fixed 

*   A wrong config handling caused subpages to be loaded when they were requested as pages in hierarchy 1.

## 0.21.4 2020-07-13

### Fixed 

*   Changed missing class to constant.

## 0.21.3 2020-07-13

### Fixed 

*   A file request of an unknown type will be redirected into the `/public` folder. Also, a `PHP` file may change the header by using the `Response` object.

### Changed

*   `\Kiwa\Header` has been deprecated.

## 0.21.2 2020-07-03

### Fixed 

*   `ConfigGenerator` sets the `useWWW` and `useSSL` correctly now.

## 0.21.1 2020-06-29

### Fixed

*   Fixed wrong parsing of URL which failed when URLs had parameters.

## 0.21.0 2020-06-23 

### Added

*   Added support for htpasswd files.
*   Added `ConfigGenerator` and `PageGenerator` to provide setter methods for handling the config.