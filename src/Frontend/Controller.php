<?php

/**
 * Kiwa. A feather-light web framework for professional static websites.
 *
 * @author Tobias Köngeter
 * @copyright Copyright © Bit&Black
 * @link https://www.bitandblack.com
 * @link https://www.kiwa.io
 * @license MIT
 */

namespace Kiwa\Frontend;

use BitAndBlack\Helpers\SanitizeHelper;
use BitAndBlack\PathInfo\PathInfo;
use Exception;
use Kiwa\AbstractController;
use Kiwa\Config\Page;
use Kiwa\DI;
use Kiwa\Exception\DependencyInjectionNotInitializedException;
use Kiwa\File\BinaryFile;
use Kiwa\File\HTMLFile;
use Kiwa\Language\CurrentLanguageCode;
use Kiwa\Language\MetaLanguageCode;
use Kiwa\Page\PageList;
use Kiwa\Page\PageTitle;
use Kiwa\Page\RelatedPages;
use Kiwa\Path;
use Kiwa\Route\RouteCollectionFactory;
use Kiwa\Route\RouteFinder;
use Kiwa\Templating\Canonicals;
use Kiwa\Templating\MetaTags;
use Kiwa\URL\URLBuilder;
use Kiwa\URL\URLFragment;
use Symfony\Component\Finder\Finder;
use Symfony\Component\Finder\SplFileInfo;
use Symfony\Component\HttpFoundation\BinaryFileResponse;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * Controller for the frontend.
 *
 * @package Kiwa\Frontend
 */
class Controller extends AbstractController
{
    /**
     * Controller constructor.
     *
     * @throws Exception
     */
    public function __construct()
    {
        parent::__construct();
        $request = DI::getRequest();
        DI::getLog()->debug('New request for "' . ($request->server->get('REQUEST_URI') ?? '/') . '"');
        
        DI::getConfig()->setBaseConfiguration();
        
        $this->buildResponse($request);
    }

    /**
     * Handles the request and starts building a response.
     *
     * @param Request $request
     * @throws DependencyInjectionNotInitializedException
     */
    private function buildResponse(Request $request): void
    {
        $requestURI = $request->server->get('REQUEST_URI');
        
        if (!is_string($requestURI)) {
            $requestURI = '/';
        }

        $indexPageToHide = [
            '/index',
            '/index.php',
            '/index.htm',
            '/index.html',
        ];

        $requestUriPath = (string) parse_url($requestURI, PHP_URL_PATH);
        $isRequestUriIndexPage = in_array($requestUriPath, $indexPageToHide);

        /**
         * Redirect from requested index pages.
         * This is important, because handling and including that index.php file would lead to infinite loops.
         */
        if (true === $isRequestUriIndexPage) {
            $response = new RedirectResponse('/', Response::HTTP_MOVED_PERMANENTLY);
            DI::setInstance(Response::class, $response);
            return;
        }

        $requestURI = (string) parse_url($requestURI, PHP_URL_PATH);

        $fileName = str_replace('/', DIRECTORY_SEPARATOR, $requestURI);
        $fileName = Path::getPublicFolder() . DIRECTORY_SEPARATOR . $fileName;

        if ('/favicon.ico' === $requestURI) {
            $files = Finder::create()
                ->files()
                ->in(Path::getPublicFolder())
                ->name('favicon.ico')
            ;
            
            $files = iterator_to_array($files);
            $fileName = Path::getPublicFolder() . DIRECTORY_SEPARATOR . $requestURI;
            
            if (!empty($files)) {
                /** @var SplFileInfo $file */
                $file = array_values($files)[0];
                $fileName = $file->getPathname();
            }
        }

        $knownPageSuffixes = ['htm', 'html', 'php'];
        $isFileExisting = file_exists($fileName);
        $hasPageSuffix = false !== DI::getConfig()->getValue('usePageSuffix');

        $extension = pathinfo(
            (string) parse_url($requestURI, PHP_URL_PATH),
            PATHINFO_EXTENSION
        );

        $extension = '' === $extension ? null : $extension;
        
        if (false === $hasPageSuffix && false === $isFileExisting) {
            $extension = null;
        }
        
        $isKnownSuffix = in_array($extension, $knownPageSuffixes, true);
        $isFileRequest = null !== $extension && false === $isKnownSuffix;
        $isPHPFileRequest = 'php' === $extension && true === $isFileExisting;

        /** @var array<string, int> $cacheControl */
        $cacheControl = DI::getConfig()->getValue('cacheControl');
        
        if ($isFileRequest || $isPHPFileRequest) {
            DI::getLog()->debug('File request detected.');
            
            $file = new BinaryFile($fileName);

            if (Response::HTTP_OK === $file->getStatus()) {
                /**
                 * PHP files may change the response header, that's why we need some extra steps here.
                 */
                $isPHP = 'php' === PathInfo::createFromFile($file->getFilePath())->getExtension();

                /**
                 * Executes the PHP code inside the file. This may change the content-type.
                 */
                $fileContent = $file->readFile();
                $contentType = $this->extractContentType();

                if ($isPHP) {
                    $response = new Response(
                        $fileContent,
                        $file->getStatus(),
                        [
                            'content-type' => $contentType,
                        ]
                    );
                    $response
                        ->setEtag(
                            md5((string) $response->getContent())
                        )
                    ;
                } else {
                    $response = new BinaryFileResponse(
                        $file->getFilePath(),
                        $file->getStatus(),
                        [
                            'content-type' => (string) $file->getMimeType(),
                        ]
                    );
                    $response
                        ->setAutoEtag()
                        ->setAutoLastModified()
                    ;
                }

                $response
                    ->prepare($request)
                    ->setPublic()
                    ->setMaxAge(
                        $response->getMaxAge() ?? $cacheControl['maxAgeAssetFiles']
                    )
                ;

                if ($file->isVersionedFile()) {
                    $response->headers->addCacheControlDirective('immutable');
                    $response
                        ->setMaxAge($cacheControl['maxAgeImmutableFiles'])
                        ->setLastModified($file->getLastModified())
                    ;
                }

                DI::setInstance(Response::class, $response);
                return;
            }
            
            $htmlFile = new HTMLFile((string) $file->getStatus());
            
            $response = new Response((string) $htmlFile, $file->getStatus());
            $response
                ->prepare($request)
                ->setPublic()
                ->setMaxAge($cacheControl['maxAgeUnknownFiles'])
                ->setLastModified($file->getLastModified())
            ;

            DI::setInstance(Response::class, $response);
            return;
        }

        /**
         * Add messages regarding the language code now. This happens when no file has been requested.
         */
        foreach (MetaLanguageCode::getErrors() as $error) {
            DI::getLog()->error($error);
        }
        
        /**
         * We are sure now that no file has been requested, so a redirect response may be sent now.
         */
        if (null !== $redirectResponse = DI::getConfig()->getRedirectResponse()) {
            DI::getLog()->debug('Returning a redirection to "' . ($redirectResponse->headers->get('location') ?? '/') . '"');
            DI::setInstance(Response::class, $redirectResponse);
            return;
        }
        
        /**
         * If the use of a language code is defined but not found, redirect to the standard language index page.
         */
        $urlStructure = DI::getConfig()->getValue('urlStructure');
        
        if (is_array($urlStructure) && in_array('languageCode', $urlStructure, true)) {
            $languageCode = URLFragment::create('languageCode')->getFragment();

            if (null === $languageCode) {
                DI::getLog()->debug('Redirecting to a language versioned page.');

                $languageCode = DI::getSession()->get('kiwaLanguageCode')
                    ?? DI::getConfig()->getValue('languageCode')
                ;

                if (false === is_string($languageCode)) {
                    $languageCode = null;
                }

                $languageCode = SanitizeHelper::htmlSpecialChars($languageCode);

                $redirectURL = (string) new URLBuilder([
                    'languageCode' => $languageCode,
                ]);

                $response = new RedirectResponse($redirectURL);
                DI::setInstance(Response::class, $response);
                return;
            }
        }

        $response = new Response(null, Response::HTTP_OK, [
            'content-type' => 'text/html',
        ]);
        $response
            ->prepare($request)
            ->setPublic()
            ->setCache([
                'no_cache' => true,
            ])
        ;

        /** Makes the response object available via DI in all pages. */
        DI::setInstance(
            Response::class,
            $response
        );
        
        $this->buildHTMLResponse($response, $requestURI);
    }

    /**
     * Creates the response of an html page.
     *
     * @param Response $response
     * @param string $requestURI
     */
    private function buildHTMLResponse(Response $response, string $requestURI): void
    {
        DI::getLog()->debug('Build page');

        if (!DI::getConfig()->isRequestValid()) {
            $this->getErrorResponse($response);
            return;
        }

        $isProduction = 'prod' === DI::getConfig()->getValue('APP_ENV');

        $routeCollection = RouteCollectionFactory::create(
            new PageList(),
            $isProduction
        );

        $routeFinder = new RouteFinder($requestURI, $routeCollection);
        $pageCurrent = $routeFinder->getPage();

        if (null === $pageCurrent) {
            if ('/' === $requestURI) {
                $pageCurrent = new Page([
                    'name' => 'index',
                    'languageCode' => (string) new CurrentLanguageCode(),
                    'enableCache' => false,
                    'cacheLifetime' => -1,
                    'getFile' => 'index',
                ]);
            } else {
                $status = Response::HTTP_NOT_FOUND;

                $pageCurrent = new Page([
                    'name' => (string) $status,
                    'languageCode' => (string) new CurrentLanguageCode(),
                    'enableCache' => false,
                    'cacheLifetime' => -1,
                    'getFile' => (string) $status,
                ]);

                $response->setStatusCode($status);
                DI::getLog()->debug('Returning status ' . $status . ' because of missing page.');
                PageTitle::setTitle((string) $status);
            }
        }

        CurrentLanguageCode::setLanguageCodeFromRoute(
            $pageCurrent->getLanguageCode()
        );

        /** Shorthand for all url fragments */
        $urlFragmentLanguageCode = URLFragment::create('languageCode')->getFragment();
        $urlFragmentName = URLFragment::create('name')->getFragment();
        $urlFragmentSubname = URLFragment::create('subname')->getFragment();

        /**
         * Because we have a request for a page here, we can change to url to lowercase. This allows easier handling.
         * To prevent duplicate content, we also add a canonical link to the lowercased url.
         *
         * @todo Make this setting configurable
         */
        $urlFragmentLanguageCodeLower = null !== $urlFragmentLanguageCode ? mb_strtolower($urlFragmentLanguageCode) : null;
        $urlFragmentNameLower = null !== $urlFragmentName ? mb_strtolower($urlFragmentName) : null;
        $urlFragmentSubnameLower = null !== $urlFragmentSubname ? mb_strtolower($urlFragmentSubname) : null;

        if ($urlFragmentLanguageCode !== $urlFragmentLanguageCodeLower
            || $urlFragmentName !== $urlFragmentNameLower
            || $urlFragmentSubname !== $urlFragmentSubnameLower
        ) {
            $url = new URLBuilder(
                [
                    'languageCode' => $urlFragmentLanguageCodeLower,
                    'name' => $urlFragmentNameLower,
                    'subname' => $urlFragmentSubnameLower,
                ],
                false
            );
            Canonicals::add((string) $url);
        }

        $this->setRelatedPages($pageCurrent);

        foreach ($pageCurrent->getMetaTags() as $property => $content) {
            MetaTags::add($property, $content);
        }

        if (null !== $pageCurrent->getPageTitle()) {
            PageTitle::setTitle($pageCurrent->getPageTitle());
        }

        $htmlFile = new HTMLFile(
            $pageCurrent->getFile() ?? '',
            $pageCurrent->isCacheEnabled(),
            $pageCurrent->getCacheLifetime()
        );

        if (null === $response->getLastModified()) {
            $response->setLastModified(
                $htmlFile->getLastModified()
            );
        }

        if (null === $response->getEtag()) {
            $response->setEtag(
                md5((string) $response->getContent())
            );
        }

        if (null !== $lastModified = $htmlFile->getLastModified()) {
            $response->setLastModified($lastModified);
        }

        $cacheLifetime = $pageCurrent->getCacheLifetime();

        if ($cacheLifetime > 0 && true === $htmlFile->isCachable()) {
            $response
                ->setCache([
                    'no_cache' => false,
                ])
                ->setMaxAge($cacheLifetime)
            ;
        }

        /**
         * If the phtml file loaded with an error, we change the status code here.
         */
        if (Response::HTTP_OK !== $status = $htmlFile->getStatus()) {
            $response->setStatusCode($status);
        }

        $response->setContent($htmlFile);
    }

    /**
     * Prints the response data.
     *
     * @return void
     */
    public function printResponse(): void
    {
        DI::getResponse()->send();
    }

    /**
     * This helper method creates and returns an error response.
     *
     * @param Response $response
     * @param int $status
     * @return Response
     */
    private function getErrorResponse(Response $response, int $status = Response::HTTP_NOT_FOUND): Response
    {
        $htmlOutput = (string) new HTMLFile((string) $status);
        PageTitle::setTitle((string) $status);
        return $response
            ->setContent($htmlOutput)
            ->setStatusCode($status)
        ;
    }

    /**
     * @param Page $pageCurrent
     * @return $this
     */
    private function setRelatedPages(Page $pageCurrent): self
    {
        $pagesWithDynamicChildren = PageList::create()
            ->hasDynamicChild($pageCurrent->getName())
        ;

        $isDynamicPage = null !== $pageCurrent->getChildOf() && $pagesWithDynamicChildren->count() > 0;

        /**
         * A page with a dynamic child adds only itself, because we don't know what else is related.
         * Also, we use the url fragment to have the real value of this URL.
         */
        if ($isDynamicPage) {
            RelatedPages::addPage($pageCurrent->getLanguageCode(), $pageCurrent->getChildOf(), (string) URLFragment::create('subname'));
            return $this;
        }

        /**
         * Register all language versions to enable alternate links.
         * We're looking for the same phtml file to find the same pages.
         *
         * @var Page $page
         */
        foreach (new PageList() as $page) {
            /** If the file name matches, this page is getting added as related page. */
            if ($page->getFile() === $pageCurrent->getFile()) {
                null !== $page->getChildOf()
                    ? RelatedPages::addPage($page->getLanguageCode(), $page->getChildOf(), $page->getName())
                    : RelatedPages::addPage($page->getLanguageCode(), $page->getName())
                ;
            }
        }

        return $this;
    }

    /**
     * @return string
     */
    private function extractContentType(): string
    {
        $contentType = null;

        /**
         * If a content type has been defined natively, we take it from here.
         */
        foreach (headers_list() as $header) {
            $header = mb_strtolower($header);
            $headers = explode(':', $header);
            
            if ('content-type' === ($headers[0] ?? null)) {
                $contentType = $headers[1] ?? null;
                break;
            }
        }
        
        if (null !== $contentType) {
            return trim($contentType);
        }

        /**
         * If the content type has been set in the Response object, take it from there.
         * Otherwise, it will be `text/html`.
         */
        $contentType = DI::getResponse()->headers->has('content-type')
            ? DI::getResponse()->headers->get('content-type')
            : 'text/html'
        ;
        
        return $contentType ?? 'text/html';
    }
}
