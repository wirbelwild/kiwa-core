<?php

/**
 * Kiwa. A feather-light web framework for professional static websites.
 *
 * @author Tobias Köngeter
 * @copyright Copyright © Bit&Black
 * @link https://www.bitandblack.com
 * @link https://www.kiwa.io
 * @license MIT
 */

namespace Kiwa\Connect;

use Kiwa\AbstractController;

/**
 * The Connect Controller has the same behaviour as the Frontend Controller
 * but doesn't handle a request and doesn't create a response.
 *
 * @package Kiwa\Connect
 */
class Controller extends AbstractController
{
}
