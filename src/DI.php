<?php

/**
 * Kiwa. A feather-light web framework for professional static websites.
 *
 * @author Tobias Köngeter
 * @copyright Copyright © Bit&Black
 * @link https://www.bitandblack.com
 * @link https://www.kiwa.io
 * @license MIT
 */

namespace Kiwa;

use DI\Container;
use DI\DependencyException;
use DI\NotFoundException;
use Kiwa\Exception\DependencyInjectionNotInitializedException;
use Psr\Log\LoggerAwareInterface;
use Psr\Log\LoggerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\Session;

/**
 * The DI class holds and returns objects.
 *
 * @package Kiwa
 */
final class DI
{
    private static ?Container $container = null;

    /**
     * @var bool
     */
    private static bool $hasBeenInitialized = false;

    /**
     * @return Container
     * @throws DependencyInjectionNotInitializedException
     */
    public static function getContainer(): Container
    {
        if (null === self::$container) {
            throw new DependencyInjectionNotInitializedException();
        }
        
        return self::$container;
    }

    /**
     * @param Container $container
     */
    public static function setContainer(Container $container): void
    {
        self::$container = $container;
        self::$hasBeenInitialized = true;
    }

    /**
     * Sets an object to the container.
     *
     * @param string $instanceName
     * @throws DependencyInjectionNotInitializedException
     */
    public static function setInstance(string $instanceName, mixed $instanceValue): void
    {
        self::setInstanceHook($instanceName, $instanceValue);
        self::getContainer()->set($instanceName, $instanceValue);
    }
    
    /**
     * Returns an instance of a class.
     *
     * @template T
     * @param class-string<T> $instanceName
     * @return T
     */
    public static function getInstance(string $instanceName)
    {
        $class = null;
        
        try {
            $class = self::getContainer()->get($instanceName);
        } catch (DependencyException) {
        } catch (NotFoundException|DependencyInjectionNotInitializedException $exception) {
            trigger_error($exception->getMessage(), E_USER_ERROR);
        }

        if ($class instanceof LoggerAwareInterface) {
            /** @var LoggerInterface $logger */
            $logger = self::getInstance(Log::class);
            $class->setLogger($logger);
        }

        return $class ?? new $class();
    }
    
    /**
     * This method is an alias and returns an instance of the Request object.
     *
     * @return Request
     */
    public static function getRequest(): Request
    {
        return self::getInstance(Request::class);
    }

    /**
     * This method is an alias and returns an instance of the Response object.
     *
     * @return Response
     */
    public static function getResponse(): Response
    {
        return self::getInstance(Response::class);
    }

    /**
     * This method is an alias and returns an instance of the Session object.
     *
     * @return Session<mixed>
     */
    public static function getSession(): Session
    {
        return self::getInstance(Session::class);
    }

    /**
     * This method is an alias and returns an instance of the Log object.
     *
     * @return Log
     */
    public static function getLog(): Log
    {
        return self::getInstance(Log::class);
    }

    /**
     * This method is an alias and returns an instance of the Config object.
     *
     * @return Config
     */
    public static function getConfig(): Config
    {
        return self::getInstance(Config::class);
    }

    /**
     * @return bool
     */
    public static function isHasBeenInitialized(): bool
    {
        return self::$hasBeenInitialized;
    }

    /**
     * @param string $instanceName
     * @return void
     */
    private static function setInstanceHook(string $instanceName, mixed $instanceValue): void
    {
        if (Response::class === $instanceName && $instanceValue instanceof Response) {
            $securityHeaders = self::getConfig()->getValue('securityHeaders');

            if (is_array($securityHeaders)) {
                foreach ($securityHeaders as $securityHeader => $value) {
                    if ($instanceValue->headers->has($securityHeader)) {
                        return;
                    }

                    if (false === is_string($value)
                        && false === is_array($value)
                        && null !== $value
                    ) {
                        continue;
                    }

                    $instanceValue->headers->set($securityHeader, $value);
                }
            }
        }
    }
}
