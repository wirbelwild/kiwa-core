<?php

/**
 * Kiwa. A feather-light web framework for professional static websites.
 *
 * @author Tobias Köngeter
 * @copyright Copyright © Bit&Black
 * @link https://www.bitandblack.com
 * @link https://www.kiwa.io
 * @license MIT
 */

namespace Kiwa;

use BitAndBlack\Composer\VendorPath;
use RuntimeException;

/**
 * The Path class provides the paths to all kiwa-related folders.
 *
 * @package Kiwa
 */
class Path
{
    /**
     * @param string $folder
     */
    private static function createFolderIfNotExists(string $folder): void
    {
        if (!is_dir($folder) && !mkdir($folder) && !is_dir($folder)) {
            throw new RuntimeException(
                sprintf('Directory "%s" was not created', $folder)
            );
        }
    }

    /**
     * Returns the path to the root folder.
     *
     * @return string
     */
    public static function getRootFolder(): string
    {
        return dirname((string) new VendorPath());
    }

    /**
     * Returns the path to the public folder.
     *
     * @return string
     */
    public static function getPublicFolder(): string
    {
        $folder = self::getRootFolder() . DIRECTORY_SEPARATOR . 'public';
        self::createFolderIfNotExists($folder);
        return $folder;
    }

    /**
     * Returns the path to the build folder.
     *
     * @return string
     */
    public static function getBuildFolder(): string
    {
        $folder = self::getPublicFolder() . DIRECTORY_SEPARATOR . 'build';
        self::createFolderIfNotExists($folder);
        return $folder;
    }

    /**
     * Returns the path to the config folder.
     *
     * @return string
     */
    public static function getConfigFolder(): string
    {
        $folder = self::getRootFolder() . DIRECTORY_SEPARATOR . 'config';
        self::createFolderIfNotExists($folder);
        return $folder;
    }

    /**
     * Returns the path to the crawler folder.
     *
     * @return string
     */
    public static function getCrawlerFolder(): string
    {
        return self::getPublicFolder();
    }

    /**
     * Returns the path to the html folder.
     *
     * @return string
     */
    public static function getHTMLFolder(): string
    {
        $folder = self::getRootFolder() . DIRECTORY_SEPARATOR . 'html';
        self::createFolderIfNotExists($folder);
        return $folder;
    }

    /**
     * Returns the path to the language folder.
     *
     * @return string
     */
    public static function getLanguageFolder(): string
    {
        $folder = self::getRootFolder() . DIRECTORY_SEPARATOR . 'language';
        self::createFolderIfNotExists($folder);
        return $folder;
    }

    /**
     * Returns the path to the temp folder.
     *
     * @return string
     */
    public static function getTempFolder(): string
    {
        $folder = self::getRootFolder() . DIRECTORY_SEPARATOR . 'var';
        self::createFolderIfNotExists($folder);
        return $folder;
    }

    /**
     * Returns the path to the log folder.
     *
     * @return string
     */
    public static function getLogFolder(): string
    {
        $folder = self::getTempFolder() . DIRECTORY_SEPARATOR . 'log';
        self::createFolderIfNotExists($folder);
        return $folder;
    }

    /**
     * Returns the path to the cache folder.
     *
     * @return string
     */
    public static function getCacheFolder(): string
    {
        $folder = self::getTempFolder() . DIRECTORY_SEPARATOR . 'cache';
        self::createFolderIfNotExists($folder);
        return $folder;
    }
}
