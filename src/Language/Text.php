<?php

/**
 * Kiwa. A feather-light web framework for professional static websites.
 *
 * @author Tobias Köngeter
 * @copyright Copyright © Bit&Black
 * @link https://www.bitandblack.com
 * @link https://www.kiwa.io
 * @license MIT
 */

namespace Kiwa\Language;

use Kiwa\DI;
use Kiwa\Path;
use Stringable;

/**
 * The Text class translates text into another language.
 *
 * @package Kiwa\Language
 * @see \Kiwa\Tests\Language\TextTest
 */
class Text implements Stringable
{
    /**
     * @var array<string, array<string, string>>
     */
    private static array $translationTexts = [];

    private static string $defaultMessageDomain = 'frontend';

    /**
     * Translates a text.
     *
     * @param string $text The source text. This one will also be returned if no translation is available.
     * @param array<string, string|Stringable|int|float|bool> $variables An array of variables for this text.
     * @param string|null $messageDomain The message domain, for example `frontend`. If this one is not set,
     * the default message domain will be used. This is `frontend` per default.
     * @param string|null $languageCode The language code you want to translate into. If this parameter is empty,
     * the current language code will be used. To leave it empty should be the default usage.
     */
    public function __construct(
        private string $text,
        array $variables = [],
        string|null $messageDomain = null,
        string|null $languageCode = null
    ) {
        $messageDomain ??= self::$defaultMessageDomain;
        $languageCode ??= (string) new CurrentLanguageCode();

        if (empty($languageCode)) {
            DI::getLog()->error('Language code is missing');
            return;
        }
        
        $languageTranslationKey = self::$defaultMessageDomain === $messageDomain
            ? $languageCode
            : $messageDomain . '.' . $languageCode
        ;

        if (!isset(self::$translationTexts[$messageDomain])) {
            self::includeFile($languageTranslationKey);
        }

        $translation = self::$translationTexts[$languageTranslationKey][$this->text] ?? $this->text;

        foreach ($variables as $key => $value) {
            $translation = (string) str_replace('{{' . $key . '}}', (string) $value, $translation);
        }

        $this->text = $translation;
    }

    /**
     * @return string
     */
    public function getText(): string
    {
        return $this->text;
    }

    /**
     * @return string
     */
    public function __toString(): string
    {
        return $this->getText();
    }

    /**
     * Set the default message domain.
     *
     * @param string $messageDomain The message domain, for example `frontend`. The domain in combination with the
     *                              current language code is building the file name then, for example
     *                              `frontend.de.php`.
     * @return bool
     */
    public static function setDefaultMessageDomain(string $messageDomain): bool
    {
        $currentLanguageCode = (new CurrentLanguageCode())->getCurrentLanguageCode();

        if (null === $currentLanguageCode) {
            DI::getLog()->error('Language code is missing');
            return false;
        }

        $languageTranslationKey = 'default' === $messageDomain
            ? $currentLanguageCode
            : $messageDomain . '.' . $currentLanguageCode
        ;
        
        return self::includeFile($languageTranslationKey);
    }

    /**
     * @param string $file
     * @return bool
     */
    private static function includeFile(string $file): bool
    {
        self::$translationTexts[$file] = [];

        $path = Path::getLanguageFolder() . DIRECTORY_SEPARATOR . $file . '.php';
        
        if (!file_exists($path)) {
            DI::getLog()->error(sprintf('No language file found under "%s".', $path));
            return false;
        }

        $translationTexts = @include($path);

        if (!is_array($translationTexts)) {
            DI::getLog()->error(sprintf('Language file "%s" has errors and can\'t be used.', $path));
            return false;
        }

        self::$translationTexts[$file] = $translationTexts;
        return true;
    }
}
