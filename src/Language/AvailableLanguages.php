<?php

/**
 * Kiwa. A feather-light web framework for professional static websites.
 *
 * @author Tobias Köngeter
 * @copyright Copyright © Bit&Black
 * @link https://www.bitandblack.com
 * @link https://www.kiwa.io
 * @license MIT
 */

namespace Kiwa\Language;

use Iterator;

/**
 * The AvailableLanguages class holds all language codes which are available.
 *
 * @package Kiwa\Language
 * @phpstan-implements Iterator<int, string>
 */
final class AvailableLanguages implements Iterator
{
    /**
     * @var array<int, string>
     */
    private static array $languageCodes = [];
    
    private static int $position = 0;

    /**
     * Sets all available languages.
     *
     * @param string ...$languageCode
     * @return void
     */
    public static function add(string ...$languageCode): void
    {
        array_push(self::$languageCodes, ...$languageCode);
    }

    /**
     * Returns all defined languages.
     *
     * @return array<int, string>
     */
    public static function getAll(): array
    {
        return self::$languageCodes;
    }

    /**
     * @param string $languageCode
     * @return bool
     */
    public static function hasLanguageCode(string $languageCode): bool
    {
        $languageCode = mb_strtolower($languageCode);

        return in_array(
            $languageCode,
            array_map(
                static fn ($item): string => mb_strtolower($item),
                self::$languageCodes
            ),
            true
        );
    }

    /**
     * @inheritDoc
     */
    public function rewind(): void
    {
        self::$position = 0;
    }

    /**
     * @inheritDoc
     */
    public function current(): string
    {
        return self::$languageCodes[self::$position];
    }

    /**
     * @inheritDoc
     */
    public function key(): int
    {
        return self::$position;
    }

    /**
     * @inheritDoc
     */
    public function next(): void
    {
        ++self::$position;
    }

    /**
     * @inheritDoc
     */
    public function valid(): bool
    {
        return isset(
            self::$languageCodes[self::$position]
        );
    }
}
