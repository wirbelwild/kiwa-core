<?php

/**
 * Kiwa. A feather-light web framework for professional static websites.
 *
 * @author Tobias Köngeter
 * @copyright Copyright © Bit&Black
 * @link https://www.bitandblack.com
 * @link https://www.kiwa.io
 * @license MIT
 */

namespace Kiwa\Language;

use Kiwa\DI;
use Stringable;
use Throwable;

/**
 * The CurrentLanguageCode class holds the current language code.
 *
 * @package Kiwa\Language
 * @see \Kiwa\Tests\Language\CurrentLanguageCodeTest
 */
final class CurrentLanguageCode implements Stringable
{
    private static ?LanguageCode $languageCode = null;

    /**
     * Sets the language code. This one is content-specific.
     * The language code can only be set once per request.
     *
     * @param string $code
     */
    public static function setLanguageCode(string $code): void
    {
        if ('' === $code) {
            return;
        }

        try {
            LanguageCode::ignoreLanguageExistence();
            LanguageCode::allowCountryUnspecificLanguageCode();
            $languageCode = new LanguageCode($code);
            LanguageCode::requireLanguageExistence();
            LanguageCode::disallowCountryUnspecificLanguageCode();
            $message = 'Language code is now \'' . $languageCode . '\'';
        } catch (Throwable) {
            $message = 'Failed handling language code ' . var_export($code, true);
            $languageCode = null;
        }

        DI::getLog()->debug($message);
        self::$languageCode = $languageCode;
    }

    public static function setLanguageCodeFromRoute(string $code): void
    {
        DI::getLog()->debug('Matched route defines language code as "' . $code . '".');
        self::setLanguageCode($code);
    }

    /**
     * Returns the language code.
     *
     * @return LanguageCode|null
     */
    public function getCurrentLanguageCode(): ?LanguageCode
    {
        return self::$languageCode;
    }
    
    /**
     * Returns the language code.
     *
     * @return string
     */
    public function __toString(): string
    {
        return (string) $this->getCurrentLanguageCode();
    }
}
