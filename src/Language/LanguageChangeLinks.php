<?php

/**
 * Kiwa. A feather-light web framework for professional static websites.
 *
 * @author Tobias Köngeter
 * @copyright Copyright © Bit&Black
 * @link https://www.bitandblack.com
 * @link https://www.kiwa.io
 * @license MIT
 */

namespace Kiwa\Language;

use Iterator;
use Kiwa\Page\RelatedPages;
use Kiwa\URL\URLBuilder;
use Kiwa\URL\URLFragment;

/**
 * Class LanguageChangeLinks.
 *
 * @package Kiwa\Language
 * @phpstan-implements Iterator<int, string>
 */
final class LanguageChangeLinks implements Iterator
{
    /**
     * @var array<string, string>
     */
    private readonly array $links;

    private int $position = 0;

    /**
     * Getting all content related pages or their parents.
     *
     * @param bool $isLinkToSameLevelRequired
     */
    public function __construct(bool $isLinkToSameLevelRequired = false)
    {
        $links = [];
        $level = null;

        if (URLFragment::create('subname')->getFragment()) {
            $level = 'subname';
        } elseif (URLFragment::create('name')->getFragment()) {
            $level = 'name';
        } elseif (URLFragment::create('languageCode')->getFragment()) {
            $level = 'languageCode';
        }

        foreach (AvailableLanguages::getAll() as $languageCode) {
            /**
             * @var array{
             *     languageCode: string|null,
             *     name: string|null,
             *     subname: string|null,
             * } $relatedPage
             */
            $relatedPage = RelatedPages::getRelatedPages($languageCode);

            $hasSameLevel = null !== ($relatedPage[$level] ?? null);

            if (false === $hasSameLevel && true === $isLinkToSameLevelRequired) {
                continue;
            }

            $links[$languageCode] = (string) new URLBuilder([
                'languageCode' => $languageCode,
                'name' => $relatedPage['name'] ?? null,
                'subname' => $relatedPage['subname'] ?? null,
            ]);
        }

        $this->links = $this->sortFirst($links, (string) new CurrentLanguageCode());
    }

    /**
     * @param bool $sameLevel
     * @return LanguageChangeLinks
     */
    public static function create(bool $sameLevel = false): self
    {
        return new self($sameLevel);
    }

    /**
     * @return array<string, string>
     */
    public function getAll(): array
    {
        return $this->links;
    }

    /**
     * Returns an array with a given key moved to first position
     *
     * @param array<string, string> $array
     * @param string $first
     * @return array<string, string>
     */
    private function sortFirst(array $array, string $first): array
    {
        $arrayNew = [];

        if (isset($array[$first])) {
            $arrayNew[$first] = $array[$first];
            unset($array[$first]);
        }

        $arrayKeys = array_keys($array);

        foreach ($arrayKeys as $key) {
            if (isset($array[$key])) {
                $arrayNew[$key] = $array[$key];
            }
        }

        return $arrayNew;
    }

    /**
     * @inheritDoc
     */
    public function rewind(): void
    {
        $this->position = 0;
    }

    /**
     * @inheritDoc
     */
    public function current(): string
    {
        return array_values($this->links)[$this->position];
    }

    /**
     * @inheritDoc
     */
    public function key(): int
    {
        return $this->position;
    }

    /**
     * @inheritDoc
     */
    public function next(): void
    {
        ++$this->position;
    }

    /**
     * @inheritDoc
     */
    public function valid(): bool
    {
        return isset(
            array_values($this->links)[$this->position]
        );
    }
}
