<?php

/**
 * Kiwa. A feather-light web framework for professional static websites.
 *
 * @author Tobias Köngeter
 * @copyright Copyright © Bit&Black
 * @link https://www.bitandblack.com
 * @link https://www.kiwa.io
 * @license MIT
 */

namespace Kiwa\Language;

use Places2Be\Locales\Exception;
use Places2Be\Locales\LanguageCode;

/**
 * The MetaLanguageCode class validates the language code by its type and against the defined url structure.
 *
 * @package Kiwa\Language
 */
class MetaLanguageCode
{
    private static string $metaLanguageCode = '';
    
    private static ?string $defaultLanguageCode = null;

    /**
     * @var array<int, string>
     */
    private static array $errors = [];

    /**
     * Sets the meta language code. This one is universal.
     *
     * @param string $metaLanguageCode
     */
    public static function setMetaLanguageCode(string $metaLanguageCode): void
    {
        $metaLanguageCode = mb_strtolower($metaLanguageCode);
        $defaultLanguageCode = mb_strtolower(self::$defaultLanguageCode ?? '');

        $isMetaLanguageCodeValid = self::isLanguageCodeValid($metaLanguageCode);
        $isDefaultLanguageCodeValid = self::isLanguageCodeValid($defaultLanguageCode);

        if ($isMetaLanguageCodeValid) {
            self::$metaLanguageCode = $metaLanguageCode;
            CurrentLanguageCode::setLanguageCode($metaLanguageCode);
            return;
        }

        $message = 'The defined meta language code "' . $metaLanguageCode . '" is invalid.';
        
        if ('' !== $metaLanguageCode && !in_array($message, self::$errors)) {
            self::$errors[] = $message;
        }

        if ($metaLanguageCode === $defaultLanguageCode) {
            return;
        }
        
        if ($isDefaultLanguageCodeValid) {
            self::$metaLanguageCode = $defaultLanguageCode;
            CurrentLanguageCode::setLanguageCode($defaultLanguageCode);
            return;
        }

        if ('' !== $defaultLanguageCode) {
            self::$errors[] = 'The defined default language code "' . $defaultLanguageCode . '" is also invalid.';
        }
    }
    
    /**
     * Gets the meta language code.
     *
     * @return string
     */
    public static function get(): string
    {
        return self::$metaLanguageCode;
    }

    /**
     * Proofs if a language code exists.
     *
     * @param string $languageCode
     * @return bool
     */
    private static function isLanguageCodeValid(string $languageCode): bool
    {
        try {
            LanguageCode::allowCountryUnspecificLanguageCode();
            new LanguageCode($languageCode);
            LanguageCode::disallowCountryUnspecificLanguageCode();
        } catch (Exception) {
            return false;
        }
        
        return true;
    }

    /**
     * @return array<int, string>
     */
    public static function getErrors(): array
    {
        return self::$errors;
    }
}
