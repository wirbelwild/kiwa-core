<?php

/**
 * Kiwa. A feather-light web framework for professional static websites.
 *
 * @author Tobias Köngeter
 * @copyright Copyright © Bit&Black
 * @link https://www.bitandblack.com
 * @link https://www.kiwa.io
 * @license MIT
 */

namespace Kiwa\Error;

use Stringable;

readonly class ErrorName implements Stringable
{
    private string $errorName;

    /**
     * @param int $errorLevel
     */
    public function __construct(int $errorLevel)
    {
        $errorName = 'UNKNOWN';

        switch ($errorLevel) {
            case E_ERROR:
            case E_CORE_ERROR:
            case E_COMPILE_ERROR:
            case E_PARSE:
                $errorName = 'FATAL';
                break;

            case E_USER_ERROR:
            case E_RECOVERABLE_ERROR:
                $errorName = 'ERROR';
                break;

            case E_WARNING:
            case E_CORE_WARNING:
            case E_COMPILE_WARNING:
            case E_USER_WARNING:
                $errorName = 'WARNING';
                break;

            case E_NOTICE:
            case E_USER_NOTICE:
            case E_DEPRECATED:
            case E_USER_DEPRECATED:
                $errorName = 'NOTICE';
                break;

            case E_STRICT:
                $errorName = 'DEBUG';
                break;
        }

        $this->errorName = $errorName;
    }

    public function __toString(): string
    {
        return $this->getErrorName();
    }

    /**
     * @return string
     */
    public function getErrorName(): string
    {
        return $this->errorName;
    }
}
