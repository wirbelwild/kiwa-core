<?php

/**
 * Kiwa. A feather-light web framework for professional static websites.
 *
 * @author Tobias Köngeter
 * @copyright Copyright © Bit&Black
 * @link https://www.bitandblack.com
 * @link https://www.kiwa.io
 * @license MIT
 */

namespace Kiwa\File;

use BitAndBlack\PathInfo\PathInfo;
use DateTime;
use Exception;
use JsonException;
use Kiwa\DI;
use Kiwa\Path;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Mime\MimeTypes;

/**
 * The BinaryFile class holds information about a binary file.
 *
 * @package Kiwa\File
 * @see \Kiwa\Tests\File\BinaryFileTest
 */
final class BinaryFile extends AbstractFile
{
    private string $filePath = '';

    private int $status;
    
    private ?string $mimeType = null;

    private ?DateTime $lastModified = null;

    /**
     * Creates a new binary file object.
     *
     * @param string $file Full path to the file.
     */
    public function __construct(string $file)
    {
        $this->status = Response::HTTP_OK;
        
        $extension = PathInfo::createFromFile($file)->getExtension();

        if (null === $extension) {
            $this->status = Response::HTTP_FORBIDDEN;
            return;
        }

        if (null === $fileNameReal = $this->getFileNameIfExists($file)) {
            DI::getLog()->debug('File doesn\'t exist at "' . $file . '".');
            $this->status = Response::HTTP_NOT_FOUND;
            return;
        }

        $this->filePath = $fileNameReal;
        
        $mimeTypes = MimeTypes::getDefault()->getMimeTypes($extension);
        $this->mimeType = $mimeTypes[0] ?? 'application/octet-stream';

        if (false !== $lastModified = DateTime::createFromFormat('U', (string) @filemtime($fileNameReal))) {
            $this->lastModified = $lastModified;
        }
    }

    /**
     * Returns a files name if it exists.
     * This method is case insensitive.
     *
     * @param string $fileName
     * @return null|string
     */
    private function getFileNameIfExists(string $fileName): ?string
    {
        $fileName = (string) parse_url($fileName, PHP_URL_PATH);

        if (is_link($fileName)) {
            $fileName = (string) readlink($fileName);
        }
        
        if (file_exists($fileName)) {
            return $fileName;
        }

        $directoryName = dirname($fileName);
        $files = glob($directoryName . DIRECTORY_SEPARATOR . '*', GLOB_NOSORT);
        $fileNameLowerCase = mb_strtolower($fileName);

        if (!is_array($files)) {
            return null;
        }

        foreach ($files as $file) {
            if (mb_strtolower($file) === $fileNameLowerCase) {
                return $file;
            }
        }

        return null;
    }
    
    /**
     * Reads a file.
     *
     * @return string|null
     */
    public function readFile(): ?string
    {
        if (null === $file = $this->getFileNameIfExists($this->getFilePath())) {
            return null;
        }

        $output = (string) file_get_contents($file);

        if (str_contains($file, '.php')) {
            try {
                ob_start();
                include $file;
                $output = (string) ob_get_clean();
            } catch (Exception $exception) {
                $output = (string) $exception;
                $output .= PHP_EOL . PHP_EOL;
                $output .= ob_get_clean();
            }
        }

        return $output;
    }

    /**
     * Gets the full path to the file.
     *
     * @return string
     */
    public function getFilePath(): string
    {
        return $this->filePath;
    }

    /**
     * Gets the http status code of the file.
     *
     * @return int
     */
    public function getStatus(): int
    {
        return $this->status;
    }

    /**
     * Gets the mime type of the file.
     *
     * @return string|null
     */
    public function getMimeType(): ?string
    {
        return $this->mimeType;
    }

    /**
     * @return bool
     */
    public function isVersionedFile(): bool
    {
        $manifestJson = Path::getBuildFolder() . DIRECTORY_SEPARATOR . 'manifest.json';

        if (!file_exists($manifestJson)) {
            return false;
        }
        
        $manifest = (string) file_get_contents($manifestJson);

        try {
            $manifest = json_decode($manifest, true, 512, JSON_THROW_ON_ERROR);
        } catch (JsonException) {
        }

        if (!is_array($manifest)) {
            $manifest = [];
        }
        
        $file = PathInfo::createFromFile($this->getFilePath())->getBasename();
        
        foreach ($manifest as $shortName => $versionedName) {
            $shortName = PathInfo::createFromFile($shortName)->getBasename();
            $versionedName = PathInfo::createFromFile($versionedName)->getBasename();
            
            if ($versionedName === $file) {
                return $shortName !== $versionedName;
            }
        }
        
        return false;
    }

    /**
     * @return DateTime|null
     */
    public function getLastModified(): ?DateTime
    {
        return $this->lastModified;
    }
}
