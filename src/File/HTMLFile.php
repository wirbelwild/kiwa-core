<?php

/**
 * Kiwa. A feather-light web framework for professional static websites.
 *
 * @author Tobias Köngeter
 * @copyright Copyright © Bit&Black
 * @link https://www.bitandblack.com
 * @link https://www.kiwa.io
 * @license MIT
 */

namespace Kiwa\File;

use BitAndBlack\PathInfo\PathInfo;
use DateTime;
use Exception;
use Kiwa\Config\BaseConfigLoader;
use Kiwa\DI;
use Kiwa\ErrorHandler;
use Kiwa\Page\PageTitle;
use Kiwa\Path;
use Kiwa\Response\ErrorPage;
use Stringable;
use Symfony\Component\HttpFoundation\Response;

/**
 * The HTMLFile class holds information about an HTML file.
 *
 * @package Kiwa\File
 * @see \Kiwa\Tests\File\HTMLFileTest
 */
final class HTMLFile extends AbstractFile implements Stringable
{
    /**
     * @var string
     */
    private readonly string $output;
    
    /**
     * @var DateTime|null
     */
    private ?DateTime $lastModified = null;
    
    /**
     * @var int
     */
    private int $status = Response::HTTP_OK;

    /**
     * @var bool
     */
    private bool $isCachable;

    private bool $fileExists = true;

    private bool $loadedWithError = false;

    /**
     * Creates a new page file object.
     *
     * @param string $name       The name of the `phtml` file.
     * @param bool $mayBeCached  If the page may be cached.
     * @param int $cacheLifetime The lifetime of the cache file.
     */
    public function __construct(string $name, bool $mayBeCached = false, int $cacheLifetime = -1)
    {
        $output = $this->handleFileLoad($name, $mayBeCached, $cacheLifetime);

        if (false === $this->fileExists) {
            $this->status = Response::HTTP_NOT_FOUND;
        }

        if (true === $this->loadedWithError) {
            $this->status = Response::HTTP_INTERNAL_SERVER_ERROR;
        }

        /**
         * If no HTML could be loaded, there was an error/exception. So we set the status to 500
         * and try to load an error page for that status code.
         */
        if (null === $output && true === $this->loadedWithError) {
            DI::getLog()->debug('An error occurred while loading the file\'s content.');
            $output = $this->handleFileLoad((string) $this->status, false, -1);
        }

        /**
         * If the output is still null, we have a missing file here.
         */
        if (null === $output && false === $this->fileExists) {
            DI::getLog()->debug('Loading system error page (status code ' . $this->status . ')');
            PageTitle::setTitle((string) $this->status);
            $output = new ErrorPage($this->status);
        }

        $this->output = (string) $output;
    }

    /**
     * @param string $name
     * @param bool $mayBeCached
     * @param int $cacheLifetime
     * @return string|null
     */
    private function handleFileLoad(string $name, bool $mayBeCached = false, int $cacheLifetime = -1): ?string
    {
        $pathOrigin = Path::getHTMLFolder() . DIRECTORY_SEPARATOR . $name . '.phtml';
        
        $request = DI::getRequest()->server->get('REQUEST_URI') ?? '/';
        
        if (!is_string($request)) {
            $request = '/';
        }
        
        $request = urlencode($request);
        
        if ('html' !== PathInfo::createFromFile($request)->getExtension()) {
            $request .= '.html';
        }
        
        $pathCached = Path::getCacheFolder() . DIRECTORY_SEPARATOR . $request;

        DI::getLog()->debug('Trying to load page ' . var_export($name, true) . '.');

        $isProduction = 'prod' === BaseConfigLoader::create()->getConfig()['APP_ENV'];

        $postData = DI::getRequest()->request->getIterator();
        $postData = iterator_to_array($postData);
        $isPostRequest = !empty($postData);
        $mayBeCached = !$isPostRequest && $mayBeCached;
        $cacheFileExists = file_exists($pathCached);
        
        if ($cacheFileExists && -1 !== $cacheLifetime) {
            $timeModified = filemtime($pathCached);
            $now = time();
            
            if ($timeModified + $cacheLifetime < $now) {
                unlink($pathCached);
                $cacheFileExists = false;
            }
        }
        
        $this->isCachable = $mayBeCached && $isProduction;
        
        if ($isProduction && !$isPostRequest && $cacheFileExists) {
            $htmlOutput = $this->includeFile($pathCached);
            $this->loadedWithError = true;

            if (null !== $htmlOutput) {
                DI::getLog()->debug('Serving cached page.');
                $this->loadedWithError = false;
            }

            return $htmlOutput;
        }

        if (!file_exists($pathOrigin)) {
            DI::getLog()->debug('File "' . $pathOrigin . '" doesn\'t exist');
            $this->fileExists = false;
            return null;
        }

        $htmlOutput = $this->includeFile($pathOrigin);

        if (null === $htmlOutput) {
            $this->loadedWithError = true;
            return null;
        }

        if (true === $isProduction && $mayBeCached) {
            file_put_contents($pathCached, $htmlOutput);
        }

        DI::getLog()->debug('Serving uncached page' . ($isPostRequest ? ' (POST request detected)' : '') . '.');
        return $htmlOutput;
    }

    /**
     * Returns the content as string.
     *
     * @return string
     */
    public function __toString(): string
    {
        return $this->output;
    }

    /**
     * @return DateTime|null
     */
    public function getLastModified(): ?DateTime
    {
        return $this->lastModified;
    }

    /**
     * @return int
     */
    public function getStatus(): int
    {
        return $this->status;
    }
    
    /**
     * @param string $file
     * @return string|null
     */
    private function includeFile(string $file): ?string
    {
        if (false !== $lastModified = DateTime::createFromFormat('U', (string) filemtime($file))) {
            $this->lastModified = $lastModified;
        }

        try {
            $obHandlers = ob_list_handlers();

            in_array('ob_gzhandler', $obHandlers, true)
                ? ob_start('ob_gzhandler')
                : ob_start()
            ;

            /**
             * Include the file scoped without access to other variables.
             */
            (static function () use ($file) {
                require $file;
            })();

            return (string) ob_get_clean();
        } catch (Exception $exception) {
            ob_end_flush();

            /** @var ErrorHandler $errorHandler */
            $errorHandler = DI::getInstance(ErrorHandler::class);
            $errorHandler->errorHandler(
                E_ERROR,
                $exception->getMessage(),
                $exception->getFile(),
                $exception->getLine(),
            );

            DI::getLog()->error(
                $exception->getMessage()
            );
        }

        return null;
    }

    /**
     * @return bool
     */
    public function isCachable(): bool
    {
        return $this->isCachable;
    }
}
