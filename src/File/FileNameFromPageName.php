<?php

/**
 * Kiwa. A feather-light web framework for professional static websites.
 *
 * @author Tobias Köngeter
 * @copyright Copyright © Bit&Black
 * @link https://www.bitandblack.com
 * @link https://www.kiwa.io
 * @license MIT
 */

namespace Kiwa\File;

use Kiwa\Page\PageList;
use Stringable;

/**
 * The FileNameFromPageName class searches for the file which belongs to a page.
 *
 * @package Kiwa\File
 */
final class FileNameFromPageName implements Stringable
{
    private static string $fileName;

    /**
     * Creates a new instance.
     *
     * @param string $pageName The name of the page.
     */
    public function __construct(string $pageName)
    {
        $pageList = new PageList();
        $pages = $pageList
            ->hasPageName($pageName)
        ;

        $pages = iterator_to_array($pages);
        $pages = array_values($pages);

        $fileName = 'index';
        
        if (count($pages) > 0) {
            $fileName = $pages[0]->getFile() ?? $fileName;
        }
        
        self::$fileName = $fileName;
    }

    /**
     * Creates a new instance.
     *
     * @param string $pageName The name of the page.
     * @return FileNameFromPageName
     */
    public static function create(string $pageName): self
    {
        return new self($pageName);
    }

    /**
     * Returns the file name.
     *
     * @return string|null
     */
    public function getFileName(): ?string
    {
        return self::$fileName;
    }

    /**
     * Returns the file name.
     *
     * @return string
     */
    public function __toString(): string
    {
        return (string) $this->getFileName();
    }
}
