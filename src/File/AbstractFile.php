<?php

/**
 * Kiwa. A feather-light web framework for professional static websites.
 *
 * @author Tobias Köngeter
 * @copyright Copyright © Bit&Black
 * @link https://www.bitandblack.com
 * @link https://www.kiwa.io
 * @license MIT
 */

namespace Kiwa\File;

use DateTime;

/**
 * The AbstractFile class defines some basics for all file classes.
 *
 * @package Kiwa\File
 */
abstract class AbstractFile
{
    /**
     * @return DateTime|null
     */
    abstract public function getLastModified(): ?DateTime;

    /**
     * Gets the http status code of the file.
     *
     * @return int
     */
    abstract public function getStatus(): int;
}
