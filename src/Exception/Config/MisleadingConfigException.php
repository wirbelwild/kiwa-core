<?php

/**
 * Kiwa. A feather-light web framework for professional static websites.
 *
 * @author Tobias Köngeter
 * @copyright Copyright © Bit&Black
 * @link https://www.bitandblack.com
 * @link https://www.kiwa.io
 * @license MIT
 */

namespace Kiwa\Exception\Config;

use Kiwa\Exception;

/**
 * Class MisleadingConfigException.
 *
 * @package Kiwa\Exception\Config
 */
class MisleadingConfigException extends Exception
{
    /**
     * MisleadingConfigException constructor.
     *
     * @param string $message
     */
    public function __construct(string $message)
    {
        parent::__construct($message);
    }
}
