<?php

/**
 * Kiwa. A feather-light web framework for professional static websites.
 *
 * @author Tobias Köngeter
 * @copyright Copyright © Bit&Black
 * @link https://www.bitandblack.com
 * @link https://www.kiwa.io
 * @license MIT
 */

namespace Kiwa\Exception\Config;

use Kiwa\Exception;
use ReflectionClass;

/**
 * Class MethodNotAvailableException
 *
 * @package Kiwa\TemplateConsole
 */
class MethodNotAvailableException extends Exception
{
    /**
     * MethodNotAvailableException constructor.
     *
     * @param string $methodName
     * @param ReflectionClass<object> $classReflected
     */
    public function __construct(string $methodName, ReflectionClass $classReflected)
    {
        parent::__construct(
            'Method "' . $methodName . '" is not available in class "' . $classReflected->getName() . '"'
        );
    }
}
