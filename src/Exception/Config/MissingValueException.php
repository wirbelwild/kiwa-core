<?php

/**
 * Kiwa. A feather-light web framework for professional static websites.
 *
 * @author Tobias Köngeter
 * @copyright Copyright © Bit&Black
 * @link https://www.bitandblack.com
 * @link https://www.kiwa.io
 * @license MIT
 */

namespace Kiwa\Exception\Config;

use Kiwa\Exception;

/**
 * Class MissingValueException.
 *
 * @package Kiwa\Exception\Config
 */
class MissingValueException extends Exception
{
    /**
     * MissingValueException constructor.
     *
     * @param string $parameter
     */
    public function __construct(string $parameter)
    {
        parent::__construct('The value "' . $parameter . '" is required but not set here.');
    }
}
