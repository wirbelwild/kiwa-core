<?php

/**
 * Kiwa. A feather-light web framework for professional static websites.
 *
 * @author Tobias Köngeter
 * @copyright Copyright © Bit&Black
 * @link https://www.bitandblack.com
 * @link https://www.kiwa.io
 * @license MIT
 */

namespace Kiwa\Exception\Config;

use Kiwa\Exception;
use Throwable;

/**
 * Class SetupNotPossibleException
 *
 * @package Kiwa\TemplateConsole
 */
class SetupNotPossibleException extends Exception
{
    /**
     * SetupNotPossibleException constructor.
     *
     * @param string $methodName
     * @param string $className
     * @param Throwable|null $previous
     */
    public function __construct(string $methodName, string $className, ?Throwable $previous = null)
    {
        parent::__construct(
            'Method "' . $methodName . '" is not available in class "' . $className . '"',
            previous: $previous,
        );
    }
}
