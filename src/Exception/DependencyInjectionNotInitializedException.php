<?php

/**
 * Kiwa. A feather-light web framework for professional static websites.
 *
 * @author Tobias Köngeter
 * @copyright Copyright © Bit&Black
 * @link https://www.bitandblack.com
 * @link https://www.kiwa.io
 * @license MIT
 */

namespace Kiwa\Exception;

use Kiwa\Exception;

/**
 * Class DependencyInjectionNotInitializedException.
 *
 * @package Kiwa\Exception
 */
class DependencyInjectionNotInitializedException extends Exception
{
    /**
     * DependencyInjectionNotInitializedException constructor.
     */
    public function __construct()
    {
        parent::__construct('Dependency Injection is not configured. This may happen when no Controller object has been created before.');
    }
}
