<?php

/**
 * Kiwa. A feather-light web framework for professional static websites.
 *
 * @author Tobias Köngeter
 * @copyright Copyright © Bit&Black
 * @link https://www.bitandblack.com
 * @link https://www.kiwa.io
 * @license MIT
 */

namespace Kiwa\Config;

use Kiwa\Exception\Config\SetupNotPossibleException;
use ReflectionClass;
use ReflectionException;

/**
 * The PropertyHandler class reflects classes and sets their properties.
 * This is used to prepare classes for later use.
 * @see \Kiwa\Tests\Config\PropertyHandlerTest
 */
class PropertyHandler
{
    /**
     * @param class-string $class
     * @param string $propertyName
     * @return void
     * @throws SetupNotPossibleException
     */
    public static function set(string $class, string $propertyName, mixed $value): void
    {
        try {
            $reflectionClass = new ReflectionClass($class);
            $property = $reflectionClass->getProperty($propertyName);
        } catch (ReflectionException $reflectionException) {
            throw new SetupNotPossibleException($propertyName, $class, $reflectionException);
        }
        
        $property->setAccessible(true);
        
        if ($property->isStatic()) {
            $reflectionClass->setStaticPropertyValue($propertyName, $value);
            return;
        }
        
        $property->setValue($reflectionClass, $value);
    }
}
