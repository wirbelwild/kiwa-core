<?php

/**
 * Kiwa. A feather-light web framework for professional static websites.
 *
 * @author Tobias Köngeter
 * @copyright Copyright © Bit&Black
 * @link https://www.bitandblack.com
 * @link https://www.kiwa.io
 * @license MIT
 */

namespace Kiwa\Config;

use Stringable;

/**
 * Class Htaccess
 *
 * @package Kiwa
 * @see \Kiwa\Tests\Config\HtaccessContentTest
 */
class HtaccessContent implements Stringable
{
    /**
     * Htaccess data
     *
     * @var array<string, string>
     */
    private array $htaccessParts = [
        'rewriteStart' =>
            '<IfModule mod_rewrite.c>' . PHP_EOL .
            '    RewriteEngine On' . PHP_EOL .
            '    RewriteBase /' . PHP_EOL . PHP_EOL,

        'withSSL' =>
            '    RewriteCond %{HTTPS} off' . PHP_EOL .
            '    RewriteRule .* https://%{HTTP_HOST}%{REQUEST_URI} [L,R=301]' . PHP_EOL . PHP_EOL,

        'withoutSSL' =>
            '    RewriteCond %{HTTPS} on' . PHP_EOL .
            '    RewriteRule .* http://%{HTTP_HOST}%{REQUEST_URI} [L,R=301]' . PHP_EOL . PHP_EOL,

        'withWWW' =>
            '    RewriteCond %{HTTP_HOST} !^www\. [NC]' . PHP_EOL,

        'withWWWwithSSL' =>
            '    RewriteRule .* https://www.%{HTTP_HOST}%{REQUEST_URI} [L,R=301]' . PHP_EOL . PHP_EOL,

        'withWWWwithoutSSL' =>
            '    RewriteRule .* http://www.%{HTTP_HOST}%{REQUEST_URI} [L,R=301]' . PHP_EOL . PHP_EOL,

        'withoutWWW' =>
            '    RewriteCond %{HTTP_HOST} ^www\.(.+)$ [NC]' . PHP_EOL,

        'withoutWWWwithSSL' =>
            '    RewriteRule ^ https://%1%{REQUEST_URI} [L,R=301]' . PHP_EOL . PHP_EOL,

        'withoutWWWwithoutSSL' =>
            '    RewriteRule ^ http://%1%{REQUEST_URI} [L,R=301]' . PHP_EOL . PHP_EOL,

        'rewriteEnd' =>
            '    RewriteRule ^ index.php [L]' . PHP_EOL .
            '</IfModule>' . PHP_EOL . PHP_EOL,

        'deflates' =>
            '<IfModule mod_deflate.c>' . PHP_EOL
            . '    AddOutputFilterByType DEFLATE application/javascript' . PHP_EOL
            . '    AddOutputFilterByType DEFLATE application/rss+xml' . PHP_EOL
            . '    AddOutputFilterByType DEFLATE application/vnd.ms-fontobject' . PHP_EOL
            . '    AddOutputFilterByType DEFLATE application/x-font' . PHP_EOL
            . '    AddOutputFilterByType DEFLATE application/x-font-opentype' . PHP_EOL
            . '    AddOutputFilterByType DEFLATE application/x-font-otf' . PHP_EOL
            . '    AddOutputFilterByType DEFLATE application/x-font-truetype' . PHP_EOL
            . '    AddOutputFilterByType DEFLATE application/x-font-ttf' . PHP_EOL
            . '    AddOutputFilterByType DEFLATE application/x-javascript' . PHP_EOL
            . '    AddOutputFilterByType DEFLATE application/xhtml+xml' . PHP_EOL
            . '    AddOutputFilterByType DEFLATE application/xml' . PHP_EOL
            . '    AddOutputFilterByType DEFLATE font/opentype' . PHP_EOL
            . '    AddOutputFilterByType DEFLATE font/otf' . PHP_EOL
            . '    AddOutputFilterByType DEFLATE font/ttf' . PHP_EOL
            . '    AddOutputFilterByType DEFLATE image/svg+xml' . PHP_EOL
            . '    AddOutputFilterByType DEFLATE image/x-icon' . PHP_EOL
            . '    AddOutputFilterByType DEFLATE text/css' . PHP_EOL
            . '    AddOutputFilterByType DEFLATE text/html' . PHP_EOL
            . '    AddOutputFilterByType DEFLATE text/javascript' . PHP_EOL
            . '    AddOutputFilterByType DEFLATE text/plain' . PHP_EOL
            . '    AddOutputFilterByType DEFLATE text/xml' . PHP_EOL
            . '</IfModule>' . PHP_EOL
        ,

        'useHtpasswd' =>
            'AuthType Basic' . PHP_EOL
            . 'AuthName "Protected Area"' . PHP_EOL
            . 'AuthUserFile {{PATH}}' . PHP_EOL
            . 'Require valid-user' . PHP_EOL
        ,
    ];
    
    /**
     * @var array<string, bool|string>
     */
    private array $settings = [
        'useSSL' => false,
        'useWWW' => false,
        'useHtpasswd' => false,
    ];
    
    private string $content = '';

    /**
     * @param array<string, bool|string> $settings
     */
    public function __construct(array $settings = [])
    {
        $this->settings = array_merge($this->settings, $settings);
        $this->createContent();
    }

    /**
     * @return self
     */
    private function createContent(): self
    {
        $useSSL = 'withSSL';
        $useWWW = 'withWWW';

        if (false === $this->settings['useSSL']) {
            $useSSL = 'withoutSSL';
        }

        if (false === $this->settings['useWWW']) {
            $useWWW = 'withoutWWW';
        }

        $content = PHP_EOL
            . '# This part has been created by Kiwa on ' . date('Y-m-d H:i:s') . '.' . PHP_EOL
            . '# It contains two parts: the first, where the SSL and subdomain handling is made, based on your config.' . PHP_EOL
            . '# The second part causes your server to compress files, to serve them faster.' . PHP_EOL
            . PHP_EOL
        ;
        
        if (false !== $path = $this->settings['useHtpasswd']) {
            $content .= str_replace('{{PATH}}', (string) $path, $this->htaccessParts['useHtpasswd']);
        }
        
        $content .= $this->htaccessParts['rewriteStart'];
        $content .= $this->htaccessParts[$useSSL];
        $content .= $this->htaccessParts[$useWWW];
        $content .= $this->htaccessParts[$useWWW . $useSSL];
        $content .= $this->htaccessParts['rewriteEnd'];
        $content .= $this->htaccessParts['deflates'];
        
        $this->content = $content;
        return $this;
    }

    /**
     * @return string
     */
    public function getContent(): string
    {
        return $this->content;
    }
    
    /**
     * @return string
     */
    public function __toString(): string
    {
        return $this->getContent();
    }
}
