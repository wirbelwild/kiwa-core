<?php

/**
 * Kiwa. A feather-light web framework for professional static websites.
 *
 * @author Tobias Köngeter
 * @copyright Copyright © Bit&Black
 * @link https://www.bitandblack.com
 * @link https://www.kiwa.io
 * @license MIT
 */

namespace Kiwa\Config;

/**
 * The Page class holds the configuration of a page.
 *
 * @package Kiwa\Config
 */
class Page
{
    private string $name = '';
    
    private string $languageCode = '';

    private ?string $getFile = null;

    private ?string $childOf = null;

    private ?string $hasDynamicChild = null;

    private ?string $pageTitle = null;

    /**
     * @var array<string, string>
     */
    private array $metaTags = [];

    /**
     * @var bool
     */
    private bool $enableCache = false;

    /**
     * @var int
     */
    private int $cacheLifetime = -1;

    /**
     * Page constructor.
     *
     * @param array{
     *     name?: string,
     *     getFile?: string,
     *     languageCode?: string,
     *     pageTitle?: string,
     *     metaTags?: array<string, string>,
     *     hasDynamicChild?: bool|string,
     *     childOf?: string,
     *     enableCache?: bool,
     *     cacheLifetime?: int
     * } $config
     */
    public function __construct(array $config)
    {
        foreach ($config as $key => $value) {
            if (!property_exists($this, $key)) {
                continue;
            }
            
            /**
             * @phpstan-ignore-next-line
             * @todo Find better way.
             */
            $this->{$key} = $value;
            
            if (false === $value
                && ('childOf' === $key || 'hasDynamicChild' === $key)
            ) {
                $this->{$key} = null;
            }
        }
    }

    /**
     * @return array<string, mixed>
     */
    public function toArray(): array
    {
        return [
            'name' => $this->getName(),
            'languageCode' => $this->getLanguageCode(),
            'getFile' => $this->getFile(),
            'childOf' => $this->getChildOf(),
            'hasDynamicChild' => $this->getDynamicChild(),
            'pageTitle' => $this->getPageTitle(),
            'metaTags' => $this->getMetaTags(),
            'enableCache' => $this->isCacheEnabled(),
            'cacheLifetime' => $this->getCacheLifetime(),
        ];
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @return string
     */
    public function getLanguageCode(): string
    {
        return $this->languageCode;
    }

    /**
     * @return string|null
     */
    public function getFile(): ?string
    {
        return $this->getFile;
    }

    /**
     * @return string|null
     */
    public function getChildOf(): ?string
    {
        return $this->childOf;
    }

    /**
     * @return string|null
     */
    public function getDynamicChild(): ?string
    {
        return $this->hasDynamicChild;
    }

    /**
     * @return string|null
     */
    public function getPageTitle(): ?string
    {
        return $this->pageTitle;
    }

    /**
     * @return string[]
     */
    public function getMetaTags(): array
    {
        return $this->metaTags;
    }

    /**
     * @return bool
     */
    public function isCacheEnabled(): bool
    {
        return $this->enableCache;
    }

    /**
     * @return int
     */
    public function getCacheLifetime(): int
    {
        return $this->cacheLifetime;
    }
}
