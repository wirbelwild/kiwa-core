<?php

/**
 * Kiwa. A feather-light web framework for professional static websites.
 *
 * @author Tobias Köngeter
 * @copyright Copyright © Bit&Black
 * @link https://www.bitandblack.com
 * @link https://www.kiwa.io
 * @license MIT
 */

namespace Kiwa\Config;

use Kiwa\Path;
use RuntimeException;

/**
 * Handles the root .htaccess file.
 */
class HtaccessFile
{
    /**
     * @return string
     */
    private static function getFile(): string
    {
        return Path::getPublicFolder() . DIRECTORY_SEPARATOR . '.htaccess';
    }
    
    /**
     * @return HtaccessContent
     */
    public static function read(): HtaccessContent
    {
        $content = (string) file_get_contents(self::getFile());
        $contentParts = explode(PHP_EOL, $content);

        /** @var non-empty-array<string, string> $contentMapped */
        $contentMapped = [];
        
        foreach ($contentParts as $contentPart) {
            $contentPartParts = explode('=', $contentPart);
            $key = trim($contentPartParts[0]);
            $value = trim($contentPartParts[0]);
            $contentMapped[$key] = $value;
        }
        
        return new HtaccessContent($contentMapped);
    }
    
    /**
     * @param HtaccessContent $htaccess
     * @return bool
     * @deprecated Please use {@see HtaccessFile::add()} instead.
     * @todo Remove in v2.0.
     */
    public static function write(HtaccessContent $htaccess): bool
    {
        if (!file_exists(Path::getPublicFolder()) && !mkdir(Path::getPublicFolder(), 0777, true) && !is_dir(Path::getPublicFolder())) {
            throw new RuntimeException(
                sprintf('Directory "%s" was not created', Path::getPublicFolder())
            );
        }

        $created = file_put_contents(
            self::getFile(),
            (string) $htaccess
        );

        return false !== $created;
    }

    /**
     * Adds some content to a `.htaccess` file.
     * The namespace will be used as a unique wrapper around the actual content.
     * This allows Kiwa to update the content without touching the rest of the file.
     *
     * @param string $content Some content for the `.htaccess` file.
     * @param string $namespace A unique identifier. This can be a libraries name, like `kiwa/core`.
     * @return bool
     */
    public static function add(string $content, string $namespace): bool
    {
        if (!file_exists(Path::getPublicFolder()) && !mkdir(Path::getPublicFolder(), 0777, true) && !is_dir(Path::getPublicFolder())) {
            throw new RuntimeException(
                sprintf('Directory "%s" was not created', Path::getPublicFolder())
            );
        }

        touch(self::getFile());

        $htaccessContent = (string) file_get_contents(
            self::getFile()
        );

        $openingPart = '###> ' . $namespace . ' ###';
        $closingPart = '###< ' . $namespace . ' ###';

        $pattern = '{' . $openingPart . '.*' . $closingPart . '}s';

        $content = $openingPart . PHP_EOL
            . $content . PHP_EOL
            . $closingPart
        ;

        $htaccessContent = preg_replace(
            $pattern,
            $content,
            $htaccessContent,
            -1,
            $count
        );

        if (0 === $count) {
            $htaccessContent .= $content . PHP_EOL;
        }

        return false !== file_put_contents(
            self::getFile(),
            $htaccessContent
        );
    }
}
