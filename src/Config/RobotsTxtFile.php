<?php

/**
 * Kiwa. A feather-light web framework for professional static websites.
 *
 * @author Tobias Köngeter
 * @copyright Copyright © Bit&Black
 * @link https://www.bitandblack.com
 * @link https://www.kiwa.io
 * @license MIT
 */

namespace Kiwa\Config;

use Kiwa\Path;

/**
 * Handles the robots.txt file
 *
 * @package Kiwa\Writer
 */
class RobotsTxtFile
{
    /**
     * @return RobotsTxtContent
     */
    public static function read(): RobotsTxtContent
    {
        $file = Path::getCrawlerFolder() . DIRECTORY_SEPARATOR . 'robots.txt';
        $content = file_get_contents($file);
        $contentParts = explode(PHP_EOL, (string) $content);
        $contentPartsWithKeys = [];

        foreach ($contentParts as $contentPart) {
            $contentPartParts = explode(': ', $contentPart);
            $key = trim($contentPartParts[0]);
            $value = trim($contentPartParts[1]);
            $contentPartsWithKeys[$key] = $value;
        }

        return new RobotsTxtContent($contentPartsWithKeys);
    }
    
    /**
     * @param RobotsTxtContent $robotsTxtContent
     * @return bool
     */
    public static function write(RobotsTxtContent $robotsTxtContent): bool
    {
        $file = Path::getCrawlerFolder() . DIRECTORY_SEPARATOR . 'robots.txt';
        return false !== file_put_contents($file, (string) $robotsTxtContent);
    }
}
