<?php

/**
 * Kiwa. A feather-light web framework for professional static websites.
 *
 * @author Tobias Köngeter
 * @copyright Copyright © Bit&Black
 * @link https://www.bitandblack.com
 * @link https://www.kiwa.io
 * @license MIT
 */

namespace Kiwa\Config;

use Stringable;

/**
 * Class RobotsTxtContent
 *
 * @package Kiwa\Config
 * @see \Kiwa\Tests\Config\RobotsTxtContentTest
 */
class RobotsTxtContent implements Stringable
{
    /**
     * robots.txt data
     *
     * @var array<string, bool|string>
     */
    private array $settings = [
        'allowAll' => true,
    ];

    private string $content = '';

    /**
     * @param array<string, bool|string> $settings
     */
    public function __construct(array $settings = [])
    {
        $this->settings = array_merge($this->settings, $settings);
        $this->createContent();
    }

    /**
     * @return self
     */
    private function createContent(): self
    {
        $content = '';
        
        if (true === $this->settings['allowAll']) {
            $content .=
                'User-agent: *' . PHP_EOL .
                'Allow: /' . PHP_EOL
            ;
        }

        if (isset($this->settings['sitemap'])) {
            $content .= $this->settings['sitemap'];
        }

        $this->content = $content;
        return $this;
    }
    
    /**
     * @return string
     */
    public function getContent(): string
    {
        return $this->content;
    }

    /**
     * @return string
     */
    public function __toString(): string
    {
        return $this->getContent();
    }
}
