<?php

/**
 * Kiwa. A feather-light web framework for professional static websites.
 *
 * @author Tobias Köngeter
 * @copyright Copyright © Bit&Black
 * @link https://www.bitandblack.com
 * @link https://www.kiwa.io
 * @license MIT
 */

namespace Kiwa\Config;

use BitAndBlack\Helpers\StringHelper;
use Kiwa\Config;
use Kiwa\Path;
use Symfony\Component\Dotenv\Dotenv;
use Symfony\Component\Yaml\Yaml;

/**
 * Class BaseConfigLoader
 *
 * @package Kiwa
 */
final class BaseConfigLoader
{
    /**
     * @var array{
     *     APP_ENV: string,
     *     usePageSuffix?: bool|string,
     *     pages: array<string, array{
     *         name?: string,
     *         getFile?: string,
     *         languageCode?: string,
     *         pageTitle?: string,
     *         metaTags?: array<string, string>,
     *         hasDynamicChild?: bool|string,
     *         childOf?: string,
     *         enableCache?: bool,
     *         cacheLifetime?: int
     *     }>,
     *     urlStructure?: array<int, string>,
     *     mainURL?: string,
     *     languageHandling?: string,
     *     htaccess?: array{
     *         useWWW: bool,
     *         useSSL: bool
     *     },
     *     languageCode?: string,
     *     baseMetaTags?: array<string, string|array<string, string>>,
     *     cacheControl?: array<string, int>,
     *     useWWW?: bool,
     *     useSSL?: bool,
     * }
     */
    private static array $config;

    /**
     * @var bool
     */
    private static bool $isLoaded = false;

    /**
     * BaseConfigLoader constructor.
     */
    public function __construct()
    {
        if (true === self::$isLoaded) {
            return;
        }

        self::$config['APP_ENV'] = 'prod';

        $configFilePHP = Path::getConfigFolder() . DIRECTORY_SEPARATOR . 'config.php';
        $configFileYaml = Path::getConfigFolder() . DIRECTORY_SEPARATOR . 'config.yaml';
        $config = [];

        if (file_exists($configFilePHP)) {
            $config = include $configFilePHP;
        } elseif (file_exists($configFileYaml)) {
            $config = Yaml::parseFile($configFileYaml);
        }

        /**
         * @phpstan-ignore-next-line
         * @todo Find better way.
         */
        self::$config = array_merge(self::$config, $config);

        self::$config['pages'] ??= [];

        $this->addEnvVars();
        $this->addLanguageCodes();

        self::$isLoaded = true;
    }

    /**
     * @return BaseConfigLoader
     */
    public static function create(): self
    {
        return new self();
    }

    /**
     * Loads environment variables and adds them to the loaded config.
     * This values won't get saved.
     * The .env file is only a template and will be ignored.
     *
     * @return BaseConfigLoader
     */
    private function addEnvVars(): self
    {
        $serverEnv = [];
        
        if (isset($_SERVER['ENV']) && is_array($_SERVER['ENV'])) {
            $serverEnv = $_SERVER['ENV'];
        }
        
        foreach ($serverEnv as $key => $value) {
            /**
             * @phpstan-ignore-next-line
             * @todo Find better way.
             */
            self::$config[$key] = $value;
        }

        foreach ($_ENV as $key => $value) {
            /**
             * @phpstan-ignore-next-line
             * @todo Find better way.
             */
            self::$config[$key] = $value;
        }

        $envFiles = [];
        $dotEnv = Path::getRootFolder() . DIRECTORY_SEPARATOR . '.env';
        $dotEnvLocal = Path::getRootFolder() . DIRECTORY_SEPARATOR . '.env.local';
        $dotEnvTest = Path::getRootFolder() . DIRECTORY_SEPARATOR . '.env.test';
        $dotEnvTestLocal = Path::getRootFolder() . DIRECTORY_SEPARATOR . '.env.test.local';
        
        if (file_exists($dotEnv)) {
            $envFiles[] = $dotEnv;
        }
        
        if (file_exists($dotEnvLocal)) {
            $envFiles[] = $dotEnvLocal;
        }
        
        if ('active' === ($_ENV['TEST_MODE'] ?? null)) {
            if (file_exists($dotEnvTest)) {
                $envFiles[] = $dotEnvTest;
            }
            
            if (file_exists($dotEnvTestLocal)) {
                $envFiles[] = $dotEnvTestLocal;
            }
        }
        
        if (empty($envFiles)) {
            return $this;
        }
        
        $env = new Dotenv();
        $env->overload(...$envFiles);

        foreach ($_ENV as $key => $value) {
            /**
             * @phpstan-ignore-next-line
             * @todo Find better way.
             */
            self::$config[$key] = $value;
        }
        
        if (isset(self::$config['useWWW'])) {
            $value = self::$config['useWWW'];
            self::$config['htaccess']['useWWW'] = StringHelper::stringToBoolean($value);
            unset(self::$config['useWWW']);
        }

        if (isset(self::$config['useSSL'])) {
            $value = self::$config['useSSL'];
            self::$config['htaccess']['useSSL'] = StringHelper::stringToBoolean($value);
            unset(self::$config['useSSL']);
        }

        return $this;
    }

    /**
     * @return array{
     *     APP_ENV: string,
     *     usePageSuffix?: bool|string,
     *     pages: array<string, array{
     *         name?: string,
     *         getFile?: string,
     *         languageCode?: string,
     *         pageTitle?: string,
     *         metaTags?: array<string, string>,
     *         hasDynamicChild?: bool|string,
     *         childOf?: string,
     *         enableCache?: bool,
     *         cacheLifetime?: int
     *     }>,
     *     urlStructure?: array<int, string>,
     *     mainURL?: string,
     *     languageHandling?: string,
     *     htaccess?: array{
     *         useWWW: bool,
     *         useSSL: bool
     *     },
     *     languageCode?: string,
     *     baseMetaTags?: array<string, string|array<string, string>>,
     *     cacheControl?: array<string, int>,
     *     useWWW?: bool,
     *     useSSL?: bool,
     * }
     */
    public function getConfig(): array
    {
        return self::$config;
    }

    /**
     * This method adds the language codes to the pages to make this information easier to access.
     */
    private function addLanguageCodes(): void
    {
        foreach (self::$config['pages'] ?? [] as $languageCode => &$pages) {
            /**
             * @var array{
             *     languageCode?: string,
             * } $page
             */
            foreach ($pages as &$page) {
                $page['languageCode'] = $languageCode;
            }
        }
    }

    /**
     * Updates the BaseConfigLoader class after the configuration has been completed.
     *
     * @param Config $config
     */
    public static function updateConfig(Config $config): void
    {
        /**
         * @phpstan-ignore-next-line
         * @todo Find better way.
         */
        self::$config = array_merge(self::$config, $config->getAllValues());
    }
}
