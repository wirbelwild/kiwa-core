<?php

/**
 * Kiwa. A feather-light web framework for professional static websites.
 *
 * @author Tobias Köngeter
 * @copyright Copyright © Bit&Black
 * @link https://www.bitandblack.com
 * @link https://www.kiwa.io
 * @license MIT
 */

namespace Kiwa\Config\Generator;

use DateTime;
use ReflectionClass;
use ReflectionException;
use Stringable;

/**
 * Class ConfigFile
 *
 * @package Kiwa\TemplateConsole
 * @see \Kiwa\Tests\Config\Generator\ConfigFileTest
 */
class ConfigFile implements Stringable
{
    /**
     * @var array<int, string>
     */
    private array $uses = [];

    /**
     * @var array<int, string>
     */
    private array $lines = [];

    private ?string $return = null;

    /**
     * @return string
     */
    public function __toString(): string
    {
        return '<?php

/**
 * Kiwa. A feather-light web framework for professional static websites.
 *
 * @author Tobias Köngeter
 * @copyright Copyright © ' . date('Y') . ' Bit&Black
 * @link https://www.bitandblack.com
 * @link https://www.kiwa.io
 * @license MIT
 *
 * This file has been auto-generated at ' . (new DateTime('now'))->format('Y-m-d H:i:s') . '.
 * It allows an easier handling of the config file. Alternatively the config file may also contain a pure php array
 * or an array stored as YAML file.
 * @kiwa-auto-generated
 */

' . implode(PHP_EOL, $this->getUses()) . '

' . implode(PHP_EOL, $this->lines) . '
' . $this->return
        ;
    }

    /**
     * Returns all used classes.
     *
     * @return array<int, string>
     */
    private function getUses(): array
    {
        sort($this->uses);
        
        return array_map(
            static fn ($name) => 'use ' . $name . ';',
            $this->uses
        );
    }

    /**
     * Creates a new variable instance.
     *
     * @param class-string $class
     * @return Variable
     * @throws ReflectionException
     */
    public function addVariableFromClass(string $class): Variable
    {
        if (!in_array($class, $this->uses, true)) {
            $this->uses[] = $class;
        }
        
        return new Variable($class);
    }

    /**
     * Adds a content fragment.
     *
     * @param Variable $variable
     * @return $this
     * @throws ReflectionException
     */
    public function addContentFragment(Variable $variable): self
    {
        $class = $variable->getClassName();
        $classVariable = $this->getVariableFromClassName($class);
        $this->lines[] = $classVariable . ' = new ' . $this->getClassShortName($class) . '();';
        $this->lines[] = $classVariable;
        array_push($this->lines, ...$variable->getLines());
        return $this;
    }

    /**
     * Adds a return statement. Because this will be the last statement in the file, we return a bool here
     * to end possible chaining. Method calls after this statement will fail then.
     *
     * @param Variable $variable
     * @param string $method
     * @return bool
     * @throws ReflectionException
     */
    public function addReturn(Variable $variable, string $method): bool
    {
        $this->return = 'return ' . $this->getVariableFromClassName($variable->getClassName()) . '->' . $method . '();';
        return true;
    }

    /**
     * Converts a class name to a variable.
     *
     * @param class-string $class
     * @return string
     * @throws ReflectionException
     */
    private function getVariableFromClassName(string $class): string
    {
        $reflect = new ReflectionClass($class);
        $classVariable = lcfirst($reflect->getShortName());
        return '$' . $classVariable;
    }

    /**
     * Returns the short name of a class.
     *
     * @param class-string $class
     * @return string
     * @throws ReflectionException
     */
    private function getClassShortName(string $class): string
    {
        $reflect = new ReflectionClass($class);
        return $reflect->getShortName();
    }
}
