<?php

/**
 * Kiwa. A feather-light web framework for professional static websites.
 *
 * @author Tobias Köngeter
 * @copyright Copyright © Bit&Black
 * @link https://www.bitandblack.com
 * @link https://www.kiwa.io
 * @license MIT
 */

namespace Kiwa\Config\Generator;

/**
 * Interface ConfigGeneratorInterface.
 *
 * @package Kiwa\Config\Generator
 */
interface ConfigGeneratorInterface
{
    /**
     * Returns the configuration as array.
     *
     * @return array<string, mixed>
     */
    public function getConfig(): array;
}
