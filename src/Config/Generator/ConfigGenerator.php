<?php

/**
 * Kiwa. A feather-light web framework for professional static websites.
 *
 * @author Tobias Köngeter
 * @copyright Copyright © Bit&Black
 * @link https://www.bitandblack.com
 * @link https://www.kiwa.io
 * @license MIT
 */

namespace Kiwa\Config\Generator;

use Kiwa\Exception\Config\MissingValueException;

/**
 * The ConfigGenerator helps to create the Kiwa config by providing simple setter methods.
 *
 * @package Kiwa\TemplateConsole
 */
final class ConfigGenerator
{
    /**
     * @var string
     */
    public static string $indentation = '    ';

    /**
     * @var array{
     *     baseMetaTags?: array{
     *         viewport: string,
     *         robots: string,
     *         description: array<string, string>,
     *         "og:description": array<string, string>,
     *         "og:url": string,
     *         "og:site_name": string,
     *         "og:image": string,
     *         "og:title": array<string, string>,
     *     },
     * }
     */
    private array $config = [
        'htaccess' => [
            'useWWW' => false,
            'useSSL' => false,
        ],
        'usePageSuffix' => 'html',
        'languageHandling' => 'strict',
        'urlStructure' => [
            'name',
            'subname',
        ],
    ];

    /**
     * Returns the whole config as array.
     *
     * @return array<string, mixed>
     */
    public function getConfig(): array
    {
        return $this->config;
    }

    /**
     * Sets the main URL. While your assets may be used with relative URLs, in some cases the absolute URL is needed,
     * for example, meta information, og information and sitemaps.
     * This setting will also set the default `og:url`.
     *
     * @param string $mainURL
     * @return $this
     */
    public function setMainURL(string $mainURL): self
    {
        $this->config['mainURL'] = $mainURL;
        
        if (!isset($this->config['baseMetaTags']['og:url'])) {
            $this->setDefaultOGURL($mainURL);
        }
        
        return $this;
    }

    /**
     * Disables the use of SSL. This setting affects what URLs will look like.
     * It will also affect the .htaccess file when you're using the Kiwa Console:
     * {@link https://bitbucket.org/wirbelwild/kiwa-console}.
     *
     * To enable the use of SSL, {@see ConfigGenerator::enableSSL}.
     *
     * @return $this
     */
    public function disableSSL(): self
    {
        $this->config['htaccess']['useSSL'] = false;
        return $this;
    }

    /**
     * Enables the use of SSL. This setting affects what URLs will look like.
     * It will also affect the .htaccess file when you're using the Kiwa Console:
     * {@link https://bitbucket.org/wirbelwild/kiwa-console}.
     *
     * To disable the use of SSL, {@see ConfigGenerator::disableSSL}.
     *
     * @return $this
     */
    public function enableSSL(): self
    {
        $this->config['htaccess']['useSSL'] = true;
        return $this;
    }

    /**
     * Enables the use of `www`. This setting affects what URLs will look like.
     * It will also affect the `.htaccess` file when you're using the Kiwa Console:
     * {@link https://bitbucket.org/wirbelwild/kiwa-console}.
     *
     * To force having a `www`, {@see ConfigGenerator::enableWWW}.
     *
     * @return $this
     */
    public function disableWWW(): self
    {
        $this->config['htaccess']['useWWW'] = false;
        return $this;
    }

    /**
     * Disables the use of `www`. This setting affects what URLs will look like.
     * It will also affect the `.htaccess` file when you're using the Kiwa Console:
     * {@link https://bitbucket.org/wirbelwild/kiwa-console}.
     *
     * Please note that `www` is a subdomain. If your project runs on a subdomain, this needs to be disabled.
     *
     * To not force having a `www`, {@see ConfigGenerator::disableWWW}.
     *
     * @return $this
     */
    public function enableWWW(): self
    {
        $this->config['htaccess']['useWWW'] = true;
        return $this;
    }

    /**
     * Enables a strict language handling. If set, a page may NOT link to another in a different language.
     *
     * To disable a strict language handling, {@see ConfigGenerator::disableStrictLanguageHandling}.
     *
     * @return $this
     */
    public function enableStrictLanguageHandling(): self
    {
        $this->config['languageHandling'] = 'strict';
        return $this;
    }

    /**
     * Disables a strict language handling. If set, a page may link to another in a different language.
     *
     * To enable a strict language handling, {@see ConfigGenerator::enableStrictLanguageHandling}.
     *
     * @return $this
     */
    public function disableStrictLanguageHandling(): self
    {
        $this->config['languageHandling'] = 'flexible';
        return $this;
    }

    /**
     * Sets the page suffix. This should be `htm`, `html`, `php`, or false when you don't want to have one.
     * Please note that without the htaccess `rewrite_mode` enabled, a suffix may bring the page to fail loading.
     * That's why we recommend disabling the suffix in environments without `mod_rewrite`. This can be done
     * with an `.env.local` file.
     *
     * @param false|string $pageSuffix
     * @return $this
     */
    public function setPageSuffix(false|string $pageSuffix): self
    {
        $allowed = [
            false,
            'php',
            'html',
            'htm',
        ];
        
        if (!in_array($pageSuffix, $allowed, false)) {
            return $this;
        }
        
        $this->config['usePageSuffix'] = $pageSuffix;
        return $this;
    }

    /**
     * Adds a page. This can be done with the help of the {@see PageGenerator}, for example:
     *
     * ```
     * PageGenerator::create()
     *     ->setLanguageCode('de')
     *     ->setName('pageName')
     * ```
     *
     * @param PageGenerator $config The page config.
     * @return $this
     * @throws MissingValueException
     */
    public function addPage(PageGenerator $config): self
    {
        $configuration = $config->getConfig();
        $languageCode = $configuration['languageCode'];
        $this->config['pages'][$languageCode][] = $configuration;
        return $this;
    }

    /**
     * Defines the URL structure. There are three possible parts of a URL:
     * – the language code, which we call `languageCode`
     * - the name of a page in level 1, which we call `name`
     * – the name of a page in level 2, which we call `subname`
     * You can define here, which part of a URL is at which position.
     *
     * @param string $position1      Parameter at the first position.
     * @param string $position2      Parameter at the second position.
     * @param string|null $position3 Parameter at the third position.
     * @return $this
     */
    public function setURLStructure(string $position1, string $position2, string|null $position3 = null): self
    {
        $allowed = [
            'languageCode',
            'name',
            'subname',
        ];

        if (!in_array($position1, $allowed, true)
            || !in_array($position2, $allowed, true)
            || (null !== $position3 && !in_array($position3, $allowed, true))
        ) {
            return $this;
        }
        
        $this->config['urlStructure'] = [
            $position1,
            $position2,
            $position3,
        ];
        
        return $this;
    }

    /**
     * Sets the default language code.
     * This one is important as a fallback for projects without the language code as part of the URL.
     *
     * @param string $languageCode The language code: `de` or `de-de`.
     * @return $this
     */
    public function setDefaultLanguageCode(string $languageCode): self
    {
        $this->config['languageCode'] = $languageCode;
        return $this;
    }

    /**
     * Sets the setting for the robot's behaviour,
     * for example `index, follow`.
     *
     * @param string $robots The robots setting.
     * @return $this
     */
    public function setDefaultRobots(string $robots): self
    {
        $this->config['baseMetaTags']['robots'] = $robots;
        return $this;
    }

    /**
     * Sets the default `og:url`. If not set, this one will be equal to the `mainURL`.
     *
     * @param string $ogURL The absolute URL.
     * @return $this
     */
    public function setDefaultOGURL(string $ogURL): self
    {
        $this->config['baseMetaTags']['og:url'] = $ogURL;
        return $this;
    }

    /**
     * Sets the default `og:site_name`.
     *
     * @param string $ogSiteName The site's name.
     * @return $this
     */
    public function setDefaultOGSiteName(string $ogSiteName): self
    {
        $this->config['baseMetaTags']['og:site_name'] = $ogSiteName;
        return $this;
    }

    /**
     * Sets the default `og:image`.
     *
     * @param string $ogImage Relative URL of the image, for example `/build/images/image.png`.
     * @return $this
     */
    public function setDefaultOGImage(string $ogImage): self
    {
        $this->config['baseMetaTags']['og:image'] = $ogImage;
        return $this;
    }

    /**
     * Sets the viewport. The recommended setting is `width=device-width, initial-scale=1`.
     *
     * @param string $viewport
     * @return $this
     */
    public function setDefaultViewport(string $viewport): self
    {
        $this->config['baseMetaTags']['viewport'] = $viewport;
        return $this;
    }

    /**
     * Sets the default `og:title`.
     *
     * @param string $languageCode The language code for this title.
     * @param string $title        The title itself.
     * @return $this
     */
    public function setDefaultOGTitle(string $languageCode, string $title): self
    {
        $this->config['baseMetaTags']['og:title'][$languageCode] = $title;
        return $this;
    }

    /**
     * Sets the default `description`.
     * This affects the meta-description and also the og:description.
     *
     * @param string $languageCode The language code for this description.
     * @param string $description  The description itself.
     * @return $this
     */
    public function setDefaultDescription(string $languageCode, string $description): self
    {
        $this->config['baseMetaTags']['description'][$languageCode] = $description;
        
        if (!isset($this->config['baseMetaTags']['og:description'][$languageCode])) {
            $this->setDefaultOGDescription($languageCode, $description);
        }
        
        return $this;
    }

    /**
     * Sets the default `og:description`.
     *
     * @param string $languageCode The language code for this description.
     * @param string $description  The description itself.
     * @return $this
     */
    public function setDefaultOGDescription(string $languageCode, string $description): self
    {
        $this->config['baseMetaTags']['og:description'][$languageCode] = $description;
        return $this;
    }

    /**
     * Adds an authentication to your page, so it will be only accessible with a name and a password.
     * This setting will only work when you have the Kiwa Console installed
     * which generates the `.htaccess` and the `.htpasswd` files and active `mod_rewrite`:
     * {@link https://bitbucket.org/wirbelwild/kiwa-console}.
     *
     * Please note that we don't recommend adding sensitive values to your config.
     * A better way is to add those values to the `.env.local` file:
     *
     * ```
     * useHtpasswd=true
     * htpasswdUser=USER_NAME
     * htpasswdPassword=USER_PASSWORD
     * ```
     *
     * @param string $user     Name of the user.
     * @param string $password The password for this user.
     * @return $this
     */
    public function enableAuthentication(string $user, string $password): self
    {
        $this->config['useHtpasswd'] = true;
        $this->config['htpasswdUser'] = $user;
        $this->config['htpasswdPassword'] = $password;
        return $this;
    }

    /**
     * Sets the cache control configuration. It defines how long each file may be cached from a browser.
     *
     * @param CacheControlGenerator $cacheControlGenerator
     * @return $this
     */
    public function setCacheControl(CacheControlGenerator $cacheControlGenerator): self
    {
        $this->config['cacheControl'] = $cacheControlGenerator->getConfig();
        return $this;
    }

    /**
     * Sets the security headers configuration.
     *
     * @param SecurityHeadersGenerator $securityHeadersGenerator
     * @return $this
     */
    public function setSecurityHeaders(SecurityHeadersGenerator $securityHeadersGenerator): self
    {
        $this->config['securityHeaders'] = $securityHeadersGenerator->getConfig();
        return $this;
    }
}
