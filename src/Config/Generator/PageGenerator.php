<?php

/**
 * Kiwa. A feather-light web framework for professional static websites.
 *
 * @author Tobias Köngeter
 * @copyright Copyright © Bit&Black
 * @link https://www.bitandblack.com
 * @link https://www.kiwa.io
 * @license MIT
 */

namespace Kiwa\Config\Generator;

use Kiwa\Exception\Config\MisleadingConfigException;
use Kiwa\Exception\Config\MissingValueException;

/**
 * The PageGenerator helps to create a page config. This can be added in the ConfigGenerator class.
 *
 * @see ConfigGenerator::addPage
 * @package Kiwa\TemplateConsole
 * @see \Kiwa\Tests\Config\Generator\PageGeneratorTest
 */
final class PageGenerator implements ConfigGeneratorInterface
{
    /**
     * @var array{
     *     name?: string,
     *     getFile?: string,
     *     languageCode?: string,
     *     pageTitle?: string,
     *     metaTags?: array<string, string>,
     *     hasDynamicChild?: bool|string,
     *     childOf?: string,
     *     enableCache?: bool,
     *     cacheLifetime?: int
     * }
     */
    private array $config = [];

    /**
     * Returns a new instance of the PageGenerator.
     *
     * @return PageGenerator
     */
    public static function create(): self
    {
        return new self();
    }

    /**
     * Returns the whole config.
     *
     * @return array{
     *     name: string,
     *     getFile: string,
     *     languageCode: string,
     *     pageTitle?: string,
     *     metaTags?: array<string, string>,
     *     hasDynamicChild?: bool|string,
     *     childOf?: string,
     *     enableCache?: bool,
     *     cacheLifetime?: int
     * }
     * @throws MissingValueException
     */
    public function getConfig(): array
    {
        if (!isset($this->config['name'])) {
            throw new MissingValueException('name');
        }

        if (!isset($this->config['getFile'])) {
            throw new MissingValueException('getFile');
        }

        if (!isset($this->config['languageCode'])) {
            throw new MissingValueException('languageCode');
        }

        return $this->config;
    }

    /**
     * Sets the page name. This will be part of the URL except the page is a dynamic subpage.
     *
     * @param string $name The name.
     * @return $this
     */
    public function setName(string $name): self
    {
        $this->config['name'] = $name;
        return $this;
    }

    /**
     * Sets the language code for this page.
     *
     * @param string $languageCode The language code.
     * @return $this
     */
    public function setLanguageCode(string $languageCode): self
    {
        $this->config['languageCode'] = $languageCode;
        return $this;
    }

    /**
     * Sets the page title. If no `og:title` is set, this one will be used.
     *
     * @param string $pageTitle The title.
     * @return $this
     */
    public function setPageTitle(string $pageTitle): self
    {
        $this->config['pageTitle'] = $pageTitle;
        
        if (!isset($this->config['metaTags']['og:title'])) {
            $this->setOGTitle($pageTitle);
        }
        
        return $this;
    }

    /**
     * Sets the `phtml` file which should be used for this page.
     *
     * @param string $fileName The `phtml` file name.
     * @return $this
     */
    public function setFile(string $fileName): self
    {
        $this->config['getFile'] = $fileName;
        return $this;
    }
    
    /**
     * Sets the `og:title`.
     *
     * @param string $title The title itself.
     * @return $this
     */
    public function setOGTitle(string $title): self
    {
        $this->config['metaTags']['og:title'] = $title;
        return $this;
    }

    /**
     * Sets the `description`.
     * This affects the meta-description and also the `og:description`.
     *
     * @param string $description The description itself.
     * @return $this
     */
    public function setDescription(string $description): self
    {
        $this->config['metaTags']['description'] = $description;

        if (!isset($this->config['metaTags']['og:description'])) {
            $this->setOGDescription($description);
        }

        return $this;
    }

    /**
     * Sets the `og:description`.
     *
     * @param string $description The description itself.
     * @return $this
     */
    public function setOGDescription(string $description): self
    {
        $this->config['metaTags']['og:description'] = $description;
        return $this;
    }

    /**
     * Sets the `og:image`.
     *
     * @param string $imagePath Relative path to the image in your build folder.
     * @return $this
     */
    public function setOGImage(string $imagePath): self
    {
        $this->config['metaTags']['og:image'] = $imagePath;
        return $this;
    }

    /**
     * Sets the `og:type`.
     *
     * @param string $pageType The type.
     * @return $this
     */
    public function setPageType(string $pageType): self
    {
        $allowed = [
            'website',
            'article',
            'book',
            'profile',
        ];

        if (!in_array($pageType, $allowed, false)) {
            return $this;
        }
        
        $this->config['metaTags']['og:type'] = $pageType;
        return $this;
    }

    /**
     * If this page should be a subpage, you need to define the parent's page name.
     *
     * @param string $parentPageName The parent pages name.
     *                               __Please note__: here we don't need the name of the `phtml` file,
     *                               because it wouldn't be unique. Instead, we need the page's name.
     * @return $this
     * @throws MisleadingConfigException
     */
    public function setChildOf(string $parentPageName): self
    {
        if (isset($this->config['hasDynamicChild']) && false !== $this->config['hasDynamicChild']) {
            throw new MisleadingConfigException(
                'This page has been defined as a page with dynamic subpages before, so it can\'t be a subpage by its own.'
            );
        }
        
        $this->config['childOf'] = $parentPageName;
        return $this;
    }

    /**
     * This setting allows a page to have dynamic subpages. All requests for subpages will be sent to the one and only
     * subpage then, which you can define here.
     * If a page has subpages, its position is at the first level, and that's why it can't be a subpage by itself.
     * A dynamic subpage needs to be defined also. It needs to be defined as a child of the parents page.
     *
     * @param string $childPageName The name of the child page.
     * @return $this
     * @throws MisleadingConfigException
     */
    public function setDynamicChild(string $childPageName): self
    {
        if (isset($this->config['childOf'])) {
            throw new MisleadingConfigException(
                'This page has been defined as a subpage before, so it can\'t have subpages by its own.'
            );
        }
        
        $this->config['hasDynamicChild'] = $childPageName;
        return $this;
    }

    /**
     * Enables to cache this page. This means that once an `html` file will be created and served with each request.
     * If your page runs some intensive functions, this may help you speed up the load time.
     * Please note that after creating the `html` file, no PHP will be executed.
     * A good way to refresh the cache is to delete all those `html` files after running a deployment.
     *
     * If you want to disable caching, {@see PageGenerator::disableCaching}.
     *
     * @param int|null $lifetimeSeconds How long the cache file should be stored before a new one will be created.
     *                                  Per default, this value is `null` what means the file should never be renewed.
     * @return $this
     */
    public function enableCaching(int|null $lifetimeSeconds = null): self
    {
        $this->config['enableCache'] = true;
        $this->config['cacheLifetime'] = $lifetimeSeconds ?? -1;
        return $this;
    }

    /**
     * Disables the cache for this page, so it will always load its `phtml` file and runs all functions in it.
     *
     * If you want to enable caching, {@see PageGenerator::enableCaching}.
     *
     * @return $this
     */
    public function disableCaching(): self
    {
        $this->config['enableCache'] = false;
        
        if (isset($this->config['cacheLifetime'])) {
            unset($this->config['cacheLifetime']);
        }
        
        return $this;
    }
}
