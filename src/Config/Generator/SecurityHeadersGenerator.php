<?php

/**
 * Kiwa. A feather-light web framework for professional static websites.
 *
 * @author Tobias Köngeter
 * @copyright Copyright © Bit&Black
 * @link https://www.bitandblack.com
 * @link https://www.kiwa.io
 * @license MIT
 */

namespace Kiwa\Config\Generator;

use BitAndBlack\Duration\Duration;

/**
 * The SecurityHeadersGenerator helps to set up the security headers config.
 * This can be added in the ConfigGenerator class.
 *
 * @see ConfigGenerator::setSecurityHeaders
 * @package Kiwa\Config\Generator
 * @see \Kiwa\Tests\Config\Generator\SecurityHeadersGeneratorTest
 */
final class SecurityHeadersGenerator implements ConfigGeneratorInterface
{
    private string $xContentTypeOptions;
    private string $strictTransportSecurity;
    private string $xFrameOptions;
    private string $referrerPolicy;
    private string $contentSecurityPolicy;

    /**
     * CacheControlGenerator constructor.
     */
    public function __construct()
    {
        $this
            ->setContentSecurityPolicy('script-src \'unsafe-inline\' \'self\'')
            ->setReferrerPolicyStrictOriginWhenCrossOrigin()
            ->setStrictTransportSecurity(
                (int) Duration::createFromDays(365)->getSeconds()
            )
            ->setXContentTypeOptionsNoSniff()
            ->setXFrameOptionsSameOrigin()
        ;
    }

    /**
     * @return SecurityHeadersGenerator
     */
    public static function create(): self
    {
        return new self();
    }

    /**
     * @return string
     */
    public function getXContentTypeOptions(): string
    {
        return $this->xContentTypeOptions;
    }

    /**
     * Sets the X-Content-Type-Options to `nosniff`.
     *
     * @return SecurityHeadersGenerator
     */
    public function setXContentTypeOptionsNoSniff(): self
    {
        $this->xContentTypeOptions = 'nosniff';
        return $this;
    }
    
    /**
     * @return string
     */
    public function getStrictTransportSecurity(): string
    {
        return $this->strictTransportSecurity;
    }

    /**
     * @param int $maxAge
     * @param bool $includeSubDomains
     * @param bool $preload
     * @return SecurityHeadersGenerator
     */
    public function setStrictTransportSecurity(int $maxAge, bool $includeSubDomains = true, bool $preload = false): self
    {
        $this->strictTransportSecurity = 'max-age=' . $maxAge;

        if (true === $includeSubDomains) {
            $this->strictTransportSecurity .= '; includeSubDomains';
        }

        if (true === $preload) {
            $this->strictTransportSecurity .= '; preload';
        }

        return $this;
    }
    
    /**
     * @return string
     */
    public function getXFrameOptions(): string
    {
        return $this->xFrameOptions;
    }

    /**
     * Sets the X-Frame-Options to `DENY`.
     *
     * @return SecurityHeadersGenerator
     */
    public function setXFrameOptionsDeny(): self
    {
        $this->xFrameOptions = 'DENY';
        return $this;
    }
    
    /**
     * Sets the X-Frame-Options to `SAMEORIGIN`.
     *
     * @return SecurityHeadersGenerator
     */
    public function setXFrameOptionsSameOrigin(): self
    {
        $this->xFrameOptions = 'SAMEORIGIN';
        return $this;
    }

    /**
     * @return string
     */
    public function getReferrerPolicy(): string
    {
        return $this->referrerPolicy;
    }

    /**
     * Sets the Referrer-Policy to `no-referrer`.
     *
     * @return $this
     */
    public function setReferrerPolicyNoReferrer(): self
    {
        $this->referrerPolicy = 'no-referrer';
        return $this;
    }

    /**
     * Sets the Referrer-Policy to `no-referrer-when-downgrade`.
     *
     * @return $this
     */
    public function setReferrerPolicyNoReferrerWhenDowngrade(): self
    {
        $this->referrerPolicy = 'no-referrer-when-downgrade';
        return $this;
    }

    /**
     * Sets the Referrer-Policy to `origin`.
     *
     * @return $this
     */
    public function setReferrerPolicyOrigin(): self
    {
        $this->referrerPolicy = 'origin';
        return $this;
    }

    /**
     * Sets the Referrer-Policy to `origin-when-cross-origin`.
     *
     * @return $this
     */
    public function setReferrerPolicyOriginWhenCrossOrigin(): self
    {
        $this->referrerPolicy = 'origin-when-cross-origin';
        return $this;
    }

    /**
     * Sets the Referrer-Policy to `same-origin`.
     *
     * @return $this
     */
    public function setReferrerPolicySameOrigin(): self
    {
        $this->referrerPolicy = 'same-origin';
        return $this;
    }

    /**
     * Sets the Referrer-Policy to `strict-origin`.
     *
     * @return $this
     */
    public function setReferrerPolicyStrictOrigin(): self
    {
        $this->referrerPolicy = 'strict-origin';
        return $this;
    }

    /**
     * Sets the Referrer-Policy to `strict-origin-when-cross-origin`.
     *
     * @return $this
     */
    public function setReferrerPolicyStrictOriginWhenCrossOrigin(): self
    {
        $this->referrerPolicy = 'strict-origin-when-cross-origin';
        return $this;
    }
    
    /**
     * Sets the Referrer-Policy to `unsafe-url`.
     *
     * @return $this
     */
    public function setReferrerPolicyUnsafeUrl(): self
    {
        $this->referrerPolicy = 'unsafe-url';
        return $this;
    }

    /**
     * @return string
     */
    public function getContentSecurityPolicy(): string
    {
        return $this->contentSecurityPolicy;
    }

    /**
     * Sets the Content-Security-Policy. This is `script-src 'unsafe-inline' 'self' https:` per default (when using SSL).
     * This method takes one or more arguments and chains them with a semicolon.
     *
     * For example:
     * ```
     * setContentSecurityPolicy(
     *     'default-src \'none\'',
     *     'script-src https://kiwa.io'
     * )
     * ```
     *
     * will result in
     *
     * ```
     * Content-Security-Policy: default-src 'none'; script-src https://kiwa.io
     * ```
     *
     * @param string ...$contentSecurityPolicy
     * @return SecurityHeadersGenerator
     */
    public function setContentSecurityPolicy(string ...$contentSecurityPolicy): self
    {
        $this->contentSecurityPolicy = implode('; ', $contentSecurityPolicy);
        return $this;
    }
    
    /**
     * @return array<string, string>
     */
    public function getConfig(): array
    {
        return [
            'Content-Security-Policy' => $this->getContentSecurityPolicy(),
            'Referrer-Policy' => $this->getReferrerPolicy(),
            'Strict-Transport-Security' => $this->getStrictTransportSecurity(),
            'X-Content-Type-Options' => $this->getXContentTypeOptions(),
            'X-Frame-Options' => $this->getXFrameOptions(),
        ];
    }
}
