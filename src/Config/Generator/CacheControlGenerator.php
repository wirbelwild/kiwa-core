<?php

/**
 * Kiwa. A feather-light web framework for professional static websites.
 *
 * @author Tobias Köngeter
 * @copyright Copyright © Bit&Black
 * @link https://www.bitandblack.com
 * @link https://www.kiwa.io
 * @license MIT
 */

namespace Kiwa\Config\Generator;

use BitAndBlack\Duration\Duration;

/**
 * The CacheControlGenerator helps to set up the cache control config. This can be added in the ConfigGenerator class.
 *
 * @see ConfigGenerator::setCacheControl
 * @package Kiwa\Config\Generator
 */
final class CacheControlGenerator implements ConfigGeneratorInterface
{
    private int $maxAgeImmutableFiles;
    private int $maxAgeUnknownFiles;
    private int $maxAgeAssetFiles;

    /**
     * CacheControlGenerator constructor.
     */
    public function __construct()
    {
        $this
            ->setMaxAgeAssetFiles(
                (int) Duration::createFromDays(60)->getSeconds()
            )
            ->setMaxAgeImmutableFiles(
                (int) Duration::createFromDays(365)->getSeconds()
            )
            ->setMaxAgeUnknownFiles(
                (int) Duration::createFromDays(7)->getSeconds()
            )
        ;
    }

    /**
     * @return CacheControlGenerator
     */
    public static function create(): self
    {
        return new self();
    }
    
    /**
     * @return int
     */
    public function getMaxAgeImmutableFiles(): int
    {
        return $this->maxAgeImmutableFiles;
    }

    /**
     * @param int $maxAgeImmutableFiles
     * @return CacheControlGenerator
     */
    public function setMaxAgeImmutableFiles(int $maxAgeImmutableFiles): self
    {
        $this->maxAgeImmutableFiles = $maxAgeImmutableFiles;
        return $this;
    }

    /**
     * @return int
     */
    public function getMaxAgeUnknownFiles(): int
    {
        return $this->maxAgeUnknownFiles;
    }

    /**
     * @param int $maxAgeUnknownFiles
     * @return CacheControlGenerator
     */
    public function setMaxAgeUnknownFiles(int $maxAgeUnknownFiles): self
    {
        $this->maxAgeUnknownFiles = $maxAgeUnknownFiles;
        return $this;
    }

    /**
     * @return int
     */
    public function getMaxAgeAssetFiles(): int
    {
        return $this->maxAgeAssetFiles;
    }

    /**
     * @param int $maxAgeAssetFiles
     * @return CacheControlGenerator
     */
    public function setMaxAgeAssetFiles(int $maxAgeAssetFiles): self
    {
        $this->maxAgeAssetFiles = $maxAgeAssetFiles;
        return $this;
    }

    /**
     * @return array<string, int>
     */
    public function getConfig(): array
    {
        return [
            'maxAgeAssetFiles' => $this->getMaxAgeAssetFiles(),
            'maxAgeImmutableFiles' => $this->getMaxAgeImmutableFiles(),
            'maxAgeUnknownFiles' => $this->getMaxAgeUnknownFiles(),
        ];
    }
}
