<?php

/**
 * Kiwa. A feather-light web framework for professional static websites.
 *
 * @author Tobias Köngeter
 * @copyright Copyright © Bit&Black
 * @link https://www.bitandblack.com
 * @link https://www.kiwa.io
 * @license MIT
 */

namespace Kiwa\Config\Generator;

use Kiwa\Exception\Config\MethodNotAvailableException;
use ReflectionClass;
use ReflectionException;

/**
 * Class Variable
 *
 * @package Kiwa\TemplateConsole
 */
class Variable
{
    /**
     * @var array<int, string>
     */
    private array $lines = [];

    /**
     * @var ReflectionClass<object>
     */
    private readonly ReflectionClass $classReflected;
    
    private bool $varDump = true;

    /**
     * Variable constructor.
     *
     * @param class-string $className
     * @throws ReflectionException
     */
    public function __construct(
        private readonly string $className,
    ) {
        $this->classReflected = new ReflectionClass($this->className);
    }

    /**
     * Adds a method.
     *
     * @param string $methodName
     * @return $this
     * @throws MethodNotAvailableException
     */
    public function addChainedMethodCall(string $methodName, mixed ...$parameter): self
    {
        if (!$this->methodExists($methodName)) {
            throw new MethodNotAvailableException($methodName, $this->classReflected);
        }
        
        $parameterDump = [];
        
        foreach ($parameter as $param) {
            if (null === $param) {
                continue;
            }
            $parameterDump[] = $this->varDump ? $this->getVarExport($param) : $param;
        }

        $this->lines[] = ConfigGenerator::$indentation . '->' . $methodName . '(' . implode(', ', $parameterDump) . ')';
        return $this;
    }

    /**
     * Returns all lines.
     *
     * @param bool $addSemicolon
     * @return array<int, string>
     */
    public function getLines(bool $addSemicolon = true): array
    {
        $lines = $this->lines;
        
        if ($addSemicolon) {
            $lines[] = ';' . PHP_EOL;
        }
        
        return $lines;
    }

    /**
     * Proofs if a method exists.
     *
     * @param string $methodName
     * @return bool
     */
    private function methodExists(string $methodName): bool
    {
        return $this->classReflected->hasMethod($methodName);
    }

    /**
     * Returns thr class name.
     *
     * @return class-string
     */
    public function getClassName(): string
    {
        return $this->className;
    }

    /**
     * Returns exported variables.
     *
     * @return mixed
     */
    private function getVarExport(mixed $expression)
    {
        $export = var_export($expression, true);
        
        $patterns = [
            '/  /' => ConfigGenerator::$indentation,
            '/array \\(/' => '[',
            '/^([ ]*)\\)(,?)$/m' => '$1]$2',
            '/=>[ ]?\n[ ]+\\[/' => '=> [',
            '/([ ]*)(\\\'[^\\\']+\\\') => ([\\[\\\'])/' => '$1$2 => $3',
            '/([\n\r])/' => '$1' . ConfigGenerator::$indentation,
            '/NULL/' => 'null',
        ];

        return preg_replace(
            array_keys($patterns),
            array_values($patterns),
            $export
        );
    }

    /**
     * Disables the dump of variables.
     *
     * @return $this
     */
    public function disableVarDump(): self
    {
        $this->varDump = false;
        return $this;
    }

    /**
     * Enables the dump of variables.
     *
     * @return $this
     */
    public function enableVarDump(): self
    {
        $this->varDump = true;
        return $this;
    }
}
