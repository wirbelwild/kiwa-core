<?php

/**
 * Kiwa. A feather-light web framework for professional static websites.
 *
 * @author Tobias Köngeter
 * @copyright Copyright © Bit&Black
 * @link https://www.bitandblack.com
 * @link https://www.kiwa.io
 * @license MIT
 */

namespace Kiwa\HTML;

use Kiwa\Language\CurrentLanguageCode;
use Kiwa\Page\PageUID;
use Kiwa\URL\URLBuilder;
use Kiwa\URL\URLFragment;
use Stringable;

/**
 * The JavascriptVariables class creates a piece of JavaScript, which may be used in the frontend.
 * It provides access to some Kiwa variables.
 *
 * @package Kiwa\HTML
 * @see \Kiwa\Tests\HTML\JavascriptVariablesTest
 */
class JavascriptVariables implements Stringable
{
    private static string $mainURL = '';

    private ?string $namespace;

    private string $code =
        /** @lang text */
        '<script>' . PHP_EOL .
        '    %6$s = {' . PHP_EOL .
        '        currentPage: %1$s,' . PHP_EOL .
        '        parentPage: %2$s,' . PHP_EOL .
        '        mainURL: %3$s,' . PHP_EOL .
        '        languageCode: %4$s,' . PHP_EOL .
        '        countryCode: %5$s,' . PHP_EOL .
        '        pageUID: %7$s,' . PHP_EOL .
        '    };' . PHP_EOL .
        '</script>' . PHP_EOL
    ;

    /**
     * Returns some JavaScript for your frontend. It contains
     * * The name of the current page
     * * The name of the parent page
     * * The main URL
     * * The language code
     * * The country code.
     *
     * Those values are available inside `window.environment`.
     * If you have conflicts with this name, you can define the namespace by your own.
     *
     * @param string|null $namespace Name of the object, which will be added to `window`.
     *                               This is `window.environment` per default.
     */
    public function __construct(string|null $namespace = null)
    {
        $this->namespace = $namespace ?? 'window.environment';
    }

    /**
     * @return string
     */
    public function __toString(): string
    {
        return $this->getScript();
    }

    /**
     * @return string
     */
    public function getScript(): string
    {
        /**
         * Take language code and use last part so `de-at` would return country code `at` and
         * `de` would return country code `de`.
         *
         * @param string $languageCode
         * @return string
         */
        $getCountryCode = static function (string $languageCode): string {
            $languageCodeArray = explode('-', $languageCode);
            return (string) end($languageCodeArray);
        };

        $currentLangCode = URLFragment::create('languageCode')->getFragment();
        $currentName = URLFragment::create('name')->getFragment();
        $currentSubname = URLFragment::create('subname')->getFragment();

        $currentPage = (string) new URLBuilder([
            'languageCode' => $currentLangCode,
            'name' => $currentName,
            'subname' => $currentSubname,
        ]);
        $parentPage = (string) new URLBuilder([
            'languageCode' => $currentLangCode,
            'name' => $currentName,
        ]);

        if ($currentPage === $parentPage) {
            $parentPage = (string) new URLBuilder([
                'languageCode' => $currentLangCode,
            ]);
        }

        $currentLanguageCode = (string) new CurrentLanguageCode();

        $currentPageString = '"' . $currentPage . '"';
        $parentPageString = '"' . $parentPage . '"';

        $mainURLString = 'null';

        if ('' !== self::$mainURL) {
            $mainURLString = '"' . self::$mainURL . '"';
        }

        $languageCodeString = 'null';
        
        if ('' !== $currentLanguageCode) {
            $languageCodeString = '"' . $currentLanguageCode . '"';
        }

        $countryCodeString = 'null';
        
        if ('' !== $countryCode = $getCountryCode($currentLanguageCode)) {
            $countryCodeString = '"' . $countryCode . '"';
        }
        
        $pageUIDString = '"' . new PageUID() . '"';
        
        return sprintf(
            $this->code,
            $currentPageString,
            $parentPageString,
            $mainURLString,
            $languageCodeString,
            $countryCodeString,
            $this->namespace,
            $pageUIDString
        );
    }
}
