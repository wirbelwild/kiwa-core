<?php

/**
 * Kiwa. A feather-light web framework for professional static websites.
 *
 * @author Tobias Köngeter
 * @copyright Copyright © Bit&Black
 * @link https://www.bitandblack.com
 * @link https://www.kiwa.io
 * @license MIT
 */

namespace Kiwa\Page;

/**
 * The RelatedPages class holds all related pages and their URLs.
 *
 * @package Kiwa\Page
 * @see \Kiwa\Tests\Page\RelatedPagesTest
 */
final class RelatedPages
{
    /**
     * @var array<string, array{
     *     languageCode: string|null,
     *     name: string|null,
     *     subname: string|null,
     * }>
     */
    private static array $relatedPages = [];

    /**
     * Adds a related page.
     *
     * @param string|null $languageCode The language code of this page.
     * @param string|null $name         The name of this page.
     * @param string|null $subname      The subname of this page.
     */
    public static function addPage(string|null $languageCode = null, string|null $name = null, string|null $subname = null): void
    {
        self::$relatedPages[$languageCode] = [
            'languageCode' => $languageCode,
            'name' => $name,
            'subname' => $subname,
        ];
    }

    /**
     * Gets related pages in other languages.
     *
     * @param string|null $languageCode
     * @return array<string, array{
     *     languageCode: string|null,
     *     name: string|null,
     *     subname: string|null,
     * }>|array{
     *     languageCode: string|null,
     *     name: string|null,
     *     subname: string|null,
     * }
     */
    public static function getRelatedPages(string|null $languageCode = null): array
    {
        if (null === $languageCode) {
            return self::$relatedPages;
        }

        if (array_key_exists($languageCode, self::$relatedPages)) {
            return self::$relatedPages[$languageCode];
        }

        return [];
    }
}
