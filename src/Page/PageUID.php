<?php

/**
 * Kiwa. A feather-light web framework for professional static websites.
 *
 * @author Tobias Köngeter
 * @copyright Copyright © Bit&Black
 * @link https://www.bitandblack.com
 * @link https://www.kiwa.io
 * @license MIT
 */

namespace Kiwa\Page;

use Kiwa\Config\Page;
use Kiwa\File\FileNameFromPageName;
use Kiwa\URL\URLFragment;
use Stringable;

/**
 * The PageUID class returns a unique name for each page.
 *
 * @package Kiwa\Page
 * @see \Kiwa\Tests\Page\PageUIDTest
 */
final class PageUID implements Stringable
{
    private static string $pageUID;

    /**
     * Returns a unique name for each page. This can be used as a class name or an id.
     * The name is based on the file's name. This means that language versions have the same id. For example
     * if `/en/contact.html` and `/de/kontakt.html` point to the same file `contact.phtml`, the UID for
     * both pages will be `page-contact`.
     */
    public function __construct()
    {
        $pageName = URLFragment::create('name')->getFragment();
        $pageSubName = URLFragment::create('subname')->getFragment();
        $key = $pageSubName ?? $pageName ?? '';
        $uid = (string) new FileNameFromPageName($key);

        if ('index' === $uid && null !== $pageSubName) {
            /** @var Page $page */
            foreach (new PageList() as $page) {
                if ($page->getName() === $pageName
                    && $page->getDynamicChild()
                ) {
                    $uid = $page->getDynamicChild();
                    break;
                }
            }
        }

        self::$pageUID = 'page-' . $uid;
    }

    /**
     * Returns a new instance.
     *
     * @return PageUID
     */
    public static function create(): self
    {
        return new self();
    }

    /**
     * @return string
     */
    public static function getPageUID(): string
    {
        return self::$pageUID;
    }

    /**
     * @return string
     */
    public function __toString(): string
    {
        return self::getPageUID();
    }
}
