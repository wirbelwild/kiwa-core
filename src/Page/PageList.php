<?php

/**
 * Kiwa. A feather-light web framework for professional static websites.
 *
 * @author Tobias Köngeter
 * @copyright Copyright © Bit&Black
 * @link https://www.bitandblack.com
 * @link https://www.kiwa.io
 * @license MIT
 */

namespace Kiwa\Page;

use ArrayObject;
use Countable;
use Iterator;
use IteratorAggregate;
use Kiwa\Config\Page;
use Kiwa\Page\Iterator\HasDynamicChildIterator;
use Kiwa\Page\Iterator\HasLanguageCodeIterator;
use Kiwa\Page\Iterator\HasPageNameIterator;
use Kiwa\Page\Iterator\IsChildIterator;
use Kiwa\Page\Iterator\IsChildOfIterator;
use Kiwa\Page\Iterator\IsNoChildIterator;
use Kiwa\Page\Iterator\UsesFileNameIterator;

/**
 * The PageList holds all configured pages as objects.
 *
 * @package Kiwa\Page
 * @implements IteratorAggregate<int, Page>
 * @see \Kiwa\Tests\Page\PageListTest
 */
final class PageList implements Countable, IteratorAggregate
{
    /**
     * @var ArrayObject<int, Page>
     */
    private static ArrayObject $pages;

    /**
     * @var array<int, string>
     */
    private array $hasLanguageCode = [];
    
    /**
     * @var array<int, string>
     */
    private array $isChildOf = [];
    
    /**
     * @var array<int, string>
     */
    private array $hasPageName = [];

    private ?bool $isChild = null;
    
    /**
     * @var array<int, string>
     */
    private array $hasDynamicChild;

    /**
     * @var array<int, string>
     */
    private array $usesFileName = [];

    /**
     * Constructor.
     */
    public function __construct()
    {
        self::$pages ??= new ArrayObject([]);
    }

    /**
     * @return PageList
     */
    public static function create(): self
    {
        return new self();
    }

    /**
     * @return Iterator<int, Page>
     */
    public function getIterator(): Iterator
    {
        $iterator = self::$pages->getIterator();

        if (!empty($this->hasLanguageCode)) {
            $iterator = new HasLanguageCodeIterator($iterator, $this->hasLanguageCode);
        }

        if (true === $this->isChild) {
            $iterator = new IsChildIterator($iterator);
        }

        if (false === $this->isChild) {
            $iterator = new IsNoChildIterator($iterator);
        }

        if (!empty($this->isChildOf)) {
            $iterator = new IsChildOfIterator($iterator, $this->isChildOf);
        }

        if (!empty($this->hasPageName)) {
            $iterator = new HasPageNameIterator($iterator, $this->hasPageName);
        }

        if (!empty($this->hasDynamicChild)) {
            $iterator = new HasDynamicChildIterator($iterator, $this->hasDynamicChild);
        }

        if (!empty($this->usesFileName)) {
            $iterator = new UsesFileNameIterator($iterator, $this->usesFileName);
        }

        return $iterator;
    }

    /**
     * @return int
     */
    public function count(): int
    {
        return iterator_count(
            $this->getIterator()
        );
    }

    /**
     * Filters pages with this language code.
     *
     * @param string $languageCode The language code.
     * @return $this
     */
    public function hasLanguageCode(string $languageCode): self
    {
        $this->hasLanguageCode[] = $languageCode;
        return $this;
    }

    /**
     * Filters pages which are declared as children.
     *
     * @return $this
     */
    public function isChild(): self
    {
        $this->isChild = true;
        return $this;
    }

    /**
     * Filters pages which are __not__ declared as children.
     *
     * @return $this
     */
    public function isNoChild(): self
    {
        $this->isChild = false;
        return $this;
    }

    /**
     * Filters pages which are children of the declared page.
     *
     * @param string $parentPageFileName The name of the parent page. This is __not__ the file name.
     * @return $this
     */
    public function isChildOf(string $parentPageFileName): self
    {
        $this->isChildOf[] = $parentPageFileName;
        return $this;
    }

    /**
     * Filters pages with the given page name.
     *
     * @param string $pageName The page name.
     * @return PageList
     */
    public function hasPageName(string $pageName): self
    {
        $this->hasPageName[] = $pageName;
        return $this;
    }

    /**
     * Filters pages which have a dynamic child.
     *
     * @param string $dynamicChildName The name of the dynamic child.
     * @return PageList
     */
    public function hasDynamicChild(string $dynamicChildName): self
    {
        $this->hasDynamicChild[] = $dynamicChildName;
        return $this;
    }

    /**
     * Filters pages with the given page name.
     *
     * @param string $fileName The page name.
     * @return PageList
     */
    public function usesFileName(string $fileName): self
    {
        $this->usesFileName[] = $fileName;
        return $this;
    }
}
