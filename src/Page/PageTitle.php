<?php

/**
 * Kiwa. A feather-light web framework for professional static websites.
 *
 * @author Tobias Köngeter
 * @copyright Copyright © Bit&Black
 * @link https://www.bitandblack.com
 * @link https://www.kiwa.io
 * @license MIT
 */

namespace Kiwa\Page;

use Kiwa\Language\CurrentLanguageCode;
use Stringable;

/**
 * The PageTitle class contains the current page title.
 *
 * @package Kiwa\Page
 * @see \Kiwa\Tests\Page\PageTitleTest
 */
final class PageTitle implements Stringable
{
    private static ?string $title = null;
    
    /**
     * @var array{
     *     viewport?: string,
     *     robots?: string,
     *     description?: array<string, string>,
     *     "og:description"?: array<string, string>,
     *     "og:url"?: string,
     *     "og:site_name"?: string,
     *     "og:image"?: string,
     *     "og:title"?: array<string, string>,
     * }
     */
    private static array $baseMetaTags = [];

    /**
     * Creates a new instance of the current page title.
     */
    public function __construct()
    {
        /**
         * If no title is defined, look for the og:title in frontend language and use that one.
         * The first look is for a language version that matches the current language code. If this version
         * can't get found, the first existing value will be used.
         */
        if (null === self::$title) {
            $ogTitles = self::$baseMetaTags['og:title'] ?? [];
            $currentLanguageCode = (string) new CurrentLanguageCode();

            if (array_key_exists($currentLanguageCode, $ogTitles)) {
                self::$title = $ogTitles[$currentLanguageCode];
                return;
            }

            $ogTitles = array_values($ogTitles);
            self::$title = $ogTitles[0] ?? null;
        }
    }

    /**
     * Creates a new instance.
     *
     * @return PageTitle
     */
    public static function create(): self
    {
        return new self();
    }

    /**
     * Sets the page title.
     *
     * @param string $title
     */
    public static function setTitle(string $title): void
    {
        self::$title = $title;
    }

    /**
     * Returns the page title.
     *
     * @return string
     */
    public function getTitle(): string
    {
        $title = (string) self::$title;
        $title = html_entity_decode($title);
        return htmlspecialchars($title);
    }

    /**
     * @return string
     */
    public function __toString(): string
    {
        return $this->getTitle();
    }
}
