<?php

/**
 * Kiwa. A feather-light web framework for professional static websites.
 *
 * @author Tobias Köngeter
 * @copyright Copyright © Bit&Black
 * @link https://www.bitandblack.com
 * @link https://www.kiwa.io
 * @license MIT
 */

namespace Kiwa\Page\Iterator;

use FilterIterator;
use Iterator;
use Kiwa\Config\Page;

/**
 * Class UsesFileNameIterator
 *
 * @package Kiwa\Page
 */
class UsesFileNameIterator extends FilterIterator
{
    /**
     * UsesFileNameIterator constructor.
     *
     * @param Iterator<int, Page> $iterator
     * @param array<int, string> $fileNames
     */
    public function __construct(
        Iterator $iterator,
        private readonly array $fileNames,
    ) {
        parent::__construct($iterator);
    }

    /**
     * @return bool
     */
    public function accept(): bool
    {
        /** @var Page $current */
        $current = $this->getInnerIterator()->current();
        $fileName = $current->getFile();
        return in_array($fileName, $this->fileNames, true);
    }
}
