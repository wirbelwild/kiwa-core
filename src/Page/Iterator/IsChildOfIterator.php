<?php

/**
 * Kiwa. A feather-light web framework for professional static websites.
 *
 * @author Tobias Köngeter
 * @copyright Copyright © Bit&Black
 * @link https://www.bitandblack.com
 * @link https://www.kiwa.io
 * @license MIT
 */

namespace Kiwa\Page\Iterator;

use FilterIterator;
use Iterator;
use Kiwa\Config\Page;

/**
 * Class IsChildOfIterator
 *
 * @package Kiwa\Page
 */
class IsChildOfIterator extends FilterIterator
{
    /**
     * IsChildOfIterator constructor.
     *
     * @param Iterator<int, Page> $iterator
     * @param array<int, string> $parentPageFileNames
     */
    public function __construct(
        Iterator $iterator,
        private readonly array $parentPageFileNames,
    ) {
        parent::__construct($iterator);
    }

    /**
     * @return bool
     */
    public function accept(): bool
    {
        /** @var Page $current */
        $current = $this->getInnerIterator()->current();
        $getFile = $current->getChildOf();
        return in_array($getFile, $this->parentPageFileNames, true);
    }
}
