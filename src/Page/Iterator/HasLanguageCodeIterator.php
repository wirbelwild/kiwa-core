<?php

/**
 * Kiwa. A feather-light web framework for professional static websites.
 *
 * @author Tobias Köngeter
 * @copyright Copyright © Bit&Black
 * @link https://www.bitandblack.com
 * @link https://www.kiwa.io
 * @license MIT
 */

namespace Kiwa\Page\Iterator;

use FilterIterator;
use Iterator;
use Kiwa\Config\Page;

/**
 * Class HasLanguageCodeIterator
 *
 * @package Kiwa\Page
 */
class HasLanguageCodeIterator extends FilterIterator
{
    /**
     * HasLanguageCodeIterator constructor.
     *
     * @param Iterator<int, Page> $iterator
     * @param array<int, string> $languageCodes
     */
    public function __construct(
        Iterator $iterator,
        private readonly array $languageCodes,
    ) {
        parent::__construct($iterator);
    }

    /**
     * @return bool
     */
    public function accept(): bool
    {
        /** @var Page $current */
        $current = $this->getInnerIterator()->current();
        $languageCode = $current->getLanguageCode();
        return in_array($languageCode, $this->languageCodes, true);
    }
}
