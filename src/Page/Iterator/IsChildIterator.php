<?php

/**
 * Kiwa. A feather-light web framework for professional static websites.
 *
 * @author Tobias Köngeter
 * @copyright Copyright © Bit&Black
 * @link https://www.bitandblack.com
 * @link https://www.kiwa.io
 * @license MIT
 */

namespace Kiwa\Page\Iterator;

use FilterIterator;
use Kiwa\Config\Page;

/**
 * Class IsChildIterator
 *
 * @package Kiwa\Page
 */
class IsChildIterator extends FilterIterator
{
    /**
     * @return bool
     */
    public function accept(): bool
    {
        /** @var Page $current */
        $current = $this->getInnerIterator()->current();
        return null !== $current->getChildOf();
    }
}
