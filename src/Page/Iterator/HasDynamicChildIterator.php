<?php

/**
 * Kiwa. A feather-light web framework for professional static websites.
 *
 * @author Tobias Köngeter
 * @copyright Copyright © Bit&Black
 * @link https://www.bitandblack.com
 * @link https://www.kiwa.io
 * @license MIT
 */

namespace Kiwa\Page\Iterator;

use FilterIterator;
use Iterator;
use Kiwa\Config\Page;

/**
 * Class HasDynamicChildIterator
 *
 * @package Kiwa\Page
 */
class HasDynamicChildIterator extends FilterIterator
{
    /**
     * HasDynamicChildIterator constructor.
     *
     * @param Iterator<int, Page> $iterator
     * @param array<int, string> $dynamicChildren
     */
    public function __construct(
        Iterator $iterator,
        private readonly array $dynamicChildren,
    ) {
        parent::__construct($iterator);
    }

    /**
     * @return bool
     */
    public function accept(): bool
    {
        /** @var Page $current */
        $current = $this->getInnerIterator()->current();
        $dynamicChildName = $current->getDynamicChild();
        return in_array($dynamicChildName, $this->dynamicChildren, true);
    }
}
