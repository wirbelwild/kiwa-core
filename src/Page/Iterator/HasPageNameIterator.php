<?php

/**
 * Kiwa. A feather-light web framework for professional static websites.
 *
 * @author Tobias Köngeter
 * @copyright Copyright © Bit&Black
 * @link https://www.bitandblack.com
 * @link https://www.kiwa.io
 * @license MIT
 */

namespace Kiwa\Page\Iterator;

use FilterIterator;
use Iterator;
use Kiwa\Config\Page;

/**
 * Class HasPageNameIterator
 *
 * @package Kiwa\Page
 */
class HasPageNameIterator extends FilterIterator
{
    /**
     * HasPageNameIterator constructor.
     *
     * @param Iterator<int, Page> $iterator
     * @param array<int, string> $pageNames
     */
    public function __construct(
        Iterator $iterator,
        private readonly array $pageNames,
    ) {
        parent::__construct($iterator);
    }

    /**
     * @return bool
     */
    public function accept(): bool
    {
        /** @var Page $current */
        $current = $this->getInnerIterator()->current();
        $pageName = $current->getName();
        return in_array($pageName, $this->pageNames, true);
    }
}
