<?php

/**
 * Kiwa. A feather-light web framework for professional static websites.
 *
 * @author Tobias Köngeter
 * @copyright Copyright © Bit&Black
 * @link https://www.bitandblack.com
 * @link https://www.kiwa.io
 * @license MIT
 */

namespace Kiwa\Log;

use Kiwa\Path;
use ParseError;

/**
 * The ErrorWriter class writes PHP errors to the file system.
 *
 * @package Kiwa\Log
 * @see \Kiwa\Tests\Log\ErrorWriterTest
 */
class ErrorWriter
{
    /**
     * ErrorWriter constructor.
     *
     * @param array{
     *     timestamp: float,
     *     message: string,
     *     file: string,
     *     site: string,
     *     line: int,
     *     type: int
     * } $error
     */
    public function __construct(array $error)
    {
        $errorFilePath = Path::getLogFolder() . DIRECTORY_SEPARATOR . 'error.php';
        $errorsAll = [];
        
        if (!file_exists($errorFilePath)) {
            file_put_contents(
                $errorFilePath,
                '<?php return []; ' . PHP_EOL
            );
        }

        try {
            $errorsExisting = include $errorFilePath;
        } catch (ParseError) {
            $errorsExisting = [];
        }

        if (is_array($errorsExisting)) {
            $errorsAll = $errorsExisting;
        }
        
        $errorsAll[] = $error;

        if (!file_exists(Path::getLogFolder())
            && !mkdir(Path::getLogFolder(), 0777, true)
            && !is_dir(Path::getLogFolder())
        ) {
            return;
        }

        file_put_contents(
            $errorFilePath,
            '<?php return ' . var_export($errorsAll, true) . '; ' . PHP_EOL
        );
    }
}
