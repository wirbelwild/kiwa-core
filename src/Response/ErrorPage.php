<?php

/**
 * Kiwa. A feather-light web framework for professional static websites.
 *
 * @author Tobias Köngeter
 * @copyright Copyright © Bit&Black
 * @link https://www.bitandblack.com
 * @link https://www.kiwa.io
 * @license MIT
 */

namespace Kiwa\Response;

use Symfony\Component\HttpFoundation\Response;

/**
 * Class HTMLResponse
 *
 * @package Kiwa
 */
class ErrorPage extends AbstractResponse
{
    /**
     * @param int $status
     */
    public function __construct(private readonly int $status = Response::HTTP_NOT_FOUND)
    {
    }

    /**
     * @return string
     */
    public function __toString(): string
    {
        return '<style>body { margin: 0; padding: 0; } </style>' . $this->style . PHP_EOL . '<div class="kiwa">' .
            '<h1>Sorry, that\'s a ' . $this->status . '.</h1>' .
            '</div>
        ';
    }
}
