<?php

/**
 * Kiwa. A feather-light web framework for professional static websites.
 *
 * @author Tobias Köngeter
 * @copyright Copyright © Bit&Black
 * @link https://www.bitandblack.com
 * @link https://www.kiwa.io
 * @license MIT
 */

namespace Kiwa\Response;

/**
 * Class PageResponse
 *
 * @package Kiwa\Response
 */
class PageResponse extends AbstractResponse
{
    /**
     * PageResponse constructor.
     *
     * @param string $content
     */
    public function __construct(private readonly string $content)
    {
    }

    /**
     * @return string
     */
    public function __toString(): string
    {
        return $this->style . PHP_EOL . '<div class="kiwa">' . $this->content . '</div>';
    }
}
