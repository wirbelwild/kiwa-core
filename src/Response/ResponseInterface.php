<?php

/**
 * Kiwa. A feather-light web framework for professional static websites.
 *
 * @author Tobias Köngeter
 * @copyright Copyright © Bit&Black
 * @link https://www.bitandblack.com
 * @link https://www.kiwa.io
 * @license MIT
 */

namespace Kiwa\Response;

/**
 * Interface ResponseInterface
 *
 * @package Kiwa\Response
 */
interface ResponseInterface
{
    /**
     * Returns a string of HTML which can be displayed.
     *
     * @return string
     */
    public function __toString(): string;
}
