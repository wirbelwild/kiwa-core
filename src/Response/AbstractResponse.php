<?php

/**
 * Kiwa. A feather-light web framework for professional static websites.
 *
 * @author Tobias Köngeter
 * @copyright Copyright © Bit&Black
 * @link https://www.bitandblack.com
 * @link https://www.kiwa.io
 * @license MIT
 */

namespace Kiwa\Response;

/**
 * Class AbstractResponse
 *
 * @package Kiwa\Response
 */
abstract class AbstractResponse implements ResponseInterface
{
    /**
     * @var string
     */
    protected string $style = '
        <style>
            .kiwa {
                margin: 1rem;
                padding: 1rem;
                color: black;
                background-color: lightgrey;
                font-family: sans-serif;
                font-size: 1rem;
                line-height: 1.5;
                font-weight: normal;
            }
            
            .kiwa .kiwa__h1 {
                font-weight: bolder;
                font-size: 2rem;
                margin: 0;
            }
            
            .kiwa .kiwa__h2 {
                font-weight: bolder;
                font-size: 1.5rem;
                margin: 0;
            }
            
            .kiwa .kiwa__h3 {
                font-weight: bolder;
                font-size: 1.25rem;
                margin: 0;
            }
            
            .kiwa table {
                width: 100%;
                border: 1px solid grey;
                color: inherit;
            }
            
            .kiwa td {
                padding: .5rem;          
                vertical-align: top;
            }
            
            .kiwa td:nth-child(2) {
                vertical-align: middle;
            }
            
            .kiwa tr + tr td {
                border-top: 1px solid grey;  
            }
        </style>
    ';
}
