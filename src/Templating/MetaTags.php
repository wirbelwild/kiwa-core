<?php

/**
 * Kiwa. A feather-light web framework for professional static websites.
 *
 * @author Tobias Köngeter
 * @copyright Copyright © Bit&Black
 * @link https://www.bitandblack.com
 * @link https://www.kiwa.io
 * @license MIT
 */

namespace Kiwa\Templating;

use BitAndBlack\Helpers\ArrayHelper;
use Iterator;
use Kiwa\URL\URLBuilder;
use Stringable;

/**
 * The class MetaTags is made for handling meta-tags.
 *
 * @package Kiwa\Templating
 * @phpstan-implements Iterator<string|int, string|array<string|int, string>>
 * @see \Kiwa\Tests\Templating\MetaTagsTest
 */
class MetaTags implements Iterator, Stringable
{
    /**
     * @var array<string, string|array<string|int, string>>
     */
    private static array $metaTags = [];
    
    /**
     * Tags that may appear multiple times.
     *
     * @var array<int, string>
     */
    private static array $multipleTags = [
        'article:tag',
        'og:image',
    ];

    /**
     * @var array<int|string, array<int|string, string>>
     */
    private static array $defaultTag = [];
    
    private static int $position = 0;

    /**
     * Sets a meta-tag. Kiwa does this in two steps:
     * * At first there will be loaded the basic meta-tags to provide some content no matter if the page
     *   has its own or not.
     * * Second the meta-tags from the loaded page will be loaded. If there are duplicates, the first tag
     *   from the basic setup will be removed.
     *
     * @param string $property                      Name of the meta tag.
     * @param string|array<string, string> $content Content/Value of the meta-tag.
     * @param bool $isDefaultTag                    If this tag is a default one and may be removed as soon as
     *                                              a specific content has been set.
     */
    public static function add(string $property, array|string $content, bool $isDefaultTag = false): void
    {
        if (empty($content)) {
            return;
        }

        if (true === $isDefaultTag) {
            if (false === is_array($content)) {
                $content = [$content];
            }

            self::$defaultTag[$property] = $content;
        }

        if (false === $isDefaultTag && array_key_exists($property, self::$defaultTag)) {
            foreach (self::$defaultTag[$property] as $defaultTag) {
                if (!isset(self::$metaTags[$property])) {
                    continue;
                }
                
                if (!is_array(self::$metaTags[$property])) {
                    unset(self::$metaTags[$property]);
                    continue;
                }
                
                if (false !== $key = array_search($defaultTag, self::$metaTags[$property], true)) {
                    unset(self::$metaTags[$property][$key]);
                }
            }
        }

        if (in_array($property, self::$multipleTags)) {
            if (!isset(self::$metaTags[$property]) || !is_array(self::$metaTags[$property])) {
                self::$metaTags[$property] = [];
            }

            if (false === is_array($content)) {
                $content = [$content];
            }

            array_push(self::$metaTags[$property], ...$content);
            return;
        }

        /**
         * If a tag must not appear multiple times and exists yet
         * it will be removed here
         */
        if (array_key_exists($property, self::$metaTags)) {
            unset(self::$metaTags[$property]);
        }

        self::$metaTags[$property] = $content;
    }

    /**
     * Returns all meta-tags as an array.
     *
     * @return array<int, string>
     */
    public static function getMetaTags(): array
    {
        $arrayTags = [];
        $attrNames = [
            'apple-mobile-web-app-capable',
            'apple-mobile-web-app-title',
            'description',
            'google-site-verification',
            'keywords',
            'p:domain_verify',
            'revisit-after',
            'robots',
            'viewport',
        ];

        foreach (self::$metaTags as $property => $content) {
            $tag = 'meta';
            
            $attribute = [
                'property',
                'content',
            ];

            if (in_array(strtolower($property), $attrNames, true)) {
                $attribute = [
                    'name',
                    'content',
                ];
            } elseif ('author' === $property) {
                $tag = 'link';
                $attribute = [
                    'rel',
                    'href',
                ];
            }

            if ('keywords' === $property && is_array($content)) {
                $content = implode(',', $content);
            }

            foreach (ArrayHelper::getArray($content) as $contentInside) {
                if (true === is_array($contentInside)) {
                    $contentInside = $contentInside[0];
                }

                /**
                 * OG tags will always have absolute urls.
                 */
                if (('og:image' === $property || 'og:url' === $property)) {
                    $host = parse_url($contentInside, PHP_URL_HOST);

                    if (null === $host) {
                        $contentInside = (string) new URLBuilder([
                            (string) parse_url($contentInside, PHP_URL_PATH),
                        ], false);
                    }
                }
                
                $arrayTags[] = '<' . $tag . ' ' . $attribute[0] . '="' . $property . '" ' . $attribute[1] . '="' . $contentInside . '">';
            }
        }
        
        return $arrayTags;
    }

    /**
     * Returns all metatags as string.
     *
     * @return string
     */
    public function __toString(): string
    {
        return implode(PHP_EOL, self::getMetaTags());
    }

    /**
     * @inheritDoc
     */
    public function rewind(): void
    {
        self::$position = 0;
    }

    /**
     * @inheritDoc
     */
    public function current(): mixed
    {
        return array_values(self::$metaTags)[self::$position];
    }

    /**
     * @inheritDoc
     */
    public function key(): int
    {
        return self::$position;
    }

    /**
     * @inheritDoc
     */
    public function next(): void
    {
        ++self::$position;
    }

    /**
     * @inheritDoc
     */
    public function valid(): bool
    {
        return isset(
            array_values(self::$metaTags)[self::$position]
        );
    }
}
