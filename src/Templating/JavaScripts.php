<?php

/**
 * Kiwa. A feather-light web framework for professional static websites.
 *
 * @author Tobias Köngeter
 * @copyright Copyright © Bit&Black
 * @link https://www.bitandblack.com
 * @link https://www.kiwa.io
 * @license MIT
 */

namespace Kiwa\Templating;

use Iterator;
use Stringable;

/**
 * Class JavaScripts
 *
 * @package Kiwa\Templating\Assets
 * @phpstan-implements Iterator<int, string>
 * @see \Kiwa\Tests\Templating\JavaScriptsTest
 */
class JavaScripts implements Iterator, Stringable
{
    /**
     * @var array<int, string>
     */
    protected static array $entries = [];
    
    private static int $position = 0;

    /**
     * Adds an asset code.
     *
     * @param string $content    The content.
     * @param int|null $position The position where this piece of code should be placed.
     */
    public static function add(string $content, int|null $position = null): void
    {
        if (in_array($content, self::$entries, false)) {
            return;
        }

        if (null !== $position) {
            array_splice(self::$entries, $position, 0, [$content]);
            return;
        }

        self::$entries[] = $content;
    }

    /**
     * Returns all added assets as an array.
     *
     * @return array<int, string>
     */
    public static function getAssets(): array
    {
        return self::$entries;
    }

    /**
     * Returns all added assets as string.
     *
     * @return string
     */
    public function __toString(): string
    {
        return implode(PHP_EOL, self::getAssets());
    }

    /**
     * @inheritDoc
     */
    public function rewind(): void
    {
        self::$position = 0;
    }

    /**
     * @inheritDoc
     */
    public function current(): string
    {
        return self::$entries[self::$position];
    }

    /**
     * @inheritDoc
     */
    public function key(): int
    {
        return self::$position;
    }

    /**
     * @inheritDoc
     */
    public function next(): void
    {
        ++self::$position;
    }

    /**
     * @inheritDoc
     */
    public function valid(): bool
    {
        return isset(
            self::$entries[self::$position]
        );
    }
}
