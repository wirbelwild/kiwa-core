<?php

/**
 * Kiwa. A feather-light web framework for professional static websites.
 *
 * @author Tobias Köngeter
 * @copyright Copyright © Bit&Black
 * @link https://www.bitandblack.com
 * @link https://www.kiwa.io
 * @license MIT
 */

namespace Kiwa\Templating;

use Iterator;
use Stringable;

/**
 * The class Canonicals is made for handling canonical tags.
 *
 * @package Kiwa\Templating
 * @phpstan-implements Iterator<int, string>
 * @see \Kiwa\Tests\Templating\CanonicalsTest
 */
class Canonicals implements Iterator, Stringable
{
    /**
     * @var array<int, string>
     */
    private static array $canonicalURLs = [];

    private static int $position = 0;

    /**
     * Adds a url which can be display as canonical link.
     *
     * @param string $url
     */
    public static function add(string $url): void
    {
        if (!in_array($url, self::$canonicalURLs, true)) {
            self::$canonicalURLs[] = $url;
        }
    }

    /**
     * Returns all canonical urls.
     *
     * @return array<int, string>
     */
    public static function getURLs(): array
    {
        return self::$canonicalURLs;
    }

    /**
     * Returns all canonical urls as string.
     *
     * @return string
     */
    public function __toString(): string
    {
        $links = '';

        foreach (self::getURLs() as $url) {
            $links .= '<link rel="canonical" href="' . $url . '">' . PHP_EOL;
        }

        return $links;
    }

    /**
     * @inheritDoc
     */
    public function rewind(): void
    {
        self::$position = 0;
    }

    /**
     * @inheritDoc
     */
    public function current(): string
    {
        return self::$canonicalURLs[self::$position];
    }

    /**
     * @inheritDoc
     */
    public function key(): int
    {
        return self::$position;
    }

    /**
     * @inheritDoc
     */
    public function next(): void
    {
        ++self::$position;
    }

    /**
     * @inheritDoc
     */
    public function valid(): bool
    {
        return isset(
            self::$canonicalURLs[self::$position]
        );
    }
}
