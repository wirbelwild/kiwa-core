<?php

/**
 * Kiwa. A feather-light web framework for professional static websites.
 *
 * @author Tobias Köngeter
 * @copyright Copyright © Bit&Black
 * @link https://www.bitandblack.com
 * @link https://www.kiwa.io
 * @license MIT
 */

namespace Kiwa\Templating;

use Iterator;
use Kiwa\Page\RelatedPages;
use Kiwa\URL\URLBuilder;
use Stringable;

/**
 * The AlternateLinks class creates html link tags.
 *
 * @package Kiwa\Templating
 * @phpstan-implements Iterator<int, string>
 * @see \Kiwa\Tests\Templating\AlternateLinksTest
 */
final class AlternateLinks implements Iterator, Stringable
{
    /**
     * @var array<string, string>
     */
    private static array $links = [];
    
    private static int $position = 0;

    /**
     * AlternateLinks constructor.
     */
    public function __construct()
    {
        foreach (RelatedPages::getRelatedPages() as $languageCode => $page) {
            if (!is_array($page)) {
                continue;
            }
            
            $url = (string) new URLBuilder([
                'languageCode' => $languageCode,
                'name' => $page['name'],
                'subname' => $page['subname'],
            ], false);

            self::$links[$languageCode] = $url;
        }
        
        ksort(self::$links);
    }

    /**
     * @return AlternateLinks
     */
    public static function create(): self
    {
        return new self();
    }

    /**
     * Returns all links as array.
     *
     * @return array<string, string>
     */
    public static function getAlternateLinks(): array
    {
        return self::$links;
    }

    /**
     * Returns a string with all links in html format.
     *
     * @return string
     */
    public function __toString(): string
    {
        $links = self::getAlternateLinks();
        $linksFormatted = array_map(
            static fn ($hreflang, $url): string => '<link rel="alternate" href="' . $url . '" hreflang="' . $hreflang . '">',
            array_keys($links),
            array_values($links)
        );
        return implode(PHP_EOL, $linksFormatted);
    }

    /**
     * @inheritDoc
     */
    public function rewind(): void
    {
        self::$position = 0;
    }

    /**
     * @inheritDoc
     */
    public function current(): string
    {
        return array_values(self::$links)[self::$position];
    }

    /**
     * @inheritDoc
     */
    public function key(): int
    {
        return self::$position;
    }

    /**
     * @inheritDoc
     */
    public function next(): void
    {
        ++self::$position;
    }

    /**
     * @inheritDoc
     */
    public function valid(): bool
    {
        return isset(
            array_values(self::$links)[self::$position]
        );
    }
}
