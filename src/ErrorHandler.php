<?php

/**
 * Kiwa. A feather-light web framework for professional static websites.
 *
 * @author Tobias Köngeter
 * @copyright Copyright © Bit&Black
 * @link https://www.bitandblack.com
 * @link https://www.kiwa.io
 * @license MIT
 */

namespace Kiwa;

use Kiwa\Error\ErrorName;
use Kiwa\Log\ErrorWriter;
use Kiwa\Response\PageResponse;
use Psr\Log\LoggerInterface;
use Psr\Log\NullLogger;

/**
 * Handles frontend errors
 */
class ErrorHandler
{
    private static string $env = 'dev';

    /**
     * Sets the environment to production mode.
     */
    public static function setEnvProduction(): void
    {
        self::$env = 'prod';
    }

    /**
     * Sets the environment to development mode.
     */
    public static function setEnvDev(): void
    {
        self::$env = 'dev';
    }

    /**
     * Define custom errors
     *
     * @param int $errorLevel
     * @param string $errorMessage
     * @param string $errorFile
     * @param int $errorLine
     * @return bool
     */
    public function errorHandler(
        int $errorLevel,
        string $errorMessage,
        string $errorFile,
        int $errorLine
    ): bool {
        $testMode = $_ENV['TEST_MODE'] ?? null;
        $isTestMode = 'active' === $testMode;
        $runsFromCli = 'cli' === PHP_SAPI;
        $isErrorOutputMuted = !(error_reporting() & $errorLevel);
        $runsInProductionMode = 'prod' === self::$env;

        $isHtmlMuted = false;

        if (true === $runsFromCli) {
            $isHtmlMuted = true;

            /**
             * The test mode disables the setting, as PHPUnit runs from the CLI
             * but needs to ignore that case for proper tests.
             */
            if ('disabled' === $testMode) {
                $isHtmlMuted = false;
            }
        }

        if (true === $isErrorOutputMuted) {
            $isHtmlMuted = true;
        }

        if (true === $runsInProductionMode) {
            $isHtmlMuted = true;
        }

        $logger = DI::isHasBeenInitialized()
            ? DI::getLog()
            : new NullLogger()
        ;

        $error = [
            'timestamp' => time(),
            'message' => $errorMessage,
            'file' => $errorFile,
            'site' => htmlspecialchars($_SERVER['REQUEST_URI'] ?? '/'),
            'line' => $errorLine,
            'type' => $errorLevel,
        ];

        $this->logByLevel($logger, $error);
        $this->showCustomError($error, $isHtmlMuted);
        
        return !$isTestMode;
    }

    /**
     * Will be called when php script ends.
     *
     * @return void
     */
    public function shutdownHandler(): void
    {
        $lastError = error_get_last();

        if (null !== $lastError) {
            $this->logByLevel(DI::getLog(), $lastError);
        }
    }

    /**
     * Show error log and write error log file
     *
     * @param array{
     *     timestamp: float,
     *     message: string,
     *     file: string,
     *     site: string,
     *     line: int,
     *     type: int
     * } $errorLog
     * @param bool $isHtmlMuted
     * @return void
     */
    private function showCustomError(array $errorLog, bool $isHtmlMuted): void
    {
        $content = [];

        foreach ($errorLog as $key => $value) {
            if ('message' === $key) {
                $valueParts = explode(PHP_EOL, $value);

                foreach ($valueParts as &$valuePart) {
                    if (str_starts_with($valuePart, '#')) {
                        $valuePartParts = explode(' ', $valuePart);
                        $valuePartPartsModified = [];
                        $valuePartPartsModified[] = '<h3 class="kiwa__h3">' . array_shift($valuePartParts) . '</h3>';
                        $valuePartPartsModified[] = '<p>';
                        $valuePartPartsModified[] = '    <i>' . array_shift($valuePartParts) . '</i>';
                        $valuePartPartsModified[] = '    <br>';
                        $valuePartPartsModified[] = '    <code>' . implode(' ', $valuePartParts) . '</code>';
                        $valuePartPartsModified[] = '</p>';
                        $valuePart = implode('', $valuePartPartsModified);
                    } else {
                        $valuePart = '<p>' . $valuePart . '</p>';
                    }
                }

                unset($valuePart);
                
                $value = implode(PHP_EOL, $valueParts);
            }

            if ('type' === $key) {
                $value = (string) new ErrorName(
                    (int) $value
                );
            }

            $content[] = '
                <tr>
                    <td>
                        <h2 class="kiwa__h2">' . ucfirst($key) . '</h2>
                    </td>
                    <td>' . $value . '</td>
                </tr>' . PHP_EOL
            ;
        }

        new ErrorWriter($errorLog);

        if (true === $isHtmlMuted) {
            return;
        }

        echo new PageResponse(
            '<h1 class="kiwa__h1">An error occured!</h1>' . PHP_EOL .
            '<p>Here are some details: </p>' . PHP_EOL .
            '<table>' . implode('', $content) . '</table>' . PHP_EOL
        );
    }

    /**
     * @param LoggerInterface $logger
     * @param array{
     *     type: int,
     *     file: string,
     *     message: string,
     *     timestamp?: int<1, max>,
     *     line: int,
     * } $error
     * @return void
     */
    private function logByLevel(LoggerInterface $logger, array $error): void
    {
        $message = $error['message'] . ' in file://' . $error['file'] . ' in line ' . $error['line'];
        
        match ($error['type']) {
            E_ERROR, E_CORE_ERROR, E_COMPILE_ERROR, E_PARSE => $logger->emergency($message),
            E_USER_ERROR, E_RECOVERABLE_ERROR => $logger->error($message),
            E_WARNING, E_CORE_WARNING, E_COMPILE_WARNING, E_USER_WARNING => $logger->warning($message),
            E_NOTICE, E_USER_NOTICE, E_DEPRECATED, E_USER_DEPRECATED => $logger->notice($message),
            E_STRICT => $logger->debug($message),
            default => $logger->info($message),
        };
    }
}
