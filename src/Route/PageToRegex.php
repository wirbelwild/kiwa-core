<?php

/**
 * Kiwa. A feather-light web framework for professional static websites.
 *
 * @author Tobias Köngeter
 * @copyright Copyright © Bit&Black
 * @link https://www.bitandblack.com
 * @link https://www.kiwa.io
 * @license MIT
 */

namespace Kiwa\Route;

use Kiwa\Config\Page;
use Kiwa\URL\URLBuilder;

/**
 * @see \Kiwa\Tests\Route\PageToRegexTest
 */
class PageToRegex
{
    public static function getRegex(Page $page): string
    {
        $languageCode = $page->getLanguageCode();
        $name = $page->getName();
        $subname = $page->getChildOf();

        if ('index' === $name || '/' === $name) {
            $name = null;
        }

        if (null !== $childOf = $page->getChildOf()) {
            $name = $childOf;
            $subname = $page->getName();
        }

        $regex = (string) new URLBuilder([
            'languageCode' => $languageCode,
            'name' => $name,
            'subname' => $subname,
        ]);

        $regex = mb_strtolower($regex);

        if ('' === $regex) {
            $regex = '/';
        }

        return $regex;
    }
}
