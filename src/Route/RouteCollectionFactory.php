<?php

/**
 * Kiwa. A feather-light web framework for professional static websites.
 *
 * @author Tobias Köngeter
 * @copyright Copyright © Bit&Black
 * @link https://www.bitandblack.com
 * @link https://www.kiwa.io
 * @license MIT
 */

namespace Kiwa\Route;

use Kiwa\Page\PageList;
use Kiwa\Path;
use Symfony\Component\Routing\Route;
use Symfony\Component\Routing\RouteCollection;

/**
 * The RouteCollectionFactory creates a {@see RouteCollection} out of the {@see PageList}.
 */
class RouteCollectionFactory
{
    public static function create(PageList $pageList, bool $cacheRoutes = false): RouteCollection
    {
        $routeCollection = new RouteCollection();

        $routesFile = Path::getCacheFolder() . DIRECTORY_SEPARATOR . 'routes.php';

        if (true === $cacheRoutes && file_exists($routesFile)) {
            $routesCollection = file_get_contents($routesFile);
            /** @var RouteCollection|false $routesCollection */
            $routesCollection = unserialize((string) $routesCollection, [RouteCollection::class]);

            if (false !== $routesCollection) {
                $routeCollection->addCollection($routesCollection);
                return $routeCollection;
            }
        }

        $pageListWithRegex = new PageListWithRegex($pageList);

        foreach ($pageListWithRegex->getPageList() as $regex => $page) {
            $route = new Route(
                $regex,
                [
                    '_page' => $page,
                ],
                [
                    'slug' => '.+',
                ],
                [
                    'utf8' => true,
                ]
            );

            $routeCollection->add($regex, $route);
        }

        if (true === $cacheRoutes) {
            file_put_contents(
                $routesFile,
                serialize($routeCollection)
            );
        }

        return $routeCollection;
    }
}
