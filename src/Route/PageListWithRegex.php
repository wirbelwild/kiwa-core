<?php

/**
 * Kiwa. A feather-light web framework for professional static websites.
 *
 * @author Tobias Köngeter
 * @copyright Copyright © Bit&Black
 * @link https://www.bitandblack.com
 * @link https://www.kiwa.io
 * @license MIT
 */

namespace Kiwa\Route;

use Kiwa\Config\Page;
use Kiwa\Page\PageList;

/**
 * The ConfigToRegex takes all {@see Page}s from the {@see PageList} and converts their routes into RegEx.
 * @see \Kiwa\Tests\Route\PageListWithRegexTest
 */
class PageListWithRegex
{
    /**
     * @var array<string, Page>
     */
    private readonly array $pageList;

    public function __construct(PageList $pageList)
    {
        $pageListRegex = [];

        /** @var Page $page */
        foreach ($pageList as $page) {
            $regex = PageToRegex::getRegex($page);

            if (true === $this->isDynamicSubPage($pageList, $page)) {
                $regex = str_replace($page->getName(), '{slug}', $regex);
            }

            $pageListRegex[$regex] = $page;
        }

        ksort($pageListRegex);

        $this->pageList = $pageListRegex;
    }

    /**
     * @return array<string, Page>
     */
    public function getPageList(): array
    {
        return $this->pageList;
    }

    /**
     * @param PageList $pageList
     * @param Page $page
     * @return bool
     */
    private function isDynamicSubPage(PageList $pageList, Page $page): bool
    {
        /**
         * Clones the PageList to reset earlier settings.
         */
        $pageList = clone $pageList;

        if (null === $page->getChildOf()) {
            return false;
        }

        $parentPages = $pageList->hasDynamicChild(
            $page->getName()
        );

        return 0 !== $parentPages->count();
    }
}
