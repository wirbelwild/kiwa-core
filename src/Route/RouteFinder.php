<?php

/**
 * Kiwa. A feather-light web framework for professional static websites.
 *
 * @author Tobias Köngeter
 * @copyright Copyright © Bit&Black
 * @link https://www.bitandblack.com
 * @link https://www.kiwa.io
 * @license MIT
 */

namespace Kiwa\Route;

use Kiwa\Config\Page;
use Symfony\Component\Routing\Exception\ResourceNotFoundException;
use Symfony\Component\Routing\Matcher\UrlMatcher;
use Symfony\Component\Routing\RequestContext;
use Symfony\Component\Routing\RouteCollection;

/**
 * The RouteFinder searches in the {@see RouteCollection} for a URI and returns its related {@see Page}.
 */
class RouteFinder
{
    private readonly ?Page $page;

    public function __construct(string $requestURI, RouteCollection $routeCollection)
    {
        $context = new RequestContext();
        $matcher = new UrlMatcher($routeCollection, $context);

        $requestURI = (string) parse_url($requestURI, PHP_URL_PATH);
        $requestURI = mb_strtolower($requestURI);

        $match = null;
        $page = null;

        try {
            $match = $matcher->match($requestURI);
        } catch (ResourceNotFoundException) {
        }

        if (null !== $match) {
            /** @var Page $page */
            $page = $match['_page'];
        }

        $this->page = $page;
    }

    public function getPage(): ?Page
    {
        return $this->page;
    }
}
