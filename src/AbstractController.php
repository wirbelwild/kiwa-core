<?php

/**
 * Kiwa. A feather-light web framework for professional static websites.
 *
 * @author Tobias Köngeter
 * @copyright Copyright © Bit&Black
 * @link https://www.bitandblack.com
 * @link https://www.kiwa.io
 * @license MIT
 */

namespace Kiwa;

use DI\Container;
use DI\ContainerBuilder;
use Kiwa\Config\BaseConfigLoader;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\HttpFoundation\Session\Storage\PhpBridgeSessionStorage;
use function DI\autowire;

/**
 * Abstract controller for dependency injection.
 */
abstract class AbstractController
{
    /**
     * @var Container
     */
    public Container $container;

    /**
     * Construct
     *
     * @throws Exception
     */
    public function __construct()
    {
        ErrorHandler::setEnvDev();
        $errorHandler = new ErrorHandler();
        
        set_error_handler($errorHandler->errorHandler(...));
        register_shutdown_function([$errorHandler, 'shutdownHandler']);

        if (session_status() !== PHP_SESSION_ACTIVE && false === headers_sent()) {
            session_start();
        }

        $baseConfig = BaseConfigLoader::create()->getConfig();
        $containerBuilder = new ContainerBuilder();

        if ('prod' === $baseConfig['APP_ENV']) {
            $containerBuilder->enableCompilation(Path::getCacheFolder());
            ErrorHandler::setEnvProduction();
        }

        $containerBuilder
            ->addDefinitions([
                Request::class => static fn () => Request::createFromGlobals(),
                Session::class => static fn () => new Session(
                    new PhpBridgeSessionStorage()
                ),
                Config::class => autowire(),
                Log::class => autowire(),
            ])
        ;

        $this->container = $containerBuilder->build();
        DI::setContainer($this->container);

        /**
         * Adds the pre-configured Session to the Request, as the Request object would have none otherwise.
         */
        DI::getRequest()
            ->setSession(
                DI::getSession()
            )
        ;

        DI::getSession()->start();
    }
}
