<?php

/**
 * Kiwa. A feather-light web framework for professional static websites.
 *
 * @author Tobias Köngeter
 * @copyright Copyright © Bit&Black
 * @link https://www.bitandblack.com
 * @link https://www.kiwa.io
 * @license MIT
 */

namespace Kiwa;

use Monolog\Handler\FingersCrossedHandler;
use Monolog\Handler\RotatingFileHandler;
use Monolog\Level;
use Monolog\Logger;
use Psr\Log\LoggerInterface;
use Psr\Log\LogLevel;
use Stringable;

/**
 * Logs system messages
 */
readonly class Log implements LoggerInterface
{
    private Logger $logger;

    /**
     * Log constructor.
     */
    public function __construct()
    {
        $logFile = Path::getLogFolder() . DIRECTORY_SEPARATOR . 'debug.log';
        $handler = $rotatingFileHandler = new RotatingFileHandler($logFile, 1, Level::Debug);

        $isTestMode = 'active' === ($_ENV['TEST_MODE'] ?? null);
        $isTestEnv = 'test' === ($_ENV['APP_ENV'] ?? 'prod');
        
        if (!$isTestMode && !$isTestEnv) {
            $handler = new FingersCrossedHandler($rotatingFileHandler);
        }

        $this->logger = new Logger('Kiwa');
        $this->logger->pushHandler($handler);
    }

    /**
     * If the script is running from the command line, all debug messages will be printed there.
     * The output will only be in dev mode. With PHPUnit running, the output will be disabled.
     *
     * @param mixed $message The message to display.
     * @phpstan-param LogLevel::* $level Name of the log level.
     */
    public function printInCLI(mixed $message, string $level): void
    {
        $isTestMode = 'active' === ($_ENV['TEST_MODE'] ?? null);
        $isTestModeTempDisabled = 'disabled' === ($_ENV['TEST_MODE'] ?? null);
        $isProduction = 'prod' === ($_ENV['APP_ENV'] ?? 'prod');
        $isNotRunningFromCLI = !str_contains(PHP_SAPI, 'cli');
    
        if ($isTestMode || $isTestModeTempDisabled || $isProduction || $isNotRunningFromCLI) {
            return;
        }

        $serverName = $_SERVER['SERVER_NAME'] ?? null;
        $remotePort = $_SERVER['REMOTE_PORT'] ?? null;
        $server = '';

        if (null !== $serverName && null !== $remotePort) {
            $server = $_SERVER['SERVER_NAME'] . ':' . $_SERVER['REMOTE_PORT'] . ' ';
        }

        $messageFormatted = $message;

        if (!is_string($message)) {
            $messageFormatted = var_export($message, true);
        }

        $colorText = match ($level) {
            LogLevel::WARNING, LogLevel::NOTICE => 33,
            LogLevel::EMERGENCY, LogLevel::ALERT, LogLevel::CRITICAL, LogLevel::ERROR => 31,
            default => 37,
        };

        $colorStringStart = "\033[" . $colorText . ";40m";
        $colorStringEnd = "\033[0m";
        
        ob_start();
        echo $server . $colorStringStart . '[' . strtoupper($level) . ']' . $colorStringEnd . ' ' . $messageFormatted;
        error_log((string) ob_get_clean(), 4);
    }

    /**
     * @inheritDoc
     */
    public function emergency(string|Stringable $message, array $context = []): void
    {
        $this->logger->emergency($message, $context);
        $this->printInCLI($message, 'emergency');
    }

    /**
     * @inheritDoc
     */
    public function alert(string|Stringable $message, array $context = []): void
    {
        $this->logger->alert($message, $context);
        $this->printInCLI($message, 'alert');
    }

    /**
     * @inheritDoc
     */
    public function critical(string|Stringable $message, array $context = []): void
    {
        $this->logger->critical($message, $context);
        $this->printInCLI($message, 'critical');
    }

    /**
     * @inheritDoc
     */
    public function error(string|Stringable $message, array $context = []): void
    {
        $this->logger->error($message, $context);
        $this->printInCLI($message, 'error');
    }

    /**
     * @inheritDoc
     */
    public function warning(string|Stringable $message, array $context = []): void
    {
        $this->logger->warning($message, $context);
        $this->printInCLI($message, 'warning');
    }

    /**
     * @inheritDoc
     */
    public function notice(string|Stringable $message, array $context = []): void
    {
        $this->logger->notice($message, $context);
        $this->printInCLI($message, 'notice');
    }

    /**
     * @inheritDoc
     */
    public function info(string|Stringable $message, array $context = []): void
    {
        $this->logger->info($message, $context);
        $this->printInCLI($message, 'info');
    }

    /**
     * @inheritDoc
     */
    public function debug(string|Stringable $message, array $context = []): void
    {
        $this->logger->debug($message, $context);
        $this->printInCLI($message, 'debug');
    }

    /**
     * @inheritDoc
     * @phpstan-param LogLevel::* $level
     */
    public function log($level, string|Stringable $message, array $context = []): void
    {
        $this->logger->log($level, $message, $context);
        $this->printInCLI($message, $level);
    }
}
