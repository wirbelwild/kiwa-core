<?php

/**
 * Kiwa. A feather-light web framework for professional static websites.
 *
 * @author Tobias Köngeter
 * @copyright Copyright © Bit&Black
 * @link https://www.bitandblack.com
 * @link https://www.kiwa.io
 * @license MIT
 */

namespace Kiwa;

use OutOfBoundsException;
use PackageVersions\Versions;

/**
 * Some global information.
 *
 * @package Kiwa
 * @see Path
 */
class Globals
{
    /**
     * Returns the Kiwa version.
     *
     * @return string
     */
    public static function getKiwaVersion(): string
    {
        try {
            $version = Versions::getVersion('kiwa/core');
        } catch (OutOfBoundsException) {
            $version = 'unknown@unknown';
        }
        
        return $version;
    }
}
