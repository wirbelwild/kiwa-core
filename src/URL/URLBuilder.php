<?php

/**
 * Kiwa. A feather-light web framework for professional static websites.
 *
 * @author Tobias Köngeter
 * @copyright Copyright © Bit&Black
 * @link https://www.bitandblack.com
 * @link https://www.kiwa.io
 * @license MIT
 */

namespace Kiwa\URL;

use BitAndBlack\Helpers\StringHelper;
use BitAndBlack\PathInfo\PathInfo;
use Exception;
use Kiwa\DI;
use Stringable;
use Symfony\Component\Asset\VersionStrategy\JsonManifestVersionStrategy;
use Symfony\Component\Asset\VersionStrategy\VersionStrategyInterface;

/**
 * The URLBuilder class helps to create a URL based on the current config.
 *
 * @package Kiwa\Config
 * @see \Kiwa\Tests\URL\URLBuilderTest
 */
class URLBuilder implements Stringable
{
    /**
     * @var array<int, string>
     */
    private static array $urlStructure = [];

    private static string|bool $usePageSuffix = false;

    private static string $mainURL = '';
    
    private string $url;

    /**
     * Creates a new URL. If you define the URL parts, Kiwa will always return the correct URLs,
     * even if the URL structure changes. Building a URL should go like that:
     *
     * ```
     * new URLBuilder([
     *     'languageCode' => 'de',
     *     'name' => 'page1',
     *     'subname' => 'page2'
     * ])
     * ```
     *
     * Depending on your config, this may result as: `/de/page1/page2.html`.
     * If your config changes on they, the URL will also change.
     *
     * If you want to crate a URL for a static file like an image or a javascript file,
     * it is enough to pass the relative path at once:
     *
     * ```
     * new URLBuilder([
     *     '/build/javascripts/template.js'
     * ])
     * ```
     *
     * To create a URL like that has one huge advantage: when Kiwa finds a manifest.json file, it will search
     * for this file and return its versioned file name. So the example may result in `/build/javascripts/template.js`
     * but also in `/build/javascripts/template.4557b2e2.js`.
     *
     * Please note: if you want to create URLs containing dots, the URLBuilder may not append the page suffix by its own.
     *
     * @param array{
     *     languageCode?: string|null,
     *     name?: string|null,
     *     subname?: string|null,
     * } $parameter The URL parameter. This array may hold the `languageCode`, `name` and `subname`.
     * @param bool $pathRelative If the URL should be relative or absolute.
     */
    public function __construct(array $parameter, bool $pathRelative = true)
    {
        $separator = '/';

        /**
         * Remove null values from list.
         */
        foreach ($parameter as $key => $parameterPiece) {
            if (null === $parameterPiece || 'index' === $parameterPiece || $separator === $parameterPiece) {
                unset($parameter[$key]);
            }
        }

        if (empty(implode('', $parameter))) {
            $this->url = $pathRelative ? $separator : self::$mainURL;
            return;
        }

        $url = '';
        $urlStructureKeys = array_keys(self::$urlStructure);

        /**
         * Removes named url pieces, that aren't matching the defined url structure.
         */
        foreach ($parameter as $key => $parameterPiece) {
            if (is_string($key) && !in_array($key, self::$urlStructure)) {
                unset($parameter[$key]);
            }
        }

        foreach ($urlStructureKeys as $key) {
            $fragment = self::$urlStructure[$key];
            $fragment = $parameter[$fragment] ?? '';

            if ('' === $fragment) {
                continue;
            }

            $fragment = mb_strtolower($fragment);

            if (!str_starts_with($fragment, $separator)) {
                $fragment = $separator . $fragment;
            }

            $url .= $fragment;
        }

        /** If the keys are undefined, we just bind the array values together. */
        if ('' === $url && !empty($parameter)) {
            $url = implode($separator, $parameter);
            
            if ('' !== $url && !str_starts_with($url, $separator)) {
                $url = $separator . $url;
            }
        }
        
        $suffix = StringHelper::stringToBoolean(self::$usePageSuffix);

        if (false !== $suffix && '' !== $url && null === PathInfo::createFromFile($url)->getExtension()) {
            $url .= '.' . $suffix;
        }

        /** @var JsonManifestVersionStrategy|null $jsonManifestVersionStrategy */
        $jsonManifestVersionStrategy = null;
        
        try {
            if (in_array(JsonManifestVersionStrategy::class, DI::getContainer()->getKnownEntryNames())) {
                $jsonManifestVersionStrategy = DI::getContainer()->get(JsonManifestVersionStrategy::class);
            }
        } catch (Exception) {
        }
        
        /**
         * If a version strategy is set, we try to get the files url from there
         * Because we have already added a backslash, we need to remove it at first
         */
        if ($jsonManifestVersionStrategy instanceof VersionStrategyInterface) {
            $url = ltrim($url, $separator);
            $url = $jsonManifestVersionStrategy->getVersion($url);
            $url = ltrim($url, $separator);
            $url = $separator . $url;
        }

        /**
         * When the URL is empty (what may happen on an index page), we normalise it by adding the separator.
         */
        if ('' === $url) {
            $url = $separator;
        }

        if (false === $pathRelative) {
            $url = self::$mainURL . rtrim($url, $separator);
        }

        $this->url = $url;
    }

    /**
     * @return string
     */
    public function getURL(): string
    {
        return $this->url;
    }

    /**
     * @return string
     */
    public function __toString(): string
    {
        return $this->getURL();
    }
}
