<?php

/**
 * Kiwa. A feather-light web framework for professional static websites.
 *
 * @author Tobias Köngeter
 * @copyright Copyright © Bit&Black
 * @link https://www.bitandblack.com
 * @link https://www.kiwa.io
 * @license MIT
 */

namespace Kiwa\URL;

use Stringable;

/**
 * The URLFragment class holds all fragments of the current URL.
 *
 * @package Kiwa\URL
 * @see \Kiwa\Tests\URL\URLFragmentTest
 */
final class URLFragment implements Stringable
{
    private static ?string $fragment = null;

    /**
     * @var array<int, string>
     */
    private static array $urlStructure = [];

    /**
     * @var array<int, string|null>
     */
    private static array $urlFragments = [];

    /**
     * Creates a new instance.
     *
     * @param string $fragmentName The name of the fragment.
     */
    public function __construct(string $fragmentName)
    {
        $position = self::getURLFragmentPosition($fragmentName);
    
        if (false === $position) {
            return;
        }
    
        self::$fragment = '' !== self::$urlFragments[$position]
            ? self::$urlFragments[$position]
            : null
        ;
    }

    /**
     * Creates a new instance.
     *
     * @param string $fragmentName The name of the fragment.
     * @return URLFragment
     */
    public static function create(string $fragmentName): self
    {
        return new self($fragmentName);
    }

    /**
     * Returns the position of an url fragment.
     *
     * @param string $fragmentName The name of the fragment.
     * @return string|int|false
     */
    public static function getURLFragmentPosition(string $fragmentName): string|int|false
    {
        return array_search($fragmentName, self::$urlStructure, true);
    }

    /**
     * Returns the value of a fragment. If it's not set, the value will be null.
     *
     * @return string|null
     */
    public function getFragment(): ?string
    {
        return self::$fragment;
    }
    
    /**
     * Returns the value of a fragment as string.
     *
     * @return string
     */
    public function __toString(): string
    {
        return (string) $this->getFragment();
    }
}
