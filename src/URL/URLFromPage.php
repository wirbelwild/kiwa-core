<?php

/**
 * Kiwa. A feather-light web framework for professional static websites.
 *
 * @author Tobias Köngeter
 * @copyright Copyright © Bit&Black
 * @link https://www.bitandblack.com
 * @link https://www.kiwa.io
 * @license MIT
 */

namespace Kiwa\URL;

use Kiwa\Config\Page;
use Stringable;

class URLFromPage implements Stringable
{
    public function __construct(private readonly Page $page, private readonly bool $pathRelative = true)
    {
    }

    /**
     * @return string
     */
    public function __toString(): string
    {
        return $this->getURL();
    }

    public function getURL(): string
    {
        $languageCode = $this->page->getLanguageCode();
        $name = $this->page->getName();
        $subname = null;

        if (null !== $this->page->getChildOf()) {
            $name = $this->page->getChildOf();
            $subname = $this->page->getName();
        }

        $urlBuilder = new URLBuilder(
            [
                'languageCode' => $languageCode,
                'name' => $name,
                'subname' => $subname,
            ],
            $this->pathRelative
        );

        return $urlBuilder->getURL();
    }
}
