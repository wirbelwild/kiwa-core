<?php

/**
 * Kiwa. A feather-light web framework for professional static websites.
 *
 * @author Tobias Köngeter
 * @copyright Copyright © Bit&Black
 * @link https://www.bitandblack.com
 * @link https://www.kiwa.io
 * @license MIT
 */

namespace Kiwa\URL;

use Kiwa\Config\Page;
use Kiwa\Language\CurrentLanguageCode;
use Kiwa\Page\PageList;
use Stringable;

/**
 * The URLFromFile class creates a URL by a given file name.
 *
 * @package Kiwa\URL
 * @see \Kiwa\Tests\URL\URLFromFileTest
 */
class URLFromFile implements Stringable
{
    protected string $url;
    
    private static string $mainURL = '';
    
    /**
     * @var array<int, string>
     */
    private static array $urlStructure = [];

    /**
     * Creates and returns a URL based on a file name.
     *
     * @param string $fileName   Name of the `phtml` file, without the suffix.
     * @param bool $pathRelative Whether it should be a relative URL or not.
     */
    public function __construct(string $fileName, bool $pathRelative = true)
    {
        if ('index' === $fileName) {
            $this->url = $pathRelative
                ? '/'
                : self::$mainURL
            ;
            return;
        }

        $urlFragments = [];

        /** @var Page $page */
        foreach (new PageList() as $page) {
            $language = $page->getLanguageCode();

            if ($page->getFile() !== $fileName) {
                continue;
            }

            foreach (self::$urlStructure as $fragment) {
                if ('languageCode' === $fragment) {
                    $urlFragments[$language][$fragment] = $language;
                } elseif ('name' === $fragment) {
                    $urlFragments[$language][$fragment] = $page->getChildOf() ?? $page->getName();
                } elseif ('subname' === $fragment && $page->getName() !== ($urlFragments[$language]['name'] ?? false)) {
                    $urlFragments[$language][$fragment] = $page->getName();
                }
            }
        }

        $currentLanguageCode = (string) new CurrentLanguageCode();

        if (isset($urlFragments[$currentLanguageCode])) {
            $urlFragments = $urlFragments[$currentLanguageCode];
            $this->url = (string) new URLBuilder($urlFragments, $pathRelative);
            return;
        }

        $this->url = '';
    }

    /**
     * Returns the URL.
     *
     * @return string
     */
    public function getURL(): string
    {
        return $this->url;
    }

    /**
     * Returns the URL.
     *
     * @return string
     */
    public function __toString(): string
    {
        return $this->getURL();
    }
}
