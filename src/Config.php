<?php

/**
 * Kiwa. A feather-light web framework for professional static websites.
 *
 * @author Tobias Köngeter
 * @copyright Copyright © Bit&Black
 * @link https://www.bitandblack.com
 * @link https://www.kiwa.io
 * @license MIT
 */

namespace Kiwa;

use ArrayObject;
use BitAndBlack\Helpers\SanitizeHelper;
use BitAndBlack\Helpers\StringHelper;
use BitAndBlack\PathInfo\PathInfo;
use Kiwa\Config\BaseConfigLoader;
use Kiwa\Config\Generator\CacheControlGenerator;
use Kiwa\Config\Generator\SecurityHeadersGenerator;
use Kiwa\Config\Page;
use Kiwa\Config\PropertyHandler;
use Kiwa\Exception\Config\SetupNotPossibleException;
use Kiwa\Exception\DependencyInjectionNotInitializedException;
use Kiwa\HTML\JavascriptVariables;
use Kiwa\Language\AvailableLanguages;
use Kiwa\Language\CurrentLanguageCode;
use Kiwa\Language\MetaLanguageCode;
use Kiwa\Page\PageList;
use Kiwa\Page\PageTitle;
use Kiwa\Templating\MetaTags;
use Kiwa\URL\URLBuilder;
use Kiwa\URL\URLFragment;
use Kiwa\URL\URLFromFile;
use Symfony\Component\Asset\VersionStrategy\JsonManifestVersionStrategy;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\Session;

/**
 * Class Config.
 *
 * @package Kiwa
 * @see \Kiwa\Tests\ConfigTest
 */
class Config
{
    /**
     * @var array{
     *     APP_ENV: string,
     *     usePageSuffix?: bool|string,
     *     pages: array<string, array{
     *         name?: string,
     *         getFile?: string,
     *         languageCode?: string,
     *         pageTitle?: string,
     *         metaTags?: array<string, string>,
     *         hasDynamicChild?: bool|string,
     *         childOf?: string,
     *         enableCache?: bool,
     *         cacheLifetime?: int
     *     }>,
     *     urlStructure?: array<int, string>,
     *     mainURL?: string,
     *     languageHandling?: string,
     *     htaccess?: array{
     *         useWWW: bool,
     *         useSSL: bool
     *     },
     *     languageCode?: string|null,
     *     baseMetaTags?: array<string, string|array<string, string>>,
     *     cacheControl?: array<string, int>,
     *     securityHeaders?: array<string, string|int>,
     *     searchQuery?: string,
     *     logoURL?: string,
     *     useWWW?: bool,
     *     useSSL?: bool,
     * }
     */
    private array $config;

    private ?Response $redirectResponse = null;
    
    /**
     * @var bool
     */
    private bool $isRequestValid = true;
    
    private string $defaultPageSuffix = 'html';

    /**
     * Config constructor.
     *
     * @param Request $request
     * @param Session<mixed> $session
     * @throws SetupNotPossibleException
     * @throws DependencyInjectionNotInitializedException
     */
    public function __construct(
        private readonly Request $request,
        private readonly Session $session,
    ) {
        $this->loadConfig();
        $this->setErrorReporting();
        BaseConfigLoader::updateConfig($this);
        $this->configureClasses();
        
        /** @var array<int, string> $languageCodes */
        $languageCodes = array_keys($this->config['pages']);
        AvailableLanguages::add(...$languageCodes);

        $uri = $this->request->server->get('REQUEST_URI');
        
        if (!is_string($uri)) {
            $uri = '';
        }
        
        $requestUri = $this->parseURLPath($uri);
        $query = $requestUri['query'] ?? null;
        
        $requestUri = ltrim($requestUri['path'] ?? $uri, '/');

        $suffix = $this->getValue('usePageSuffix');
        /** @var bool|string $suffix */
        $suffix = StringHelper::stringToBoolean($suffix);
        $suffix = !is_bool($suffix) ? mb_strtolower($suffix) : $suffix;

        if (true === $suffix) {
            $suffix = $this->defaultPageSuffix;
        }
        
        if (false === $suffix) {
            $suffix = '';
        }
        
        $suffixCurrent = mb_strtolower(PathInfo::createFromFile($requestUri)->getExtension() ?? '');
        $pageWithoutSuffix = $requestUri;

        if ('' !== $suffixAttached = PathInfo::createFromFile($requestUri)->getExtension()) {
            $pageWithoutSuffix = str_replace('.' . $suffixAttached, '', $pageWithoutSuffix);
        }

        $suffixMatchesFilter = in_array(
            (string) $suffixCurrent,
            [
                '',
                'htm',
                'html',
                'php',
            ],
            true
        );
        
        if ('' !== $suffixCurrent) {
            $requestUri = substr($requestUri, 0, -(strlen($suffixCurrent) + 1));
        }

        $requestUris = explode('/', $requestUri);

        $urlFragments = array_fill(0, 3, null);

        foreach ($requestUris as $key => $uri) {
            $urlFragments[$key] = $uri;
        }

        PropertyHandler::set(
            URLFragment::class,
            'urlStructure',
            $this->getValue('urlStructure')
        );

        PropertyHandler::set(
            URLFragment::class,
            'urlFragments',
            $urlFragments
        );
        
        $languageCodeBelongsToURL = false !== URLFragment::getURLFragmentPosition('languageCode');
        $languageCodeExistsInConfig = in_array(
            URLFragment::create('languageCode')->getFragment(),
            AvailableLanguages::getAll(),
            false
        );

        /**
         * If the URL should contain a language code we proof if a page with the given language code exists.
         * If not the request will be marked invalid here. The Controller returns a 404 response then.
         */
        if ($languageCodeBelongsToURL && !$languageCodeExistsInConfig) {
            $this->isRequestValid = false;
        }

        /**
         * If the URL contains a wrong suffix, we set up a redirect response here. As soon as we are sure that
         * no file has been requested, the redirect response will be released.
         */
        if ($suffixMatchesFilter && $suffixCurrent !== $suffix && '' !== $requestUri) {
            $this->redirectResponse = new RedirectResponse(
                '/' . $pageWithoutSuffix .
                ('' !== $suffix ? '.' . $suffix : null) .
                (null !== $query ? '?' . $query : null),
                Response::HTTP_MOVED_PERMANENTLY
            );
        }
    }

    /**
     * @return $this
     */
    private function loadConfig(): self
    {
        $this->config = BaseConfigLoader::create()->getConfig();
        $this->completeConfig();
        return $this;
    }
    
    /**
     * Adding missing information to the config
     */
    private function completeConfig(): void
    {
        /**
         * mainURL is necessary to set proper absolute urls
         */
        $protocol = 'https';

        if (null === $this->request->server->get('HTTPS')) {
            $protocol = 'http';
        }
        
        $www = 'www.';

        /**
         * The templates' config may override here
         */
        if ($this->hasValue('htaccess')) {
            $htaccess = $this->getValue('htaccess');
            
            if (!is_array($htaccess)) {
                $htaccess = [];
            }
            
            if (array_key_exists('useSSL', $htaccess)) {
                $protocol = $htaccess['useSSL'] ? 'https' : 'http';
            }

            if (array_key_exists('useWWW', $htaccess)) {
                $www = $htaccess['useWWW'] ? 'www.' : '';
            }
        }
        
        $serverName = $this->request->server->get('SERVER_NAME') ?? 'localhost';
        
        if (!is_string($serverName)) {
            $serverName = 'localhost';
        }
        
        /** @var string $serverName */
        $serverName = SanitizeHelper::htmlSpecialChars($serverName);
        
        if (str_starts_with($serverName, 'www.')) {
            $serverName = substr($serverName, 4);
        }
        
        $serverName = $www . $serverName;
        
        $startURLLong = $protocol . '://' . $serverName;

        if (!isset($this->config['mainURL'])) {
            $this->config['mainURL'] = $startURLLong;
        }

        /**
         * languageHandling is important to handle language versions and linking
         */
        if (!isset($this->config['languageHandling'])) {
            $this->config['languageHandling'] = 'strict';
        }

        /**
         * languageCode is important if pages and language codes go wrong
         * Also it allows an index without having a language version in the url
         */
        if (!isset($this->config['languageCode'])) {
            $existingLanguage = null;

            if (!empty($this->config['pages'])) {
                $existingLanguages = array_keys($this->config['pages']);
                $existingLanguage = (string) ($existingLanguages[0] ?? 'en');
            }

            $this->config['languageCode'] = $existingLanguage;
        }

        /**
         * Add empty base meta tags
         */
        if (!isset($this->config['baseMetaTags'])) {
            $this->config['baseMetaTags'] = [];
        }

        /**
         * Add url structure
         */
        if (!isset($this->config['urlStructure'])) {
            $this->config['urlStructure'] = [
                0 => 'name',
                1 => 'subname',
            ];
        }
        
        /**
         * Add the url logic
         */
        if (!isset($this->config['usePageSuffix'])) {
            $this->config['usePageSuffix'] = $this->defaultPageSuffix;
        }

        /**
         * If no cache control has been defined, we add the default values here.
         */
        if (!isset($this->config['cacheControl'])) {
            $this->config['cacheControl'] = CacheControlGenerator::create()->getConfig();
        }
        
        /**
         * If no security headers have been defined, we add the default values here.
         */
        if (!isset($this->config['securityHeaders'])) {
            $securityHeadersGenerator = new SecurityHeadersGenerator();
            
            if ('https' === $protocol) {
                $securityHeadersGenerator->setContentSecurityPolicy('script-src \'unsafe-inline\' \'self\' https:');
            }
            
            $this->config['securityHeaders'] = $securityHeadersGenerator->getConfig();
        }
    }

    /**
     * Set error reporting
     *
     * @return void
     */
    private function setErrorReporting(): void
    {
        $developmentMode = 'prod' !== $this->getValue('APP_ENV')
            && 'test' !== $this->getValue('APP_ENV')
        ;

        error_reporting(0);
        ini_set('display_errors', 'off');
        
        if ($developmentMode) {
            error_reporting(E_ALL);
        }
    }

    /**
     * Start configuration
     *
     * @return void
     * @throws SetupNotPossibleException
     */
    public function setBaseConfiguration(): void
    {
        MetaTags::add('generator', 'Kiwa');

        if ($this->request->query->get('kiwaAction') === 'changeLanguage') {
            $this->session->set(
                'kiwaLanguageCode',
                SanitizeHelper::htmlSpecialChars(
                    $this->request->query->get('kiwaLanguageCode')
                )
            );

            $scheme = null !== $this->request->server->get('HTTPS') ? 'https' : 'http';
            $host = $this->request->server->get('HTTP_HOST');
            $requestUri = $this->request->server->get('REQUEST_URI');
            
            $oldURL = $scheme . '://' . $host . $requestUri;
            /** @var string $oldURL */
            $oldURL = SanitizeHelper::htmlSpecialChars($oldURL);
            $newURL = parse_url($oldURL, PHP_URL_SCHEME) . '://' .
                parse_url($oldURL, PHP_URL_HOST) .
                parse_url($oldURL, PHP_URL_PATH)
            ;

            $response = new RedirectResponse($newURL);
            $response->sendContent();
            exit;
        }

        /** @var string|null $sessionLanguageCode */
        $sessionLanguageCode = $this->session->get('kiwaLanguageCode');
        $urlFragmentLanguageCode = URLFragment::create('languageCode')->getFragment();
        $configLanguageCode = $this->config['languageCode'] ?? null;

        PropertyHandler::set(
            MetaLanguageCode::class,
            'defaultLanguageCode',
            $configLanguageCode
        );
        
        /**
         * Priority for the meta language code:
         * 1. Coming from the url
         * 2. Defined as standard in the config
         * 3. Coming from the session
         */
        $metaLanguageCode = $urlFragmentLanguageCode
            ?? $configLanguageCode
            ?? $sessionLanguageCode
            ?? ''
        ;

        MetaLanguageCode::setMetaLanguageCode((string) $metaLanguageCode);

        $this->session->set(
            'kiwaLanguageCode',
            MetaLanguageCode::get()
        );

        foreach ($this->config['baseMetaTags'] ?? [] as $property => $content) {
            $languageContent = null;
            
            if (is_array($content)) {
                $content = array_change_key_case($content, CASE_LOWER);
                $currentLanguageCode = (string) new CurrentLanguageCode();
                $languageContent = $content[$currentLanguageCode] ?? null;
            }
            
            if (is_array($content) && null !== $languageContent) {
                MetaTags::add($property, $languageContent, true);
                continue;
            }

            MetaTags::add($property, $content, true);
        }
    }

    /**
     * Return part of the config
     *
     * @param string $value
     * @return string|int|float|array<mixed>|bool|null
     */
    public function getValue(string $value)
    {
        $value = strip_tags($value);
        return $this->config[$value];
    }

    /**
     * Returns if a value exists
     *
     * @param string $value
     * @return bool
     */
    public function hasValue(string $value): bool
    {
        $value = strip_tags($value);
        return array_key_exists($value, $this->config);
    }

    /**
     * @return $this
     * @throws DependencyInjectionNotInitializedException
     * @throws SetupNotPossibleException
     */
    private function configureClasses(): self
    {
        $manifest = Path::getBuildFolder() . DIRECTORY_SEPARATOR . 'manifest.json';

        if (file_exists($manifest)) {
            $jsonManifestVersionStrategy = new JsonManifestVersionStrategy($manifest);
            DI::getContainer()->set(
                $jsonManifestVersionStrategy::class,
                $jsonManifestVersionStrategy
            );
        }
        
        PropertyHandler::set(
            URLBuilder::class,
            'mainURL',
            $this->getValue('mainURL')
        );

        PropertyHandler::set(
            URLBuilder::class,
            'urlStructure',
            $this->getValue('urlStructure')
        );
        
        PropertyHandler::set(
            URLBuilder::class,
            'usePageSuffix',
            $this->getValue('usePageSuffix')
        );

        PropertyHandler::set(
            JavascriptVariables::class,
            'mainURL',
            $this->getValue('mainURL')
        );

        PropertyHandler::set(
            URLFromFile::class,
            'mainURL',
            $this->getValue('mainURL')
        );
        
        PropertyHandler::set(
            URLFromFile::class,
            'urlStructure',
            $this->getValue('urlStructure')
        );

        PropertyHandler::set(
            PageTitle::class,
            'baseMetaTags',
            $this->getValue('baseMetaTags')
        );
        
        $pageClasses = [];
        
        foreach ($this->config['pages'] as $pages) {
            /**
             * @var array{
             *     name?: string,
             *     getFile?: string,
             *     languageCode?: string,
             *     pageTitle?: string,
             *     metaTags?: array<string, string>,
             *     hasDynamicChild?: bool|string,
             *     childOf?: string,
             *     enableCache?: bool,
             *     cacheLifetime?: int
             * } $page
             */
            foreach ($pages as $page) {
                $pageClasses[] = new Page($page);
            }
        }

        PropertyHandler::set(
            PageList::class,
            'pages',
            new ArrayObject($pageClasses)
        );
        
        return $this;
    }

    /**
     * Returns the whole config.
     *
     * @return array<string, mixed>
     */
    public function getAllValues(): array
    {
        return $this->config;
    }

    /**
     * This method parses an url and encodes its parts.
     *
     * @param string $url
     * @return array<string, string>
     */
    private function parseURLPath(string $url): array
    {
        $result = [];

        $entities = ['%21', '%2A', '%27', '%28', '%29', '%3B', '%3A', '%40', '%26', '%3D', '%24', '%2C', '%2F', '%3F', '%23', '%5B', '%5D'];
        $replacements = ['!', '*', '\'', '(', ')', ';', ':', '@', '&', '=', '$', ',', '/', '?', '#', '[', ']'];

        $encodedURL = str_replace($entities, $replacements, urlencode($url));
        $encodedParts = parse_url($encodedURL);

        if ($encodedParts) {
            foreach ($encodedParts as $key => $value) {
                $result[$key] = urldecode(
                    str_replace($replacements, $entities, (string) $value)
                );
            }
        }

        return $result;
    }

    /**
     * @return Response|null
     */
    public function getRedirectResponse(): ?Response
    {
        return $this->redirectResponse;
    }

    /**
     * Returns if the request has been marked valid or invalid.
     *
     * @return bool
     */
    public function isRequestValid(): bool
    {
        return $this->isRequestValid;
    }
}
