<?php

/**
 * Kiwa. A feather-light web framework for professional static websites.
 *
 * @author Tobias Köngeter
 * @copyright Copyright © Bit&Black
 * @link https://www.bitandblack.com
 * @link https://www.kiwa.io
 * @license MIT
 */

namespace Kiwa;

use Composer\DependencyResolver\Operation\InstallOperation;
use Composer\DependencyResolver\Operation\UninstallOperation;
use Composer\Installer\PackageEvent;
use RuntimeException;

/**
 * The ComposerHandler class handles all composer events.
 */
class ComposerHandler
{
    /**
     * @return string
     */
    private static function getRootDir(): string
    {
        return dirname(__DIR__, 4);
    }
    
    /**
     * @param PackageEvent $event
     * @return void
     */
    public static function postPackageInstall(PackageEvent $event): void
    {
        /** @var InstallOperation $operation */
        $operation = $event->getOperation();
        $installedPackage = $operation->getPackage();
        $packageName = $installedPackage->getName();

        if ('kiwa/console' === $packageName) {
            $binPath = self::getRootDir() . DIRECTORY_SEPARATOR . 'bin';
            $consoleFile = $binPath . DIRECTORY_SEPARATOR . 'console';

            if (!file_exists($consoleFile)) {
                $fileSource = self::getRootDir() . DIRECTORY_SEPARATOR . 'vendor' . DIRECTORY_SEPARATOR . 'kiwa' . DIRECTORY_SEPARATOR . 'console' .
                    DIRECTORY_SEPARATOR . 'src' . DIRECTORY_SEPARATOR . 'static' . DIRECTORY_SEPARATOR . 'console.php'
                ;
                
                if (!file_exists($fileSource)) {
                    return;
                }
                
                if (!is_dir($binPath) && !mkdir($binPath) && !is_dir($binPath)) {
                    throw new RuntimeException(
                        sprintf('Directory "%s" was not created', $binPath)
                    );
                }
                
                $fileContent = (string) file_get_contents($fileSource);
                $fileContent = substr($fileContent, 18);
                
                file_put_contents($consoleFile, $fileContent);
                chmod($consoleFile, 0755);
            }
        }
    }

    /**
     * @param PackageEvent $event
     * @return void
     */
    public static function postPackageUninstall(PackageEvent $event): void
    {
        /** @var UninstallOperation $operation */
        $operation = $event->getOperation();
        $uninstalledPackage = $operation->getPackage();
        $packageName = $uninstalledPackage->getName();

        if ('kiwa/console' === $packageName) {
            $binPath = self::getRootDir() . DIRECTORY_SEPARATOR . 'bin';
            $consoleFile = $binPath . DIRECTORY_SEPARATOR . 'console';
            
            if (!file_exists($consoleFile)) {
                return;
            }

            unlink($consoleFile);
            
            $folderContent = glob($binPath . DIRECTORY_SEPARATOR . '*');
            $isFolderEmpty = false !== $folderContent && 0 === count($folderContent);
            
            if ($isFolderEmpty) {
                rmdir($binPath);
            }
        }
    }
}
