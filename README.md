[![PHP from Packagist](https://img.shields.io/packagist/php-v/kiwa/core)](http://www.php.net)
[![Latest Stable Version](https://poser.pugx.org/kiwa/core/v/stable)](https://packagist.org/packages/kiwa/core)
[![Total Downloads](https://poser.pugx.org/kiwa/core/downloads)](https://packagist.org/packages/kiwa/core)
[![License](https://poser.pugx.org/kiwa/core/license)](https://packagist.org/packages/kiwa/core)

<p align="center">
    <a href="https://www.kiwa.io" target="_blank">
        <img src="https://www.kiwa.io/build/images/kiwa-logo.png" alt="Kiwa Logo" width="150">
    </a>
</p>

# Kiwa 

A feather-light web framework for professional static websites.

## Installation 

This library is made for the use with [Composer](https://packagist.org/packages/kiwa/core). Add it to your project by running `$ composer require kiwa/core`. 

__Please note__: The easiest way to create a website with Kiwa is by using the [Website Skeleton](https://packagist.org/packages/kiwa/website-skeleton). It will create all the needed files and loads the `kiwa/core` along with the `kiwa/console` as dependencies.

## Usage

Learn how to get started under [www.kiwa.io/getting-started.html](https://www.kiwa.io/getting-started.html).

The full documentation can be found under [www.kiwa.io/documentation.html](https://www.kiwa.io/documentation.html).

## Help 

If you have any questions, feel free to contact us under `hello@bitandblack.com`.

Further information about Bit&Black can be found under [www.bitandblack.com](https://www.bitandblack.com).